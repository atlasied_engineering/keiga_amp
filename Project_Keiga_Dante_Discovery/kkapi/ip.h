/*****************************************************************************
| Copyright (c) Keiga Inc. All Rights Reserved.
|
| Project : amor
| FileName: kk_socket.h
| Module  : No
|
| Author  : Dennis Lu
| Revise  :
| Version : 1.8.5.7
| Comment : Windows socket version 2.2
|
\*****************************************************************************/

#ifndef _IP_H_
#define _IP_H_

#ifdef  __cplusplus
extern  "C" {
#endif

//#include "stdafx.h"
#include <winSock2.h>
#include <ws2tcpip.h>
#include "kk.h"

#define	SERVER_PORT				5555
#define	MAX_NUM_CLIENT			10
#define	MAX_NUM_BUF				48

#define TIME_EXIT_5000			5000
#define TIME_HELP_1500			1500
#define TIME_SLEEP_500			500

#define CS_INIT					0
#define	CS_SERVER				1
#define	CS_CLIENT				2
#define	TYPE_TCP				1
#define	TYPE_UDP				2
#define	IO_SELECT				1
#define	IO_WSAASYNCSELECT		2
#define	IO_WSAEVENTSELECT		3
#define	IO_OVERLAPPED			4
#define	IO_IOCP					5

#define SOCKET_OK				0
#define SOCKET_ERR_DLL			1
#define SOCKET_ERR_API			2

extern UINT cs_role;
extern UINT cs_type;
extern UINT cs_mode;


//header 4 bytes
typedef struct _targetHeader
{
	char	type;			//
	unsigned short	len;	//data length
}hdr, *phdr;

//data 48 bytes
typedef struct _targetData
{
	char	buf[MAX_NUM_BUF];
}DATABUF, *pDataBuf;

typedef struct tagNodeClient
{
	SOCKET s;
	sockaddr_in saddr;

	HANDLE hsrvRecv;		//receive data
	HANDLE hsrvParse;		//parse data
	HANDLE hsrvSend;		//send data as same as receiver
	HANDLE hparseEvent;
	HANDLE hsendEvent;
	CRITICAL_SECTION cs;

	BOOL isConnect;
	BOOL isExit;
	char buf[MAX_NUM_BUF];
	struct tagNodeClient *next_node;
}NODECLIENT, *PNODECLIENT;

extern PNODECLIENT g_psrv1stClient;

//-----------------------------------------------------------------------------
//CreateEvent(NULL, FALSE, FALSE, NULL)
// 產生一個Event Object並回傳一個 HANDLE
// SetEvent(HANDLE)   ,發訊息
// ResetEvent(HANDLE) ,不發訊息
// PulseEvent(HANDLE) ,由無訊息轉換為發訊息, 再瞬間轉為無訊息
//SetEvent() - WaitForSingleObject()
DWORD WINAPI tsrvHelper(LPVOID lpa);
DWORD WINAPI tsrvAccept(LPVOID lpa);

//-----------------------------------------------------------------------------
//int recv(...)
// int表收到資料的長度, 0表連線被關閉, SOCKET_ERROR表接收失敗
//int send(...)
DWORD WINAPI tsrvReadCmd(LPVOID lpa);
DWORD WINAPI tsrvParseCmd(LPVOID lpa);
DWORD WINAPI tsrvSendCmd(LPVOID lpa);
DWORD WINAPI tusrReadCmd(LPVOID lpa);
DWORD WINAPI tusrParseCmd(LPVOID lpa);
DWORD WINAPI tusrSendCmd(LPVOID lpa);
void WSASendMsgToServer(char *msg);

//-----------------------------------------------------------------------------
//InitializeCriticalSection()
// 解決在多thread時, 碰到不同thread同時存取一個元件而造成順序上的錯誤
// 需加入標頭檔 afxmt.h
//
//CRITICAL_SECTION cs; //宣告cs 為一個CRITICAL_SECTION
//InitializeCriticalSection(&cs); //初始化 cs
//EnterCriticalSection(&cs); //進入Critical Section接下來的程式碼同時只能有一個thread動作
//
//放一次只讓一個thread存取的程式碼區塊
//
//LeaveCriticalSection(&cs); //離開Critical Section 其他thread可以進入此Critical Section
int WSAStart();
int WSAErrorMsg(char *msg);
void WSAStop();

//-----------------------------------------------------------------------------
//Server node
// socket(), bind(), listen(), accept()
int WSAServerON();
BOOL WSAReadParseSendData(PNODECLIENT pnode);

//-----------------------------------------------------------------------------
//Client nodeYouTube         
// connect()
int WSAClientON(char *srvname);
void WSAFindAllNodes();

//-----------------------------------------------------------------------------
//show info on dialog
void WSAShowHostInfo();

//node's list 
void kk_ClientNodeAdded(SOCKET s, sockaddr_in saddr);
void kk_ClientNodeModified(PNODECLIENT pnode);
void kk_ClientNodeDeleted(PNODECLIENT pnode);
PNODECLIENT kk_ClientNodeFound(SOCKET s);

#ifdef  __cplusplus
}
#endif
#endif // _IP_H_