/*****************************************************************************
| Copyright (c) Keiga Inc. All Rights Reserved.
|
| Project : amor
| FileName: dante.h
| Module  : No
|
| Author  :	Andy Jhang
| Revise  :
| Version : V1.7.7.01
| Comment : enxtend dante api
|
\*****************************************************************************/

#ifndef _DT_EXTEND_API_H_
#define _DT_EXTEND_API_H_
#ifdef  __cplusplus
extern  "C" {
#endif

#include "dt.h"
#include <winsock2.h>
#include <ws2tcpip.h>
#include <IPHlpApi.h>


#pragma comment(lib, "iphlpapi.lib")
#pragma comment(lib, "ws2_32.lib")


	//Extend Struct
	typedef struct tagDeviceFlow {
		aud_bool_t isMulticast;
		char nameRemote[LEN_NAME];	//name of remote device
		uint16_t idx;			//index at flows
		uint16_t nSlot;			//number of channel in flow
		unsigned char idxTxSlot[8];		//array of index of tx channel at slot in flow,  unicast:4,  mutilcast:8
		char nameTxSlot[8][LEN_NAME];	//array of name  of tx channel at slot in flow,  unicast:4,  mutilcast:8
		struct tagDeviceFlow* nextFlow;
	}DEVICEFLOW, *PDEVICEFLOW;

	typedef struct tagDeviceFlows {
		char name[LEN_NAME];				//device name
		uint16_t maxTxFlows;				//max number of txFlows
		uint16_t maxRxFlows;				//max number of rxFlows
		uint16_t maxTxSlot;					//max tx slot
		uint16_t maxRxSlot;					//max rx slot
		uint16_t ntf;						//number of txFlows
		uint16_t nrf;						//number of rxFlows
		PDEVICEFLOW tx;						//list of txFlows
		PDEVICEFLOW rx;						//list of rxFlows
	}DEVICEFLOWS, *PDEVICEFLOWS;

	typedef struct tagListSubscriber {
		char name[LEN_NAME];	//device name
		uint16_t nSub;			//number of subscriber
		dante_name_t *nameSub;	//the list of name of subscriber
	}LISTSUBSCRIBER, *PLISTSUBSCRIBER;

	typedef struct tagListSource {
		char name[LEN_NAME];	//device name
		uint16_t nSrc;			//number of Source
		dante_name_t *nameSrc;	//the list of name of source
	}LISTSOURCE, *PLISTSOURCE;


	typedef struct tagSubscriberRelationship {
		char nameSub[LEN_NAME];		//name of subscriber device	(rx device)
		char nameSrc[LEN_NAME];		//name of source device		(tx device)

									//subscriber relationship is as follow:
									//txSrc[0] -> txSub[0],		txSrc[1] -> rxSub[1] .... ,txSrc[nrx_with_tx - 1]  ->  txSub[nrx_with_tx - 1]
		uint16_t nrx_with_tx;		//the number of rx channels of subscriber to tx channels of source
		uint16_t *rxSub;			//rx array of subscriber
		uint16_t *txSrc;			//tx array of source

		uint16_t numMFlows;			//number of multicast flows
		uint16_t numUFlows;			//number of unicast flows  
		uint16_t isOverRxFlows;		//is over rxflows

	}SUBSCRIBERRLAT, *PSUBSCRIBERRLAT;



	typedef struct tagNetworkInterface {
		int	idxIf;			//index of interface
		int type;			//MIB_IF_TYPE_ETHERNET , IF_TYPE_IEEE80211(wireless)
		char ip[17];		//192.168.0.0
		char mac[24];		//FF:FF:FF:FF:FF:FF
		WCHAR name[255];		//name
		char desc[255];		//description
		struct tagNetworkInterface* nextNet;
	}NETINTERFACE, *PNETINTERFACE;

	//------------------------Dante Extend API--------------------------------------------
	PNETINTERFACE db_GetNetworkInterface();

	void dt_InitConMonClient_Serial(conmon_client_config_t* client, aud_bool_t enable);
	aud_error_t dc_SendDeviceSerialMsg(char *msg);


	void dc_SetupLocalRegistration_Extend();	//serial
	void dc_SetupRemoteRegistration_Extend();	//serial

	aud_error_t dc_SendDeviceConMsg_Manufacturer(char *manufacturer, char *device, char *msg);

	void kk_dtSendOutConMsg_Audinate(char *device, char *msg);
	aud_error_t dc_SendDeviceConMsg_Audinate(char *device, char *msg);

	void dc_SysReset(char *device);

	//Query
	void dc_QueryEnc(char *device);
	void dc_QuerySrate(char *device);
	void dc_QueryInterface(char *device);
	void dc_QueryManfVer(char *device);

	//Set
	void dc_SetEnc(char *device, int enc); // enc: 16,24,32
	void dc_SetSrate(char *device, enum conmon_srate srate);
	void dc_SetInterface_static(char *device, uint16_t netInterface, char *ip, char *netmask, char *dns, char *gateway);
	void dc_SetInterface_dynamic(char *device, uint16_t netInterface);

	//Response
	void dc_MonMsgPayloadCB_Audinate
		(
			conmon_client_t *client,
			conmon_channel_type_t type,
			conmon_channel_direction_t dir,
			const conmon_message_head_t *head,
			const conmon_message_body_t *body
			);


	void dc_MonMsgPayload_interface_status(const char *device, const conmon_message_body_t * aud_msg, size_t body_size);
	void dc_MonMsgPayload_srate_status(const char *device, const conmon_message_body_t * aud_msg, size_t body_size);
	void dc_MonMsgPayload_enc_status(const char *device, const conmon_message_body_t * aud_msg, size_t body_size);
	void dc_MonMsgPayload_manf_versions_status(const char *device, const conmon_message_body_t * aud_msg, size_t body_size);

	char * dt_cmStringIpAddress(uint32_t addr, char * buf, size_t len);
	char * dt_cmStringInterfaceFlags(uint16_t flags, char * buf, size_t len);




	void dr_RenameRxChannel(const char *rx, const char *rxch, const char *rename);
	void dr_RenameTxLabel(const char *tx, const char *rxch, const char *rename);
	void dr_AddTxLabel(const char *tx, const char *txch, const char *rename);
	void dr_RemoveTxLabel(const char *tx, const char *txch);
	void dr_MuteRxChannel(const char *rx, const char *rxch, const char *mute);
	void dr_MuteTxChannel(const char *tx, const char *txch, const char *mute);
	void dr_EnableTxChannel(const char *tx, const char *txch, const char *en);
	aud_bool_t dr_RenameDevice(const char* dev, const char *rename);


	const char* dr_GetDeviceActualName(const char* dev);
	const char* dr_GetDeviceAdvertisedName(const char* dev);
	const char* dr_GetDeviceDefaultName(const char* dev);

	void dr_PingDevice(const char* dev);
	void dr_HasNetworkLoopBack(const char* dev);
	void dr_SetNetworkLoopBack(const char* dev, aud_bool_t sw);

	void dr_GetSampleRate(const char* dev, const char* tx);
	void dr_GetFormatTx(const char* dev, const char* tx);

	void dr_SetPerformance_Rx(const char* dev, dante_latency_us_t latency_us, dante_fpp_t fpp);
	void dr_SetPerformance_Tx(const char* dev, dante_latency_us_t latency_us, dante_fpp_t fpp);
	void dr_SetPerformance_Unicast(const char* dev, dante_latency_us_t latency_us, dante_fpp_t fpp);


	//void dr_Get
	void dr_GetDeviceFromSubscription(const char* dev, const char* rx);
	void dr_GetRxSubscriptionStatus(const char* dev, const char* rx);

	void dr_SetTxMulticast(const char* txDev, const char* txCh);
	void dr_SetTxMulticast_vlg(const char* txDev, uint16_t txChCount, ...); //VLG: Variable-length argument
	void dr_DeleteMulticast(const char* txDev);
	BOOL dr_hasMulticast(const char* txDev);
	uint16_t dr_numTxMulticast(const char* txDev);

	aud_bool_t dr_GetDeviceFlows(const char* Dev, PDEVICEFLOWS *devFlows_ptr);
	aud_bool_t dr_GetDeviceTxFlow(const char* Dev, uint16_t index, PDEVICEFLOW *ptxFlow_ptr);
	aud_bool_t dr_GetDeviceRxFlow(const char* Dev, uint16_t index, PDEVICEFLOW *prxFlow_ptr);
	aud_bool_t dr_AddListDeviceFlow(PDEVICEFLOW *headFlow_ptr, PDEVICEFLOW addFlow_ptr);

	void dr_FreeDeviceFlows(PDEVICEFLOWS devFlows);



	aud_bool_t dr_GetListSubscriber(const char* Dev, PLISTSUBSCRIBER * devListSub_ptr);
	aud_bool_t dr_GetListSource(const char* Dev, PLISTSOURCE * devListSrc_ptr);
	aud_bool_t dr_GetSubscriberRelationship(const char* rxDev, const char* txDev, PSUBSCRIBERRLAT *devSubRlat_ptr);




	void dr_FreeListSubscriber(PLISTSUBSCRIBER devListSub_ptr);
	void dr_FreeListSource(PLISTSOURCE devListSrc_ptr);
	void dr_FreeSubscriberRelationship(PSUBSCRIBERRLAT devSubRlat_ptr);

	void dr_SetLockDown(const char* Dev, aud_bool_t sw);


	uint16_t dr_GetNumTxlabel(const char* txDev);
	char* dr_GetTxLabel(const char* txDev, int num);

	uint16_t dr_GetNumRxName(const char* rxDev);
	char* dr_GetRxName(const char*rxDev, int num);


	aud_bool_t dr_isSubscribe(const char* rxDev, const char* rxCh, const char* txDev, const char* txCh);
	aud_bool_t dr_isSubscribeDevice(const char* rxDev, const char* txDev);

	aud_bool_t dr_isValidTxFlows(const char* Dev);
	aud_bool_t dr_isValidRxFlows(const char* Dev);

	uint16_t dr_GetTxCh_id(PDANTENODE pnode, const char* txCh);
	uint16_t dr_GetRxCh_id(PDANTENODE pnode, const char* rxCh);


	//-----------------------KK Exten API --------------------
	aud_bool_t kk_dtPingAllDevice(void);

	void kk_dtShowRXCH_TXCH(HWND hdlg, int id, char *device);
	void kk_dtUnSubscribeCH_All(const char *rx);


	//------------------------Dante Extend API - Combo ----------------------------
	void dr_enBroadcast(const char* broadcaster, bool isEnable);
	void dr_unSubscribe(const char* rxDev, const char* txDev);
	void dr_addListener(const char* broadcaster, const char* listener);
	void dr_leaveListener(const char* broadcaster, const char* listener);




	/*
	aud_bool_t dr_GetSoultionFlow();
	*/


#ifdef  __cplusplus
}
#endif

#endif // _DT_EXTEND_API_H_
