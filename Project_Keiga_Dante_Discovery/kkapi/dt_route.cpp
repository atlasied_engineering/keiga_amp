
/*****************************************************************************
| Copyright (c) Keiga Inc. All Rights Reserved.
|
| Project : amor
| FileName: dt_route.cpp
| Author  : Dennis Lu
| Revise  : 
| Version : V1.7.7.7
| Comment : 
\*****************************************************************************/

#include "stdafx.h"
//#include "ap_main.h"
//#include "define.h"
#include "dt.h"
#include "kk.h"

void dt_InitRouting()
{
	aud_error_t err;
	aud_errbuf_t errbuf;

	if (g_dtDR == NULL)
		return;

	//set number of handles
	err = dr_devices_set_num_handles(g_dtDR, MAX_NODE_CHANNELS);
	if (err != AUD_SUCCESS)
	{
		KKMSG(g_MsgBuf, "ERR- set handle number (%s)", aud_error_message(err, errbuf));
		_kkmsg(MSG_BOX, NULL);
		return;
	}

	//set request limition
	err = dr_devices_set_request_limit(g_dtDR, MAX_REQUESTS);
	if (err != AUD_SUCCESS)
	{
		KKMSG(g_MsgBuf, "ERR- set request limited (%s)", aud_error_message(err, errbuf));
		_kkmsg(MSG_LOG, NULL);
		return;
	}

	//set context
	//dr_devices_set_context(g_dtDR, void *context);
}

void dr_DeviceChangedCB(dr_device_t *device, dr_device_change_flags_t change_flags)
{
	for (int index = 0; index < DR_DEVICE_CHANGE_INDEX_COUNT; index++)
	{
		if (change_flags & (1 << index))
		{
			sprintf(g_MsgBuf, "EVENT: device changed (%s)", dr_device_change_index_to_string(index));
			_kkmsg(MSG_LOG, NULL);
		}
	}

	if (change_flags & DR_DEVICE_CHANGE_FLAG_STATE)
	{
		dr_device_state_t state = dr_device_get_state(device);

		sprintf(g_MsgBuf, "%s device change to %s", dr_device_get_name(device), dr_device_state_to_string(state));
		_kkmsg(MSG_LOG, NULL);

		dr_DeviceStateChanged(device);
	}

	if (change_flags & DR_DEVICE_CHANGE_FLAG_ADDRESSES)
	{
		dr_DeviceAddressChanged(device);
	}

	uint32_t num_request = dr_devices_num_requests_pending(g_dtDR);
	uint32_t max_request = dr_devices_get_request_limit(g_dtDR);
	sprintf(g_MsgBuf, "active requests: %d/%d", num_request, max_request);
	_kkmsg(MSG_LOG, NULL);
}

//-----------------------------------------------------------------------------
// dr_device_state_t
// DR_DEVICE_STATE_ERROR
// DR_DEVICE_STATE_RESOLVING
// DR_DEVICE_STATE_RESOLVED
// DR_DEVICE_STATE_QUERYING
// DR_DEVICE_STATE_ACTIVE
// 
void dr_DeviceStateChanged(dr_device_t *device)
{
	aud_error_t err;
	aud_errbuf_t errbuf;

	int idx;
	PDEVICEREQUEST request;

	switch (dr_device_get_state(device))
	{
	case DR_DEVICE_STATE_RESOLVED:
		request = dr_AllocateRequest("KK-capability");
		if (!request)
		{
			_kkmsg(MSG_LOG, "ERR- no more request for capabilities");
			return;
		}

		err = dr_device_query_capabilities(device, &dr_RequestResponseCB, &request->id);
		if (err != AUD_SUCCESS)
		{
			sprintf(g_MsgBuf, "ERR- get capabilities (%s)", dr_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			dr_ReleaseRequest(request);
			return;
		}
		return;
	case DR_DEVICE_STATE_ACTIVE:
	{
		PDANTENODE p;

		//the node must be in listing
		const char *name = dr_device_get_name(device);
		p = dt_FindNode(name);
		if (!p) return;

		dr_device_get_rxchannels(device, &p->nrx, &p->rx);
		dr_device_get_txchannels(device, &p->ntx, &p->tx);

		//update all components when we enter the active state
		dr_device_status_flags_t status_flags;
		err = dr_device_get_status_flags(device, &status_flags);
		if (err != AUD_SUCCESS)
		{
			sprintf(g_MsgBuf, "ERR- get status flags (%s)", dr_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			return;
		}

		//unless we have a strange status
		if (status_flags)
		{
			sprintf(g_MsgBuf, "ERR- got a strange status (0x%08x)", status_flags);
			_kkmsg(MSG_LOG, NULL);
			return;
		}

		bool result;
		result = dr_UpdateComponents(device);
		if (result)
		{
			//new node is active
			p->active = true;
			sprintf(g_MsgBuf, "%s is active", name);
			_kkmsg(MSG_BOX, NULL);
		}
		return;
	}
	default:
		break;
	}
}

void dr_DeviceAddressChanged(dr_device_t *device)
{
	unsigned int na = 2;
	dante_ipv4_address_t addresses[2];
	const char *name = dr_device_get_name(device);

	dr_device_get_addresses(device, &na, addresses);

	for (unsigned int i = 0; i < na; i++)
	{
		uint8_t * host = (uint8_t *)&(addresses[i].host);
		KKMSG(g_MsgBuf, "%s IP address_%d is %u.%u.%u.%u:%u",name, i, host[0], host[1], host[2], host[3], addresses[i].port);
		_kkmsg(MSG_LOG, NULL);
	}
}

PDEVICEREQUEST dr_AllocateRequest(const char *description)
{
	uint16_t i;

	for (i = 0; i < MAX_REQUESTS; i++)
	{
		if (g_dtRequests[i].id == DANTE_NULL_REQUEST_ID)
		{
			aud_strlcpy(g_dtRequests[i].description, description ? description : "", LEN_REQUEST_DESCRIPTION);
			return g_dtRequests + i;
		}
	}
	return NULL;
}

void dr_ReleaseRequest(PDEVICEREQUEST request)
{
	request->id = DANTE_NULL_REQUEST_ID;
	request->description[0] = '\0';
}

void dr_ReleaseRequestDescription(const char *description) {
	uint16_t i;

	for (i = 0; i < MAX_REQUESTS; i++)
	{
		if (strcmp(g_dtRequests[i].description, description) == 0) {
			g_dtRequests[i].id = DANTE_NULL_REQUEST_ID;
			g_dtRequests[i].description[0] = '\0';
		}
	}
	return;
}

bool dr_UpdateComponents(dr_device_t *device)
{
	aud_error_t err;
	aud_errbuf_t errbuf;

	PDEVICEREQUEST request;
	dr_device_component_t c;
	const char *name = dr_device_get_name(device);

	for (unsigned int i = 0; i < DR_DEVICE_COMPONENT_COUNT; i++)
	{
		c = (dr_device_component_t)i;
		if (!dr_device_is_component_stale(device, c))
		{
			continue;
		}

		sprintf(g_MsgBuf, "%s is updating %s component", name, dr_device_component_to_string(c));
		_kkmsg(MSG_LOG, NULL);

		request = dr_AllocateRequest(NULL);
		if (!request)
		{
			_kkmsg(MSG_LOG, "ERR- no more request for component");
			return FALSE;
		}
		SNPRINTF(request->description, LEN_REQUEST_DESCRIPTION, "%s", dr_device_component_to_string(c));

		//why need to sleep some milliseconds
		Sleep(10);

		err = dr_device_update_component(device, &dr_RequestResponseCB, &request->id, c);
		if (err != AUD_SUCCESS)
		{
			sprintf(g_MsgBuf, "ERR- update %s %s: %s", name, request->description, dr_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			dr_ReleaseRequest(request);
			return FALSE;
		}
	}
	return TRUE;
}

void dr_RequestResponseCB(dr_device_t *device, dante_request_id_t id, aud_error_t result)
{
	uint16_t i;

	for (i = 0; i < MAX_REQUESTS; i++)
	{
		if (g_dtRequests[i].id == id)
		{
			if (!strcmp(g_dtRequests[i].description, dr_device_change_index_to_string(DR_DEVICE_COMPONENT_TXCHANNELS)))
			{
				dr_UpdateTxChannel(device);
			}
			else if (!strcmp(g_dtRequests[i].description, dr_device_change_index_to_string(DR_DEVICE_COMPONENT_RXCHANNELS)))
			{
				dr_UpdateRxChannel(device);
			}
			else if (!strcmp(g_dtRequests[i].description, dr_device_change_index_to_string(DR_DEVICE_COMPONENT_TXLABELS)))
			{
				dr_UpdateTxLabel(device);
			}
			else if (!strcmp(g_dtRequests[i].description, dr_device_change_index_to_string(DR_DEVICE_COMPONENT_TXFLOWS)))
			{
				dr_UpdateTxFlow(device);
			}
			else if (!strcmp(g_dtRequests[i].description, dr_device_change_index_to_string(DR_DEVICE_COMPONENT_RXFLOWS)))
			{
				dr_UpdateRxFlow(device);
			}
			else if (!strcmp(g_dtRequests[i].description, dr_device_change_index_to_string(DR_DEVICE_COMPONENT_PROPERTIES)))
			{
				dr_UpdateProperty(device);
			}
			else if (!strcmp(g_dtRequests[i].description, "KK-subscribe"))
			{
				dr_UpdateSubscribe(device);
			}
			else if (!strcmp(g_dtRequests[i].description, "KK-capability"))
			{
				dr_UpdateCapability(device);
			}
			else if (!strcmp(g_dtRequests[i].description, "Ping_TEST")) {
				dr_UpdateActive(device);
			}

			//one-to-one for request-response
			dr_ReleaseRequest(g_dtRequests + i);
			return;
		}
	}
}

void dr_UpdateTxChannel(dr_device_t *device)
{
	PDANTENODE pnode;

	if (dr_device_is_component_stale(device, DR_DEVICE_COMPONENT_TXCHANNELS))
	{
		_kkmsg(MSG_LOG, "ERR- TXCHANNELS has been marked as stale and needs updating");
		return;
	}

	//find device to add txc
	const char *name = dr_device_get_name(device);
	pnode = dt_FindNode(name);
	if (!pnode) return;

	PNODECHANNEL pch, ptail = NULL;
	dr_txchannel_t *txc;
	uint16_t i;

	for (i = 0; i < pnode->ntx; i++)
	{
		//new txc
		pch = (NODECHANNEL *)malloc(sizeof(NODECHANNEL));
		if (!pch)
		{
			_kkmsg(MSG_LOG, "ERR- allocate memory for new TX channel");
			return;
		}

		//txc id and name
		txc = dr_device_txchannel_at_index(pnode->device, i);
		pch->id = dr_txchannel_get_id(txc);
		pch->name = dr_txchannel_get_canonical_name(txc);
		strcpy(pch->label, "");

		//txc to node
		if (pnode->ptx == NULL)
		{
			pnode->ptx = pch;
			pch->next_ch = NULL;
		}
		else
		{
			ptail->next_ch = pch;
			pch->next_ch = NULL;
		}
		ptail = pch;
	}
}

void dr_UpdateRxChannel(dr_device_t *device)
{
	PDANTENODE pnode;

	if (dr_device_is_component_stale(device, DR_DEVICE_COMPONENT_RXCHANNELS))
	{
		_kkmsg(MSG_LOG, "ERR- RXCHANNELS has been marked as stale and needs updating");
		return;
	}

	//find device to add rxc
	const char *name = dr_device_get_name(device);
	pnode = dt_FindNode(name);
	if (!pnode) return;

	PNODECHANNEL pch, ptail = NULL;
	dr_rxchannel_t *rxc;
	uint16_t i;

	//TODO: 2019.01.18 by Andy fix update failure
	ptail = pnode->prx;
	while (ptail != NULL) {
		pch = ptail;
		ptail = ptail->next_ch;
		free(pch);
	}
	pnode->prx = NULL;


	for (i = 0; i < pnode->nrx; i++)
	{
		//new rxc
		pch = (NODECHANNEL *)malloc(sizeof(NODECHANNEL));
		if (!pch)
		{
			_kkmsg(MSG_LOG, "ERR- allocate memory for new RX channel");
			return;
		}

		//rxc id and name
		rxc = dr_device_rxchannel_at_index(pnode->device, i);
		if (rxc) {
			pch->id = dr_rxchannel_get_id(rxc);
			pch->name = dr_rxchannel_get_name(rxc);
		}
		strcpy(pch->label, "");//no use

		//rxc to node
		if (pnode->prx == NULL)
		{
			pnode->prx = pch;
			pch->next_ch = NULL;
		}
		else
		{
			ptail->next_ch = pch;
			pch->next_ch = NULL;
		}
		ptail = pch;
	}
}

void dr_UpdateTxLabel(dr_device_t *device)
{
	aud_error_t err;
	PDANTENODE pnode;

	if (dr_device_is_component_stale(device, DR_DEVICE_COMPONENT_TXLABELS))
	{
		_kkmsg(MSG_LOG, "ERR- TXLABELS has been marked as stale and needs updating");
		return;
	}

	//find device
	const char *name = dr_device_get_name(device);
	pnode = dt_FindNode(name);
	if (!pnode) return;

	PNODECHANNEL pch, ptail = NULL;
	dr_txlabel_t txlabel;
	dante_id_t id;

	id = 1;
	err = dr_device_txlabel_with_id(device, id, &txlabel);
	while (err == AUD_SUCCESS)
	{
		pch = pnode->ptx;
		while (pch)
		{
			if (pch->id == dr_txchannel_get_id(txlabel.tx))
			{
				strcpy(pch->label, txlabel.name);
			}
			pch = pch->next_ch;
		}
		id++;
		err = dr_device_txlabel_with_id(device, id, &txlabel);
	}
}

void dr_UpdateTxFlow(dr_device_t *device)
{
	aud_error_t err;
	aud_errbuf_t errbuf;

	PDANTENODE pnode;

	if (dr_device_is_component_stale(device, DR_DEVICE_COMPONENT_TXFLOWS))
	{
		_kkmsg(MSG_LOG, "ERR- TxFlow has been marked as stale and needs updating");
	}

	//find device
	const char *txname = dr_device_get_name(device);
	pnode = dt_FindNode(txname);
	if (!pnode) return;

	uint16_t f, nf;//f=flow, nf=number of flow
	nf = dr_device_num_txflows(device);
	for (f = 0; f < nf; f++)
	{
		dr_txflow_t *flow = NULL;
		err = dr_device_txflow_at_index(device, f, &flow);
		if (err != AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "ERR- get tx flow handle (%s)", dr_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			return;
		}

		char *rxname;
		char *fname;
		err = dr_txflow_get_destination(flow, &rxname, &fname);
		if (err != AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "ERR- get rx device and flow name (%s)", dr_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			dr_txflow_release(&flow);
			return;
		}

		uint16_t nslot;
		err = dr_txflow_num_slots(flow, &nslot);
		if (err != AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "ERR- get number of tx slot (%s)", dr_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			dr_txflow_release(&flow);
			return;
		}

		for (uint16_t i = 0; i < nslot; i++)
		{
			dr_txchannel_t *tx;
			err = dr_txflow_channel_at_slot(flow, i, &tx);
			if (err != AUD_SUCCESS)
			{
				KKMSG(g_MsgBuf, "ERR- get channel at tx slot (%s)", dr_error_message(err, errbuf));
				_kkmsg(MSG_LOG, NULL);
				dr_txflow_release(&flow);
				return;
			}

			if (tx)
			{
				KKMSG(g_MsgBuf, "update tx flow %d [%s >> %s][%s]", f+1, txname, rxname, dr_txchannel_get_canonical_name(tx));
				_kkmsg(MSG_LOG, NULL);
			}
		}

		dante_id_t id;
		err = dr_txflow_get_id(flow, &id);
		if (err != AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "ERR- get tx flow id (%s)", dr_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			dr_txflow_release(&flow);
			return;
		}

		char *name = NULL;
		err = dr_txflow_get_name(flow, &name);
		if (err != AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "ERR- get tx flow name (%s)", dr_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			dr_txflow_release(&flow);
			return;
		}

		aud_bool_t manual;
		err = dr_txflow_is_manual(flow, &manual);
		if (err != AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "ERR- get tx flow flag of manual (%s)", dr_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			dr_txflow_release(&flow);
			return;
		}

		dante_samplerate_t srate;
		dante_encoding_t enc;
		err = dr_txflow_get_format(flow, &srate, &enc);
		if (err != AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "ERR- get tx flow format (%s)", dr_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			dr_txflow_release(&flow);
			return;
		}

		dante_latency_us_t latency;
		err = dr_txflow_get_latency_us(flow, &latency);
		if (err != AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "ERR- get tx flow latency (%s)", dr_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			dr_txflow_release(&flow);
			return;
		}

		dante_fpp_t fpp;
		err = dr_txflow_get_fpp(flow, &fpp);
		if (err != AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "ERR- get tx flow fpp (%s)", dr_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			dr_txflow_release(&flow);
			return;
		}

		uint16_t nif;
		dante_ipv4_address_t addresses[MAX_INTERFACES];
		err = dr_txflow_num_interfaces(flow, &nif);
		if (err != AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "ERR- get tx flow interface count (%s)", dr_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			dr_txflow_release(&flow);
			return;
		}

		for (uint16_t i = 0; i < nif; i++)
		{
			err = dr_txflow_address_at_index(flow, i, addresses + i);
			if (err != AUD_SUCCESS)
			{
				KKMSG(g_MsgBuf, "ERR- get tx address (%s)", dr_error_message(err, errbuf));
				_kkmsg(MSG_LOG, NULL);
				dr_txflow_release(&flow);
				return;
			}
		}

		dante_flow_class_t cflow;
		uint32_t origin_addr;
		err = dr_txflow_get_flow_class(flow, &cflow);
		if (err != AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "ERR- get tx flow transport type (%s)", dr_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			dr_txflow_release(&flow);
			return;
		}

		if (cflow == DANTE_FLOW_CLASS__AES67_MCAST_IP)
		{
			err = dr_txflow_get_aes67_sdp_origin_addr(flow, &origin_addr);
			if (err != AUD_SUCCESS)
			{
				KKMSG(g_MsgBuf, "ERR- get SAP message id hash (%s)", dr_error_message(err, errbuf));
				_kkmsg(MSG_LOG, NULL);
				dr_txflow_release(&flow);
				return;
			}
		}
		else {
			origin_addr = 0;
		}

		char transport_buf[100];
		if (cflow == DANTE_FLOW_CLASS__AES67_MCAST_IP)
		{
			SNPRINTF(transport_buf, sizeof(transport_buf), "%s:%02x%02x%02x%02x", "AES67"
			, origin_addr & 0xff, (origin_addr >> 8) & 0xff
			, (origin_addr >> 16) & 0xff, (origin_addr >> 24) & 0xff);
		}
		else {
			SNPRINTF(transport_buf, sizeof(transport_buf), "%s", "DANTE");
		}

		dr_txflow_release(&flow);
	}
}

void dr_UpdateRxFlow(dr_device_t *device)
{
	aud_error_t err;
	aud_errbuf_t errbuf;

	PDANTENODE pnode;

	if (dr_device_is_component_stale(device, DR_DEVICE_COMPONENT_TXFLOWS))
	{
		_kkmsg(MSG_LOG, "ERR- RxFlow has been marked as stale and needs updating");
	}

	//find device
	const char *rxname = dr_device_get_name(device);
	pnode = dt_FindNode(rxname);
	if (!pnode) return;

	uint16_t i, j, k, f, nf;//f=flow, nf=number of flow
	nf = dr_device_num_rxflows(device);
	for (f = 0; f < nf; f++)
	{
		dr_rxflow_t *flow = NULL;
		err = dr_device_rxflow_at_index(device, f, &flow);
		if (err != AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "ERR- get rx flow handle (%s)", dr_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			return;
		}

		uint16_t nslot;
		err = dr_rxflow_num_slots(flow, &nslot);
		if (err != AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "ERR- get number of rx slot (%s)", dr_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			dr_rxflow_release(&flow);
			return;
		}

		for (i = 0; i < nslot; i++)
		{
			err = dr_rxflow_num_slot_channels(flow, i, &k);
			if (err != AUD_SUCCESS)
			{
				KKMSG(g_MsgBuf, "ERR- get channel count for rx slot %d (%s)", i, dr_error_message(err, errbuf));
				_kkmsg(MSG_LOG, NULL);
				dr_rxflow_release(&flow);
				return;
			}

			for (j = 0; j < k; j++)
			{
				dr_rxchannel_t *rx;
				err = dr_rxflow_slot_channel_at_index(flow, i, j, &rx);
				if (err != AUD_SUCCESS)
				{
					KKMSG(g_MsgBuf, "ERR- get channel %d for slot %d (%s)", j, i, dr_error_message(err, errbuf));
					_kkmsg(MSG_LOG, NULL);
					dr_rxflow_release(&flow);
					return;
				}
				KKMSG(g_MsgBuf, "update rx flow [%s][%s]", rxname, (dr_rxchannel_get_name(rx)));
				_kkmsg(MSG_LOG, NULL);
			}
		}

		dante_id_t id;
		err = dr_rxflow_get_id(flow, &id);
		if (err != AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "ERR- get flow id (%s)", dr_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			dr_rxflow_release(&flow);
			return;
		}

		char *name;
		err = dr_rxflow_get_name(flow, &name);
		if (err != AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "ERR- get flow name (%s)", dr_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			dr_rxflow_release(&flow);
			return;
		}

		aud_bool_t manual;
		err = dr_rxflow_is_manual(flow, &manual);
		if (err != AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "ERR- get flow 'manual' setting (%s)", dr_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			dr_rxflow_release(&flow);
			return;
		}

		aud_bool_t multicast;
		err = dr_rxflow_is_multicast_template(flow, &multicast);
		if (err != AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "ERR- get flow 'multicast template' setting (%s)", dr_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			dr_rxflow_release(&flow);
			return;
		}

		aud_bool_t unicast;
		err = dr_rxflow_is_unicast_template(flow, &unicast);
		if (err != AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "ERR- get flow 'unicast template' setting (%s)", dr_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			dr_rxflow_release(&flow);
			return;
		}

		dante_samplerate_t samplerate;
		dante_encoding_t encoding;
		err = dr_rxflow_get_format(flow, &samplerate, &encoding);
		if (err != AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "ERR- get flow format (%s)", dr_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			dr_rxflow_release(&flow);
			return;
		}

		uint16_t nif;
		dante_ipv4_address_t addresses[MAX_INTERFACES];
		err = dr_rxflow_num_interfaces(flow, &nif);
		if (err != AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "ERR- get flow interface count (%s)", dr_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			dr_rxflow_release(&flow);
			return;
		}

		for (i = 0; i < nif; i++)
		{
			err = dr_rxflow_address_at_index(flow, i, addresses + i);
			if (err != AUD_SUCCESS)
			{
				KKMSG(g_MsgBuf, "ERR- get address %u (%s)", dr_error_message(err, errbuf));
				_kkmsg(MSG_LOG, NULL);
				dr_rxflow_release(&flow);
				return;
			}
		}

		uint16_t connections_active;
		err = dr_rxflow_get_connections_active(flow, &connections_active);
		if (err != AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "ERR- get active connections (%s)", dr_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			dr_rxflow_release(&flow);
			return;
		}

		dante_latency_us_t latency_us;
		err = dr_rxflow_get_latency_us(flow, &latency_us);
		if (err != AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "ERR- get latency (%s)", dr_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			dr_rxflow_release(&flow);
			return;
		}

		char *tx_device_name;
		err = dr_rxflow_get_tx_device_name(flow, &tx_device_name);
		if (err != AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "ERR- get tx device name (%s)", dr_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			dr_rxflow_release(&flow);
			return;
		}

		char *tx_flow_name;
		err = dr_rxflow_get_tx_flow_name(flow, &tx_flow_name);
		if (err != AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "ERR- get tx flow name (%s)", dr_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			dr_rxflow_release(&flow);
			return;
		}

		dante_flow_class_t flow_class;
		err = dr_rxflow_get_flow_class(flow, &flow_class);
		if (err != AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "ERR- get rx transport type (%s)", dr_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			dr_rxflow_release(&flow);
			return;
		}

		char source[DANTE_NAME_LENGTH * 2];
		if (tx_device_name && tx_flow_name)
		{
			SNPRINTF(source, DANTE_NAME_LENGTH * 2, "%s@%s", tx_flow_name, tx_device_name);
		}
		else if (tx_device_name)
		{
			SNPRINTF(source, DANTE_NAME_LENGTH * 2, "%s", tx_device_name);
		}
		else {
			strcpy(source, "-");
		}

		uint32_t aes67_origin_addr;
		if (flow_class == DANTE_FLOW_CLASS__AES67_MCAST_IP)
		{
			err = dr_rxflow_get_aes67_sdp_origin_addr(flow, &aes67_origin_addr);
			if (err != AUD_SUCCESS)
			{
				KKMSG(g_MsgBuf, "ERR- get SDP origin address (%s)", dr_error_message(err, errbuf));
				_kkmsg(MSG_LOG, NULL);
				dr_rxflow_release(&flow);
				return;
			}
			SNPRINTF(source, DANTE_NAME_LENGTH * 2, "%d.%d.%d.%d",aes67_origin_addr & 0xff
				, (aes67_origin_addr >> 8) & 0xff, (aes67_origin_addr >> 16) & 0xff
				, (aes67_origin_addr >> 24) & 0xff);
		}
		else {
			aes67_origin_addr = 0;
		}

		char format_buf[512];
		SNPRINTF(format_buf, sizeof(format_buf), "%d/%d", samplerate, encoding);

		char transport_buf[100];
		if (flow_class == DANTE_FLOW_CLASS__AES67_MCAST_IP)
		{
			SNPRINTF(transport_buf, sizeof(transport_buf), "%s:%02x%02x%02x%02x", "AES67"
				, aes67_origin_addr & 0xff, (aes67_origin_addr >> 8) & 0xff
				, (aes67_origin_addr >> 16) & 0xff, (aes67_origin_addr >> 24) & 0xff);
		}
		else {
			SNPRINTF(transport_buf, sizeof(transport_buf), "%s", "DANTE");
		}
		dr_rxflow_release(&flow);
	}
}

void dr_UpdateProperty(dr_device_t *device)
{
	aud_error_t err;
	aud_errbuf_t errbuf;

	//device state
	dr_device_state_t state = dr_device_get_state(device);
	if (state == DR_DEVICE_STATE_ERROR)
	{
		err = dr_device_get_error_state_error(device);
		const char *errdo = dr_device_get_error_state_action(device);

		KKMSG(g_MsgBuf, "error=%s action(%s)", dr_error_message(err, errbuf), (errdo ? errdo : "no"));
		_kkmsg(MSG_LOG, NULL);
	}
	KKMSG(g_MsgBuf, "%s in UpdateProperty state: %s", dr_device_get_name(device), dr_device_state_to_string(state));
	_kkmsg(MSG_LOG, NULL);

	//device address
	unsigned int num = 2;
	dante_ipv4_address_t addr[2];
	dr_device_get_addresses(device, &num, addr);
	for (int i = 0; i < num; i++)
	{
		uint8_t *host = (uint8_t *) &(addr[i].host);
		KKMSG(g_MsgBuf, "address: %u.%u.%u.%u, %u", host[0], host[1], host[2], host[3], addr[i].port);
		_kkmsg(MSG_LOG, NULL);
	}

	if (state == DR_DEVICE_STATE_ACTIVE)
	{
		dr_device_component_t c;
		KKMSG(g_MsgBuf, "%s components stale:", dr_device_get_name(device));
		_kkmsg(MSG_LOG, NULL);
		for (int i = 0; i < DR_DEVICE_COMPONENT_COUNT; i++)
		{
			c = (dr_device_component_t)i;
			aud_bool_t stale = dr_device_is_component_stale(device, c);
			KKMSG(g_MsgBuf, "- %s: %s", dr_device_component_to_string(c), (stale ? "stale" : "ok"));
			_kkmsg(MSG_LOG, NULL);
		}

		KKMSG(g_MsgBuf, "%s properties:", dr_device_get_name(device));								_kkmsg(MSG_LOG, NULL);
		KKMSG(g_MsgBuf, "- tx latency: %dus", dr_device_get_tx_latency_us(device));					_kkmsg(MSG_LOG, NULL);
		KKMSG(g_MsgBuf, "- min txlatency: %dus", dr_device_get_tx_latency_min_us(device));			_kkmsg(MSG_LOG, NULL);
		KKMSG(g_MsgBuf, "- tx fpp: %d", dr_device_get_tx_fpp(device));								_kkmsg(MSG_LOG, NULL);
		KKMSG(g_MsgBuf, "- rx latency: %dus", dr_device_get_rx_latency_us(device));					_kkmsg(MSG_LOG, NULL);
		KKMSG(g_MsgBuf, "- max rxlatency: %dus", dr_device_get_rx_latency_max_us(device));			_kkmsg(MSG_LOG, NULL);
		KKMSG(g_MsgBuf, "- rx fpp: %d", dr_device_get_rx_fpp(device));								_kkmsg(MSG_LOG, NULL);
		KKMSG(g_MsgBuf, "- min rxfpp: %d", dr_device_get_rx_fpp_min(device));						_kkmsg(MSG_LOG, NULL);
		KKMSG(g_MsgBuf, "- num rxflow(default): %d", dr_device_get_rx_flow_default_slots(device));	_kkmsg(MSG_LOG, NULL);

		const char *clock_subdomain_name = dr_device_get_clock_subdomain_name(device);
		KKMSG(g_MsgBuf, "- clock subdomain name: %s", (clock_subdomain_name ? clock_subdomain_name : "no"));
		_kkmsg(MSG_LOG, NULL);

		KKMSG(g_MsgBuf, "- network loopback: %s",
			dr_device_has_network_loopback(device) ?
			(dr_device_get_network_loopback(device) ? "on" : "off")	: "N/A");
		_kkmsg(MSG_LOG, NULL);

		uint16_t min_port, max_port;
		err = dr_device_get_manual_unicast_receive_port_range(device, &min_port, &max_port);
		if (err == AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "- unicast port range: %d->%d", min_port, max_port);
			_kkmsg(MSG_LOG, NULL);
		}
		else {
			_kkmsg(MSG_LOG, "- unicast port range: N/A");
		}

		uint32_t ipv4_prefix;
		err = dr_device_get_aes67_mcast_prefix(device, &ipv4_prefix);
		if (err == AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "- aes67 prefix: 0x%x", ipv4_prefix);
			_kkmsg(MSG_LOG, NULL);
		}
		else {
			_kkmsg(MSG_LOG, "- aes67 prefix: N/A");
		}

		dr_device_status_flags_t status_flags = 0;
		const char *STATUS_STRINGS[] =
		{
			"NAME_CONFLICT",
			"UNLICENSED",
			"LOCKDOWN"
		};
		err = dr_device_get_status_flags(device, &status_flags);
		if (err == AUD_SUCCESS)
		{
			uint32_t i;
			KKMSG(g_MsgBuf, "%s status flags:", dr_device_get_name(device));
			for (i = 0; i < DR_DEVICE_STATUS_COUNT; i++)
			{
				aud_bool_t b = (status_flags & (1 << i));
				KKMSG(g_MsgBuf, "- %13s: %s", STATUS_STRINGS[i], b ? "true" : "false");
			}
		}
		else {
			KKMSG(g_MsgBuf, "- unknown(error: %s)", dr_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
		}

		dante_rxflow_error_type_t type;
		dante_rxflow_error_flags_t rxflow_error_flags = dr_device_available_rxflow_error_flags(device);
		dante_rxflow_error_flags_t rxflow_error_fields = dr_device_available_rxflow_error_fields(device);
		uint32_t subsecond_range = dr_device_rxflow_error_subsecond_range(device);

		KKMSG(g_MsgBuf, "%s rxflow error flags: 0x%04x", dr_device_get_name(device), rxflow_error_flags);
		_kkmsg(MSG_LOG, NULL);
		for (type = 0; type < DANTE_NUM_RXFLOW_ERROR_TYPES; type++)
		{
			KKMSG(g_MsgBuf, "- %s: %s",
				dante_rxflow_error_type_to_string(type),
				(rxflow_error_flags & (1 << type)) ? "true" : "false");
			_kkmsg(MSG_LOG, NULL);
		}
			
		KKMSG(g_MsgBuf, "%s rxflow error fields: 0x%04x\n", dr_device_get_name(device), rxflow_error_fields);
		for (type = 0; type < DANTE_NUM_RXFLOW_ERROR_TYPES; type++)
		{
			KKMSG(g_MsgBuf, "- %s: %s",
				dante_rxflow_error_type_to_string(type),
				(rxflow_error_fields & (1 << type)) ? "true" : "false");
			_kkmsg(MSG_LOG, NULL);
		}
		KKMSG(g_MsgBuf, "RxFlow Error Subsecond Range: %u", subsecond_range);
		_kkmsg(MSG_LOG, NULL);
	}
}

void dr_UpdateCapability(dr_device_t *device)
{
	aud_error_t err;
	aud_errbuf_t errbuf;

	//device state
	dr_device_state_t state = dr_device_get_state(device);
	if (state == DR_DEVICE_STATE_ERROR)
	{
		err = dr_device_get_error_state_error(device);
		const char *errdo = dr_device_get_error_state_action(device);

		KKMSG(g_MsgBuf, "error=%s action(%s)", dr_error_message(err, errbuf), (errdo ? errdo : "no"));
		_kkmsg(MSG_LOG, NULL);
	}
	KKMSG(g_MsgBuf, "%s in UpdateCapability state: %s", dr_device_get_name(device), dr_device_state_to_string(state));
	_kkmsg(MSG_LOG, NULL);

	if (state == DR_DEVICE_STATE_ACTIVE)
	{
		KKMSG(g_MsgBuf, "%s capabilities:", dr_device_get_name(device));				_kkmsg(MSG_LOG, NULL);
		KKMSG(g_MsgBuf, "- num interfaces: %d", dr_device_num_interfaces(device));		_kkmsg(MSG_LOG, NULL);
		KKMSG(g_MsgBuf, "- num tx channel: %d", dr_device_num_txchannels(device));		_kkmsg(MSG_LOG, NULL);
		KKMSG(g_MsgBuf, "- num rx channel: %d", dr_device_num_rxchannels(device));		_kkmsg(MSG_LOG, NULL);
		KKMSG(g_MsgBuf, "- num max txflow: %d", dr_device_max_txflow_slots(device));	_kkmsg(MSG_LOG, NULL);
		KKMSG(g_MsgBuf, "- num max rxflow: %d", dr_device_max_rxflow_slots(device));	_kkmsg(MSG_LOG, NULL);

		uint16_t max_txlabels;
		err = dr_device_max_txlabels(device, &max_txlabels);
		if (err == AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "- max tx labels: %d", max_txlabels);
			_kkmsg(MSG_LOG, NULL);
		}
		else {
			_kkmsg(MSG_LOG, "- max tx labels: N/A");
		}

		uint16_t max_txflows;
		err = dr_device_max_txflows(device, &max_txflows);
		if (err == AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "- max tx flows: %d", max_txflows);
			_kkmsg(MSG_LOG, NULL);
		}
		else {
			_kkmsg(MSG_LOG, "- max tx flows: N/A");
		}

		uint16_t max_rxflows;
		err = dr_device_max_rxflows(device, &max_rxflows);
		if (err == AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "- max rx flows: %d", max_rxflows);
			_kkmsg(MSG_LOG, NULL);
		}
		else {
			_kkmsg(MSG_LOG, "- max rx flows: N/A");
		}
	}
}

void dr_UpdateSubscribe(dr_device_t *device)
{
	//TODO: 2019.01.18 by Andy
	dr_DeviceStateChanged(device);
	//tbd
}

void dr_UpdateActive(dr_device_t *device) {
	PDANTENODE pnode;
	
	const char *name = dr_device_get_name(device);
	pnode = dt_FindNode(name);
	if (!pnode) return;

	pnode->active = TRUE;

}

void dr_SubscribeChannel(const char *rx, const char *rxch, const char *tx, const char *txch)
{
	aud_error_t err;
	aud_errbuf_t errbuf;

	PDANTENODE pnode;
	uint16_t rxch_id;

	//find rx node
	pnode = dt_FindNode(rx);
	if (!pnode) return;

	//get rxch_id
	PNODECHANNEL pch = NULL;

	rxch_id = 0;
	pch = pnode->prx;
	while (pch && strcmp(pch->name, rxch) != 0)
	{
		pch = pch->next_ch;
	}
	if (pch) rxch_id = pch->id;

	if (rxch_id < 1 || rxch_id > pnode->nrx)
	{
		sprintf(g_MsgBuf, "ERR- invalid RX channel (must be in range 1..%u)", pnode->nrx);
		_kkmsg(MSG_LOG, NULL);
		return;
	}

	// allocate a request
	PDEVICEREQUEST request;
	request = dr_AllocateRequest("KK-subscribe");
	if (!request)
	{
		_kkmsg(MSG_LOG, "ERR- no more request for subscribe");
		return;
	}

	//rxch_id is zero based array
	err = dr_rxchannel_subscribe(pnode->rx[rxch_id - 1], dr_RequestResponseCB, &request->id, tx, txch);
	if (err != AUD_SUCCESS)
	{
		sprintf(g_MsgBuf, "ERR- sending subscribe message: %s", dr_error_message(err, errbuf));
		_kkmsg(MSG_LOG, NULL);
		dr_ReleaseRequest(request);
		return;
	}
}

const char *dt_GetRxSubscription(const char *device, const char *rxch)
{
	PDANTENODE pnode;

	//find rx node
	pnode = dt_FindNode(device);
	if (!pnode) return NULL;

	PNODECHANNEL pch = NULL;
	pch = pnode->prx;
	while (pch && strcmp(pch->name, rxch) != 0)
	{
		pch = pch->next_ch;
	}

	if (pch)
	{
		dr_rxchannel_t *rxc = dr_device_rxchannel_at_index(pnode->device, pch->id - 1);
		return dr_rxchannel_get_subscription(rxc);
	}
	else
	{
		return NULL;
	}
}


