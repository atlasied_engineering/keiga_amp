
/*****************************************************************************
| Copyright (c) Keiga Inc. All Rights Reserved.
|
| Project : amor
| FileName: dt_routing.cpp
| Author  : Dennis Lu
| Revise  : 
| Version : V1.7.7.7
| Comment : 
\*****************************************************************************/

#include "stdafx.h"
//#include "ap_main.h"
//#include "define.h"
#include "dt.h"
#include "kk.h"
#include "dt_extend_api.h"
#include "device_state.h"
void dt_InitBrowsing()
{
	//set callbacks
	db_browse_set_network_changed_callback(g_dtDB, db_NetworkChangedCB);
	db_browse_set_event_callback(g_dtDB, db_EventCB);
	db_browse_set_sockets_changed_callback(g_dtDB, db_SocketsChangedCB);
//	db_browse_set_node_changed_callback(g_dtDB, db_NodeChangedCB);

	//set context
	//db_browse_set_context(g_dtDB, void *context);
}

void dt_SocketChanged(db_browse_t *browse, dante_sockets_t *sockets)
{
	sprintf(g_MsgBuf, "SOCKETS=%d", sockets->n);
	_kkmsg(MSG_LOG, NULL);

	for (int i = 0; i < sockets->n; i++)
	{
		sprintf(g_MsgBuf, "%d", sockets->read_fds.fd_array[i]);
		_kkmsg(MSG_LOG, NULL);
	}
}

void db_SocketsChangedCB(const db_browse_t * browse)
{
	_kkmsg(MSG_LOG, "CALL: db_SocketsChangedCB");
}

void db_EventCB(db_browse_event_t *event)
{
	_kkmsg(MSG_LOG, "CALL: db_EventCB");
}

void db_NetworkChangedCB(const db_browse_t *browse)
{
	_kkmsg(MSG_LOG, "CALL: db_NetworkChangedCB");
}

void db_NodeChangedCB(db_browse_t *browse, const db_node_t *node, db_node_change_t node_change)
{
	const char *name = db_browse_device_get_name(node->_.device);
	switch (node->type)
	{
	case DB_NODE_TYPE_DEVICE:
		if (node_change == DB_NODE_CHANGE_ADDED)
		{
			db_NodeAdded(node->_.device);
			KKMSG(g_MsgBuf, "%s node added", name);
			_kkmsg(MSG_LOG, NULL);
		}
		else if (node_change == DB_NODE_CHANGE_MODIFIED)
		{
			db_NodeModified(node->_.device);
		}
		else if (node_change == DB_NODE_CHANGE_REMOVED)
		{
			db_NodeRemoved(node->_.device);
		}
		break;
	case DB_NODE_TYPE_CHANNEL:
		//should not be used and ask TX channels in routing API
		//DB_NODE_CHANGE_ADDED
		//DB_NODE_CHANGE_MODIFIED
		//DB_NODE_CHANGE_REMOVED
		break;
	case DB_NODE_TYPE_LABEL:
		if (node_change == DB_NODE_CHANGE_ADDED)
		{
			db_TxLabelAdded(node->_.label);
		}
		else if (node_change == DB_NODE_CHANGE_MODIFIED)
		{
			db_TxLabelModified(node->_.label);
		}
		else if (node_change == DB_NODE_CHANGE_REMOVED)
		{
			db_TxLabelRemoved(node->_.label);
		}
		break;
	case DB_NODE_TYPE_AES67_SAP:
		//db_test_print_aes67_sap(test, node->_.aes67);
	{
		int i = 0;
	}
		break;
	}
}

//-----------------------------------------------------------------------------
// node added
// - device name is the index key which should be unique in dante listing
// - x
//
void db_NodeAdded(const db_browse_device_t *device)
{
	aud_error_t err;
	aud_errbuf_t errbuf;

	PDANTENODE p, ptail;
	const char *name = db_browse_device_get_name(device);

	//check device name
	p = dt_FindNode(name);
	if (p)
	{
		KKMSG(g_MsgBuf, "ERR- duplicate node (%s)", name);
		_kkmsg(MSG_BOX, NULL);
		return;
	}

	//get ptail in listing
	p = g_dtHead;
	ptail = g_dtHead;
	while (p)
	{
		ptail = p;
		p = ptail->next_node;
	}

	//create new node
	p = (DANTENODE *)malloc(sizeof(DANTENODE));
	if (p == NULL)
	{
		_kkmsg(MSG_BOX, "ERR- no memory to add new node");
		return;
	}

	//set node's data
	memset(p, 0, sizeof(DANTENODE));
	p->active = false;
	p->name = db_browse_device_get_name(device);
	p->db_device = (db_browse_device_t *)device;
	p->rid = CONMON_CLIENT_NULL_REQ_ID;
	p->ptx = NULL;

	
	//open a device connection
	dr_device_open_t *config = NULL;
	config = dr_device_open_config_new(name);
	if (!config)
	{
		_kkmsg(MSG_BOX, "ERR- open a device connection");
		return;
	}

	//create a device object to communicate with a remote device
	err = dr_device_open_with_config(g_dtDR, config, &p->device);
	dr_device_open_config_free(config);
	config = NULL;
	if (err != AUD_SUCCESS)
	{
		KKMSG(g_MsgBuf, "ERR- creating device (%s)", dr_error_message(err, errbuf));
		_kkmsg(MSG_BOX, NULL);
		return;
	}

	//set device changed callback
	dr_device_set_changed_callback(p->device, dr_DeviceChangedCB);
	//dr_DeviceStateChanged(p->device);
	

	//add node in the listing
	if (g_dtHead == NULL)
	{
		g_dtHead = p;
		g_dtHead->next_node = NULL;
	}
	else
	{
		ptail->next_node = p;
		p->next_node = NULL;
	}
}


//-----------------------------------------------------------------------------
// node modified
//
void db_NodeModified(const db_browse_device_t *device)
{
	_kkmsg(MSG_BOX, "CALL: db_NodeModified");
}

//-----------------------------------------------------------------------------
// node removed
//
void db_NodeRemoved(const db_browse_device_t *device)
{
	PDANTENODE p, prev;
	const char *name = db_browse_device_get_name(device);

	prev = NULL;
	p = g_dtHead;
	while (p && strcmp(p->name, name) != 0)
	{
		prev = p;
		p = p->next_node;
	}

	if (!p)
	{
		KKMSG(g_MsgBuf, "ERR- no this node (%s)", name);
		_kkmsg(MSG_BOX, NULL);
		return;
	}

	//delete node
	if (p == g_dtHead)
	{
		//first node
		g_dtHead = p->next_node;
		free(p);
		return;
	}
	else
	{
		if (!p->next_node)
			//tail node
			prev->next_node = NULL;
		else
			//middle node
			prev->next_node = p->next_node;
		free(p);
	}
}

void db_AddTxChannel(const db_browse_channel_t *txch)
{
	PDANTENODE pnode;

	//find device node
	const char *device_name = db_browse_device_get_name(db_browse_channel_get_device(txch));
	pnode = dt_FindNode(device_name);
	if (!pnode) return;

	//create new channel
	PNODECHANNEL pch = (NODECHANNEL *)malloc(sizeof(NODECHANNEL));
	if (pch == NULL)
	{
		_kkmsg(MSG_LOG, "ERR- no memory to add new channel");
		return;
	}

	//set channel data
	pch->name = db_browse_channel_get_canonical_name(txch);
	pch->id = db_browse_channel_get_id(txch);
	if (pch->id == 0)
	{
		_kkmsg(MSG_LOG, "WARNING- this component has been marked as stale and needs updating");
		return;
	}

	//add channels to node
	PNODECHANNEL ptail;
	if (pnode->ptx == NULL)
	{
		pnode->ptx = pch;
		pch->next_ch = NULL;
	}
	else
	{
		ptail = pnode->ptx;
		while (ptail->next_ch)
		{
			ptail = ptail->next_ch;
		}
		ptail->next_ch = pch;
		pch->next_ch = NULL;
	}
	pnode->ntx++;
}

void db_ModifyTxChannel(const db_browse_channel_t *txch)
{
	//tbd
}

void db_RemoveTxChannel(const db_browse_channel_t *txch)
{
	PDANTENODE pnode;

	//find device node
	const char *device_name = db_browse_device_get_name(db_browse_channel_get_device(txch));
	pnode = dt_FindNode(device_name);
	if (!pnode) return;

	//delete txch in node
	PNODECHANNEL pch, prev;
	dante_id_t txid = db_browse_channel_get_id(txch);

	prev = NULL;
	pch = pnode->ptx;
	while (pch && (pch->id != txid))
	{
		prev = pch;
		pch = pch->next_ch;
	}

	if (!pch)
	{
		_kkmsg(MSG_LOG, "ERR- cannot find the channel to remove");
		return;
	}

	if (pch == pnode->ptx)
	{
		//first channel
		pnode->ptx = pch->next_ch;
		pnode->ntx--;
		free(pch);
		return;
	}
	else
	{
		if (!pch->next_ch)
		{
			//tail channel
			prev->next_ch = NULL;
		}
		else
		{
			//middle node
			prev->next_ch = pch->next_ch;
		}
		pnode->ntx--;
		free(pch);
	}
}

//-----------------------------------------------------------------------------
// add label in dante network
//
void db_TxLabelAdded(const db_browse_label_t *txlabel)
{
	PDANTENODE pnode;

	//find device
	const char *device_name = db_browse_device_get_name(db_browse_channel_get_device(db_browse_label_get_channel(txlabel)));
	pnode = dt_FindNode(device_name);
	if (!pnode) return;

	//find channel
	PNODECHANNEL pch = pnode->ptx;
	const char *label_name = db_browse_label_get_name(txlabel);
	dante_id_t id = db_browse_channel_get_id(db_browse_label_get_channel(txlabel));
	
	while (pch)
	{
		if (pch->id == id)
		{
			strcpy(pch->label, label_name);
			pch = NULL;
		}
		else
		{
			pch = pch->next_ch;
		}
	}
}

void db_TxLabelModified(const db_browse_label_t *txlabel)
{
	_kkmsg(MSG_BOX, "CALL: db_TxLabelModified");
}

void db_TxLabelRemoved(const db_browse_label_t *txlabel)
{
	PDANTENODE pnode;

	//find device
	const char *device_name = db_browse_device_get_name(db_browse_channel_get_device(db_browse_label_get_channel(txlabel)));
	pnode = dt_FindNode(device_name);
	if (!pnode) return;

	//search channel
	PNODECHANNEL pch = pnode->ptx;
	const char *label_name = db_browse_label_get_name(txlabel);
	dante_id_t id = db_browse_channel_get_id(db_browse_label_get_channel(txlabel));

	while (pch)
	{
		if (pch->id == id)
		{
			strcpy(pch->label, "");
			pch = NULL;
		}
		else
		{
			pch = pch->next_ch;
		}
	}
}