/*****************************************************************************
| Copyright (c) Keiga Inc. All Rights Reserved.
|
| Project : amor
| FileName: dt_conmon.cpp
| Author  : Dennis Lu
| Revise  :
| Version : V1.8.8.21
| Comment :
\*****************************************************************************/
#include "stdafx.h"
#include "dt.h"
#include "kk.h"
#include "dt_extend_api.h"

//callbacks
static dt_ReceiveMsg_fn *dt_cbReiveMsg = NULL;

void dt_SetReceiveMsgCB(dt_ReceiveMsg_fn *fn)
{
	dt_cbReiveMsg = fn;
}

void dc_MonMsgPayloadCB
(
	conmon_client_t *client,
	conmon_channel_type_t type,
	conmon_channel_direction_t dir,
	const conmon_message_head_t *head,
	const conmon_message_body_t *body
)
{
	//conmon_info_t *info = conmon_client_context (client);

	const conmon_vendor_id_t *vid = conmon_message_head_get_vendor_id(head);
	if (g_rx_vendor_id && !conmon_vendor_id_equals(g_rx_vendor_id, vid))
	{
		return;
	}

	conmon_instance_id_t instance_id;
	char id_buf[LEN_NAME];
	conmon_message_head_get_instance_id(head, &instance_id);
	sprintf(g_MsgBuf, "body_size=0x%04x", conmon_message_head_get_body_size(head));
	sprintf(g_MsgBuf, "seq=0x%04x", conmon_message_head_get_seqnum(head));
	sprintf(g_MsgBuf, "class=0x%04x", conmon_message_head_get_message_class(head));
	sprintf(g_MsgBuf, "source=%s", conmon_client_device_name_for_instance_id(client, &instance_id));
	sprintf(g_MsgBuf, "vendor=%s", dt_cmStringVendorID(vid, id_buf, LEN_NAME));
	
	//payload
	int ch;
	uint16_t len = conmon_message_head_get_body_size(head);
	char *data = (char*)malloc(len * sizeof(char));//(char *)body->data;
	char *name = (char*)malloc(33 * sizeof(char));

	memset(data, 0, len);
	memset(name, 0, 33);
	sprintf(name, "%s", conmon_client_device_name_for_instance_id(client, &instance_id));
	for (int i = 0; i < len; i++)
		data[i] = body->data[i];

	if (type == CONMON_CHANNEL_TYPE_STATUS) ch = DT_CHTYPE_STATUS;
	else if (type == CONMON_CHANNEL_TYPE_LOCAL) ch = DT_CHTYPE_LOCAL;
	else if (type == CONMON_CHANNEL_TYPE_BROADCAST) ch = DT_CHTYPE_BROADCAST;

	dt_cbReiveMsg(name, (char *)data, len, ch);

	//add parse audinate data
	dc_MonMsgPayloadCB_Audinate(client, type, dir, head, body);
}

void dc_SubStatusAllCB(conmon_client_t *client, conmon_client_request_id_t rid, aud_error_t err)
{
	aud_errbuf_t errbuf;

	if (rid != CONMON_CLIENT_NULL_REQ_ID)
	{
		// find the remote target we were trying to subscribe
		if (rid == g_idSubStatus)
		{
			if (err == AUD_SUCCESS)
			{
				//timestamp_event();
				sprintf(g_MsgBuf, "CB:subscribe STATUS message ALL, id=%p(success)",rid);
			}
			else {
				//timestamp_error();
				sprintf(g_MsgBuf, "CB:subscribe STATUS message ALL failed, id=%p(%s)",rid,aud_error_message(err, errbuf));
			}
			_kkmsg(MSG_LOG, NULL);

			g_idSubStatus = CONMON_CLIENT_NULL_REQ_ID;
			return;
		}
	}

	sprintf(g_MsgBuf, "CB:invalid subscription request id %p", rid);
	_kkmsg(MSG_LOG, NULL);
}

void dc_SubStatusOneCB(conmon_client_t *client, conmon_client_request_id_t rid, aud_error_t err)
{
	aud_errbuf_t errbuf;

	PDANTENODE p;
	const conmon_client_subscription_t *sub;

	if (err != AUD_SUCCESS)
	{
		sprintf(g_MsgBuf, "CB:subscribe STATUS message ONE failed, id=%p(%s)",rid,aud_error_message(err, errbuf));
		_kkmsg(MSG_LOG, NULL);
		return;
	}

	if (rid != CONMON_CLIENT_NULL_REQ_ID)
	{
		p = g_dtHead;
		while (p)
		{
			if (p->active && p->rid == rid)
			{
				//find the device
				sub = conmon_client_get_subscription(g_dtClient, CONMON_CHANNEL_TYPE_STATUS, p->name);
				if (!sub)
				{
					//timestamp_error();
					sprintf(g_MsgBuf, "CB:subscription registration to %s - server error", p->name);
					_kkmsg(MSG_LOG, NULL);
				}
				p->rid = CONMON_CLIENT_NULL_REQ_ID;
				return;
			}
			else {
				p = p->next_node;
			}
		}
		sprintf(g_MsgBuf, "CB:don't find the request id in node list");
		_kkmsg(MSG_LOG, NULL);
	}

	//timestamp_error();
	sprintf(g_MsgBuf, "CB:invalid subscription request id %p", rid);
	_kkmsg(MSG_LOG, NULL);
}

void dc_ConMsgPayloadCB(conmon_client_t *client, const conmon_message_head_t *head, const conmon_message_body_t *body)
{
	conmon_instance_id_t instance_id;
	const conmon_vendor_id_t *vid = conmon_message_head_get_vendor_id(head);

	if (g_rx_vendor_id && !conmon_vendor_id_equals(g_rx_vendor_id, vid))
	{
		return;
	}

	char buf[LEN_NAME];
	conmon_message_head_get_instance_id(head, &instance_id);
	sprintf(g_MsgBuf, "body_size=0x%04x", conmon_message_head_get_body_size(head));
	sprintf(g_MsgBuf, "seq=0x%04x", conmon_message_head_get_seqnum(head));
	sprintf(g_MsgBuf, "class=0x%04x", conmon_message_head_get_message_class(head));
	sprintf(g_MsgBuf, "source=%s", conmon_client_device_name_for_instance_id(client, &instance_id));
	sprintf(g_MsgBuf, "vendor=%s", dt_cmStringVendorID(vid, buf, LEN_NAME));

	//payload
	uint16_t len = conmon_message_head_get_body_size(head);
	uint8_t *data = (unsigned char *)body->data;

	sprintf(g_MsgBuf, "Receive control data(%d bytes):", len);
	for (int i = 0; i < len; i++)
	{
	sprintf(g_MsgBuf, "%s %2X", g_MsgBuf, data[i]);
	}
	_kkmsg(MSG_LOG, NULL);
}

void dc_ConnectResponseCB(conmon_client_t *client, conmon_client_request_id_t request_id, aud_error_t err)
{
	aud_errbuf_t errbuf;

	sprintf(g_MsgBuf, "connect-response for request %p (%s)", request_id, aud_error_message(err, errbuf));
	_kkmsg(MSG_LOG, NULL); //why error with MSG_BOX ?

	if (err == AUD_SUCCESS)
	{
		char buf[1024];
		ZeroMemory(buf, sizeof(buf));
		//sprintf(g_MsgBuf, "networks (%s)", dt_cmStringNetworks(conmon_client_get_networks(client), buf, sizeof(buf)));
		sprintf(g_MsgBuf, "device name (%s)", conmon_client_get_dante_device_name(client));
		sprintf(g_MsgBuf, "DNS name (%s)", conmon_client_get_dns_domain_name(client));
	}

	if (g_idConMon == request_id)
	{
		g_idConMon = CONMON_CLIENT_NULL_REQ_ID;
	}
}

//ok
void dc_MonMsgResponseCB(conmon_client_t *client, conmon_client_request_id_t rid, aud_error_t err)
{
	(void)client;
	(void)rid;

	aud_errbuf_t errbuf;
	if (err == AUD_SUCCESS)
	{
		//conmon_info_t *info = conmon_client_context (client);
		//timestamp_event();
		sprintf(g_MsgBuf, "CB:MonMsg response id=%p(%s)", rid, aud_error_message(err, errbuf));
	}
	else {
		//timestamp_error();
		sprintf(g_MsgBuf, "CB:MonMsg response id=%p(%s)", rid, aud_error_message(err, errbuf));
	}
	_kkmsg(MSG_LOG, NULL);

	if (g_idConMon == rid)
	{
		g_idConMon = CONMON_CLIENT_NULL_REQ_ID;
	}
}

aud_bool_t dc_EnableTxMeter(aud_bool_t flag)
{
	aud_bool_t res;
	g_dtErr = conmon_client_set_tx_metering_enabled(g_dtClient, &dc_MonMsgResponseCB, &g_idConMon, flag);

	if (g_dtErr == AUD_SUCCESS)
	{
		if (flag)
			sprintf(g_MsgBuf, "Enable TX metering, request id %p", g_idConMon);
		else
			sprintf(g_MsgBuf, "Disable TX metering, request id %p", g_idConMon);
		res = AUD_TRUE;
	}
	else
	{
		sprintf(g_MsgBuf, "Err- performing action: %s", aud_error_message(g_dtErr, g_dtErrBuf));
		res = AUD_FALSE;
	}
	_kkmsg(MSG_BOX, NULL);

	return res;
}

aud_bool_t dc_EnableRemoteAccess(aud_bool_t flag)
{
	aud_bool_t res;
	g_idConMon = CONMON_CLIENT_NULL_REQ_ID;

	g_dtErr = conmon_client_set_remote_control_allowed(g_dtClient, &dc_MonMsgResponseCB, &g_idConMon, flag);


	if (g_dtErr == AUD_SUCCESS)
	{
		if (flag)
			sprintf(g_MsgBuf, "Enable remote control access(No ResponseCB), request id %p", g_idConMon);
		else
			sprintf(g_MsgBuf, "Disable remote control access(No ResponseCB), request id %p", g_idConMon);
		res = AUD_TRUE;
	}
	else
	{
		sprintf(g_MsgBuf, "Err- performing action: %s", aud_error_message(g_dtErr, g_dtErrBuf));
		res = AUD_FALSE;
	}
	_kkmsg(MSG_BOX, NULL);

	return res;
}

aud_error_t dc_SendDeviceConMsg(char *device, char *msg)
{
	aud_error_t err;
	aud_errbuf_t errbuf;
	conmon_message_body_t body;

	int n = 0;
	char *ptr = strtok(msg, " ");
	while (ptr != NULL)
	{
		body.data[n++] = strtol(ptr, NULL, 16);
		ptr = strtok(NULL, " ");
	}

	uint32_t num_conmon = conmon_client_requests_pending(g_dtClient);
	uint32_t limit_conmon = conmon_client_request_limit(g_dtClient);

	//control message to send an ACC command for tx or local device
	err = conmon_client_send_control_message(
		g_dtClient,
		NULL,//&dc_MonMsgResponseCB,
		NULL,//&g_idConMon,
		device,
		CONMON_MESSAGE_CLASS_VENDOR_SPECIFIC,
		&g_tx_vendor_id,
		&body,
		n,
		NULL);

	if (err == AUD_SUCCESS)
	{
		sprintf(g_MsgBuf, "send %d bytes to %s device on CONTROL channel", n, device ? device : "Local");
	}
	else
	{
		sprintf(g_MsgBuf, "ERR- send control message (%s)", aud_error_message(err, errbuf));
	}
	_kkmsg(MSG_BOX, NULL);
	/*
	DWORD waiting_response_time = GetTickCount();
	while (g_idConMon != CONMON_CLIENT_NULL_REQ_ID)
	{
		conmon_example_sleep(&ONE_MS); // just to yield...
		conmon_client_process(g_dtClient);
	
		if ((GetTickCount() - waiting_response_time) > 500) {
			conmon_client_request_cancel(g_dtClient, g_idConMon);
			g_idConMon = CONMON_CLIENT_NULL_REQ_ID; //BugDante: the "g_idConMon" set to null if the response time waits too long.
		}
	}
	*/
	g_tx_done = TRUE;
	return err;
}

aud_error_t dc_SendMonitoringMsg(conmon_channel_type_t type, char *msg)
{
	aud_error_t err;
	aud_errbuf_t errbuf;

	//pack data into a packet
	conmon_message_body_t body;
	uint16_t body_size;
	char *ptr;
	char cmd[LEN_MSG];

	body_size = 0;
	while (1)
	{
		strcpy(cmd, msg);//copy remaining data to cmd
		ptr = strchr(cmd, ' ');//get first cmd
		if (ptr)
		{
			*ptr = NULL;
			msg = ++ptr;//move to next cmd
			body.data[body_size] = strtol(cmd, NULL, 16);
			body_size++;
		}
		else
		{
			body.data[body_size] = strtol(cmd, NULL, 16);
			body_size++;
			break;
		}
	}

	// create a message and send it
	err = conmon_client_send_monitoring_message(g_dtClient,
		&dc_MonMsgResponseCB,
		&g_idConMon,
		type,
		CONMON_MESSAGE_CLASS_VENDOR_SPECIFIC,
		&g_tx_vendor_id,
		&body,
		body_size);

	if (err == AUD_SUCCESS)
	{
		sprintf(g_MsgBuf, "Send %d byte(s) to %s channel, request id %p", body_size, typeString(type), g_idConMon);
	}
	else
	{
		sprintf(g_MsgBuf, "ERR- send a %s message (%s)", typeString(type), aud_error_message(err, errbuf));
	}
	_kkmsg(MSG_BOX, NULL);

	while (g_idConMon != CONMON_CLIENT_NULL_REQ_ID)
	{
		conmon_example_sleep(&ONE_MS); // just to yield...
		conmon_client_process(g_dtClient);
	}
	return err;
}

aud_error_t dc_RegisterControlMsg(aud_bool_t reg)
{
	aud_error_t err;
	aud_errbuf_t errbuf;

	if (reg)
	{
		sprintf(g_MsgBuf, "%s", "Register");
		err = conmon_client_register_control_messages(
			g_dtClient,
			&dc_MonMsgResponseCB,
			&g_idConMon,
			dc_ConMsgPayloadCB);
	}
	else
	{
		sprintf(g_MsgBuf, "%s", "Deregister");
		err = conmon_client_register_control_messages(
			g_dtClient,
			&dc_MonMsgResponseCB,
			&g_idConMon,
			NULL);
	}

	if (err != AUD_SUCCESS)
	{
		sprintf(g_MsgBuf, "ERR- %s control message(%s)", g_MsgBuf, aud_error_message(err, errbuf));
		_kkmsg(MSG_LOG, NULL);
	}
	return err;
}

aud_error_t dc_UnSubscribeStatusMsg(aud_bool_t all, char *device)
{
	aud_error_t err;
	aud_errbuf_t errbuf;

	if (all)
	{
		err = conmon_client_unsubscribe_global(
			g_dtClient,
			&dc_SubStatusAllCB,
			&g_idSubStatus,
			CONMON_CHANNEL_TYPE_STATUS);

		if (err != AUD_SUCCESS)
		{
			sprintf(g_MsgBuf, "ERR-unsubscrib STATUS message from ALL devices(%s)",aud_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
		}
		return err;
	}

	//subscribe for specific device
	err = conmon_client_unsubscribe(
		g_dtClient,
		&dc_SubStatusOneCB,
		&g_idSubStatus,
		CONMON_CHANNEL_TYPE_STATUS,
		device);

	if (err != AUD_SUCCESS)
	{
		sprintf(g_MsgBuf, "ERR-unsubscrib STATUS message for %s(%s)", device, aud_error_message(err, errbuf));
		_kkmsg(MSG_LOG, NULL);
	}

	return err;
}

aud_error_t dc_SubscribeStatusMsg(aud_bool_t all, char *device)
{
	aud_error_t err;
	aud_errbuf_t errbuf;

	if (all)
	{
		err = conmon_client_subscribe_global(
			g_dtClient,
			&dc_SubStatusAllCB,
			&g_idSubStatus,
			CONMON_CHANNEL_TYPE_STATUS);

		if (err == AUD_SUCCESS)
		{
			sprintf(g_MsgBuf, "Subscrib STATUS message from ALL devices id=%p(success)", g_idSubStatus);
			_kkmsg(MSG_LOG, NULL);
		}
		return err;
	}

	//subscribe for specific device
	err = conmon_client_subscribe(
		g_dtClient,
		&dc_SubStatusOneCB,
		&g_idSubStatus,
		CONMON_CHANNEL_TYPE_STATUS,
		device);

	if (err == AUD_SUCCESS)
	{
		//store g_idConMon in node list
		PDANTENODE p;
		p = dt_FindNode(device);
		p->rid = g_idSubStatus;
	}
	else {
		sprintf(g_MsgBuf, "ERR-subscrib STATUS message for %s(%s)", device, aud_error_message(err, errbuf));
		_kkmsg(MSG_LOG, NULL);
	}
	return err;
}

aud_error_t dc_RegisterIncomingCHmsg(conmon_channel_type_t type, aud_bool_t reg)
{
	aud_error_t err;

	if (reg)
	{
		sprintf(g_MsgBuf, "%s", "Register");
		err = conmon_client_register_monitoring_messages(
			g_dtClient,
			&dc_MonMsgResponseCB,
			NULL,//&g_idConMon,
			type,
			CONMON_CHANNEL_DIRECTION_RX,
			&dc_MonMsgPayloadCB);
	}
	else
	{
		sprintf(g_MsgBuf, "%s", "Deregister");
		err = conmon_client_register_monitoring_messages(
			g_dtClient,
			&dc_MonMsgResponseCB,
			NULL,
			type,
			CONMON_CHANNEL_DIRECTION_RX,
			NULL);
	}

	if (err == AUD_SUCCESS)
	{
		sprintf(g_MsgBuf, "%s rx-message %s(success)", g_MsgBuf, typeString(type));
		_kkmsg(MSG_LOG, NULL);
	}

	return err;
}

aud_error_t dc_RegisterOutgoingCHmsg(conmon_channel_type_t type, aud_bool_t reg)
{
	aud_error_t err;

	if (reg)
	{
		sprintf(g_MsgBuf, "%s", "Register");
		err = conmon_client_register_monitoring_messages(
			g_dtClient,
			&dc_MonMsgResponseCB,
			NULL,//&g_idConMon,
			type,
			CONMON_CHANNEL_DIRECTION_TX,
			&dc_MonMsgPayloadCB);
	}
	else
	{
		sprintf(g_MsgBuf, "%s", "Deregister");
		err = conmon_client_register_monitoring_messages(
			g_dtClient,
			&dc_MonMsgResponseCB,
			NULL,
			type,
			CONMON_CHANNEL_DIRECTION_TX,
			NULL);
	}

	if (err == AUD_SUCCESS)
	{
		sprintf(g_MsgBuf, "%s tx-message %s(success)", g_MsgBuf, typeString(type));
		_kkmsg(MSG_LOG, NULL);
	}
	return err;
}
