/*****************************************************************************
| Copyright (c) Keiga Inc. All Rights Reserved.
|
| Project : amor
| FileName: dante.h
| Module  : No
|
| Author  : Dennis Lu
| Revise  :
| Version : V1.7.7.10
| Comment : Linked-list structure for Audio Node & Channels as below:
|           Head --> Node --> Node --> Node --> NULL
|                    ||||     ||||     ||||
|                  Channels Channels Channels
\*****************************************************************************/

#ifndef _DANTE_H_
#define _DANTE_H_

#ifdef  __cplusplus
extern  "C" {
#endif

//#include "stdafx.h"
#include "dante_api.h"
#include "kk.h"

	//subscriptions in file
#define SUB_INIT					1
#define SUB_LOAD					2
#define SUB_SAVE					3
#define SUB_DELETE					4

#define MAX_RESOLVES				16
#define MAX_SOCKETS					(MAX_RESOLVES+2) // 2 for device and channel browsing
#define MAX_DANTE_NODES				64
#define MAX_NODE_CHANNELS			16 // both Tx and Rx
#define MAX_INTERFACES				2
#define MAX_REQUESTS				128

#define LEN_REQUEST_DESCRIPTION		64

	static const aud_utime_t ONE_MS = { 0, 1000 };

	//-----------------------------------------------------------------------------
	//typedef

	typedef void dt_ReceiveMsg_fn(char *devicename, char *msg, int len, int chtype);

	typedef struct tagNodeChannel
	{
		dante_id_t id;
		const char *name;
		dante_name_t label;
		struct tagNodeChannel *next_ch;
	}NODECHANNEL, *PNODECHANNEL;

	typedef struct tagDeviceInfo
	{
		db_browse_types_t types;			//browsing types
		const char *dname;					//default name
		const dante_id64_t *vid;			//vendor id
		const dante_id64_t *mfid;			//manufacturer id
		const dante_id64_t *model;			//model id
		char mf[DANTE_NAME_LENGTH + 1];		//manufacturer
		char mfname[DANTE_NAME_LENGTH + 1];		//manufacturer name
		char modelname[DANTE_NAME_LENGTH + 1];	//model name
		dante_version_t model_ver;			//model version
											//version = xx_ver + xx_verb
		dante_version_t sw_ver;				//software verson
		dante_version_t fw_ver;				//firmware version
		dante_version_build_t sw_verb;		//software build version  
		dante_version_build_t fw_verb;		//firmware build version
		dante_samplerate_t rate;			//sampling rate
		dante_encoding_t encoding;			//pcm encoding format
		conmon_networks_t networks;			//device networks
	}DEVICEINFO, *PDEVICEINFO;

	typedef struct tagDanteNode
	{
		bool active;					//device stop(0) start(1)
		const char *name;				//device name
		db_browse_t *browse;			//device browsing
		dr_device_t *device;			//device handle as the node
		db_browse_device_t *db_device;	//?
		DEVICEINFO devInfo;				//device Information
		conmon_client_request_id_t rid;	//conmon request id
		uint16_t ntx;					//txc number
		uint16_t nrx;					//rxc number
		dr_txchannel_t **tx;			//txc linking
		dr_rxchannel_t **rx;			//rxc linking
		PNODECHANNEL ptx;				//txc head
		PNODECHANNEL prx;				//rxc head
		struct tagDanteNode *next_node;	//point to next node
	}DANTENODE, *PDANTENODE;

	typedef struct tagDeviceRequest
	{
		dante_request_id_t id;
		char description[LEN_REQUEST_DESCRIPTION];
	}DEVICEREQUEST, *PDEVICEREQUEST;

	typedef struct tagDanteDevice
	{
		uint16_t nintf;
		uint16_t max_txflow_slots;
		uint16_t max_rxflow_slots;
		uint16_t txlabels_buflen;
		dr_txlabel_t *txlabels_buf;
	};

	typedef struct
	{
		conmon_client_request_id_t id;
		aud_bool_t reg;
		aud_bool_t dir;
	} register_msg_t;

	typedef struct
	{
		uint16_t id;
		const char * name;
		aud_bool_t reg;
	} name_map_t;


	//?
	extern aud_error_t g_dtErr;
	extern aud_errbuf_t g_dtErrBuf;

	//-----------------------------------------------------------------------------
	//extern.browse
	extern dapi_t *g_dtAPI;
	extern aud_env_t *g_dtENV;
	extern dante_runtime_t *g_dtRT;
	extern dante_domain_handler_t *g_dtHDR;
	extern db_browse_t *g_dtDB;
	extern dr_devices_t *g_dtDR;
	extern db_browse_config_t g_dbConfig;
	extern conmon_client_t *g_dtClient;
	extern DANTENODE *g_dtHead;

	extern DEVICEREQUEST g_dtRequests[MAX_REQUESTS];
	extern conmon_vendor_id_t g_tx_vendor_id;
	extern conmon_vendor_id_t *g_rx_vendor_id;
	extern conmon_client_request_id_t g_idSubStatus, g_idConMon;
	extern name_map_t CHANNEL_NAMES[];
	extern register_msg_t g_dtCHmsg[CONMON_NUM_CHANNELS];

	//dante
	void dt_InitDAPI();
	void dt_SetDBConfig(int ifn1, int ifn2);
	void dt_GetDBConfig(int *pifn1, int *pifn2);
	void dt_StartDAPI();
	void dt_InitBrowsing();
	void dt_InitRouting();
	void dt_InitConMonClient();
	void dt_StopDAPI();
	void dt_SetReceiveMsgCB(dt_ReceiveMsg_fn *fn);
	PDANTENODE dt_FindNode(const char *name);
	PDEVICEINFO dt_GetDeviceInfo(const char *name);
	DWORD WINAPI dt_RuntimeSockets(LPVOID lpa);

	//callback
	void db_NodeChangedCB(db_browse_t *browse, const db_node_t *node, db_node_change_t node_change);
	void db_NetworkChangedCB(const db_browse_t *browse);
	void db_SocketsChangedCB(const db_browse_t * browse);
	void db_EventCB(db_browse_event_t *event);
	void dr_DeviceChangedCB(dr_device_t *device, dr_device_change_flags_t change_flags);
	void dr_RequestResponseCB(dr_device_t *device, dante_request_id_t id, aud_error_t result);

	//device
	void db_NodeAdded(const db_browse_device_t *device);
	void db_NodeModified(const db_browse_device_t *device);
	void db_NodeRemoved(const db_browse_device_t *device);
	void dr_DeviceStateChanged(dr_device_t *device);
	void dr_DeviceAddressChanged(dr_device_t *device);
	void dr_ReleaseRequest(PDEVICEREQUEST request);
	void dr_ReleaseRequestDescription(const char * description);
	bool dr_UpdateComponents(dr_device_t *device);
	void dr_UpdateCapability(dr_device_t *device);
	void dr_UpdateProperty(dr_device_t *device);
	void dr_UpdateSubscribe(dr_device_t *device);
	void dr_UpdateActive(dr_device_t *device);
	PDEVICEREQUEST dr_AllocateRequest(const char *description);

	//label
	void db_TxLabelAdded(const db_browse_label_t *txlabel);
	void db_TxLabelModified(const db_browse_label_t *txlabel);
	void db_TxLabelRemoved(const db_browse_label_t *txlabel);
	void dr_UpdateTxLabel(dr_device_t *device);

	//channel
	void db_AddTxChannel(const db_browse_channel_t *txch);
	void db_ModifyTxChannel(const db_browse_channel_t *txch);
	void db_RemoveTxChannel(const db_browse_channel_t *txch);
	void dr_UpdateRxChannel(dr_device_t *device);
	void dr_UpdateTxChannel(dr_device_t *device);
	void dr_SubscribeChannel(const char *rx, const char *rxch, const char *tx, const char *txch);

	//flow
	void dr_UpdateRxFlow(dr_device_t *device);
	void dr_UpdateTxFlow(dr_device_t *device);

	//client
	void dc_SetupLocalRegistration();
	void dc_SetupRemoteRegistration();
	void dc_SetupRemoteSubscribeAll();
	void dc_NotifyEventCB(const conmon_client_event_t *ev);

	void dc_ClientInfo();
	void dc_StateChangedCB(conmon_client_t *client);
	void dc_NetworkChangedCB(conmon_client_t *client);
	void dc_SocketChangedCB(conmon_client_t *client);
	void dc_DeviceNameChangedCB(conmon_client_t *client);
	void dc_DomainNameChangedCB(conmon_client_t *client);
	void dc_SubscriptionChangedCB(conmon_client_t *client, unsigned int num_changes, const conmon_client_subscription_t *const *changes);
	void dc_ConnectResponseCB(conmon_client_t *client, conmon_client_request_id_t request_id, aud_error_t result);

	//client.register msg
	aud_error_t dc_RegisterOutgoingCHmsg(conmon_channel_type_t type, aud_bool_t reg);
	aud_error_t dc_RegisterIncomingCHmsg(conmon_channel_type_t type, aud_bool_t reg);
	aud_error_t dc_RegisterControlMsg(aud_bool_t reg);

	//client.subscribe
	aud_error_t dc_SubscribeStatusMsg(aud_bool_t all, char *device);
	aud_error_t dc_UnSubscribeStatusMsg(aud_bool_t all, char *device);

	void dc_Subscription(const conmon_client_subscription_t *sub);
	void dc_ShowAllSubscriptions();

	//client.response callback
	void dc_MonMsgResponseCB(conmon_client_t *client, conmon_client_request_id_t rid, aud_error_t err);
	void dc_SubStatusAllCB(conmon_client_t *client, conmon_client_request_id_t rid, aud_error_t err);
	void dc_SubStatusOneCB(conmon_client_t *client, conmon_client_request_id_t rid, aud_error_t err);

	//client.send msg
	aud_error_t dc_SendDeviceConMsg(char *device, char *msg);
	aud_error_t dc_SendMonitoringMsg(conmon_channel_type_t type, char *msg);

	//client.payload callback
	void dc_MonMsgPayloadCB(conmon_client_t *client, conmon_channel_type_t type, conmon_channel_direction_t dir,
		const conmon_message_head_t *head, const conmon_message_body_t *body);
	void dc_ConMsgPayloadCB(conmon_client_t *client, const conmon_message_head_t *head, const conmon_message_body_t *body);




	//client.other
	aud_bool_t dc_EnableTxMeter(aud_bool_t flag);
	aud_bool_t dc_EnableRemoteAccess(aud_bool_t flag);







	//???
	void dt_SocketChanged(db_browse_t *browse, dante_sockets_t *sockets);
	const char *dt_GetRxSubscription(const char *device, const char *rxch);

	/*
	void dt_LBtxLabels(HWND hdlg, int id, char *device);
	aud_bool_t dt_LBtxIsWorking(HWND hdlg, int id, char *device);
	void db_msgAES67SAPNode(const db_browse_aes67_t *device);
	*/


	AUD_INLINE const char *typeString(conmon_channel_type_t type)
	{
		return CHANNEL_NAMES[type].name;
	}

	AUD_INLINE conmon_channel_type_t channel_type_from_string(const char * name)
	{
		conmon_channel_type_t i;
		for (i = 0; i < CONMON_NUM_CHANNELS; i++)
		{
			if (!STRCASECMP(name, CHANNEL_NAMES[i].name))
			{
				return CHANNEL_NAMES[i].id;
			}
		}
		return CONMON_CHANNEL_TYPE_NONE;
	}

	AUD_INLINE const char *dirString(conmon_channel_direction_t dir)
	{
		return dir == CONMON_CHANNEL_DIRECTION_TX ? "TX" : "RX"; 

	}

	AUD_INLINE aud_error_t conmon_example_sleep(const aud_utime_t * at)
	{
		if (at == NULL || at->tv_sec < 0 || at->tv_usec < 0)
		{
			return AUD_ERR_INVALIDPARAMETER;
		}
		else
		{
			DWORD result;
			int ms = (at->tv_sec * 1000) + ((at->tv_usec + 999) / 1000);
			result = SleepEx(ms, TRUE);
			if (result)
			{
				return aud_error_from_system_error(result);
			}
		}
		return AUD_SUCCESS;
	}

	//-----------------------------------------------------------------------------
	// ConMon client
	//
	char *dt_cmStringState(conmon_client_state_t state);
	char *dt_cmStringRXstatus(conmon_rxstatus_t rxstatus);
	char *dt_cmStringInstanceID(const conmon_instance_id_t *id, char *buf, size_t len);
	char *dt_cmStringNetworks(const conmon_networks_t *networks, char *buf, size_t len);
	char *dt_cmStringSubscriptions(uint16_t num_subscriptions, const conmon_client_subscription_t *const *subscriptions, char *buf, size_t len);
	char *dt_cmStringCHtype(conmon_channel_type_t channel_type);
	char *dt_cmStringDeviceID(const conmon_device_id_t *id, char *buf, size_t len);
	char *dt_cmStringVendorID(const conmon_vendor_id_t *id, char *buf, size_t len);
	char *dt_cmStringModelID(const conmon_audinate_model_id_t *id, char *buf, size_t len);
	char *dt_cmStringEPaddresses(const conmon_endpoint_addresses_t *addresses, char *buf, size_t len);
	char *dt_cmStringSubscriptionInfo(uint16_t num_subscriptions, const conmon_subscription_info_t **subscriptions, char *buf, size_t len);
	char *dt_cmStringMeteringPeaks(const conmon_metering_message_peak_t *peaks, uint16_t num_peaks, char *buf, size_t len);
	aud_error_t dt_cmClientProcess(conmon_client_t * client, dante_sockets_t * sockets, const aud_utime_t * timeout, aud_utime_t * next_action_timeout);




#ifdef  __cplusplus
}
#endif
#endif // _DANTE_H_