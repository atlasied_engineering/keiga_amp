#include "stdafx.h"
#include "dt_extend_api.h"
//show message 
#ifndef PRINT_MSG
#define PRINT_MSG(b, bu, ...  ) \
do{								\
sprintf(bu, __VA_ARGS__);		\
_kkmsg(b, bu); }				\
while (0)


//sprintf(g_MsgBuf, "ERR- invalid RX channel (must be in range 1..%u)", pnode->nrx);
//_kkmsg(MSG_LOG, NULL);

#endif // !PRINT_MSG
static uint32_t parse_address(const char* msg);
static uint32_t parse_address(const char* msg)
{
	uint32_t addr;
	int v1, v2, v3, v4;

	if (sscanf(msg, "%d.%d.%d.%d", &v1, &v2, &v3, &v4) < 4)
	{
		return 0;
	}
	if (v1 < 0 || v1 > 255 || v2 < 0 || v2 > 255 || v3 < 0 || v3 > 255 || v4 < 0 || v4 > 255)
	{
		return 0;
	}
	addr = v1 << 24 | v2 << 16 | v3 << 8 | v4;
	return addr;
}



//------------------------Dante Extend API--------------------------------------------
PNETINTERFACE db_GetNetworkInterface() {
	PNETINTERFACE ret = NULL;

	//function: GetAdaptersInfo 
	//refer: https://docs.microsoft.com/en-us/windows/win32/api/iphlpapi/nf-iphlpapi-getadaptersinfo

	/* Declare and initialize variables */

	// It is possible for an adapter to have multiple
	// IPv4 addresses, gateways, and secondary WINS servers
	// assigned to the adapter. 
	//
	// Note that this sample code only prints out the 
	// first entry for the IP address/mask, and gateway, and
	// the primary and secondary WINS server for each adapter. 

	PIP_ADAPTER_INFO pAdapterInfo;
	PIP_ADAPTER_INFO pAdapter = NULL;
	DWORD dwRetVal = 0;
	UINT i;

	ULONG ulOutBufLen = sizeof(IP_ADAPTER_INFO);
	pAdapterInfo = (IP_ADAPTER_INFO*)malloc(sizeof(IP_ADAPTER_INFO));
	if (pAdapterInfo == NULL) {
		//printf("Error allocating memory needed to call GetAdaptersinfo\n");
		return NULL;
	}

	// Make an initial call to GetAdaptersInfo to get
	// the necessary size into the ulOutBufLen variable
	if (GetAdaptersInfo(pAdapterInfo, &ulOutBufLen) == ERROR_BUFFER_OVERFLOW) {
		free(pAdapterInfo);
		pAdapterInfo = (IP_ADAPTER_INFO*)malloc(ulOutBufLen);
		if (pAdapterInfo == NULL) {
			//printf("Error allocating memory needed to call GetAdaptersinfo\n");
			return NULL;
		}
	}

	if ((dwRetVal = GetAdaptersInfo(pAdapterInfo, &ulOutBufLen)) == NO_ERROR) {
		pAdapter = pAdapterInfo;
		while (pAdapter) {
			//create node
			PNETINTERFACE netInterface = (NETINTERFACE*)malloc(sizeof(NETINTERFACE));
			if (!netInterface) {
				//free node
				if (ret) {
					PNETINTERFACE tmp;
					while (ret) {
						tmp = ret->nextNet;
						free(ret);
						ret = tmp;
					}
				}
				return NULL;
			}
			netInterface->nextNet = NULL;

			//set value
			netInterface->idxIf = pAdapter->ComboIndex;
			netInterface->type = pAdapter->Type;
			//memcpy(netInterface->name, pAdapter->AdapterName,
			//	strlen(pAdapter->AdapterName) + 1);
			memcpy(netInterface->desc, pAdapter->Description,
				strlen(pAdapter->Description) + 1);
			memcpy(netInterface->ip, pAdapter->IpAddressList.IpAddress.String,
				strlen(pAdapter->IpAddressList.IpAddress.String) + 1);

			sprintf(netInterface->mac, "");
			for (i = 0; i < pAdapter->AddressLength; i++) {
				if (i == (pAdapter->AddressLength - 1))
					sprintf(netInterface->mac, "%s%.2X", netInterface->mac, (int)pAdapter->Address[i]);
				else
					sprintf(netInterface->mac, "%s%.2X:", netInterface->mac, (int)pAdapter->Address[i]);
			}


			//get alias name
			//refer index : https://docs.microsoft.com/zh-tw/windows/win32/iphlp/what-s-new-in-ip-helper
			//refer index to luid : https://docs.microsoft.com/en-us/windows/win32/api/netioapi/nf-netioapi-convertinterfaceindextoluid
			//refer luid to alias : https://docs.microsoft.com/en-us/windows/win32/api/netioapi/nf-netioapi-convertinterfaceluidtoalias
			NET_LUID pnet_luind;
			int err = ConvertInterfaceIndexToLuid(netInterface->idxIf, &pnet_luind);
			if (err != NO_ERROR) {
				switch (err) {
				case ERROR_FILE_NOT_FOUND: {
					break;
				}
				case ERROR_INVALID_PARAMETER: {
					break;
				}
				}

			}
			//			wchar_t wstr[255];
			//			mbstowcs(wstr, netInterface->name, strlen(netInterface->name));
			ConvertInterfaceLuidToAlias(&pnet_luind, netInterface->name, 255);
			//ConvertInterfaceLuidToAlias(&pnet_luind, wstr, 255);

			pAdapter = pAdapter->Next;
			//add node
			if (!ret)
				ret = netInterface;
			else {
				PNETINTERFACE p = ret;
				while (p->nextNet) {
					p = p->nextNet;
				}
				p->nextNet = netInterface;
			}

		}
	}
	if (pAdapterInfo)
		free(pAdapterInfo);



	//const int BUFFSIZE = 128;
	//char	psBuffer[BUFFSIZE];
	//FILE	*pPipe;
	//// open pipe line
	//if ((pPipe = _popen("netstat -ano", "rt")) == NULL)
	//	return ret;
	//
	//while (fgets(psBuffer, BUFFSIZE, pPipe))
	//{
	//	int i = 5;
	//	psBuffer;
	//	i = 6;
	//}
	//// close pipe line
	//_pclose(pPipe);







	return ret;
}


void dt_InitConMonClient_Serial(conmon_client_config_t* client, aud_bool_t enable) {
	conmon_client_config_set_serial_channel_enabled(client, enable);
}

void dc_SetupLocalRegistration_Extend() {	//serial
	aud_error_t err;
	aud_errbuf_t errbuf;

	err = dc_RegisterOutgoingCHmsg(CONMON_CHANNEL_TYPE_SERIAL, true);
	if (err != AUD_SUCCESS)
	{
		sprintf(g_MsgBuf, "ERR:register SERIAL outgoing message(%s)", aud_error_message(err, errbuf));
		_kkmsg(MSG_LOG, NULL);
		return;
	}


}
void dc_SetupRemoteRegistration_Extend() {	//serial
	aud_error_t err;
	aud_errbuf_t errbuf;

	err = dc_RegisterIncomingCHmsg(CONMON_CHANNEL_TYPE_SERIAL, true);
	if (err != AUD_SUCCESS)
	{
		sprintf(g_MsgBuf, "ERR:register SERIAL incoming message(%s)", aud_error_message(err, errbuf));
		_kkmsg(MSG_LOG, NULL);
		return;
	}

	err = dc_RegisterIncomingCHmsg(CONMON_CHANNEL_TYPE_VENDOR_BROADCAST, true);
	if (err != AUD_SUCCESS)
	{
		sprintf(g_MsgBuf, "ERR:register VENDOR_BROADCAST incoming message(%s)", aud_error_message(err, errbuf));
		_kkmsg(MSG_LOG, NULL);
		return;
	}
}


aud_error_t dc_SendDeviceConMsg_Manufacturer(char* manufacturer, char* device, char* msg)
{
	aud_error_t err;
	aud_errbuf_t errbuf;
	conmon_message_body_t body;

	int n = 0;
	char* ptr = strtok(msg, " ");
	while (ptr != NULL)
	{
		body.data[n++] = strtol(ptr, NULL, 16);
		ptr = strtok(NULL, " ");
	}

	uint32_t num_conmon = conmon_client_requests_pending(g_dtClient);
	uint32_t limit_conmon = conmon_client_request_limit(g_dtClient);

	//control message to send an ACC command for tx or local device
	err = conmon_client_send_control_message(
		g_dtClient,
		NULL,//&dc_MonMsgResponseCB,
		NULL,//&g_idConMon,
		device,
		CONMON_MESSAGE_CLASS_VENDOR_SPECIFIC,
		(conmon_vendor_id_t*)manufacturer,
		&body,
		n,
		NULL);

	if (err == AUD_SUCCESS)
	{
		sprintf(g_MsgBuf, "send %d bytes to %s device on CONTROL channel", n, device ? device : "Local");
	}
	else
	{
		sprintf(g_MsgBuf, "ERR- send control message (%s)", aud_error_message(err, errbuf));
	}
	_kkmsg(MSG_BOX, NULL);
	//DWORD waiting_response_time = GetTickCount();
	while (g_idConMon != CONMON_CLIENT_NULL_REQ_ID)
	{
		conmon_example_sleep(&ONE_MS); // just to yield...
		conmon_client_process(g_dtClient);
		/*
		if ((GetTickCount() - waiting_response_time) > 500) {
			conmon_client_request_cancel(g_dtClient, g_idConMon);
			g_idConMon = CONMON_CLIENT_NULL_REQ_ID; //BugDante: the "g_idConMon" is set to null if the response time waits too long.
		}
		*/
		g_idConMon = CONMON_CLIENT_NULL_REQ_ID;
	}
	/*
	DWORD waiting_response_time = GetTickCount();
	while (g_idConMon != CONMON_CLIENT_NULL_REQ_ID)
	{
	conmon_example_sleep(&ONE_MS); // just to yield...
	conmon_client_process(g_dtClient);

	if ((GetTickCount() - waiting_response_time) > 500) {
	conmon_client_request_cancel(g_dtClient, g_idConMon);
	g_idConMon = CONMON_CLIENT_NULL_REQ_ID; //BugDante: the "g_idConMon" set to null if the response time waits too long.
	}
	}
	*/
	g_tx_done = TRUE;
	return err;
}


void kk_dtSendOutConMsg_Audinate(char* device, char* msg) {
	aud_error_t err;

	if (!strlen(msg))
	{
		MessageBox(NULL, "No message data!", "Error", MB_OK);
		return;
	}

	if (device && !STRCASECMP(device, "None"))
	{
		MessageBox(NULL, "please connect to gateway for sending data!", "Error", MB_OK);
		return;
	}

	err = dc_SendDeviceConMsg_Audinate(device, msg);
}


aud_error_t
dapi_utils_step(dante_runtime_t* runtime, aud_socket_t in_sock, dante_sockets_t* out_sockets)
{
	dante_sockets_t step_sockets;
	if (out_sockets == NULL)
	{
		out_sockets = &step_sockets;
	}
	dante_sockets_clear(out_sockets);
	aud_utime_t my_timeout = { 1, 0 };

	aud_error_t result = dante_runtime_get_sockets_and_timeout(runtime, out_sockets, &my_timeout);
	if (result != AUD_SUCCESS)
	{
		return result;
	}
	if (in_sock != AUD_SOCKET_INVALID)
	{
		// Add input stream to our list of sockets
		dante_sockets_add_read(out_sockets, in_sock);
	}
	if (my_timeout.tv_sec >= 1)
	{
		my_timeout.tv_sec = 1;
		my_timeout.tv_usec = 0;
	}
	else if (my_timeout.tv_sec == 0 && my_timeout.tv_usec == 0)
	{
		//fprintf(stderr, "Timeout zero\n");
	}

	if (out_sockets->n == 0)
	{
#ifdef WIN32
		DWORD result;
		int ms = (my_timeout.tv_sec * 1000) + ((my_timeout.tv_usec + 999) / 1000);
		result = SleepEx(ms, TRUE);
		if (result)
		{
			return aud_error_from_system_error(result);
		}
#else
		if ((my_timeout.tv_sec > 0) && sleep(my_timeout.tv_sec) > 0)
		{
			return AUD_ERR_INTERRUPTED;
		}
		if ((my_timeout.tv_usec > 0) && (usleep(my_timeout.tv_usec) < 0))
		{
			return aud_error_get_last();
		}
#endif
	}
	else
	{
		int select_result = select(out_sockets->n, &out_sockets->read_fds, &out_sockets->write_fds, NULL, &my_timeout);
		if (select_result < 0)
		{
			result = aud_error_get_last();
			printf("Error processing sockets: %s (%d)\n"
				, aud_error_get_name(result)
				, result
			);
			return result;
		}
	}
	return dante_runtime_process_with_sockets(runtime, out_sockets);
}

aud_error_t dc_SendDeviceConMsg_Audinate(char* device, char* msg) {
	aud_error_t err;
	aud_errbuf_t errbuf;
	conmon_message_body_t body;
	aud_utime_t control_timeout = { 5, 0 };

	int n = 0;
	char* ptr = strtok(msg, " ");
	while (ptr != NULL)
	{
		body.data[n++] = strtol(ptr, NULL, 16);
		ptr = strtok(NULL, " ");
	}

	uint32_t num_request = dr_devices_num_requests_pending(g_dtDR);
	uint32_t max_request = dr_devices_get_request_limit(g_dtDR);

	if (num_request >= max_request) {
		sprintf(g_MsgBuf, "pending: %d  over  limit: %d", num_request, max_request);
		_kkmsg(MSG_BOX, NULL);
	}

	if (!g_dtClient) {
		err = AUD_ERR_INVALIDDATA;
		return err;
	}
	//control message to send an ACC command for tx or local device
	err = conmon_client_send_control_message(
		g_dtClient,
		&dc_MonMsgResponseCB,
		&g_idConMon,
		device,
		CONMON_MESSAGE_CLASS_VENDOR_SPECIFIC,
		CONMON_VENDOR_ID_AUDINATE,
		&body,
		n,
		&control_timeout);

	if (err == AUD_SUCCESS)
	{
		//	err = wait_for_response(g_dtRT, &g_comms_timeout);
		sprintf(g_MsgBuf, "send %d bytes to %s device on CONTROL channel", n, device ? device : "Local");
	}
	else
	{
		sprintf(g_MsgBuf, "ERR- send control message (%s)", aud_error_message(err, errbuf));
	}
	_kkmsg(MSG_BOX, NULL);

	//DWORD waiting_response_time = GetTickCount();
	while (g_idConMon != CONMON_CLIENT_NULL_REQ_ID)
	{
		conmon_example_sleep(&ONE_MS); // just to yield...
		conmon_client_process(g_dtClient);
		/*
		if ((GetTickCount() - waiting_response_time) > 500) {
			conmon_client_request_cancel(g_dtClient, g_idConMon);
			g_idConMon = CONMON_CLIENT_NULL_REQ_ID; //BugDante: the "g_idConMon" is set to null if the response time waits too long.
		}
		*/
		g_idConMon = CONMON_CLIENT_NULL_REQ_ID;
	}

	return err;
}

void dc_SysReset(char* device) {
	conmon_message_body_t body;
	uint16_t body_size = 0;
	char txMSG[256];
	uint16_t param = CONMON_AUDINATE_SYS_RESET_SOFT; // or CONMON_AUDINATE_SYS_RESET_FACTORY

	PDANTENODE pnode;

	//check if the device has exist
	pnode = dt_FindNode(device);
	if (!pnode) return;

	//set system reset
	conmon_audinate_init_sys_reset_control(&body, 1000000); // 1sec = 1000000us
	conmon_audinate_sys_reset_control_set_mode(&body, param);

	body_size = conmon_audinate_sys_reset_control_get_size(&body);

	//change hex to ascii
	memset(txMSG, 0, sizeof(txMSG));
	for (int i = 0; i < body_size; i++)
		sprintf(txMSG, "%s %02x", txMSG, body.data[i]);

	//Send message vondor is Audinate
	kk_dtSendOutConMsg_Audinate(device, txMSG);

	return;










	return;
}

//Query
void dc_QueryEnc(char* device) {
	conmon_message_body_t body;
	uint16_t body_size = 0;
	char txMSG[256];

	PDANTENODE pnode;

	//check if the device has exist
	pnode = dt_FindNode(device);
	if (!pnode) return;

	//set query message : encodeing 
	conmon_audinate_init_enc_control(&body, 0);
	body_size = conmon_audinate_enc_control_get_size(&body);

	//change hex to ascii
	memset(txMSG, 0, sizeof(txMSG));
	for (int i = 0; i < body_size; i++)
		sprintf(txMSG, "%s %02x", txMSG, body.data[i]);

	//Send message vondor is Audinate
	kk_dtSendOutConMsg_Audinate(device, txMSG);

	return;
}
void dc_QuerySrate(char* device) {
	conmon_message_body_t body;
	uint16_t body_size = 0;
	char txMSG[256];

	PDANTENODE pnode;

	//check if the device has exist
	pnode = dt_FindNode(device);
	if (!pnode) return;

	//set query message : sample rate 
	conmon_audinate_init_srate_control(&body, 0);
	body_size = conmon_audinate_srate_control_get_size(&body);

	//change hex to ascii
	memset(txMSG, 0, sizeof(txMSG));
	for (int i = 0; i < body_size; i++)
		sprintf(txMSG, "%s %02x", txMSG, body.data[i]);

	//Send message vondor is Audinate
	kk_dtSendOutConMsg_Audinate(device, txMSG);

	return;
}
void dc_QueryInterface(char* device) {
	conmon_message_body_t body;
	uint16_t body_size = 0;
	char txMSG[256];

	PDANTENODE pnode;

	//check if the device has exist
	pnode = dt_FindNode(device);
	if (!pnode) return;

	//set query message : interface
	conmon_audinate_init_interface_control(&body, 0);
	body_size = conmon_audinate_interface_control_get_size(&body);

	//change hex to ascii
	memset(txMSG, 0, sizeof(txMSG));
	for (int i = 0; i < body_size; i++)
		sprintf(txMSG, "%s %02x", txMSG, body.data[i]);

	//Send message vondor is Audinate
	kk_dtSendOutConMsg_Audinate(device, txMSG);

	return;
}
void dc_QueryManfVer(char* device) {
	conmon_message_body_t body;
	uint16_t body_size = 0;
	char txMSG[256];

	PDANTENODE pnode;

	//check if the device has exist
	pnode = dt_FindNode(device);
	if (!pnode) return;

	//set query message : manufacturer version
	conmon_audinate_init_query_message(&body, CONMON_AUDINATE_MESSAGE_TYPE_MANF_VERSIONS_QUERY, 1000000);// 1sec = 1000000us
	body_size = conmon_audinate_query_message_get_size(&body);

	//change hex to ascii
	memset(txMSG, 0, sizeof(txMSG));
	for (int i = 0; i < body_size; i++)
		sprintf(txMSG, "%s %02x", txMSG, body.data[i]);

	//Send message vondor is Audinate
	kk_dtSendOutConMsg_Audinate(device, txMSG);

	return;
}

//Set
void dc_SetEnc(char* device, int enc) {
	conmon_message_body_t body;
	uint16_t body_size = 0;
	char txMSG[256];

	PDANTENODE pnode;
	uint16_t txch_id;

	//check if the device has exist
	pnode = dt_FindNode(device);
	if (!pnode) return;

	//set message : encodeing 
	conmon_audinate_init_enc_control(&body, 0);
	conmon_audinate_enc_control_set_encoding(&body, enc);
	body_size = conmon_audinate_enc_control_get_size(&body);

	//change hex to ascii
	memset(txMSG, 0, sizeof(txMSG));
	for (int i = 0; i < body_size; i++)
		sprintf(txMSG, "%s %02x", txMSG, body.data[i]);

	//Send message vondor is Audinate
	kk_dtSendOutConMsg_Audinate(device, txMSG);

	return;
}
void dc_SetSrate(char* device, enum conmon_srate srate) {
	conmon_message_body_t body;
	uint16_t body_size = 0;
	char txMSG[256];

	PDANTENODE pnode;

	if (!(srate == CONMON_AUDINATE_SRATE_44K ||
		srate == CONMON_AUDINATE_SRATE_48K ||
		srate == CONMON_AUDINATE_SRATE_96K ||
		srate == CONMON_AUDINATE_SRATE_192K ||
		srate == CONMON_AUDINATE_SRATE_88K ||
		srate == CONMON_AUDINATE_SRATE_176K)
		)
		return;

	//check if the device has exist
	pnode = dt_FindNode(device);
	if (!pnode) return;

	//set query message : sample rate 
	conmon_audinate_init_srate_control(&body, 0);
	conmon_audinate_srate_control_set_rate(&body, srate);
	body_size = conmon_audinate_srate_control_get_size(&body);

	//change hex to ascii
	memset(txMSG, 0, sizeof(txMSG));
	for (int i = 0; i < body_size; i++)
		sprintf(txMSG, "%s %02x", txMSG, body.data[i]);

	//Send message vondor is Audinate
	kk_dtSendOutConMsg_Audinate(device, txMSG);

	return;
}
void dc_SetInterface_static(char* device, uint16_t netInterface, char* ip, char* netmask, char* dns, char* gateway) {

	conmon_message_body_t body;
	uint16_t body_size = 0;
	char txMSG[256];
	uint16_t index = netInterface;
	uint32_t ip_address = htonl(parse_address(ip));
	uint32_t ip_netmask = htonl(parse_address(netmask));
	uint32_t dns_server = htonl(parse_address(dns));
	uint32_t gate = htonl(parse_address(gateway));

	PDANTENODE pnode;

	//check if the device has exist
	pnode = dt_FindNode(device);
	if (!pnode) return;

	//set query message : interface
	conmon_audinate_init_interface_control(&body, 0);
	conmon_audinate_interface_control_set_interface_address_static(&body, index,
		ip_address, ip_netmask, dns_server, gate);
	body_size = conmon_audinate_interface_control_get_size(&body);

	conmon_message_size_info_t size = { CONMON_MESSAGE_MAX_BODY_SIZE };
	size.curr = body_size;
	conmon_audinate_interface_control_set_interface_domain_name(&body, &size, index, device);
	body_size = (uint16_t)size.curr;

	//change hex to ascii
	memset(txMSG, 0, sizeof(txMSG));
	for (int i = 0; i < body_size; i++)
		sprintf(txMSG, "%s %02x", txMSG, body.data[i]);

	//Send message vondor is Audinate
	kk_dtSendOutConMsg_Audinate(device, txMSG);

	return;
}
void dc_SetInterface_dynamic(char* device, uint16_t netInterface) {

	conmon_message_body_t body;
	uint16_t body_size = 0;
	char txMSG[256];
	uint16_t index = netInterface;

	PDANTENODE pnode;

	//check if the device has exist
	pnode = dt_FindNode(device);
	if (!pnode) return;

	//set query message : interface
	conmon_audinate_init_interface_control(&body, 0);
	conmon_audinate_interface_control_set_interface_address_dynamic(&body, index);
	body_size = conmon_audinate_interface_control_get_size(&body);

	//change hex to ascii
	memset(txMSG, 0, sizeof(txMSG));
	for (int i = 0; i < body_size; i++)
		sprintf(txMSG, "%s %02x", txMSG, body.data[i]);

	//Send message vondor is Audinate
	kk_dtSendOutConMsg_Audinate(device, txMSG);

	return;
}


//Response
void dc_MonMsgPayloadCB_Audinate
(
	conmon_client_t* client,
	conmon_channel_type_t type,
	conmon_channel_direction_t dir,
	const conmon_message_head_t* head,
	const conmon_message_body_t* body
)
{

	const conmon_vendor_id_t* vid = conmon_message_head_get_vendor_id(head);
	if (!conmon_vendor_id_equals(CONMON_VENDOR_ID_AUDINATE, vid))
	{
		return;
	}

	conmon_instance_id_t instance_id;
	char id_buf[LEN_NAME];
	const char* source;

	conmon_message_head_get_instance_id(head, &instance_id);
	source = conmon_client_device_name_for_instance_id(client, &instance_id);


	conmon_audinate_message_type_t msg_type = conmon_audinate_message_get_type(body);
	uint16_t body_size = conmon_message_head_get_body_size(head);


	switch (msg_type) {
	case CONMON_AUDINATE_MESSAGE_TYPE_INTERFACE_STATUS: {
		PRINT_MSG(MSG_BOX, g_MsgBuf, "> Audinate message: %s (0x%04x)\n", "INTERFACE_STATUS", msg_type);
		dc_MonMsgPayload_interface_status(source, body, body_size);
		break;
	}
	case CONMON_AUDINATE_MESSAGE_TYPE_SRATE_STATUS: {
		PRINT_MSG(MSG_BOX, g_MsgBuf, "> Audinate message: %s (0x%04x)\n", "SRATE_STATUS", msg_type);
		dc_MonMsgPayload_srate_status(source, body, body_size);
		break;
	}
	case CONMON_AUDINATE_MESSAGE_TYPE_ENC_STATUS: {
		PRINT_MSG(MSG_BOX, g_MsgBuf, "> Audinate message: %s (0x%04x)\n", "ENC_STATUS", msg_type);
		dc_MonMsgPayload_enc_status(source, body, body_size);

		break;
	}
	case CONMON_AUDINATE_MESSAGE_TYPE_MANF_VERSIONS_STATUS: {
		PRINT_MSG(MSG_BOX, g_MsgBuf, "> Audinate message: %s (0x%04x)\n", "MANF_VERSIONS_STATUS", msg_type);
		dc_MonMsgPayload_manf_versions_status(source, body, body_size);
		/*
		char name[32];
		strcpy(name, source);
		kk_dtGetNextDevice(name);
		if (strlen(name))
			dc_QueryManfVer(name);
			*/
		break;
	}


	}


}


void
dc_MonMsgPayload_interface_status
(
	const char* device,
	const conmon_message_body_t* aud_msg,
	size_t body_size
) {
	uint16_t p, np = conmon_audinate_interface_status_num_interfaces(aud_msg);
	uint16_t mode = conmon_audinate_interface_status_get_mode(aud_msg);
	conmon_audinate_interfaces_flags_t flags = conmon_audinate_interface_status_get_flags(aud_msg);
	if (mode == CONMON_AUDINATE_INTERFACE_MODE_DIRECT)
	{
		//printf(">> mode=DIRECT\n");
		PRINT_MSG(MSG_BOX, g_MsgBuf, ">> mode=DIRECT");
	}
	else if (mode == CONMON_AUDINATE_INTERFACE_MODE_SWITCHED)
	{
		//printf(">> mode=SWITCHED\n");
		PRINT_MSG(MSG_BOX, g_MsgBuf, ">> mode=SWITCHED\n");
	}
	else
	{
		//printf(">> mode=%u\n", mode);
		_kkmsg(MSG_BOX, ">> mode=SWITCHED");
	}
	//printf(">> flags=0x%08x ", flags);
	PRINT_MSG(MSG_BOX, g_MsgBuf, ">> flags=0x%08x ", flags);
	if (flags & CONMON_AUDINATE_INTERFACES_SWITCH_REDUNDANCY)
	{
		//printf(" SWITCH_REDUNDANCY is on ");
		PRINT_MSG(MSG_BOX, g_MsgBuf, " SWITCH_REDUNDANCY is on ");
	}
	else
	{
		//printf(" SWITCH_REDUNDANCY is off ");
		PRINT_MSG(MSG_BOX, g_MsgBuf, " SWITCH_REDUNDANCY is off ");
	}
	if (flags & CONMON_AUDINATE_INTERFACES_SWITCH_REDUNDANCY_REBOOT)
	{
		//printf("/ SWITCH_REDUNDANCY_REBOOT is on");
		PRINT_MSG(MSG_BOX, g_MsgBuf, "/ SWITCH_REDUNDANCY_REBOOT is on");
	}
	else
	{
		//printf("/ SWITCH_REDUNDANCY_REBOOT is off");
		PRINT_MSG(MSG_BOX, g_MsgBuf, "/ SWITCH_REDUNDANCY_REBOOT is off");
	}
	//printf("\n");
	//printf(">> num ports=%u\n", np);
	PRINT_MSG(MSG_BOX, g_MsgBuf, ">> num ports=%u\n", np);
	for (p = 0; p < np; p++)
	{
		char buf[128];
		const conmon_audinate_interface_t* i = conmon_audinate_interface_status_interface_at_index(aud_msg, p);

		flags = conmon_audinate_interface_get_flags(i, aud_msg);
		uint32_t link_speed = conmon_audinate_interface_get_link_speed(i, aud_msg);
		const uint8_t* mac = conmon_audinate_interface_get_mac_address(i, aud_msg);
		uint32_t ip = conmon_audinate_interface_get_ip_address(i, aud_msg);
		uint32_t netmask = conmon_audinate_interface_get_netmask(i, aud_msg);
		uint32_t dns = conmon_audinate_interface_get_dns_server(i, aud_msg);
		uint32_t gateway = conmon_audinate_interface_get_gateway(i, aud_msg);
		const char* domain_name = conmon_audinate_interface_status_get_domain_name(i, aud_msg, body_size);
		//printf(">>  flags=%s\n", dt_cmStringInterfaceFlags(flags, buf, sizeof(buf)));
		//printf(">>  link speed=%u\n", link_speed);
		//printf(">>  mac=%02x:%02x:%02x:%02x:%02x:%02x\n", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
		//printf(">>  ip=%s\n", dt_cmStringIpAddress(ip, buf, sizeof(buf)));
		//printf(">>  netmask=%s\n", dt_cmStringIpAddress(netmask, buf, sizeof(buf)));
		//printf(">>  dns=%s\n", dt_cmStringIpAddress(dns, buf, sizeof(buf)));
		//printf(">>  gateway=%s\n", dt_cmStringIpAddress(gateway, buf, sizeof(buf)));
		PRINT_MSG(MSG_BOX, g_MsgBuf, ">>  flags=%s\n", dt_cmStringInterfaceFlags(flags, buf, sizeof(buf)));
		PRINT_MSG(MSG_BOX, g_MsgBuf, ">>  link speed=%u\n", link_speed);
		PRINT_MSG(MSG_BOX, g_MsgBuf, ">>  mac=%02x:%02x:%02x:%02x:%02x:%02x\n", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);
		PRINT_MSG(MSG_BOX, g_MsgBuf, ">>  ip=%s\n", dt_cmStringIpAddress(ip, buf, sizeof(buf)));
		PRINT_MSG(MSG_BOX, g_MsgBuf, ">>  netmask=%s\n", dt_cmStringIpAddress(netmask, buf, sizeof(buf)));
		PRINT_MSG(MSG_BOX, g_MsgBuf, ">>  dns=%s\n", dt_cmStringIpAddress(dns, buf, sizeof(buf)));
		PRINT_MSG(MSG_BOX, g_MsgBuf, ">>  gateway=%s\n", dt_cmStringIpAddress(gateway, buf, sizeof(buf)));


		if (domain_name)
		{
			//printf(">>  domain name =%s\n", domain_name);
			PRINT_MSG(MSG_BOX, g_MsgBuf, ">>  domain name =%s\n", domain_name);
		}

		if (conmon_audinate_interface_is_reboot_configured(i, aud_msg))
		{
			uint16_t reboot_flags = conmon_audinate_interface_get_reboot_flags(i, aud_msg);
			uint32_t reboot_ip = conmon_audinate_interface_get_reboot_ip_address(i, aud_msg);
			uint32_t reboot_netmask = conmon_audinate_interface_get_reboot_netmask(i, aud_msg);
			uint32_t reboot_dns = conmon_audinate_interface_get_reboot_dns_server(i, aud_msg);
			uint32_t reboot_gateway = conmon_audinate_interface_get_reboot_gateway(i, aud_msg);
			const char* reboot_domain_name = conmon_audinate_interface_status_get_reboot_domain_name(i, aud_msg, body_size);
			//printf(">>  reboot_config=yes\n");
			//printf(">>    flags=%s\n", dt_cmStringInterfaceFlags(reboot_flags, buf, sizeof(buf)));
			//printf(">>    ip=%s\n", dt_cmStringIpAddress(reboot_ip, buf, sizeof(buf)));
			//printf(">>    netmask=%s\n", dt_cmStringIpAddress(reboot_netmask, buf, sizeof(buf)));
			//printf(">>    dns=%s\n", dt_cmStringIpAddress(reboot_dns, buf, sizeof(buf)));
			//printf(">>    gateway=%s\n", dt_cmStringIpAddress(reboot_gateway, buf, sizeof(buf)));
			PRINT_MSG(MSG_BOX, g_MsgBuf, ">>  reboot_config=yes\n");
			PRINT_MSG(MSG_BOX, g_MsgBuf, ">>    flags=%s\n", dt_cmStringInterfaceFlags(reboot_flags, buf, sizeof(buf)));
			PRINT_MSG(MSG_BOX, g_MsgBuf, ">>    ip=%s\n", dt_cmStringIpAddress(reboot_ip, buf, sizeof(buf)));
			PRINT_MSG(MSG_BOX, g_MsgBuf, ">>    netmask=%s\n", dt_cmStringIpAddress(reboot_netmask, buf, sizeof(buf)));
			PRINT_MSG(MSG_BOX, g_MsgBuf, ">>    dns=%s\n", dt_cmStringIpAddress(reboot_dns, buf, sizeof(buf)));
			PRINT_MSG(MSG_BOX, g_MsgBuf, ">>    gateway=%s\n", dt_cmStringIpAddress(reboot_gateway, buf, sizeof(buf)));
			if (reboot_domain_name)
			{
				//printf(">>  domain_name=%s\n", reboot_domain_name);
				PRINT_MSG(MSG_BOX, g_MsgBuf, ">>  domain_name=%s\n", reboot_domain_name);
			}
		}
		else
		{
			//printf(">>  reboot_config=no\n");
			PRINT_MSG(MSG_BOX, g_MsgBuf, ">>  reboot_config=no\n");
		}
	}




	//set data to g_dtHead
	PDANTENODE pnode;
	PDEVICEINFO pdevinfo = NULL;

	//find device node
	pnode = dt_FindNode(device);
	if (!pnode) return;
	pdevinfo = &pnode->devInfo;

	pdevinfo->networks.num_networks = np;

	for (p = 0; p < np; p++)
	{
		char buf[128];
		const conmon_audinate_interface_t* i = conmon_audinate_interface_status_interface_at_index(aud_msg, p);

		flags = conmon_audinate_interface_get_flags(i, aud_msg);
		uint32_t link_speed = conmon_audinate_interface_get_link_speed(i, aud_msg);
		const uint8_t* mac = conmon_audinate_interface_get_mac_address(i, aud_msg);
		uint32_t ip = conmon_audinate_interface_get_ip_address(i, aud_msg);
		uint32_t netmask = conmon_audinate_interface_get_netmask(i, aud_msg);
		uint32_t dns = conmon_audinate_interface_get_dns_server(i, aud_msg);
		uint32_t gateway = conmon_audinate_interface_get_gateway(i, aud_msg);
		const char* domain_name = conmon_audinate_interface_status_get_domain_name(i, aud_msg, body_size);

		//set data to g_dtHead
		conmon_network_t* pnet = &pdevinfo->networks.networks[p];
		pnet->interface_index = p;
		pnet->flags = flags;
		pnet->link_speed = link_speed;
		memcpy(&pnet->mac_address, mac, sizeof(pnet->mac_address));
		pnet->ip_address = ip;
		pnet->netmask = netmask;
		pnet->dns_server = dns;
		pnet->gateway = gateway;
		pnet->is_up = link_speed > 0 ? AUD_TRUE : AUD_FALSE; //not sure is right
	}
}

void
dc_MonMsgPayload_srate_status
(
	const char* device,
	const conmon_message_body_t* aud_msg,
	size_t body_size
) {
	const int NUM_SRATE_MODES = 3;
	const char* SRATE_MODE_NAMES[] =
	{
		"Fixed",
		"Reboot",
		"Immediate"
	};



	uint16_t mode = conmon_audinate_srate_get_mode(aud_msg);
	unsigned long value_curr = conmon_audinate_srate_get_current(aud_msg);
	unsigned long value_next = conmon_audinate_srate_get_new(aud_msg);
	unsigned int avail_count = conmon_audinate_srate_get_available_count(aud_msg);
	//printf(">> mode = %lu (%s)\n", (unsigned long)mode,
	//	(mode < NUM_SRATE_MODES ? SRATE_MODE_NAMES[mode] : "???"));
	//printf(">> current value   = %lu\n", value_curr);
	//printf(">> value on reboot = %lu\n", value_next);
	PRINT_MSG(MSG_BOX, g_MsgBuf, ">> mode = %lu (%s)\n", (unsigned long)mode,
		(mode < NUM_SRATE_MODES ? SRATE_MODE_NAMES[mode] : "???"));
	PRINT_MSG(MSG_BOX, g_MsgBuf, ">> current value   = %lu\n", value_curr);
	PRINT_MSG(MSG_BOX, g_MsgBuf, ">> value on reboot = %lu\n", value_next);
	if (avail_count)
	{
		unsigned int i;
		//printf(">> available values [%u]:", avail_count);
		sprintf(g_MsgBuf, ">> available values [%u]:", avail_count);
		for (i = 0; i < avail_count; i++)
		{
			unsigned long rate =
				conmon_audinate_srate_get_available(aud_msg, i);
			//printf(" %lu", rate);
			//PRINT_MSG(MSG_BOX, g_MsgBuf, " %lu", rate);
			sprintf(g_MsgBuf, "%s %lu,", g_MsgBuf, rate);
		}
		PRINT_MSG(MSG_BOX, g_MsgBuf, "%s", g_MsgBuf);
		//putchar('\n');
	}
	else
	{
		//puts(">> no options available");
		PRINT_MSG(MSG_BOX, g_MsgBuf, ">> no options available");
	}

	//set data to g_dtHead
	PDANTENODE pnode;
	PDEVICEINFO pdevinfo = NULL;

	//find device node
	pnode = dt_FindNode(device);
	if (!pnode) return;

	pdevinfo = &pnode->devInfo;
	pdevinfo->rate = value_curr;

}

void
dc_MonMsgPayload_enc_status
(
	const char* device,
	const conmon_message_body_t* aud_msg,
	size_t body_size
) {
	const int NUM_ENC_MODES = 3;
	const char* ENC_MODE_NAMES[] =
	{
		"Fixed",
		"Reboot",
		"Immediate"
	};

	uint16_t mode = conmon_audinate_enc_get_mode(aud_msg);
	unsigned long value_curr = conmon_audinate_enc_get_current(aud_msg);
	unsigned long value_next = conmon_audinate_enc_get_new(aud_msg);
	unsigned int avail_count = conmon_audinate_enc_get_available_count(aud_msg);

	//printf(">> mode = %lu (%s)\n", (unsigned long)mode,
	//	(mode < NUM_ENC_MODES ? ENC_MODE_NAMES[mode] : "???"));
	//printf(">> current value   = %lu\n", value_curr);
	//printf(">> value on reboot = %lu\n", value_next);
	PRINT_MSG(MSG_BOX, g_MsgBuf, ">> mode = %lu (%s)\n", (unsigned long)mode,
		(mode < NUM_ENC_MODES ? ENC_MODE_NAMES[mode] : "???"));
	PRINT_MSG(MSG_BOX, g_MsgBuf, ">> current value   = %lu\n", value_curr);
	PRINT_MSG(MSG_BOX, g_MsgBuf, ">> value on reboot = %lu\n", value_next);
	if (avail_count)
	{
		unsigned int i;
		//printf(">> available values [%u]:", avail_count);
		sprintf(g_MsgBuf, ">> available values [%u]:", avail_count);
		for (i = 0; i < avail_count; i++)
		{
			unsigned long enc =
				conmon_audinate_enc_get_available(aud_msg, i);
			//printf(" %lu", enc);
			sprintf(g_MsgBuf, "%s %lu,", g_MsgBuf, enc);
		}
		PRINT_MSG(MSG_BOX, g_MsgBuf, "%s", g_MsgBuf);
		//putchar('\n');
	}
	else
	{
		PRINT_MSG(MSG_BOX, g_MsgBuf, ">> no options available");
	}
	//fflush(stdout);



	//set data to g_dtHead
	PDANTENODE pnode;
	PDEVICEINFO pdevinfo = NULL;

	//find device node
	pnode = dt_FindNode(device);
	if (!pnode) return;

	pdevinfo = &pnode->devInfo;
	pdevinfo->encoding = value_curr;
}

void
dc_MonMsgPayload_manf_versions_status
(
	const char* device,
	const conmon_message_body_t* aud_msg,
	size_t body_size
) {
	char buf[BUFSIZ];

	const conmon_vendor_id_t* manufacturer = conmon_audinate_manf_versions_status_get_manufacturer(aud_msg);
	const char* manufacturer_name = conmon_audinate_manf_versions_status_get_manufacturer_name(aud_msg);
	const conmon_audinate_model_id_t* model_id = conmon_audinate_manf_versions_status_get_model_id(aud_msg);
	const char* model_name = conmon_audinate_manf_versions_status_get_model_name(aud_msg);
	const conmon_device_id_t* serial_id = conmon_audinate_manf_versions_status_get_serial_id(aud_msg);
	dante_version_t model_version, sw_version, fw_version;
	dante_version_build_t sw_build, fw_build;
	uint32_t model_version_raw = conmon_audinate_manf_versions_status_get_model_version(aud_msg);
	uint32_t capabilities = conmon_audinate_manf_versions_status_get_capabilities(aud_msg);
	const char* model_version_string = conmon_audinate_manf_versions_status_get_model_version_string(aud_msg);

	conmon_audinate_manf_versions_status_get_software_version_build(aud_msg, &sw_version, &sw_build);
	conmon_audinate_manf_versions_status_get_firmware_version_build(aud_msg, &fw_version, &fw_build);
	dante_version_from_uint32_8_8_16(model_version_raw, &model_version);

	PRINT_MSG(MSG_BOX, g_MsgBuf, ">> manufacturer=%s\n", conmon_vendor_id_to_string(manufacturer, buf, BUFSIZ));
	//PRINT_MSG(MSG_BOX, g_MsgBuf, ">> manufacturer name=\""); print_utf8((const unsigned char *)manufacturer_name); printf("\"\n");
	PRINT_MSG(MSG_BOX, g_MsgBuf, ">> manufacturer name=\"%s\"", manufacturer_name);
	PRINT_MSG(MSG_BOX, g_MsgBuf, ">> model id=%s\n", conmon_audinate_model_id_to_string(model_id, buf, BUFSIZ));
	//PRINT_MSG(MSG_BOX, g_MsgBuf, ">> model name=\""); print_utf8((const unsigned char *)model_name); printf("\"\n");
	PRINT_MSG(MSG_BOX, g_MsgBuf, ">> model name=\"%s\"", model_name);
	PRINT_MSG(MSG_BOX, g_MsgBuf, ">> model version=%u.%u.%u\n", model_version.major, model_version.minor, model_version.bugfix);
	PRINT_MSG(MSG_BOX, g_MsgBuf, ">> model version string=\"%s\"\n", model_version_string ? model_version_string : "");
	PRINT_MSG(MSG_BOX, g_MsgBuf, ">> serial id=%s\n", conmon_device_id_to_string(serial_id, buf, BUFSIZ));

	PRINT_MSG(MSG_BOX, g_MsgBuf, ">> software version=%u.%u.%u.%u\n",
		sw_version.major, sw_version.minor, sw_version.bugfix, sw_build.build_number);
	PRINT_MSG(MSG_BOX, g_MsgBuf, ">> firmware version=%u.%u.%u.%u\n",
		fw_version.major, fw_version.minor, fw_version.bugfix, fw_build.build_number);

	PRINT_MSG(MSG_BOX, g_MsgBuf, ">> capabilities=0x%08x\n", capabilities);


	//set data to g_dtHead
	PDANTENODE pnode;
	PDEVICEINFO pdevinfo = NULL;

	//find device node
	pnode = dt_FindNode(device);
	if (!pnode) return;

	pdevinfo = &pnode->devInfo;
	memcpy(&pdevinfo->mf, manufacturer, sizeof(pdevinfo->mf));
	memcpy(&pdevinfo->mfname, manufacturer_name, sizeof(pdevinfo->mfname));
	memcpy(&pdevinfo->modelname, model_name, sizeof(pdevinfo->modelname));
	memcpy(&pdevinfo->model_ver, &model_version, sizeof(dante_version_t));
	memcpy(&pdevinfo->sw_ver, &sw_version, sizeof(dante_version_t));
	memcpy(&pdevinfo->fw_ver, &fw_version, sizeof(dante_version_t));
	memcpy(&pdevinfo->sw_verb, &sw_build, sizeof(dante_version_build_t));
	memcpy(&pdevinfo->fw_verb, &fw_build, sizeof(dante_version_build_t));

	//avoid overflow memory for read string
	pdevinfo->mfname[sizeof(pdevinfo->mfname) - 1] = '\0';
	pdevinfo->modelname[sizeof(pdevinfo->modelname) - 1] = '\0';
}


char* dt_cmStringIpAddress(uint32_t addr, char* buf, size_t len)
{
	uint8_t* pip = (uint8_t*)&addr;
	SNPRINTF(buf, len, "%d.%d.%d.%d", pip[0], pip[1], pip[2], pip[3]);
	return buf;
}

char* dt_cmStringInterfaceFlags(uint16_t flags, char* buf, size_t len)
{
	size_t off = 0;
	off += SNPRINTF(buf + off, len - off, "0x%08x", flags);
	if (flags & CONMON_AUDINATE_INTERFACE_FLAG_UP)
	{
		off += SNPRINTF(buf + off, len - off, " UP");
	}
	if (flags & CONMON_AUDINATE_INTERFACE_FLAG_STATIC)
	{
		off += SNPRINTF(buf + off, len - off, " STATIC");
	}
	if (flags & CONMON_AUDINATE_INTERFACE_FLAG_LINK_LOCAL_DISABLED)
	{
		off += SNPRINTF(buf + off, len - off, " LINK_LOCAL_DISABLED");
	}
	if (flags & CONMON_AUDINATE_INTERFACE_FLAG_LINK_LOCAL_DELAYED)
	{
		off += SNPRINTF(buf + off, len - off, " LINK_LOCAL_DELAYED");
	}
	if (flags & CONMON_AUDINATE_INTERFACE_FLAG_DHCP_DISABLED)
	{
		off += SNPRINTF(buf + off, len - off, " DHCP DISABLED");
	}
	return buf;
}




void dr_RenameRxChannel(const char* rx, const char* rxch, const char* rename)
{
	aud_error_t err;
	aud_errbuf_t errbuf;

	PDANTENODE pnode;
	uint16_t rxch_id;

	//find rx node
	pnode = dt_FindNode(rx);
	if (!pnode) return;

	//get rxch_id
	PNODECHANNEL pch = NULL;

	rxch_id = 0;
	pch = pnode->prx;
	while (pch && strcmp(pch->name, rxch) != 0)
	{
		pch = pch->next_ch;
	}
	if (pch) rxch_id = pch->id;

	if (rxch_id < 1 || rxch_id > pnode->nrx)
	{
		sprintf(g_MsgBuf, "ERR- invalid RX channel (must be in range 1..%u)", pnode->nrx);
		_kkmsg(MSG_LOG, NULL);
		return;
	}

	// allocate a request
	PDEVICEREQUEST request;
	request = dr_AllocateRequest("KK-renameRxCh");

	if (!request)
	{
		_kkmsg(MSG_LOG, "ERR- no more request for rx rename");
		return;
	}

	//rxch_id is zero based array
	err = dr_rxchannel_set_name(pnode->rx[rxch_id - 1], dr_RequestResponseCB, &request->id, rename);
	if (err != AUD_SUCCESS)
	{
		sprintf(g_MsgBuf, "ERR- sending RX rename message: %s", dr_error_message(err, errbuf));
		_kkmsg(MSG_LOG, NULL);
		dr_ReleaseRequest(request);
		return;
	}

	dr_rxchannel_mark_stale(pnode->rx[rxch_id - 1]);

}

void dr_RenameTxLabel(const char* tx, const char* txch, const char* rename) {

	aud_error_t err;
	aud_errbuf_t errbuf;

	PDANTENODE pnode;
	uint16_t txch_id;

	//find rx node
	pnode = dt_FindNode(tx);
	if (!pnode) return;

	//get rxch_id
	PNODECHANNEL pch = NULL;

	txch_id = 0;
	//pch = pnode->ptx;
	//while (pch && strcmp(pch->name, "02") != 0)
	//{
	//	pch = pch->next_ch;
	//}
	//if (pch) txch_id = pch->id;
	txch_id = dr_GetTxCh_id(pnode, txch);


	if (txch_id < 1 || txch_id > pnode->ntx)
	{
		sprintf(g_MsgBuf, "ERR- invalid TX channel (must be in range 1..%u)", pnode->ntx);
		_kkmsg(MSG_LOG, NULL);
		return;
	}


	// allocate a request
	PDEVICEREQUEST request;
	request = dr_AllocateRequest("KK-renameTxCh");
	if (!request)
	{
		_kkmsg(MSG_LOG, "ERR- no more request for tx rename");
		return;
	}

	//txch_id is zero based array
	err = dr_txchannel_replace_txlabel(pnode->tx[txch_id - 1], dr_RequestResponseCB, &request->id, txch, rename, DR_MOVEFLAG_MOVE_EXISTING);
	if (err != AUD_SUCCESS)
	{
		sprintf(g_MsgBuf, "ERR- sending TX rename message: %s", dr_error_message(err, errbuf));
		_kkmsg(MSG_LOG, NULL);
		dr_ReleaseRequest(request);
		return;
	}
}

void dr_AddTxLabel(const char* tx, const char* txch, const char* rename) {

	aud_error_t err;
	aud_errbuf_t errbuf;

	PDANTENODE pnode;
	uint16_t txch_id;

	//find rx node
	pnode = dt_FindNode(tx);
	if (!pnode) return;

	//get rxch_id
	PNODECHANNEL pch = NULL;

	txch_id = 0;
	pch = pnode->ptx;
	while (pch && strcmp(pch->name, txch) != 0)
	{
		pch = pch->next_ch;
	}
	if (pch) txch_id = pch->id;

	if (txch_id < 1 || txch_id > pnode->ntx)
	{
		sprintf(g_MsgBuf, "ERR- invalid TX channel (must be in range 1..%u)", pnode->ntx);
		_kkmsg(MSG_LOG, NULL);
		return;
	}

	// allocate a request
	PDEVICEREQUEST request;
	request = dr_AllocateRequest("KK-addTxCh");
	if (!request)
	{
		_kkmsg(MSG_LOG, "ERR- no more request for tx rename");
		return;
	}

	//txch_id is zero based array
	err = dr_txchannel_add_txlabel(pnode->tx[txch_id - 1], dr_RequestResponseCB, &request->id, rename, DR_MOVEFLAG_MOVE_EXISTING);
	if (err != AUD_SUCCESS)
	{
		sprintf(g_MsgBuf, "ERR- sending TX add message: %s", dr_error_message(err, errbuf));
		_kkmsg(MSG_LOG, NULL);
		dr_ReleaseRequest(request);
		return;
	}
}

void dr_RemoveTxLabel(const char* tx, const char* txch) {

	aud_error_t err;
	aud_errbuf_t errbuf;

	PDANTENODE pnode;
	uint16_t txch_id;

	//find rx node
	pnode = dt_FindNode(tx);
	if (!pnode) return;

	//get rxch_id
	PNODECHANNEL pch = NULL;

	txch_id = 0;
	pch = pnode->ptx;
	while (pch && strcmp(pch->name, "02") != 0)
	{
		pch = pch->next_ch;
	}
	if (pch) txch_id = pch->id;

	if (txch_id < 1 || txch_id > pnode->ntx)
	{
		sprintf(g_MsgBuf, "ERR- invalid TX channel (must be in range 1..%u)", pnode->ntx);
		_kkmsg(MSG_LOG, NULL);
		return;
	}

	// allocate a request
	PDEVICEREQUEST request;
	request = dr_AllocateRequest("KK-removeTxCh");
	if (!request)
	{
		_kkmsg(MSG_LOG, "ERR- no more request for tx remove");
		return;
	}

	//txch_id is zero based array
	err = dr_txchannel_remove_txlabel(pnode->tx[txch_id - 1], dr_RequestResponseCB, &request->id, txch);
	if (err != AUD_SUCCESS)
	{
		sprintf(g_MsgBuf, "ERR- sending TX remove message: %s", dr_error_message(err, errbuf));
		_kkmsg(MSG_LOG, NULL);
		dr_ReleaseRequest(request);
		return;
	}
}


void dr_MuteRxChannel(const char* rx, const char* rxch, const char* mute)
{
	//執行沒作用
	aud_error_t err;
	aud_errbuf_t errbuf;

	PDANTENODE pnode;
	uint16_t rxch_id;

	//find rx node
	pnode = dt_FindNode(rx);
	if (!pnode) return;

	//get rxch_id
	PNODECHANNEL pch = NULL;

	rxch_id = 0;
	pch = pnode->prx;
	while (pch && strcmp(pch->name, rxch) != 0)
	{
		pch = pch->next_ch;
	}
	if (pch) rxch_id = pch->id;

	if (rxch_id < 1 || rxch_id > pnode->nrx)
	{
		sprintf(g_MsgBuf, "ERR- invalid RX channel (must be in range 1..%u)", pnode->nrx);
		_kkmsg(MSG_LOG, NULL);
		return;
	}

	// allocate a request
	PDEVICEREQUEST request;
	request = dr_AllocateRequest("KK-muteRxCh");
	if (!request)
	{
		_kkmsg(MSG_LOG, "ERR- no more request for rxch mute");
		return;
	}

	aud_bool_t isMute;
	if (!_stricmp(mute, "mute"))
		isMute = AUD_TRUE;
	else if (!_stricmp(mute, "unmute"))
		isMute = AUD_FALSE;
	else {
		_kkmsg(MSG_LOG, "ERR- parameter over range for rxch mute");
		return;
	}

	//rxch_id is zero based array
	err = dr_rxchannel_set_muted(pnode->rx[rxch_id - 1], dr_RequestResponseCB, &request->id, isMute);
	if (err != AUD_SUCCESS)
	{
		sprintf(g_MsgBuf, "ERR- sending RX mute message: %s", dr_error_message(err, errbuf));
		_kkmsg(MSG_LOG, NULL);
		dr_ReleaseRequest(request);
		return;
	}
}

void dr_MuteTxChannel(const char* tx, const char* txch, const char* mute)
{
	//執行沒作用
	aud_error_t err;
	aud_errbuf_t errbuf;

	PDANTENODE pnode;
	uint16_t txch_id;

	//find tx node
	pnode = dt_FindNode(tx);
	if (!pnode) return;

	//get txch_id
	PNODECHANNEL pch = NULL;

	txch_id = 0;
	pch = pnode->prx;
	while (pch && strcmp(pch->name, txch) != 0)
	{
		pch = pch->next_ch;
	}
	if (pch) txch_id = pch->id;

	if (txch_id < 1 || txch_id > pnode->nrx)
	{
		sprintf(g_MsgBuf, "ERR- invalid RX channel (must be in range 1..%u)", pnode->nrx);
		_kkmsg(MSG_LOG, NULL);
		return;
	}

	// allocate a request
	PDEVICEREQUEST request;
	request = dr_AllocateRequest("KK-muteTxCh");
	if (!request)
	{
		_kkmsg(MSG_LOG, "ERR- no more request for txch mute");
		return;
	}

	aud_bool_t isMute;
	if (!_stricmp(mute, "mute"))
		isMute = AUD_TRUE;
	else if (!_stricmp(mute, "unmute"))
		isMute = AUD_FALSE;
	else {
		_kkmsg(MSG_LOG, "ERR- parameter over range for txch mute");
		return;
	}

	//rxch_id is zero based array
	err = dr_txchannel_set_muted(pnode->tx[txch_id - 1], dr_RequestResponseCB, &request->id, isMute);
	if (err != AUD_SUCCESS)
	{
		sprintf(g_MsgBuf, "ERR- sending TX mute message: %s", dr_error_message(err, errbuf));
		_kkmsg(MSG_LOG, NULL);
		dr_ReleaseRequest(request);
		return;
	}
}

void dr_EnableTxChannel(const char* tx, const char* txch, const char* en) {
	//執行沒作用
	aud_error_t err;
	aud_errbuf_t errbuf;

	PDANTENODE pnode;
	uint16_t txch_id;

	//find tx node
	pnode = dt_FindNode(tx);
	if (!pnode) return;

	//get txch_id
	PNODECHANNEL pch = NULL;

	txch_id = 0;
	pch = pnode->prx;
	while (pch && strcmp(pch->name, txch) != 0)
	{
		pch = pch->next_ch;
	}
	if (pch) txch_id = pch->id;

	if (txch_id < 1 || txch_id > pnode->nrx)
	{
		sprintf(g_MsgBuf, "ERR- invalid RX channel (must be in range 1..%u)", pnode->nrx);
		_kkmsg(MSG_LOG, NULL);
		return;
	}

	// allocate a request
	PDEVICEREQUEST request;
	request = dr_AllocateRequest("KK-enableTxCh");
	if (!request)
	{
		_kkmsg(MSG_LOG, "ERR- no more request for txch enable");
		return;
	}

	aud_bool_t isEnable;
	if (!_stricmp(en, "Enable"))
		isEnable = AUD_TRUE;
	else if (!_stricmp(en, "Disable"))
		isEnable = AUD_FALSE;
	else {
		_kkmsg(MSG_LOG, "ERR- parameter over range for txch enable");
		return;
	}

	//rxch_id is zero based array
	err = dr_txchannel_set_enabled(pnode->tx[txch_id - 1], dr_RequestResponseCB, &request->id, isEnable);
	if (err != AUD_SUCCESS)
	{
		sprintf(g_MsgBuf, "ERR- sending TX enable message: %s", dr_error_message(err, errbuf));
		_kkmsg(MSG_LOG, NULL);
		dr_ReleaseRequest(request);
		return;
	}
}

aud_bool_t dr_RenameDevice(const char* dev, const char* rename) {
	aud_error_t err;
	aud_errbuf_t errbuf;

	PDANTENODE pnode;
	uint16_t rxch_id;

	//find rx node
	pnode = dt_FindNode(dev);
	if (!pnode) return 0;

	//rename device
	err = dr_device_close_rename(pnode->device, rename);
	if (err != AUD_SUCCESS)
	{
		sprintf(g_MsgBuf, "ERR- sending Device rename message: %s", dr_error_message(err, errbuf));
		_kkmsg(MSG_LOG, NULL);
		return 0;
	}
	return 1;
}



const char* dr_GetDeviceActualName(const char* dev) {

	PDANTENODE pnode;
	//find rx node
	pnode = dt_FindNode(dev);
	if (!pnode) return NULL;

	const char* DeviceName;
	DeviceName = dr_device_get_actual_name(pnode->device);
	return DeviceName;
}

const char* dr_GetDeviceAdvertisedName(const char* dev) {
	PDANTENODE pnode;
	//find rx node
	pnode = dt_FindNode(dev);
	if (!pnode) return NULL;

	const char* DeviceName;
	DeviceName = dr_device_get_advertised_name(pnode->device);
	return DeviceName;
}

const char* dr_GetDeviceDefaultName(const char* dev) {
	PDANTENODE pnode;
	//find rx node
	pnode = dt_FindNode(dev);
	if (!pnode) return NULL;

	const char* DeviceName;
	DeviceName = dr_device_get_default_name(pnode->device);
	return DeviceName;
}

void dr_PingDevice(const char* dev) {
	aud_error_t err;
	aud_errbuf_t errbuf;

	PDANTENODE pnode;

	//find device node
	pnode = dt_FindNode(dev);
	if (!pnode) return;
	// allocate a request
	PDEVICEREQUEST request;
	request = dr_AllocateRequest("Ping_TEST");
	if (!request)
	{
		_kkmsg(MSG_LOG, "ERR- no more request for Ping_TEST");
		return;
	}
	err = dr_device_ping(pnode->device, dr_RequestResponseCB, &request->id);
	if (err != AUD_SUCCESS)
	{
		sprintf(g_MsgBuf, "ERR- sending device ping message: %s", dr_error_message(err, errbuf));
		_kkmsg(MSG_LOG, NULL);
		dr_ReleaseRequest(request);
		kk_dtStop();
		kk_dtStart();
		return;
	}
	return;
}

void dr_HasNetworkLoopBack(const char* dev) {
	static int sw_state = 0;
	aud_error_t err;
	aud_errbuf_t errbuf;

	PDANTENODE pnode;

	//find device node
	pnode = dt_FindNode(dev);
	if (!pnode) return;

	bool hasNetworkLoopBack = AUD_FALSE;
	hasNetworkLoopBack = dr_device_has_network_loopback(pnode->device);
	return;
}
void dr_SetNetworkLoopBack(const char* dev, aud_bool_t sw) {
	aud_error_t err;
	aud_errbuf_t errbuf;

	PDANTENODE pnode;

	//find device node
	pnode = dt_FindNode(dev);
	if (!pnode) return;

	// allocate a request
	PDEVICEREQUEST request;
	request = dr_AllocateRequest("KK-networkLoopback");
	if (!request) {
		_kkmsg(MSG_LOG, "ERR- no more request for ping device");
		return;
	}

	err = dr_device_set_network_loopback(pnode->device, sw, dr_RequestResponseCB, &request->id);
	if (err != AUD_SUCCESS) {
		sprintf(g_MsgBuf, "ERR- sending device ping message: %s", dr_error_message(err, errbuf));
		_kkmsg(MSG_LOG, NULL);
		dr_ReleaseRequest(request);
		return;
	}
	return;
}

void dr_GetSampleRate(const char* dev, const char* tx) {
	aud_error_t err;
	aud_errbuf_t errbuf;

	PDANTENODE pnode;
	uint16_t txch_id;

	//find rx node
	pnode = dt_FindNode(dev);
	if (!pnode) return;

	//get rxch_id
	PNODECHANNEL pch = NULL;

	txch_id = 0;
	pch = pnode->ptx;
	while (pch && strcmp(pch->name, tx) != 0)
	{
		pch = pch->next_ch;
	}
	if (pch) txch_id = pch->id;

	if (txch_id < 1 || txch_id > pnode->ntx)
	{
		sprintf(g_MsgBuf, "ERR- invalid TX channel (must be in range 1..%u)", pnode->ntx);
		_kkmsg(MSG_LOG, NULL);
		return;
	}

	dante_samplerate_t sampleRate = 0;

	//txch_id is zero based array
	sampleRate = dr_txchannel_get_sample_rate(pnode->tx[txch_id - 1]);
	sampleRate = sampleRate;
	return;
}

void dr_GetFormatTx(const char* dev, const char* tx) {
	aud_error_t err;
	aud_errbuf_t errbuf;

	PDANTENODE pnode;
	uint16_t txch_id;

	//find tx node
	pnode = dt_FindNode(dev);
	if (!pnode) return;

	//get txch_id
	PNODECHANNEL pch = NULL;

	txch_id = 0;
	pch = pnode->ptx;
	while (pch && strcmp(pch->name, tx) != 0)
	{
		pch = pch->next_ch;
	}
	if (pch) txch_id = pch->id;

	if (txch_id < 1 || txch_id > pnode->ntx)
	{
		sprintf(g_MsgBuf, "ERR- invalid TX channel (must be in range 1..%u)", pnode->ntx);
		_kkmsg(MSG_LOG, NULL);
		return;
	}

	const dante_formats_t* formats;
	//txch_id is zero based array
	formats = dr_txchannel_get_formats(pnode->tx[txch_id - 1]);

	dante_samplerate_t samplerateFormat;
	dante_encoding_t encodingFormat;
	uint16_t numNonPcmEncodingFormat;
	const dante_encoding_t* p_numNonPcmEncodingFormat;
	dante_encoding_t nativePcmFormat;
	dante_encoding_pcm_map_t pcmPcmMapFormat;


	samplerateFormat = dante_formats_get_samplerate(formats);
	encodingFormat = dante_formats_get_native_encoding(formats);
	numNonPcmEncodingFormat = dante_formats_num_non_pcm_encodings(formats);
	p_numNonPcmEncodingFormat = dante_formats_get_non_pcm_encodings(formats);
	nativePcmFormat = dante_formats_get_native_pcm(formats);
	pcmPcmMapFormat = dante_formats_get_pcm_map(formats);

	KKMSG(g_MsgBuf, "%s Tx Formats:", dev);								_kkmsg(MSG_BOX, NULL);
	KKMSG(g_MsgBuf, "- Sample Rate: %d", samplerateFormat);					_kkmsg(MSG_BOX, NULL);
	KKMSG(g_MsgBuf, "- Encoding: PCM%d", encodingFormat);					_kkmsg(MSG_BOX, NULL);
	KKMSG(g_MsgBuf, "- Number of non-pcm encodings: %d", numNonPcmEncodingFormat);					_kkmsg(MSG_BOX, NULL);
	KKMSG(g_MsgBuf, "- Native pcm: %d", nativePcmFormat);					_kkmsg(MSG_BOX, NULL);
	KKMSG(g_MsgBuf, "- Pcm map: %d", pcmPcmMapFormat);					_kkmsg(MSG_BOX, NULL);



	//conmon_message_body_t aud_msg;
	//conmon_audinate_message_type_t type;
	//uint32_t congestion_delay_window_us = 5000;
	////get version status
	//type = CONMON_AUDINATE_MESSAGE_TYPE_VERSIONS_STATUS;
	//conmon_audinate_init_query_message(&aud_msg, type, congestion_delay_window_us);
	//conmon_audinate_message_version_t version = conmon_audinate_message_get_version(&aud_msg);
	//
	//
	//type = CONMON_AUDINATE_MESSAGE_TYPE_VERSIONS_QUERY;
	//conmon_audinate_init_query_message(&aud_msg, type, congestion_delay_window_us);
	//uint32_t sw_ver = conmon_audinate_versions_status_get_dante_api_version(&aud_msg);
	//
	//
	////get clocking status
	////type = CONMON_AUDINATE_MESSAGE_TYPE_CLOCKING_STATUS;
	//conmon_audinate_init_clocking_control(&aud_msg, congestion_delay_window_us);
	//conmon_audinate_message_type_t msg_type = conmon_audinate_message_get_type(&aud_msg);
	//conmon_audinate_clock_state_t servo_state = conmon_audinate_clocking_status_get_clock_state(&aud_msg);
	//const char* strServo_state = conmon_audinate_servo_state_string(servo_state);
	//
	//
	////get IGMP status
	//type = CONMON_AUDINATE_MESSAGE_TYPE_IGMP_VERS_STATUS;
	//conmon_audinate_init_query_message(&aud_msg, type, congestion_delay_window_us);
	//uint16_t verIgmp = conmon_audinate_igmp_version_status_get_version(&aud_msg);
	//
	//congestion_delay_window_us = congestion_delay_window_us;
	return;

}



void dr_SetPerformance_Rx(const char* dev, dante_latency_us_t latency_us, dante_fpp_t fpp) {
	aud_error_t err;
	aud_errbuf_t errbuf;

	PDANTENODE pnode;

	//find device node
	pnode = dt_FindNode(dev);
	if (!pnode) return;

	// allocate a request
	PDEVICEREQUEST request;
	request = dr_AllocateRequest("KK-RxPerformance");
	if (!request) {
		_kkmsg(MSG_LOG, "ERR- no more request for RxPerformace");
		return;
	}

	err = dr_device_set_rx_performance_us(pnode->device,
		latency_us, fpp, dr_RequestResponseCB, &request->id);

	if (err != AUD_SUCCESS) {
		sprintf(g_MsgBuf, "ERR- sending device RxPerformace message: %s", dr_error_message(err, errbuf));
		_kkmsg(MSG_LOG, NULL);
		dr_ReleaseRequest(request);
		return;
	}
	return;
}
void dr_SetPerformance_Tx(const char* dev, dante_latency_us_t latency_us, dante_fpp_t fpp) {
	aud_error_t err;
	aud_errbuf_t errbuf;

	PDANTENODE pnode;

	//find device node
	pnode = dt_FindNode(dev);
	if (!pnode) return;

	// allocate a request
	PDEVICEREQUEST request;
	request = dr_AllocateRequest("KK-TxPerformance");
	if (!request) {
		_kkmsg(MSG_LOG, "ERR- no more request for TxPerformace ");
		return;
	}

	err = dr_device_set_tx_performance_us(pnode->device,
		latency_us, fpp, dr_RequestResponseCB, &request->id);

	if (err != AUD_SUCCESS) {
		sprintf(g_MsgBuf, "ERR- sending device TxPerformace message: %s", dr_error_message(err, errbuf));
		_kkmsg(MSG_LOG, NULL);
		dr_ReleaseRequest(request);
		return;
	}
	return;
}
void dr_SetPerformance_Unicast(const char* dev, dante_latency_us_t latency_us, dante_fpp_t fpp) {
	aud_error_t err;
	aud_errbuf_t errbuf;

	PDANTENODE pnode;

	//find device node
	pnode = dt_FindNode(dev);
	if (!pnode) return;

	// allocate a request
	PDEVICEREQUEST request;
	request = dr_AllocateRequest("KK-UnicastPerformance");
	if (!request) {
		_kkmsg(MSG_LOG, "ERR- no more request for UnicastPerformace");
		return;
	}

	err = dr_device_set_unicast_performance_us(pnode->device,
		latency_us, fpp, dr_RequestResponseCB, &request->id);

	if (err != AUD_SUCCESS) {
		sprintf(g_MsgBuf, "ERR- sending device UnicastPerformace  message: %s", dr_error_message(err, errbuf));
		_kkmsg(MSG_LOG, NULL);
		dr_ReleaseRequest(request);
		return;
	}
	return;
}



void dr_GetDeviceFromSubscription(const char* dev, const char* rx) {
	PDANTENODE pnode;
	uint16_t rxch_id;

	//find rx node
	pnode = dt_FindNode(dev);
	if (!pnode) return;

	//get rxch_id
	PNODECHANNEL pch = NULL;

	rxch_id = 0;
	pch = pnode->ptx;
	while (pch && strcmp(pch->name, rx) != 0)
	{
		pch = pch->next_ch;
	}
	if (pch) rxch_id = pch->id;

	if (rxch_id < 1 || rxch_id > pnode->nrx)
	{
		sprintf(g_MsgBuf, "ERR- invalid RX channel (must be in range 1..%u)", pnode->ntx);
		_kkmsg(MSG_LOG, NULL);
		return;
	}

	const char* channel_device, * channel, * device;
	//rxch_id is zero based array
	channel_device = dr_rxchannel_get_subscription(pnode->rx[rxch_id - 1]);
	channel = dr_rxchannel_get_subscription_channel(pnode->rx[rxch_id - 1]);
	device = dr_rxchannel_get_subscription_device(pnode->rx[rxch_id - 1]);

	KKMSG(g_MsgBuf, "%s - %s (Rx) name of subscribed of device  :", dev, rx);		_kkmsg(MSG_BOX, NULL);
	KKMSG(g_MsgBuf, "- TxChannel\@device : %s", channel_device);		_kkmsg(MSG_BOX, NULL);
	KKMSG(g_MsgBuf, "- TxChannel : %s", channel);		_kkmsg(MSG_BOX, NULL);
	KKMSG(g_MsgBuf, "- device : %s", device);		_kkmsg(MSG_BOX, NULL);
}
void dr_GetRxSubscriptionStatus(const char* dev, const char* rx) {
	PDANTENODE pnode;
	uint16_t rxch_id;

	//find rx node
	pnode = dt_FindNode(dev);
	if (!pnode) return;

	//get rxch_id
	PNODECHANNEL pch = NULL;

	rxch_id = 0;
	pch = pnode->ptx;
	while (pch && strcmp(pch->name, rx) != 0)
	{
		pch = pch->next_ch;
	}
	if (pch) rxch_id = pch->id;

	if (rxch_id < 1 || rxch_id > pnode->nrx)
	{
		sprintf(g_MsgBuf, "ERR- invalid RX channel (must be in range 1..%u)", pnode->ntx);
		_kkmsg(MSG_LOG, NULL);
		return;
	}

	dante_rxstatus_t rxStatus;

	rxStatus = dr_rxchannel_get_status(pnode->rx[rxch_id - 1]);
	KKMSG(g_MsgBuf, "%s - %s (Rx) Subscription Status:", dev, rx);
	switch ((rxStatus))
	{
	case DANTE_RXSTATUS_NONE:
		KKMSG(g_MsgBuf, "%s %s", g_MsgBuf, "None");
		break;
	case DANTE_RXSTATUS_UNRESOLVED:
		KKMSG(g_MsgBuf, "%s %s", g_MsgBuf, "Can't found tx of remote");
		break;
	case DANTE_RXSTATUS_DYNAMIC:
	case DANTE_RXSTATUS_STATIC:
		KKMSG(g_MsgBuf, "%s %s", g_MsgBuf, "Active Subscription");
		break;

	default:
		break;
	}
	_kkmsg(MSG_BOX, NULL);

}

void dr_SetTxMulticast_vlg(const char* txDev, uint16_t txChCount, ...) {
	aud_error_t err;
	aud_errbuf_t errbuf;

	PDANTENODE pnode;
	uint16_t txch_id;

	dante_name_t* pTxChName = NULL;

	//find rx node
	pnode = dt_FindNode(txDev);
	if (!pnode) return;

	dr_txflow_config_t* txflow_cfg;

	uint16_t maxFlow, numFlow;
	uint16_t maxSlot;
	//Get currently number of txFlow
	numFlow = dr_device_num_txflows(pnode->device);
	dr_device_max_txflows(pnode->device, &maxFlow);
	if (numFlow == maxFlow) {
		_kkmsg(MSG_LOG, "ERR- no more tx flows for set multicast");
		goto goto_exit;
	}

	maxSlot = dr_device_max_txflow_slots(pnode->device);
	if (txChCount > maxSlot) {
		_kkmsg(MSG_LOG, "ERR- no more tx flows slot for set multicast");
		goto goto_exit;
	}

	//Get tx channel name from parameter list
	pTxChName = (dante_name_t*)malloc(sizeof(dante_name_t) * txChCount);
	va_list ap;
	va_start(ap, txChCount);
	for (int i = 0; i < txChCount; i++) {
		char* name = va_arg(ap, char*);
		sprintf(pTxChName[i], "%s", name);
	}
	va_end(ap);

	//Make a multicast flow
	err = dr_txflow_config_new(pnode->device, 0x00, txChCount, &txflow_cfg);

	//Setting tx channel to multicast flow
	for (int i = 0; i < txChCount; i++) {
		PNODECHANNEL pch = NULL;

		txch_id = 0;
		pch = pnode->ptx;
		while (pch && strcmp(pch->name, pTxChName[i]) != 0)
		{
			pch = pch->next_ch;
		}
		if (pch) txch_id = pch->id;

		if (txch_id < 1 || txch_id > pnode->ntx)
		{
			sprintf(g_MsgBuf, "ERR- invalid TX channel (must be in range 1..%u)", pnode->ntx);
			_kkmsg(MSG_LOG, NULL);
			goto goto_exit;
		}

		uint16_t numSlot = dr_txflow_config_num_slots(txflow_cfg);
		err = dr_txflow_config_add_channel(txflow_cfg, pnode->tx[txch_id - 1], i);
		//dr_txflow_config_set_name(txflow_cfg, "test1");

		//	err = dr_txflow_config_set_latency_us(txflow_cfg, 1000);
		//	err = dr_txflow_config_set_fpp(txflow_cfg, 0);
		//	err = dr_txflow_config_set_encoding(txflow_cfg, DANTE_ENCODING_PCM24);
	}

	// allocate a request
	PDEVICEREQUEST request;
	request = dr_AllocateRequest("KK-setMulticast");
	if (!request) {
		_kkmsg(MSG_LOG, "ERR- no more request for set multicast");
		goto goto_exit;
	}

	err = dr_txflow_config_commit(txflow_cfg, dr_RequestResponseCB, &request->id);

	if (err != AUD_SUCCESS)
	{
		sprintf(g_MsgBuf, "ERR- sending multicast message: %s", dr_error_message(err, errbuf));
		_kkmsg(MSG_LOG, NULL);
		dr_ReleaseRequest(request);
		goto goto_exit;
	}

goto_exit:
	free(pTxChName);
	return;
}

void dr_SetTxMulticast(const char* txDev, const char* txCh) {

	aud_error_t err;
	aud_errbuf_t errbuf;

	PDANTENODE pnode;
	uint16_t txch_id;

	//find tx node
	pnode = dt_FindNode(txDev);
	if (!pnode) return;

	//get txch_id
	PNODECHANNEL pch = NULL;

	txch_id = 0;
	pch = pnode->ptx;
	while (pch && strcmp(pch->name, txCh) != 0)
	{
		pch = pch->next_ch;
	}
	if (pch) txch_id = pch->id;

	if (txch_id < 1 || txch_id > pnode->ntx)
	{
		sprintf(g_MsgBuf, "ERR- invalid TX channel (must be in range 1..%u)", pnode->ntx);
		_kkmsg(MSG_LOG, NULL);
		return;
	}

	//rxch_id is zero based array
	dr_txflow_config_t* txflow_cf;

	//Get number of txFlow
	uint16_t numFlow = dr_device_num_txflows(pnode->device);

	err = dr_txflow_config_new(pnode->device, 0x00, 0x03, &txflow_cf);

	uint16_t numSlot = dr_txflow_config_num_slots(txflow_cf);
	err = dr_txflow_config_add_channel(txflow_cf, pnode->tx[txch_id - 1], 0);
	//err = dr_txflow_config_set_name(txflow_cf, "test1");

	//	err = dr_txflow_config_set_latency_us(txflow_cf, 1000);
	//	err = dr_txflow_config_set_fpp(txflow_cf, 0);
	//	err = dr_txflow_config_set_encoding(txflow_cf, DANTE_ENCODING_PCM24);

	// allocate a request
	PDEVICEREQUEST request;
	request = dr_AllocateRequest("KK-setMulticast");
	if (!request) {
		_kkmsg(MSG_LOG, "ERR- no more request for set multicast");
		return;
	}

	err = dr_txflow_config_commit(txflow_cf, dr_RequestResponseCB, &request->id);

	if (err != AUD_SUCCESS)
	{
		sprintf(g_MsgBuf, "ERR- sending multicast message: %s", dr_error_message(err, errbuf));
		_kkmsg(MSG_LOG, NULL);
		dr_ReleaseRequest(request);
		return;
	}


}

void dr_DeleteMulticast(const char* txDev) {
	aud_error_t err;
	aud_errbuf_t errbuf;

	PDANTENODE pnode;
	uint16_t txch_id;

	//find rx node
	pnode = dt_FindNode(txDev);
	if (!pnode) return;

	uint16_t f, nf;//f=flow, nf=number of flow
	nf = dr_device_num_txflows(pnode->device);
	for (f = 0; f < nf; f++)
	{
		dr_txflow_t* flow = NULL;
		err = dr_device_txflow_at_index(pnode->device, f, &flow);
		if (err != AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "ERR- get tx flow handle (%s)", dr_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			return;
		}

		aud_bool_t isMulticast = FALSE;
		err = dr_txflow_is_manual(flow, &isMulticast);

		if (!isMulticast)
			continue;

		// allocate a request
		PDEVICEREQUEST request;
		request = dr_AllocateRequest("KK-deleteMulticast");
		if (!request) {
			_kkmsg(MSG_LOG, "ERR- no more request for delete multicast");
			return;
		}
		err = dr_txflow_delete(&flow, dr_RequestResponseCB, &request->id);
		if (err != AUD_SUCCESS)
		{
			sprintf(g_MsgBuf, "ERR- sending delete multicast message: %s", dr_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			dr_ReleaseRequest(request);
			return;
		}
	}
}

BOOL dr_hasMulticast(const char* txDev) {
	aud_error_t err;
	aud_errbuf_t errbuf;

	PDANTENODE pnode;
	//find tx node
	pnode = dt_FindNode(txDev);
	if (!pnode) return FALSE;

	uint16_t f, nf;//f=flow, nf=number of flow
	nf = dr_device_num_txflows(pnode->device);
	for (f = 0; f < nf; f++)
	{
		dr_txflow_t* flow = NULL;
		err = dr_device_txflow_at_index(pnode->device, f, &flow);
		if (err != AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "ERR- get tx flow handle (%s)", dr_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			return FALSE;
		}

		aud_bool_t isMulticast;
		err = dr_txflow_is_manual(flow, &isMulticast);
		if (isMulticast)
			return TRUE;
	}
	return FALSE;
}
uint16_t dr_numTxMulticast(const char* txDev) {
	uint16_t retNum = 0;
	aud_error_t err;
	aud_errbuf_t errbuf;

	PDANTENODE pnode;
	//find tx node
	pnode = dt_FindNode(txDev);
	if (!pnode) return 0;

	uint16_t f, nf;//f=flow, nf=number of flow
	nf = dr_device_num_txflows(pnode->device);
	for (f = 0; f < nf; f++)
	{
		dr_txflow_t* flow = NULL;
		err = dr_device_txflow_at_index(pnode->device, f, &flow);
		if (err != AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "ERR- get tx flow handle (%s)", dr_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			return 0;
		}

		aud_bool_t isMulticast;
		err = dr_txflow_is_manual(flow, &isMulticast);
		if (isMulticast)
			retNum++;
	}
	return retNum;
}

/**
* Get current flow of the device
*
* @param Dev the name of device
* @param devFlows_ptr a pointer to get tx and rx flows
*
*/
aud_bool_t dr_GetDeviceFlows(const char* Dev, PDEVICEFLOWS* devFlows_ptr) {

	aud_error_t err;
	aud_errbuf_t errbuf;

	PDANTENODE pnode;
	//find tx node
	pnode = dt_FindNode(Dev);
	if (!pnode) return FALSE;

	//create Flows
	PDEVICEFLOWS pFlows;
	pFlows = (PDEVICEFLOWS)malloc(sizeof(DEVICEFLOWS));
	if (pFlows == NULL)
		return FALSE;
	*devFlows_ptr = pFlows;
	memset(pFlows, 0, sizeof(DEVICEFLOWS));


	//Get device name
	sprintf(pFlows->name, pnode->name);
	//Get maxTxFlow , maxRxFlow
	dr_device_max_txflows(pnode->device, &pFlows->maxTxFlows);
	dr_device_max_rxflows(pnode->device, &pFlows->maxRxFlows);
	//Get maxTxSlot , maxrRxSlot
	pFlows->maxTxSlot = dr_device_max_txflow_slots(pnode->device);
	pFlows->maxRxSlot = dr_device_max_rxflow_slots(pnode->device);
	//Get numTxFlow, numRxFlow
	pFlows->ntf = dr_device_num_txflows(pnode->device);
	pFlows->nrf = dr_device_num_rxflows(pnode->device);

	//Get TxFlows
	pFlows->tx = NULL;
	for (int i = 0; i < pFlows->maxTxFlows; i++) {
		PDEVICEFLOW txFlow = NULL;
		if (dr_GetDeviceTxFlow(Dev, i, &txFlow))
			dr_AddListDeviceFlow(&(pFlows->tx), txFlow);
	}
	//Get RxFlows
	pFlows->rx = NULL;
	for (int i = 0; i < pFlows->maxRxFlows; i++) {
		PDEVICEFLOW rxFlow = NULL;
		if (dr_GetDeviceRxFlow(Dev, i, &rxFlow)) {
			dr_AddListDeviceFlow(&(pFlows->rx), rxFlow);
		}
	}
	return TRUE;
}

/**
* Get device tx flow
*
* @param Dev the name of device
* @param index the index of the flow to be obtained
* @param ptxFlow_ptr a pointer to get tx flow
*
*/
aud_bool_t dr_GetDeviceTxFlow(const char* Dev, uint16_t index, PDEVICEFLOW* ptxFlow_ptr) {

	aud_error_t err;
	aud_errbuf_t errbuf;

	PDANTENODE pnode;
	//find tx node
	pnode = dt_FindNode(Dev);
	if (!pnode)
		return FALSE;

	*ptxFlow_ptr = NULL;
	//Create Flow
	PDEVICEFLOW pFlow;
	pFlow = (PDEVICEFLOW)malloc(sizeof(DEVICEFLOW));
	if (!pFlow)
		return FALSE;

	//initial
	memset(pFlow, 0, sizeof(DEVICEFLOW));
	memset(pFlow->idxTxSlot, 0xFF, sizeof(pFlow->idxTxSlot));

	//Get maxTxFlow
	uint16_t maxTxFlows;
	dr_device_max_txflows(pnode->device, &maxTxFlows);
	//Get numTxFlow
	uint16_t numTxFlows;
	numTxFlows = dr_device_num_txflows(pnode->device);
	//Get maxTxSlot
	uint16_t maxTxSlot;
	maxTxSlot = dr_device_max_txflow_slots(pnode->device);

	if (index >= maxTxFlows) {	//over max Flow
		free(pFlow);
		return FALSE;
	}
	if (index >= numTxFlows) {	//over currently existing flow
		free(pFlow);
		return FALSE;
	}

	//get flow at index
	dr_txflow_t* flow = NULL;
	err = dr_device_txflow_at_index(pnode->device, index, &flow);
	if (err != AUD_SUCCESS) {	//no flow
		free(pFlow);
		dr_txflow_release(&flow);
		return FALSE;
	}
	//save index of tx flow
	pFlow->idx = index;
	//Get tx Flow whether Multicast
	dr_txflow_is_manual(flow, &pFlow->isMulticast);
	//Get destination name
	char* rxname, * fname;
	dr_txflow_get_destination(flow, &rxname, &fname);
	strcpy(pFlow->nameRemote, rxname);  //if rxname is "", this flow is multicast
										//Get Slot in flow
	uint16_t nslot;
	uint16_t slotCount = 0;
	//dr_txflow_num_slots(flow, &nslot);    //In unicast, the number of slots is 4.  
	//ptxFlow->nSlot = nslot; 

	for (int s = 0; s < maxTxSlot; s++) {
		//get Slot channel
		dr_txchannel_t* tx;
		err = dr_txflow_channel_at_slot(flow, s, &tx);
		if (err != AUD_SUCCESS)
			continue;
		if (tx) {
			//add index of channel
			char txname[LEN_NAME];
			sprintf(txname, "%s", dr_txchannel_get_canonical_name(tx));

			uint16_t txch_id = strtol(txname, NULL, 10);
			pFlow->idxTxSlot[slotCount] = txch_id - 1;

			//add name of channel
			char* txLabel = NULL;
			txLabel = dr_GetTxLabel(Dev, txch_id - 1);
			sprintf(pFlow->nameTxSlot[slotCount], "%s", txLabel);
			free(txLabel);

			slotCount++;
		}
	}
	pFlow->nSlot = slotCount;
	dr_txflow_release(&flow);
	*ptxFlow_ptr = pFlow;
	return TRUE;
}

/**
* Get device rx flow
*
* @param Dev the name of device
* @param index the index of the flow to be obtained
* @param prxFlow_ptr a pointer to get rx flow
*
*/
aud_bool_t dr_GetDeviceRxFlow(const char* Dev, uint16_t index, PDEVICEFLOW* prxFlow_ptr) {

	aud_error_t err;
	aud_errbuf_t errbuf;

	PDANTENODE pnode;
	//find tx node
	pnode = dt_FindNode(Dev);
	if (!pnode) return FALSE;

	*prxFlow_ptr = NULL;
	//Create Flow
	PDEVICEFLOW pFlow;
	pFlow = (PDEVICEFLOW)malloc(sizeof(DEVICEFLOW));
	if (!pFlow)
		return FALSE;

	//initial
	memset(pFlow, 0, sizeof(DEVICEFLOW));
	memset(pFlow->idxTxSlot, 0xFF, sizeof(pFlow->idxTxSlot));

	//Get maxRxFlow
	uint16_t maxRxFlows;
	dr_device_max_rxflows(pnode->device, &maxRxFlows);
	//Get numRxFlow
	uint16_t numRxFlows;
	numRxFlows = dr_device_num_rxflows(pnode->device);
	//Get maxRxSlot
	uint16_t maxRxSlot;
	maxRxSlot = dr_device_max_rxflow_slots(pnode->device);

	if (index >= maxRxFlows) {	//over max Flow
		free(pFlow);
		return FALSE;
	}
	if (index >= numRxFlows) {	//over currently existing flow
		free(pFlow);
		return FALSE;
	}

	//get flow at index
	dr_rxflow_t* flow = NULL;
	err = dr_device_rxflow_at_index(pnode->device, index, &flow);
	if (err != AUD_SUCCESS) {	//no flow
		free(pFlow);
		dr_rxflow_release(&flow);
		return FALSE;
	}
	//save index of rx flow
	pFlow->idx = index;

	////Get tx name
	//char* txname;
	//strcpy(txname, dr_rxchannel_get_subscription_device(rx));
	//strcpy(prxFlow->remoteName, txname);

	//Get is muticast
	aud_error_t isMulticast;
	dr_rxflow_is_multicast(flow, &isMulticast);
	pFlow->isMulticast = isMulticast;

	//Get Slot in flow
	uint16_t nslot;
	uint16_t slotCount = 0;

	dr_rxflow_num_slots(flow, &nslot);
	//dr_txflow_num_slots(flow, &nslot);    //In unicast, the number of slots is 4.  
	//ptxFlow->nSlot = nslot; 


	//slots
	for (int s = 0; s < nslot; s++) {
		//get Slot channel
		dr_rxchannel_t* rx;

		//Reference 1 : flow formate as follwing:
		//unicast slot in flow: 
		//		s1, s2, s3, s4
		//		 |   |   |   |
		//		 |   |   |   ---- slot 4 :tx channel
		//		 |   |   -------- slot 3 :tx channel
		//		 |   ------------ slot 2 :tx channel
		//		 ---------------- slot 1 :tx channel
		//
		//rx channels subscribe to tx channel
		//		s1, s2, s3, s4
		//		 |   |   |   |
		//		 |   |   |   ---- tx : 
		//		 |   |   -------- tx :  rx3, rx4
		//		 |   ------------ tx :  rx2
		//		 ---------------- tx :	rx1, rx5, rx6
		//
		//Reference 2 : how many rx channel in s1,s2,s3 or s4
		//	ex: get number of rx channel use in s1
		//		using "dr_rxflow_num_slot_channels(flow, s1, &numRxch)"
		//
		//Reference 3 : how to get the handle of rx channel ( dr_rxchannel_t )
		//	ex: get first rx channel use in s1
		//		using "dr_rxflow_slot_channel_at_index(flow, s1, 0, &rxChannel)"
		//	ex: get second rx channel use in s1
		//		using "dr_rxflow_slot_channel_at_index(flow, s1, 1, &rxChannel)"


		//number of rx channels of subscriber in the tx channel
		uint16_t numRxch;
		err = dr_rxflow_num_slot_channels(flow, s, &numRxch); //Reference 1&2
		if (err != AUD_SUCCESS)
			continue;
		//the tx channel subscription is zero
		if (numRxch == 0) {
			continue;
		}

		//Get first rx channel in order to get tx channel
		err = dr_rxflow_slot_channel_at_index(flow, s, 0, &rx); //Reference 3
		if (err != AUD_SUCCESS)
			continue;

		//Get tx device name
		if (strcmp(pFlow->nameRemote, "") == 0) {
			char txnameDevice[32];
			strcpy(txnameDevice, dr_rxchannel_get_subscription_device(rx));
			strcpy(pFlow->nameRemote, txnameDevice);
		}


		//Get tx channel
		//add name of channel
		char txname[LEN_NAME];
		sprintf(txname, "%s", dr_rxchannel_get_subscription_channel(rx));
		sprintf(pFlow->nameTxSlot[slotCount], "%s", txname);

		//add index of channel
		PDANTENODE pnodeTx;
		uint16_t  txch_id;
		pnodeTx = dt_FindNode(pFlow->nameRemote);
		txch_id = dr_GetTxCh_id(pnodeTx, txname);

		pFlow->idxTxSlot[slotCount] = txch_id - 1;

		slotCount++;

	}
	dr_rxflow_release(&flow);
	pFlow->nSlot = slotCount;
	*prxFlow_ptr = pFlow;
	return TRUE;
}

/**
* Add the flow to list.
*
* @param headFlow_ptr a pointer of list of device flows
* @param addFlow_ptr a pointer will add list of headFlow
*
*/
aud_bool_t dr_AddListDeviceFlow(PDEVICEFLOW* headFlow_ptr, PDEVICEFLOW addFlow_ptr) {
	if (addFlow_ptr == NULL)
		return FALSE;
	//find last node
	PDEVICEFLOW p, pTail;
	p = *headFlow_ptr;
	pTail = *headFlow_ptr;
	while (p) {
		pTail = p;
		p = pTail->nextFlow;
	}
	//p = (PDEVICEFLOW)malloc(sizeof(DEVICEFLOW));
	//memcpy(p, addFlow, sizeof(DEVICEFLOW));
	p = addFlow_ptr;

	//add flow in the listing
	if (*headFlow_ptr == NULL) {
		*headFlow_ptr = p;
		(*headFlow_ptr)->nextFlow = NULL;
	}
	else {
		pTail->nextFlow = p;
		p->nextFlow = NULL;
	}

}

/**
* Free DeviceFlows
*
* @param devFlows_ptr a pointer of list of device flows
*/
void dr_FreeDeviceFlows(PDEVICEFLOWS devFlows_ptr) {
	//Free tx flow
	PDEVICEFLOW ptx, ptxNext;
	ptx = devFlows_ptr->tx;
	ptxNext = devFlows_ptr->tx;
	while (ptx) {
		ptxNext = ptx->nextFlow;
		free(ptx);
		ptx = ptxNext;
	}

	//Free rx flow
	PDEVICEFLOW prx, prxNext;
	prx = devFlows_ptr->rx;
	prxNext = devFlows_ptr->rx;
	while (prx) {
		prxNext = prx->nextFlow;
		free(prx);
		prx = prxNext;
	}


	free(devFlows_ptr);
}


/**
* Get list of subscriber for the device
*
* @param Dev the name of device that you want get
* @param devListSub_ptr a pointer to get list of subscriber
*/
aud_bool_t dr_GetListSubscriber(const char* Dev, PLISTSUBSCRIBER* devListSub_ptr) {

	PDANTENODE pnode;
	PLISTSUBSCRIBER plistSub = (LISTSUBSCRIBER*)malloc(sizeof(LISTSUBSCRIBER));
	if (!plistSub)
		return FALSE;

	*devListSub_ptr = plistSub;

	plistSub->nameSub = NULL;

	//set name of device
	sprintf(plistSub->name, "%s", Dev);

	pnode = g_dtHead;
	if (!pnode) {
		free(plistSub);
		return FALSE;
	}
	//Find all subscribers to this Dev from the list device
	dante_name_t nameTemp[512];
	plistSub->nSub = 0;
	while (pnode) {
		if (dr_isSubscribeDevice(pnode->name, Dev)) {
			sprintf(nameTemp[plistSub->nSub++], pnode->name);
		}
		pnode = pnode->next_node;
	}
	if (!plistSub->nSub) {
		return TRUE;
	}

	//save name to struct
	plistSub->nameSub = (dante_name_t*)malloc(sizeof(dante_name_t) * plistSub->nSub);
	for (int i = 0; i < plistSub->nSub; i++) {
		sprintf(plistSub->nameSub[i], "%s", nameTemp[i]);
	}

	return TRUE;
}

/**
* Get list of source for the device
*
* @param Dev the name of device that you want get
* @param devListSrc_ptr a pointer to get list of source
*/
aud_bool_t dr_GetListSource(const char* Dev, PLISTSOURCE* devListSrc_ptr) {

	PDANTENODE pnode;
	PLISTSOURCE plistSrc = (LISTSOURCE*)malloc(sizeof(LISTSOURCE));
	if (!plistSrc)
		return FALSE;

	*devListSrc_ptr = plistSrc;

	plistSrc->nameSrc = NULL;

	//set name of device
	sprintf(plistSrc->name, "%s", Dev);

	pnode = g_dtHead;
	if (!pnode) {
		free(plistSrc);
		return FALSE;
	}
	//Find all source for this Dev from the list device
	dante_name_t nameTemp[512];
	plistSrc->nSrc = 0;
	while (pnode) {
		if (dr_isSubscribeDevice(Dev, pnode->name)) {
			sprintf(nameTemp[plistSrc->nSrc++], pnode->name);
		}
		pnode = pnode->next_node;
	}
	if (!plistSrc->nSrc) {
		return TRUE;
	}

	//save name to struct
	plistSrc->nameSrc = (dante_name_t*)malloc(sizeof(dante_name_t) * plistSrc->nSrc);
	for (int i = 0; i < plistSrc->nSrc; i++) {
		sprintf(plistSrc->nameSrc[i], "%s", nameTemp[i]);
	}

	return TRUE;
}


/**
* Get the relationship between subscriber and source
* 針對rxDev為出發點，去取得txDev之間關係，
* 需占用rx多少multicast flow & unicast flow
*
* @param rxDev the rxDev is subscriber device
* @param txDev the txDev is source device
* @param devSubRlat_ptr a pointer to get relationship
*/
aud_bool_t dr_GetSubscriberRelationship(const char* rxDev, const char* txDev, PSUBSCRIBERRLAT* devSubRlat_ptr) {

	PDANTENODE pnodeRx, pnodeTx;
	//find node
	pnodeRx = dt_FindNode(rxDev);
	pnodeTx = dt_FindNode(txDev);
	if (!pnodeRx || !pnodeTx) {
		return FALSE;
	}

	PSUBSCRIBERRLAT pSubRlat = (SUBSCRIBERRLAT*)malloc(sizeof(SUBSCRIBERRLAT));
	if (!pSubRlat) {
		return FALSE;
	}
	*devSubRlat_ptr = pSubRlat;

	pSubRlat->rxSub = NULL;
	pSubRlat->txSrc = NULL;


	//set name
	sprintf(pSubRlat->nameSub, "%s", rxDev);
	sprintf(pSubRlat->nameSrc, "%s", txDev);

	//find relationship between rx channel with tx channel
	uint16_t nrx = pnodeRx->nrx;
	uint16_t ntx = pnodeTx->ntx;
	char* pnameRx, * pnameTx;
	pSubRlat->nrx_with_tx = 0;
	uint16_t rxTemp[512];
	uint16_t txTemp[512];
	for (int i = 0; i < nrx; i++) {
		//get nameRx of subscriber
		pnameRx = dr_GetRxName(rxDev, i);
		//check each  nameTx of source with nameRx of subscriber 
		//to find relationship 
		for (int j = 0; j < ntx; j++) {
			pnameTx = dr_GetTxLabel(txDev, j);
			if (dr_isSubscribe(rxDev, pnameRx, txDev, pnameTx)) {
				rxTemp[pSubRlat->nrx_with_tx] = i;
				txTemp[pSubRlat->nrx_with_tx] = j;
				pSubRlat->nrx_with_tx++;
				break;
			}
			free(pnameTx);
		}
		free(pnameRx);
	}

	//return result when the list of subscriber is empty
	if (!pSubRlat->nrx_with_tx) {
		return TRUE;
	}

	//save channel of relation
	pSubRlat->rxSub = (uint16_t*)malloc(sizeof(uint16_t) * pSubRlat->nrx_with_tx);
	pSubRlat->txSrc = (uint16_t*)malloc(sizeof(uint16_t) * pSubRlat->nrx_with_tx);
	for (int i = 0; i < pSubRlat->nrx_with_tx; i++) {
		pSubRlat->rxSub[i] = rxTemp[i];
		pSubRlat->txSrc[i] = txTemp[i];
	}

	//get number of flow 
	pSubRlat->numMFlows = 0;
	pSubRlat->numUFlows = 0;
	PDEVICEFLOWS devFlows;
	if (dr_GetDeviceFlows(txDev, &devFlows)) {
		PDEVICEFLOW ptxFlow;
		ptxFlow = devFlows->tx;
		//get number of multicast flows, 
		//and when multicast flow include rx channel, and than mark rx channel to 0xFFFF 
		while (ptxFlow) {
			if (ptxFlow->isMulticast) {
				char isCoverRx = FALSE;
				//get each tx channel in multicast flow
				for (int i = 0; i < ptxFlow->nSlot; i++) {
					uint16_t txId = ptxFlow->idxTxSlot[i];
					uint16_t nrxTemp = pSubRlat->nrx_with_tx;
					//get each tx channel from subscriber
					for (int j = 0; j < nrxTemp; j++) {
						uint16_t rxid = pSubRlat->txSrc[j];
						if (txId == rxid) {
							//mark rxTemp to 0xFFFF in order to sort it. 
							rxTemp[j] = 0xFFFF;
							isCoverRx = TRUE;
						}
					}
				}
				if (isCoverRx) {
					pSubRlat->numMFlows++;
				}
			}
			//next flow
			ptxFlow = ptxFlow->nextFlow;
		}

		////for (int i = 0; i < devFlows->ntf; i++) {
		////	if (ptxFlow->isMulticast) {
		////		char isCoverRx=FALSE;
		////		//get each tx channel in multicast flow
		////		for (int j = 0; j < ptxFlow->nSlot; j++) {
		////			uint16_t txId = ptxFlow->idxTxSlot[j];
		////			uint16_t nrxTemp = pSubRlat->nrx_with_tx;
		////			//get each tx channel from subscriber
		////			for (int k = 0; k < nrxTemp; k++) {
		////				uint16_t rxid = pSubRlat->txSrc[k];
		////				if (txId == rxid) {
		////					//mark rxTemp to 0xFFFF in order to sort it. 
		////					rxTemp[k] = 0xFFFF;
		////					isCoverRx = TRUE;
		////				}
		////			}
		////		}
		////		if (isCoverRx) {
		////			pSubRlat->numMFlows++;
		////		}
		////	}
		////	//next flow
		////	ptxFlow = ptxFlow->nextFlow;
		////}

		//calculate whether the number of flows is enough
		//step1. sort of rx channel 
		uint16_t nrxTemp = pSubRlat->nrx_with_tx;
		int head = 0, jump = 0;
		while (jump < nrxTemp) {
			if (rxTemp[jump] != 0xFFFF) {
				rxTemp[head++] = rxTemp[jump];
			}
			jump++;
		}
		nrxTemp = head;

		//step2. combine the rest of rx channel into unicast flows
		int nFlow = (nrxTemp + 3) / 4;
		pSubRlat->numUFlows = nFlow;

		//step3. check number of flow whether than max of rxFlow
		int totalFlow = nFlow + pSubRlat->numMFlows;
		uint16_t maxRxFlow;
		dr_device_max_rxflows(pnodeRx->device, &maxRxFlow);
		pSubRlat->isOverRxFlows = totalFlow > maxRxFlow ? TRUE : FALSE;

		//Free devFlows
		dr_FreeDeviceFlows(devFlows);
	}




	return TRUE;
}

/**
* Free the list of subscriber
*
* @param devListSub_ptr a pointer that list of subscriber
*/
void dr_FreeListSubscriber(PLISTSUBSCRIBER devListSub_ptr) {
	free(devListSub_ptr->nameSub);
	free(devListSub_ptr);
}

/**
* Free the list of source
*
* @param devListSrc_ptr a pointer that list of source
*/
void dr_FreeListSource(PLISTSOURCE devListSrc_ptr) {
	free(devListSrc_ptr->nameSrc);
	free(devListSrc_ptr);
}

/**
* Free the relationship
*
* @param devSubRlat_ptr a pointer that relationship
*/
void dr_FreeSubscriberRelationship(PSUBSCRIBERRLAT devSubRlat_ptr) {
	free(devSubRlat_ptr->rxSub);
	free(devSubRlat_ptr->txSrc);
	free(devSubRlat_ptr);
}



void dr_SetLockDown(const char* Dev, aud_bool_t sw) {

	aud_error_t err;
	aud_errbuf_t errbuf;

	PDANTENODE pnode;

	//find tx node
	pnode = dt_FindNode(Dev);
	if (!pnode) return;

	// allocate a request
	PDEVICEREQUEST request;
	request = dr_AllocateRequest("KK-setLockdown");
	if (!request) {
		_kkmsg(MSG_LOG, "ERR- no more request for set lockdown");
		return;
	}

	aud_bool_t isLocal = true;
	isLocal = dr_device_is_local_connection(pnode->device);
	err = dr_device_set_lockdown(pnode->device, sw, dr_RequestResponseCB, &request->id);
	if (err != AUD_SUCCESS)
	{
		sprintf(g_MsgBuf, "ERR- sending set lockdown message: %s", dr_error_message(err, errbuf));
		_kkmsg(MSG_LOG, NULL);
		dr_ReleaseRequest(request);
		return;
	}
}

uint16_t dr_GetNumTxlabel(const char* txDev) {

	PDANTENODE pnode;

	//find tx node
	pnode = dt_FindNode(txDev);
	if (!pnode)
		return NULL;
	return pnode->ntx;
}
char* dr_GetTxLabel(const char* txDev, int num) {
	char* retName = (char*)malloc(sizeof(char) * LEN_NAME);

	aud_error_t err;
	aud_errbuf_t errbuf;

	PDANTENODE pnode;

	//find tx node
	pnode = dt_FindNode(txDev);
	if (!pnode)
		goto goto_error;
	if (num >= pnode->ntx)
		goto goto_error;

	dr_txchannel_t* txChannel = dr_device_txchannel_at_index(pnode->device, num);

	dr_txlabel_t labels;
	uint16_t numLabel = 1;
	err = dr_txchannel_get_txlabels(txChannel, &numLabel, &labels);
	if (err != AUD_SUCCESS)
	{
		goto goto_error;
	}
	if (numLabel == 0)	//沒有label時
		sprintf(retName, "%02d", num + 1);
	else
		sprintf(retName, "%s", labels.name);
	goto goto_exit;

goto_error:
	free(retName);
	return NULL;
goto_exit:
	return retName;
}

uint16_t dr_GetNumRxName(const char* rxDev) {
	PDANTENODE pnode;
	PNODECHANNEL pch;
	//find device node
	pnode = dt_FindNode(rxDev);
	if (!pnode) return 0;
	return pnode->nrx;
}
char* dr_GetRxName(const char* rxDev, int num) {

	char* retName = (char*)malloc(sizeof(char) * LEN_NAME);
	PDANTENODE pnode;
	PNODECHANNEL pch;
	int countChannel = 0;
	//find device node
	pnode = dt_FindNode(rxDev);
	if (!pnode) goto goto_error;
	pch = pnode->prx;
	while (pch)
	{
		if (countChannel == num) {
			sprintf(retName, "%s", pch->name);
			goto goto_exit;
		}
		countChannel++;
		pch = pch->next_ch;
	}
	goto goto_error;

goto_error:
	free(retName);
	return NULL;
goto_exit:
	return retName;
}



/**
* Is the subscription relationship between
* the rx channel of subscriber and the tx channel of source?
*
* @param rxDev the rxDev is subscriber device
* @param rxCh the rxCh is name of channel
* @param txDev the txDev is source device
* @param txCh the txCh is name of channel
*
* @return true if a channel is subscribed between the rxch of rxDev and the txCh of txDev
*/
aud_bool_t
dr_isSubscribe(
	const char* rxDev,
	const char* rxCh,
	const char* txDev,
	const char* txCh)
{
	PDANTENODE pnodeRx, pnodeTx;
	uint16_t rxch_id, txch_id;
	//find node
	pnodeRx = dt_FindNode(rxDev);
	pnodeTx = dt_FindNode(txDev);
	if (!pnodeRx || !pnodeTx)
		return FALSE;

	//find channel id
	rxch_id = dr_GetRxCh_id(pnodeRx, rxCh);
	txch_id = dr_GetTxCh_id(pnodeTx, txCh);
	if (rxch_id == 0 || txch_id == 0)
		return FALSE;

	//get tx name on rx channel
	const char* channel, * device;
	device = dr_rxchannel_get_subscription_device(pnodeRx->rx[rxch_id - 1]);
	channel = dr_rxchannel_get_subscription_channel(pnodeRx->rx[rxch_id - 1]);
	if (!device || !channel) //沒訂閱
		return FALSE;

	//check is subscribe
		if (strcmp(device, txDev) == 0) {
			if (dr_txchannel_has_name(pnodeTx->tx[txch_id - 1],
				channel,
				DANTE_TXNAME_TYPE_ALL)) {
				return TRUE;
			}
		}

	////check is subscribe
	//if (strcmp(device, txDev) == 0 &&
	//	strcmp(channel, txCh) == 0
	//	)
	//	return TRUE;
	return FALSE;
}

/**
* Is the subscription relationship between the subscriber and the source?
*
* @param rxDev the rxDev is subscriber device
* @param txDev the txDev is source device
*
* @return true if a channel is subscribed between the rxDev and the txDev
*/
aud_bool_t
dr_isSubscribeDevice(
	const char* rxDev,
	const char* txDev)
{
	aud_bool_t ret = FALSE;
	PDANTENODE pnodeRx, pnodeTx;
	uint16_t rxch_id, txch_id;

	if (strcmp(rxDev, txDev) == 0)
		goto goto_error;

	//find node
	pnodeRx = dt_FindNode(rxDev);
	pnodeTx = dt_FindNode(txDev);
	if (!pnodeRx || !pnodeTx)
		goto goto_error;

	//Get rxChannel name of rxDev
	//malloc rxBuffer
	uint16_t numRxName = dr_GetNumRxName(rxDev);
	if (numRxName == 0) goto goto_error;
	char** rxName = (char**)malloc(sizeof(char*) * numRxName);
	//for (int i = 0; i < numRxName; i++) rxName[i] = (char*)malloc(sizeof(char) * LEN_NAME);
	//get rxName
	for (int i = 0; i < numRxName; i++) {
		rxName[i] = dr_GetRxName(rxDev, i);
		//sprintf(rxName[i], "%s", dr_GetRxName(rxDev, i));
	}

	//Get txChannel name of txDev
	//malloc txBuffer
	uint16_t numTxName = dr_GetNumTxlabel(txDev);
	if (numTxName == 0) goto goto_error;
	char** txName = (char**)malloc(sizeof(char*) * numTxName);
	//for (int i = 0; i < numTxName; i++) txName[i] = (char*)malloc(sizeof(char) * LEN_NAME);
	//get txName
	for (int i = 0; i < numTxName; i++) {
		txName[i] = dr_GetTxLabel(txDev, i);
		//sprintf(txName[i], "%s", dr_GetTxLabel(txDev, i));
	}

	for (int i = 0; i < numRxName; i++)
		for (int j = 0; j < numTxName; j++)
			if (dr_isSubscribe(rxDev, rxName[i], txDev, txName[j])) {
				goto goto_true;
			}

	goto goto_false;

goto_true:
	ret = TRUE;
	goto goto_exit;
goto_false:
	ret = FALSE;
	goto goto_exit;

goto_error:
	return FALSE;
goto_exit:
	//Free txName
	for (int i = 0; i < numTxName; i++)
		free(txName[i]);
	free(txName);
	//Free rxName
	for (int i = 0; i < numRxName; i++)
		free(rxName[i]);
	free(rxName);
	return ret;
}


/**
* Is the device's txFlow valid?
*
* @param Dev the name of device
*
* @return true if the tx flow is not over max of flow in network.
*/
aud_bool_t dr_isValidTxFlows(const char* Dev) {
	aud_bool_t retValid = TRUE;

	PDANTENODE pnodeTx;
	//find node
	pnodeTx = dt_FindNode(Dev);
	if (!pnodeTx) {
		return FALSE;
	}

	uint16_t numTxUFlow = 0; //get number of sum of unicast flow from all subscriber
	uint16_t numTxMFlows = 0; //get number of multicast of tx flow
	uint16_t maxTxFlows = 0; //get max of tx flow

							 //get numTxUFlow
	numTxUFlow = 0;
	PLISTSUBSCRIBER devListSub;
	if (dr_GetListSubscriber(Dev, &devListSub)) {
		int nSub = devListSub->nSub;
		PSUBSCRIBERRLAT* devSubRlat = (PSUBSCRIBERRLAT*)malloc(sizeof(PSUBSCRIBERRLAT) * nSub);

		//get subscriber relation for each 
		for (int i = 0; i < nSub; i++) {
			dr_GetSubscriberRelationship(devListSub->nameSub[i], Dev, &devSubRlat[i]);
		}

		//count number of unicast flow for tx device
		for (int i = 0; i < nSub; i++) {
			//if (devSubRlat[i]->isOverRxFlows) {
			//	KKMSG(g_MsgBuf, "---%s is over rx flows", devListSub->nameSub[i]);		_kkmsg(MSG_BOX, NULL);
			//}
			numTxUFlow += devSubRlat[i]->numUFlows;
		}

		//free memory
		for (int i = 0; i < nSub; i++) {
			dr_FreeSubscriberRelationship(devSubRlat[i]);
		}
		free(devSubRlat);
	}
	dr_FreeListSubscriber(devListSub);

	//get numTxMFlows
	numTxMFlows = 0;
	PDEVICEFLOWS devFlows;
	if (dr_GetDeviceFlows(Dev, &devFlows)) {
		PDEVICEFLOW ptxFlow;
		ptxFlow = devFlows->tx;
		while (ptxFlow) {
			if (ptxFlow->isMulticast)
				numTxMFlows++;
			ptxFlow = ptxFlow->nextFlow;
		}
		dr_FreeDeviceFlows(devFlows);
	}

	//get maxTxFlows
	maxTxFlows = 0;
	dr_device_max_txflows(pnodeTx->device, &maxTxFlows);

	//sum the unicast and multicast of flows, 
	//and check whether the maximum is exceeded.
	if (numTxUFlow + numTxMFlows
		>
		maxTxFlows) {
		//KKMSG(g_MsgBuf, "---%s is over tx flows", txDev);		_kkmsg(MSG_BOX, NULL);
		retValid = FALSE;
	}

	return retValid;
}

/**
* Is the device's rxFlow valid?
*
* @param Dev the name of device
*
* @return true if the rx flow is not over max of flow in network.
*/
aud_bool_t dr_isValidRxFlows(const char* Dev) {
	aud_bool_t retValid = TRUE;

	PDANTENODE pnodeRx;
	//find node
	pnodeRx = dt_FindNode(Dev);
	if (!pnodeRx) {
		return FALSE;
	}

	uint16_t numRxUFlows = 0; //get number of sum of unicast flow from all subscriber
	uint16_t numRxMFlows = 0; //get number of multicast of rx flow
	uint16_t maxRxFlows = 0; //get max of rx flow


							 //get numRxUFlows & numRxMFlows
	numRxUFlows = 0;
	numRxMFlows = 0;
	PLISTSOURCE devListSrc;
	if (dr_GetListSource(Dev, &devListSrc)) {
		PSUBSCRIBERRLAT devSubRlat[20];
		//get subscriber Relation
		for (int i = 0; i < devListSrc->nSrc; i++) {
			dr_GetSubscriberRelationship(Dev, devListSrc->nameSrc[i], &devSubRlat[i]);
		}

		//get numRxUFlows & numRxMFlows
		for (int i = 0; i < devListSrc->nSrc; i++) {
			numRxUFlows += devSubRlat[i]->numUFlows;
			numRxMFlows += devSubRlat[i]->numMFlows;
		}

		//Free memory
		for (int i = 0; i < devListSrc->nSrc; i++) {
			dr_FreeSubscriberRelationship(devSubRlat[i]);
		}
	}
	dr_FreeListSource(devListSrc);

	//get maxRxFlows
	dr_device_max_rxflows(pnodeRx->device, &maxRxFlows);

	//sum the unicast and multicast of flows, 
	//and check whether the maximum is exceeded.
	if (numRxUFlows + numRxMFlows
		>
		maxRxFlows) {
		retValid = FALSE;
	}

	return retValid;
}


uint16_t dr_GetTxCh_id(PDANTENODE pnode, const char* txCh) {
	PNODECHANNEL pch = NULL;
	uint16_t ret_txch_id = 0;
	pch = pnode->ptx;

	//由txCh 找出 index
	uint16_t numTxlabel = dr_GetNumTxlabel(pnode->name);

	for (int i = 0; i < numTxlabel; i++) {

		if (dr_txchannel_has_name(pnode->tx[i],
			txCh,
			DANTE_TXNAME_TYPE_ALL)) {
			ret_txch_id = i + 1;
			break;
		}
	}

	if (ret_txch_id < 1 || ret_txch_id > pnode->ntx)
	{
		sprintf(g_MsgBuf, "ERR- invalid TX channel (must be in range 1..%u)", pnode->ntx);
		_kkmsg(MSG_LOG, NULL);
		return 0;
	}
	return ret_txch_id;


	//	PNODECHANNEL pch = NULL;
	//	uint16_t ret_txch_id = 0;
	//	pch = pnode->ptx;
	//
	//	char* txIndexName = (char*)malloc(sizeof(char) * 20);
	//	//由txCh 找出 index
	//	uint16_t numTxlabel = dr_GetNumTxlabel(pnode->name);
	//	uint16_t countLabel = 0;
	//	char* ptxName;
	//	while (countLabel < numTxlabel) {
	//		ptxName = dr_GetTxLabel(pnode->name, countLabel);
	//		if (ptxName) {
	//			if (strcmp(ptxName, txCh) == 0)
	//				break;
	//			free(ptxName);
	//		}
	//		countLabel++;
	//	}
	//	if (countLabel == numTxlabel)
	//		goto goto_error;
	//
	//	sprintf(txIndexName, "%02d", countLabel + 1);
	//
	//	while (pch && strcmp(pch->name, txIndexName) != 0)
	//	{
	//		pch = pch->next_ch;
	//	}
	//	if (pch) ret_txch_id = pch->id;
	//
	//	if (ret_txch_id < 1 || ret_txch_id > pnode->ntx)
	//	{
	//		sprintf(g_MsgBuf, "ERR- invalid TX channel (must be in range 1..%u)", pnode->ntx);
	//		_kkmsg(MSG_LOG, NULL);
	//		goto goto_error;
	//	}
	//	goto goto_result;
	//
	//goto_error:
	//	ret_txch_id = 0;
	//goto_result:
	//	free(txIndexName);
	//	return ret_txch_id;
}
uint16_t dr_GetRxCh_id(PDANTENODE pnode, const char* rxCh) {
	PNODECHANNEL pch = NULL;
	uint16_t ret_rxch_id = 0;
	pch = pnode->prx;
	while (pch && strcmp(pch->name, rxCh) != 0)
	{
		pch = pch->next_ch;
	}
	if (pch) ret_rxch_id = pch->id;

	if (ret_rxch_id < 1 || ret_rxch_id > pnode->nrx)
	{
		sprintf(g_MsgBuf, "ERR- invalid RX channel (must be in range 1..%u)", pnode->nrx);
		_kkmsg(MSG_LOG, NULL);
		return 0;
	}

	return ret_rxch_id;
}


//-----------------------KK Exten API --------------------
aud_bool_t kk_dtPingAllDevice(void) {
	PDANTENODE p;
	int cnt = 0;
	int sizeOfDevice = 0;
	aud_bool_t isRetModified = NO;

	//cleaer request queue
	dr_ReleaseRequestDescription("Ping_TEST");

	//count number of device
	sizeOfDevice = 0;
	p = g_dtHead;
	while (p != 0) {
		sizeOfDevice++;
		p = p->next_node;
	}

	//Create data
	typedef struct tagStateActive {
		PDANTENODE node;
		bool preActive;
	}*PSTACT, STACT;

	PSTACT state = (PSTACT)malloc(sizeof(STACT) * sizeOfDevice);
	if (!state)
		return NO;

	//initial data
	p = g_dtHead;
	for (int i = 0; i < sizeOfDevice; i++) {
		state[i].node = p;
		state[i].preActive = p->active;
		p = p->next_node;
	}

	//ping device
	cnt = 0;
	p = g_dtHead;
	while (p != 0)
	{
		p->active = NO;
		dr_PingDevice(p->name);
		p = p->next_node;

		if ((cnt++) % 10 == 0) {
			Sleep(2);
		}
	}
	Sleep(10);

	//check active
	for (int i = 0; i < sizeOfDevice; i++) {
		if (state[i].node->active != state[i].preActive) {
			isRetModified = TRUE;
			break;
		}
	}

	free(state);
	return isRetModified;
}

void kk_dtShowRXCH_TXCH(HWND hdlg, int id, char* device)
{

	//char wstr[256];
	PDANTENODE pnode;
	PNODECHANNEL pch;
	char str[256];
	char* rxch_dev;
	const char* txch_dev;
	//reset tx channels
	SendDlgItemMessage(hdlg, id, LB_RESETCONTENT, 0, 0);

	//find device node
	pnode = dt_FindNode(device);
	if (!pnode) return;

	rxch_dev = (char*)malloc(sizeof(char) * 80);
	pch = pnode->prx;
	while (pch)
	{
		//get txch@dev
		txch_dev = dt_GetRxSubscription(device, pch->name);
		//get rxch@dev
		sprintf(rxch_dev, "%s@%s", pch->name, device);

		sprintf(str, "%s <-- %s", rxch_dev, txch_dev);

		//mbstowcs(wstr, str, strlen(str) + 1);
		SendDlgItemMessage(hdlg, id, LB_ADDSTRING, 0, (LPARAM)str);
		pch = pch->next_ch;
	}
	SendDlgItemMessage(hdlg, id, LB_SETCURSEL, 0, 0);
}

void kk_dtUnSubscribeCH_All(const char* rx) {
	PDANTENODE pnode;
	PNODECHANNEL pch;

	//find device node
	pnode = dt_FindNode(rx);
	if (!pnode) return;

	pch = pnode->prx;
	while (pch)
	{
		dr_SubscribeChannel(rx, pch->name, NULL, NULL);
		pch = pch->next_ch;
	}
}

//------------------------Dante Extend API - Combo ----------------------------

void dr_enBroadcast(const char* broadcaster, bool isEnable)//啟動廣播
{
	PDANTENODE pnode;
	//find rx node
	pnode = dt_FindNode(broadcaster);
	if (!pnode) return;

	if (isEnable) {
		//get max of slot
		uint16_t maxSlot = dr_device_max_txflow_slots(pnode->device);
		switch (maxSlot) {
		case 1:
			dr_SetTxMulticast_vlg(broadcaster, maxSlot, "01");
			break;
		case 2:
			dr_SetTxMulticast_vlg(broadcaster, maxSlot, "01", "02");
			break;
		case 3:
			dr_SetTxMulticast_vlg(broadcaster, maxSlot, "01", "02", "03");
			break;
		case 4:
			dr_SetTxMulticast_vlg(broadcaster, maxSlot, "01", "02", "03", "04");
			break;
		}
	}
	else {
		dr_DeleteMulticast(broadcaster);
	}
	return;
}
void tuneDSP(const char* dev)//連線至裝置
{
	//link device to get dsp detail 

	return;
}
void dr_unSubscribe(const char* rxDev, const char* txDev)
{

	//Get rxChannel name of rxDev
	//malloc rxBuffer
	uint16_t numRxName = dr_GetNumRxName(rxDev);
	if (numRxName == 0) return;
	char** rxName = (char**)malloc(sizeof(char*) * numRxName);
	for (int i = 0; i < numRxName; i++) rxName[i] = (char*)malloc(sizeof(char) * LEN_NAME);
	//get rxName
	for (int i = 0; i < numRxName; i++)
		sprintf(rxName[i], "%s", dr_GetRxName(rxDev, i));

	//the rxDev unsubscribe all channel if txDev is NULL
	if (txDev == NULL) {
		//unSubscribe all of channel
		for (int i = 0; i < numRxName; i++)
			kk_dtSubscribeCH((char*)rxDev, rxName[i], NULL, NULL);

		//char str[LEN_NAME];
		//PDANTENODE pnode;
		//PNODECHANNEL pch;
		////find device node
		//pnode = dt_FindNode(rxDev);
		//if (!pnode) return;
		//pch = pnode->prx;
		//while (pch)
		//{
		//	kk_dtSubscribeCH((char*)rxDev, (char*)pch->name, NULL, NULL);
		//	pch = pch->next_ch;
		//}

	}
	//the rxDev unsubscribe channel from txDev.
	else {
		//Get txChannel name of txDev
		//malloc txBuffer
		uint16_t numTxName = dr_GetNumTxlabel(txDev);
		if (numTxName == 0) return;
		char** txName = (char**)malloc(sizeof(char*) * numTxName);
		for (int i = 0; i < numTxName; i++) txName[i] = (char*)malloc(sizeof(char) * LEN_NAME);
		//get txName
		for (int i = 0; i < numTxName; i++)
			sprintf(txName[i], "%s", dr_GetTxLabel(txDev, i));

		//unSubscribe specify txDevice
		for (int i = 0; i < numRxName; i++)
			for (int j = 0; j < numTxName; j++)
				if (dr_isSubscribe(rxDev, rxName[i], txDev, txName[j]))
					kk_dtSubscribeCH((char*)rxDev, rxName[i], NULL, NULL);

		//Free txName
		for (int i = 0; i < numTxName; i++)
			free(txName[i]);
		free(txName);

	}

	//Free rxName
	for (int i = 0; i < numRxName; i++)
		free(rxName[i]);
	free(rxName);
	return;
}


void dr_addListener(const char* broadcaster, const char* listener)//將broadcaster加入一個listener
{
	if (!broadcaster || !listener)
		return;
	if (strcmp(broadcaster, listener) == 0)
		return;

	//the listener subscribe channel of broadcaster 

	//Get rxChannel name of rxDev
	//malloc rxBuffer
	uint16_t numRxName = dr_GetNumRxName(listener);
	if (numRxName == 0) return;
	char** rxName = (char**)malloc(sizeof(char*) * numRxName);
	for (int i = 0; i < numRxName; i++) rxName[i] = (char*)malloc(sizeof(char) * LEN_NAME);
	//get rxName
	for (int i = 0; i < numRxName; i++)
		sprintf(rxName[i], "%s", dr_GetRxName(listener, i));


	//Get txChannel name of txDev
	//malloc txBuffer
	uint16_t numTxName = dr_GetNumTxlabel(broadcaster);
	if (numTxName == 0) return;
	char** txName = (char**)malloc(sizeof(char*) * numTxName);
	for (int i = 0; i < numTxName; i++) txName[i] = (char*)malloc(sizeof(char) * LEN_NAME);
	//get txName
	for (int i = 0; i < numTxName; i++)
		sprintf(txName[i], "%s", dr_GetTxLabel(broadcaster, i));

	//Subscribe specify txDevice
	for (int i = 0; i < numRxName && i < numTxName; i++)
		kk_dtSubscribeCH((char*)listener, rxName[i], (char*)broadcaster, txName[i]);

	//Free txName
	for (int i = 0; i < numTxName; i++)
		free(txName[i]);
	free(txName);

	//Free rxName
	for (int i = 0; i < numRxName; i++)
		free(rxName[i]);
	free(rxName);
	return;

}
void dr_leaveListener(const char* broadcaster, const char* listener)//將broadcaster移除一個listener。
{
	if (!broadcaster)
		return;
	if (listener)
		if (strcmp(broadcaster, listener) == 0)
			return;

	//the listener leave channel of broadcaster
	if (listener != NULL) {
		dr_unSubscribe(listener, broadcaster);
	}
	//remove all listener in broadcaster
	else {
		char name[LEN_NAME];
		kk_dtGetFirstDevice(name);
		while (strlen(name))
		{
			if (strcmp(broadcaster, name) != 0) {
				dr_unSubscribe(name, broadcaster);
			}
			kk_dtGetNextDevice(name);
		}
	}

	return;
}

/*
int numTxFlows = 0;
int numRxFlows = 0;
int maxTxFlow =2;
int maxRxFlow =2;

int mFlowTemp[128][8];

int solutionFlows[128][8];
int solutionBestValue=0;

//生成下一個節點
void addChToMFlows(){

//加入channel to multicast

//第一層
mflow[0] = { {1}, {2}, {3},. {n}}
//第二層 (第一組)
mflow[0] = { {1,2}, {1,3}, {1,4},. {1, n}}

mflow[0] = { {1}}
mflow[1] = { {2}, {3}, {4},{5}...{n}}

第三層 (第二層的第一組)
mflow[0] = { {1,2,3}, {1,2,4}, {1,2,4},. {1,n-1, n-1}}

mflow[0] = { {1}}
mflow[1] = { {2}, {3}, {4},{5}...{n}}

第四層.....
第N層

}

//
void undoChToMFlows(){

}

int calValue(){
//計算 成本
const int costMCh = 5;
const int costUCh = 0;

int totalMChannel = getTotalMChnnel();

int costM = costMCh * totalMChannel ;
int costU = costUCh;
int costTotal = costM + costU;

return costTotal
}


int dr_GetSoultionFlow() {


//檢查此組是否合理 (note* 只要符合,就是目前最佳解)
if( isValidFlows){
value = calCost();
return value;
}

//發聲端
if(numTxFlows >= maxTxFlows )
return 32767;

//訂閱端
for(check each rx flow of subscriber){
if(numRxFlows >= maxRxFlows )
return 32767;
}

//生成所有解點 第一層  將每個channel 加入flow 內
int solutionBestValue = 32767;
for(add each channel to flow){

//add channel to multicast Flow
addChToMFlows();

//遞迴 找解
int value = dr_GetSoultionFlow()

//紀錄最佳解
if ( value > solutionBestValue){
//紀錄這次解答
for(save each ch to solutionFlows[]){
solutionFlows =  mFlowTemp
solutionBestValue = value;
}
}

//undo channel
undoChToMFlows();
}

return solutionBestValue;

return 0;
}
*/
