/*****************************************************************************
| Copyright (c) Keiga Inc. All Rights Reserved.
|
| Project : amor
| FileName: kk.h
| Module  : No
|
| Author  : Dennis Lu
| Revise  :
| Version : 1.8.5.7
| Comment : 
|
\*****************************************************************************/

#ifndef _KK_H_
#define _KK_H_

#ifdef  __cplusplus
extern  "C" {
#endif

#include <winSock2.h>
#include <ws2tcpip.h>
#include <stdio.h>
#include <time.h>
#include <assert.h>

#ifdef WIN32
#define KKMSG		NULL //sprintf
#define SNPRINTF	_snprintf
#define STRCASECMP	_stricmp
#include <conio.h>
#else
#include <stdlib.h>
#define SNPRINTF snprintf
#endif

#define LEN_NAME				32		//32
#define LEN_MSG					128//2300 	//1536  //128

//msg. box
#define MSG_CLEAR				1
#define MSG_BOX					2
#define	MSG_HEX					3
#define	MSG_LOG					4

//subscriptions in file
#define SUB_INIT				1
#define SUB_LOAD				2
#define SUB_SAVE				3
#define SUB_DELETE				4

//AMP devices
#define AMP_INIT				1
#define AMP_SYSTEM				2
#define AMP_VOLUME				3
#define AMP_EQUALIZER			4
#define AMP_SUBWOOFER			5
#define AMP_POWER				6
#define AMP_DOLBY				7

//DT devices

#define DT_CHTYPE_NONE			0
#define DT_CHTYPE_CONTROL		1
#define DT_CHTYPE_METERING		2
#define DT_CHTYPE_STATUS		3
#define DT_CHTYPE_LOCAL			4
#define DT_CHTYPE_SERIAL		5
#define DT_CHTYPE_BROADCAST		6
#define DT_CHTYPE_VBROASCAST	7

typedef char kk_msgbuf_t[LEN_MSG];

typedef void kk_apReiveMsg_fn(char *name, char *msg, int len);

extern kk_msgbuf_t g_MsgBuf;



//-----------------------------------------------------------------------------
//module: alinkon AMP
//
void kk_InitKKAPI();
void kk_InitAMP();
bool kk_ampCheckPassword(char *gateway, char *account, char *password);

//tools
void kk_InitMsgBox(HWND dlg, int idc);
bool kk_logOpenFile();
void kk_logCloseFile();
void kk_logWriteMsg(char *msg);
void _kkmsg(int id, char *msg);


//-----------------------------------------------------------------------------
//module: dante DAPI v4
//
void kk_dtInit();
void kk_dtDBSetConfig(int ifn1, int ifn2);
void kk_dtDBGetConfig(int *pifn1, int *pifn2);
void kk_dtStart();
void kk_dtStop();

void kk_dtShowTXCH(HWND hdlg, int id, char *device);
void kk_dtShowRXCH(HWND hdlg, int id, char *device);
void kk_dtShowRX(HWND hdlg, int id, char *tx);//rx = all - tx
void kk_dtGetFirstDevice(char *name);
void kk_dtGetNextDevice(char *name);
void kk_dtSubscribeCH(char *rx, char *rxch, char *tx, char *txch);
void kk_dtSendOutConMsg(char *device, char *msg);
void kk_dtSendOutMonMsg(int chtype, char *msg);

void kk_dtReceiveMonMsgCB(char *name, char *msg, int len, int ch);

//-----------------------------------------------------------------------------
//module: winsocket v2.2
//
void kk_StartWSA();
void kk_StopWSA();


//-----------------------------------------------------------------------------
//module: application UI
//

void kk_apSetReceiveMsgCB(kk_apReiveMsg_fn *fn);

void kk_ParseCmd(char* name, char* msg, int len);

#ifdef  __cplusplus
}
#endif
#endif // _KK_H_