
/*****************************************************************************
| Copyright (c) Keiga Inc. All Rights Reserved.
|
| Project : amor
| FileName: dt_start.cpp
| Author  : Dennis Lu
| Revise  : 
| Version : 1.8.5.7
| Comment : 
\*****************************************************************************/

#include "stdafx.h"
#include "dt.h"
#include "kk.h"
#include "dt_extend_api.h"

//???
aud_error_t g_dtErr;
aud_errbuf_t g_dtErrBuf;

//global
dapi_t *g_dtAPI; //dante API
aud_env_t *g_dtENV; //environment
dante_runtime_t *g_dtRT; //runtime
dante_domain_handler_t *g_dtHDR; //handler
db_browse_t	*g_dtDB;
dr_devices_t *g_dtDR;
DANTENODE *g_dtHead;
HANDLE g_dtThread;
db_browse_config_t g_dbConfig;

//extern.conmon
conmon_vendor_id_t g_tx_vendor_id = { 0x4B, 0x65, 0x69, 0x67, 0x61, 0x00, 0x00, 0x00 };
conmon_vendor_id_t *g_rx_vendor_id = NULL;
conmon_client_request_id_t g_idSubStatus, g_idConMon;
register_msg_t g_dtCHmsg[CONMON_NUM_CHANNELS];
conmon_client_t *g_dtClient = NULL;
DEVICEREQUEST g_dtRequests[MAX_REQUESTS];

name_map_t CHANNEL_NAMES[] =
{
	{ CONMON_CHANNEL_TYPE_CONTROL,			"CONTRO" },
	{ CONMON_CHANNEL_TYPE_METERING,			"METERING" },
	{ CONMON_CHANNEL_TYPE_STATUS,			"STATUS" },
	{ CONMON_CHANNEL_TYPE_BROADCAST,		"BROADCAST" },
	{ CONMON_CHANNEL_TYPE_LOCAL,			"LOCA" },
	{ CONMON_CHANNEL_TYPE_SERIAL,			"SERIA" },
	{ CONMON_CHANNEL_TYPE_KEEPALIVE,		"KEEPALIVE" },
	{ CONMON_CHANNEL_TYPE_VENDOR_BROADCAST,	"VBROADCAST" },
	{ CONMON_CHANNEL_TYPE_MONITORING,		"MONITORING" }
};

//-----------------------------------------------------------------------------
// dante initial & setting
//
void dt_InitDAPI() {
	db_browse_config_init_defaults(&g_dbConfig);
	/*
	static PNETINTERFACE p = NULL, nInterface = NULL;
	nInterface = db_GetNetworkInterface();
	p = nInterface;
	while (p) {
		if (p->type == IF_TYPE_ETHERNET_CSMACD) {
			dt_SetDBConfig(p->idxIf, 0);
			break;
		}
	}
	if (nInterface) {
		p = nInterface->nextNet;
		while (p) {
			p = nInterface->nextNet;
			free(nInterface);
			nInterface = p;
		}
		nInterface = NULL;
	}
	*/
}

void dt_SetDBConfig(int ifn1, int ifn2) {
	const int def_InterfaceIndexAny = 0; // kDNSServiceInterfaceIndexAny

										 //default
	if (ifn1 == def_InterfaceIndexAny) {
		db_browse_config_init_defaults(&g_dbConfig);
		return;
	}

	if (ifn1 == ifn2 ||
		ifn2 == def_InterfaceIndexAny) {
		//setting number interface
		g_dbConfig.num_interface_indexes = 1;
		//setting interface 1
		g_dbConfig.interface_indexes[0] = ifn1;
		g_dbConfig.interface_indexes[1] = 0;
	}
	else {
		//setting number interface
		g_dbConfig.num_interface_indexes = 2;
		//setting interface 1/2
		g_dbConfig.interface_indexes[0] = ifn1;
		g_dbConfig.interface_indexes[1] = ifn2;
	}
}

void dt_GetDBConfig(int* pifn1, int* pifn2) {
	*pifn1 = g_dbConfig.interface_indexes[0];
	*pifn2 = g_dbConfig.interface_indexes[1];
}
//-----------------------------------------------------------------------------
// dante started
// 
void dt_StartDAPI()
{
	aud_error_t err;
	aud_errbuf_t errbuf;

	//default
	g_dtENV = NULL;
	g_dtDB = NULL;
	g_dtDR = NULL;
	g_dtHead = NULL;
	g_dtThread = NULL;
	g_idConMon = CONMON_CLIENT_NULL_REQ_ID;
	g_idSubStatus = CONMON_CLIENT_NULL_REQ_ID;
	memset(&g_dtRequests, 0, sizeof(DEVICEREQUEST));

	//browse object
//	db_browse_config_t config;
//	db_browse_config_init_defaults(&config);

//	memcpy(&config, &g_dbConfig, sizeof(db_browse_config_t));


	//dapi environment
	err = dapi_new(&g_dtAPI);
	if (err != AUD_SUCCESS)
	{
		KKMSG(g_MsgBuf, "ERR- init environment (%s)", aud_error_message(err, errbuf));
		_kkmsg(MSG_BOX, NULL);
		return;
	}
	g_dtENV = dapi_get_env(g_dtAPI);
	g_dtRT = dapi_get_runtime(g_dtAPI);
//	g_dtHDR = dapi_get_domain_handler(g_dtAPI);
	assert(g_dtAPI);
	assert(g_dtENV);
	assert(g_dtRT);
//	assert(g_dtHDR);

	//new browse object (types specified)
	db_browse_types_t types = DB_BROWSE_TYPE_CONMON_DEVICE;
	/*
	err = db_browse_new(g_dtENV, types, &g_dtDB);
	if (err != AUD_SUCCESS)
	{
		KKMSG(g_MsgBuf, "Error creating browse: %s\n", aud_error_message(err, errbuf));
		_kkmsg(MSG_BOX, NULL);
		return;
	}
	*/
	
	err = db_browse_new_dapi(g_dtAPI, types, &g_dtDB);
	if (err != AUD_SUCCESS)
	{
		KKMSG(g_MsgBuf, "ERR- create browse with type specified, %s", aud_error_message(err, errbuf));
		_kkmsg(MSG_BOX, NULL);
		return;
	}
	
	//maximum sockets
	err = db_browse_set_max_sockets(g_dtDB, MAX_SOCKETS);
	if (err != AUD_SUCCESS)
	{
		KKMSG(g_MsgBuf, "ERR- set maximun of sockets to %d, %s", MAX_SOCKETS, aud_error_message(err, errbuf));
		_kkmsg(MSG_BOX, NULL);
		return;
	}

	//node callback
	db_browse_set_node_changed_callback(g_dtDB, db_NodeChangedCB);


	//start browse
	err = db_browse_start_config(g_dtDB, &g_dbConfig);
	if (err != AUD_SUCCESS)
	{
		KKMSG(g_MsgBuf, "ERR- start browsing, %s", aud_error_message(err, errbuf));
		_kkmsg(MSG_BOX, NULL);
		return;
	}

	//create a devices structure and set its options
	
	err = dr_devices_new_dapi(g_dtAPI, &g_dtDR);
	if (err != AUD_SUCCESS)
	{
		KKMSG(g_MsgBuf, "ERR- create device factory, %s", aud_error_message(err, errbuf));
		_kkmsg(MSG_BOX, NULL);
		return;
	}
	

	//set context
	//dante_domain_handler_set_context(g_dtHDR, void *context);

	//start thread
	g_dtThread = CreateThread(NULL, 0, (LPTHREAD_START_ROUTINE)dt_RuntimeSockets, 0, 0, NULL);
}

//-----------------------------------------------------------------------------
// only thread in dante network
// 
DWORD WINAPI dt_RuntimeSockets(LPVOID lpa)
{
	aud_error_t err;
	aud_errbuf_t errbuf;

	aud_utime_t my_timeout;
	aud_socket_t in_sock;
	dante_sockets_t *out_sockets, step_sockets;

	in_sock = AUD_SOCKET_INVALID;
	out_sockets = &step_sockets;
	my_timeout.tv_sec = 1;
	my_timeout.tv_usec = 0;
	while (g_dtThread)
	{
		dante_sockets_clear(out_sockets);

		err = dante_runtime_get_sockets_and_timeout(g_dtRT, out_sockets, &my_timeout);
		if (err != AUD_SUCCESS)
		{
			KKMSG(g_MsgBuf, "ERR- get runtime sockets, %s", aud_error_message(err, errbuf));
			_kkmsg(MSG_BOX, NULL);
			return err;
		}

		if (in_sock != AUD_SOCKET_INVALID)
		{
			//add input stream to our list of sockets
			dante_sockets_add_read(out_sockets, in_sock);
		}

		if (my_timeout.tv_sec >= 1)
		{
			my_timeout.tv_sec = 1;
			my_timeout.tv_usec = 0;
		}
		else if (my_timeout.tv_sec == 0 && my_timeout.tv_usec == 0)
		{
			_kkmsg(MSG_BOX, "timeout zero");
		}

		if (out_sockets->n == 0)
		{
			DWORD result;
			int ms = (my_timeout.tv_sec * 1000) + ((my_timeout.tv_usec + 999) / 1000);
			result = SleepEx(ms, TRUE);
			if (result)
			{
				return aud_error_from_system_error(result);
			}
		}
		else
		{
			int select_result = select(out_sockets->n, &out_sockets->read_fds, &out_sockets->write_fds, NULL, &my_timeout);
			if (select_result < 0)
			{
				err = aud_error_get_last();
				KKMSG(g_MsgBuf, "ERR- processing sockets: %s (%d)", aud_error_get_name(err), err);
				_kkmsg(MSG_BOX, NULL);
				return err;
			}
		}

		dante_runtime_process_with_sockets(g_dtRT, out_sockets);
	}//while(1)

	return 0;
}

//-----------------------------------------------------------------------------
// dante closed
// 
void dt_StopDAPI()
{
	PDANTENODE pnode;
	PNODECHANNEL pch;
	
	if (g_dtThread) {
		CloseHandle(g_dtThread);
		g_dtThread = NULL;
	}
	g_idConMon = CONMON_CLIENT_NULL_REQ_ID;

	//free nodes
	pnode = g_dtHead;
	while (g_dtHead)
	{

		pnode = g_dtHead->next_node;
		free(g_dtHead);
		g_dtHead = pnode;
	}

	if (g_dtClient) conmon_client_delete(g_dtClient);
	if (g_dtDB) db_browse_delete(g_dtDB);
	if (g_dtAPI) dapi_delete(g_dtAPI);
}

PDANTENODE dt_FindNode(const char *name)
{
	PDANTENODE p;

	p = g_dtHead;
	if (name == NULL || strlen(name) == 0)
		return NULL;
	while (p && strcmp(p->name, name) != 0)
	{
		p = p->next_node;
	}

	if (!p)
	{
		KKMSG(g_MsgBuf, "ERR- cannot find the node (%s)", name);
		_kkmsg(MSG_LOG, NULL);
		return NULL;
	}
	return p;
}

PDEVICEINFO dt_GetDeviceInfo(const char *name)
{
	PDANTENODE p;
	PDEVICEINFO info;

	//default
	memset(&info, 0, sizeof(DEVICEINFO));

	p = dt_FindNode(name);
	info->dname = db_browse_device_get_default_name(p->db_device);
	info->types = db_browse_device_get_browse_types(p->db_device);

	//network interface ?
	//unsigned int nn = db_browse_num_interface_indexes(browse);
	db_browse_types_t network_types = db_browse_device_get_browse_types_on_network(p->db_device, 0);

	//localhost ?
	//aud_bool_t localhost = db_browse_using_localhost(browse);
	db_browse_types_t localhost_types = db_browse_device_get_browse_types_on_localhost(p->db_device);

	if (info->types & DB_BROWSE_TYPE_MEDIA_DEVICE)
	{
		// The media (audio) routing interface of a Dante device.
		const dante_version_t *router_version = db_browse_device_get_router_version(p->db_device);
		const dante_version_t *arcp_version = db_browse_device_get_arcp_version(p->db_device);
		const dante_version_t *arcp_min_version = db_browse_device_get_arcp_min_version(p->db_device);
		const char *router_info = db_browse_device_get_router_info(p->db_device);
	}

	if (info->types & DB_BROWSE_TYPE_SAFE_MODE_DEVICE)
	{
		//devices that have reverted to 'safe' mode.
		uint16_t version = db_browse_device_get_safe_mode_version(p->db_device);
	}

	if (info->types & DB_BROWSE_TYPE_UPGRADE_MODE_DEVICE)
	{
		//devices that have reverted to 'upgrade' mode.
		uint16_t version = db_browse_device_get_upgrade_mode_version(p->db_device);
	}

	if (info->types & DB_BROWSE_TYPE_CONMON_DEVICE)
	{
		//the control interface of a Dante device.
		const conmon_instance_id_t *instance_id = db_browse_device_get_instance_id(p->db_device);
		const uint8_t *d = instance_id->device_id.data;
		info->vid = db_browse_device_get_vendor_id(p->db_device);
		uint32_t vendor_broadcast_address = db_browse_device_get_vendor_broadcast_address(p->db_device);

		if (vendor_broadcast_address)
		{
			uint8_t * a = (uint8_t *)&vendor_broadcast_address;
			sprintf(g_MsgBuf, "Multicast address = %u.%u.%u.%u", a[0], a[1], a[2], a[3]);
		}
	}

	if (info->types & (DB_BROWSE_TYPE_CONMON_DEVICE | DB_BROWSE_TYPE_MEDIA_DEVICE))
	{
		info->mfid = db_browse_device_get_manufacturer_id(p->db_device);
		info->model = db_browse_device_get_model_id(p->db_device);
	}

	if (info->types & DB_BROWSE_TYPE_VIA_DEVICE)
	{
		const dante_version_t *via_min_version = db_browse_device_get_via_min_version(p->db_device);
		const dante_version_t *via_curr_version = db_browse_device_get_via_curr_version(p->db_device);
		const uint16_t via_port = db_browse_device_get_via_port(p->db_device);
	}

	return info;
}

