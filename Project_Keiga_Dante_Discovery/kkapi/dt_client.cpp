/*****************************************************************************
| Copyright (c) Keiga Inc. All Rights Reserved.
|
| Project : amor
| FileName: dt_client.cpp
| Author  : Dennis Lu
| Revise  :
| Version : V1.8.5.7
| Comment :
\*****************************************************************************/

#include "stdafx.h"
//#include "ap_main.h"
//#include "define.h"
#include "dt.h"
#include "kk.h"

void dc_NotifyEventCB(const conmon_client_event_t *ev)
{
	conmon_client_event_flags_t flags = conmon_client_event_get_flags(ev);

	_kkmsg(MSG_LOG, "CB:got a ConMon client event");
	if (flags & CONMON_CLIENT_EVENT_FLAG__LOCAL_READY_CHANGED)
	{
		//setup for TX messages (outgoing)
		if (conmon_client_is_local_ready(g_dtClient))
		{
			dc_SetupLocalRegistration();
		}
	}

	if (flags & CONMON_CLIENT_EVENT_FLAG__REMOTE_READY_CHANGED)
	{
		//setup for RX messages (incoming)
		if (conmon_client_is_remote_ready(g_dtClient))
		{
			dc_SetupRemoteRegistration();
			dc_SetupRemoteSubscribeAll();
		}
	}
}

void dc_SetupLocalRegistration()
{
	aud_error_t err;
	aud_errbuf_t errbuf;

	err = dc_RegisterOutgoingCHmsg(CONMON_CHANNEL_TYPE_LOCAL, true);
	if (err != AUD_SUCCESS)
	{
		sprintf(g_MsgBuf, "ERR:register LOCAL outgoing message(%s)",aud_error_message(err, errbuf));
		_kkmsg(MSG_LOG, NULL);
		return;
	}

	err = dc_RegisterOutgoingCHmsg(CONMON_CHANNEL_TYPE_STATUS, true);
	if (err != AUD_SUCCESS)
	{
		sprintf(g_MsgBuf, "ERR:register STATUS outgoing message(%s)", aud_error_message(err, errbuf));
		_kkmsg(MSG_LOG, NULL);
		return;
	}

	err = dc_RegisterOutgoingCHmsg(CONMON_CHANNEL_TYPE_BROADCAST, true);
	if (err != AUD_SUCCESS)
	{
		sprintf(g_MsgBuf, "ERR:register BROADCAST outgoing message(%s)", aud_error_message(err, errbuf));
		_kkmsg(MSG_LOG, NULL);
		return;
	}
}

void dc_SetupRemoteRegistration()
{
	aud_error_t err;
	aud_errbuf_t errbuf;

	err = dc_RegisterIncomingCHmsg(CONMON_CHANNEL_TYPE_STATUS, true);
	if (err != AUD_SUCCESS)
	{
		sprintf(g_MsgBuf, "ERR:register STATUS incoming message(%s)", aud_error_message(err, errbuf));
		_kkmsg(MSG_LOG, NULL);
		return;
	}

	err = dc_RegisterIncomingCHmsg(CONMON_CHANNEL_TYPE_BROADCAST, true);
	if (err != AUD_SUCCESS)
	{
		sprintf(g_MsgBuf, "ERR:register BROADCAST incoming message(%s)", aud_error_message(err, errbuf));
		_kkmsg(MSG_LOG, NULL);
		return;
	}
}

void dc_SetupRemoteSubscribeAll()
{
	aud_error_t err;
	aud_errbuf_t errbuf;

	err = dc_SubscribeStatusMsg(true, NULL);
	if (err != AUD_SUCCESS)
	{
		sprintf(g_MsgBuf, "ERR-subscribe STATUS message from ALL devices(%s)", aud_error_message(err, errbuf));
		_kkmsg(MSG_LOG, NULL);
	}
}

//-----------------------------------------------------------------------------
// init ConMon client to connect server
//
void dt_InitConMonClient()
{
	aud_error_t err;
	aud_errbuf_t errbuf;

	//domain info.
	_kkmsg(MSG_LOG, "created Domain Handler, current state is ?");
	_kkmsg(MSG_LOG, "current domain configuration");

	//new config
	conmon_client_config_t *config = NULL;
	config = conmon_client_config_new("Audinate");
	if (!config)
	{
		_kkmsg(MSG_LOG, "ERR- create conmon-client config");
		return;
	}

	//multicast address for the vendor-specific broadcast channel endpoint
//	uint32_t vendor_broadcast_address = 0;// 0xeffe327b; //IP=239.254.50.122(0xeffe327a)
//  uint8_t *ip = (uint8_t *)&vendor_broadcast_address;
//	sprintf(g_MsgBuf, "multicast address (%u.%u.%u.%u)", ip[0], ip[1], ip[2], ip[3]);
//	_kkmsg(MSG_BOX, NULL);

	uint16_t server_port = 0;
	conmon_client_config_set_server_port(config, server_port);
//	if (vendor_broadcast_address)
//	{
//		conmon_client_config_set_vendor_broadcast_channel_enabled(config, AUD_TRUE);
//		conmon_client_config_set_vendor_broadcast_channel_address(config, vendor_broadcast_address);
//	}

	//new client handle
	err = conmon_client_new_dapi(g_dtAPI, config, &g_dtClient);
	conmon_client_config_delete(config);
	config = NULL;
	if (err != AUD_SUCCESS)
	{
		sprintf(g_MsgBuf, "ERR- creating client (%s)", aud_error_message(err, errbuf));
		_kkmsg(MSG_LOG, NULL);
		return;
	}

	//set context
	//void *my_data = NULL;
	//conmon_client_set_context(g_dtcmClient, my_data);

	//set callbacks
//	conmon_client_set_connection_state_changed_callback(g_dtClient, dc_StateChangedCB);
//	conmon_client_set_sockets_changed_callback(g_dtClient, dc_SocketChangedCB);
//	conmon_client_set_networks_changed_callback(g_dtClient, dc_NetworkChangedCB);
//	conmon_client_set_dante_device_name_changed_callback(g_dtClient, dc_DeviceNameChangedCB);
//	conmon_client_set_dns_domain_name_changed_callback(g_dtClient, dc_DomainNameChangedCB);
//	conmon_client_set_subscriptions_changed_callback(g_dtClient, dc_SubscriptionChangedCB);
	conmon_client_set_event_callback(g_dtClient, dc_NotifyEventCB);

	//client connect to server
	aud_bool_t auto_connect = AUD_FALSE;
	if (auto_connect)
	{
		err = conmon_client_auto_connect(g_dtClient);
		if (err != AUD_SUCCESS)
		{
			sprintf(g_MsgBuf, "ERR- enable auto-connect (%s)", aud_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			return;
		}
		sprintf(g_MsgBuf, "auto-connect, request id (%p)", g_idConMon);
		_kkmsg(MSG_BOX, NULL);

		while (conmon_client_state(g_dtClient) != CONMON_CLIENT_CONNECTED)
		{
			conmon_example_sleep(&ONE_MS); // just to yield...
			conmon_client_process(g_dtClient);
		}
	}
	else
	{
		err = conmon_client_connect(g_dtClient, &dc_ConnectResponseCB, &g_idConMon);
		if (err != AUD_SUCCESS)
		{
			sprintf(g_MsgBuf, "ERR- connecting client (%s)", aud_error_message(err, errbuf));
			_kkmsg(MSG_LOG, NULL);
			return;
		}
		sprintf(g_MsgBuf, "client connected, request id (%p)", g_idConMon);
		_kkmsg(MSG_BOX, NULL);

		while (g_idConMon != CONMON_CLIENT_NULL_REQ_ID)
		{
			conmon_example_sleep(&ONE_MS); // just to yield...
			if(g_idConMon != CONMON_CLIENT_NULL_REQ_ID)
			conmon_client_process(g_dtClient);
		}
		return;
	}

	if (conmon_client_state(g_dtClient) == CONMON_CLIENT_CONNECTED)
	{
		const dante_version_t * ver = conmon_client_get_server_version(g_dtClient);
		const dante_version_build_t * ver_build = conmon_client_get_server_version_build(g_dtClient);
		sprintf(g_MsgBuf, "client connect to server, version (%u.%u.%u.%u)", ver->major, ver->minor, ver->bugfix, ver_build->build_number);
		_kkmsg(MSG_BOX, NULL);
	}
	else
	{
		MessageBox(NULL, "ERR- aborting to connect", NULL, MB_OK);
		sprintf(g_MsgBuf, "ERR- aborting to connect");
		_kkmsg(MSG_LOG, NULL);
		return;
	}
}

void dc_ShowAllSubscriptions()
{
	uint16_t n, max;
		
	max = conmon_client_max_subscriptions(g_dtClient);
	sprintf(g_MsgBuf, "Max subscriptions: %d", max);
	_kkmsg(MSG_LOG, NULL);

	for (n = 0; n < max; n++)
	{
		const conmon_client_subscription_t *sub = conmon_client_subscription_at_index(g_dtClient, n);
		if (sub)
		{
			dc_Subscription(sub);
		}
	}
}

void dc_Subscription(const conmon_client_subscription_t *sub)
{
	unsigned int n;
	char buf[64];
	uint16_t active = conmon_client_subscription_get_connections_active(sub);
	uint16_t available = conmon_client_subscription_get_connections_available(sub);

	sprintf(g_MsgBuf, "%s@%s: status=%s id=%s connections=",
					 typeString(conmon_client_subscription_get_channel_type(sub)),
					 conmon_client_subscription_get_device_name(sub),
					 dt_cmStringRXstatus(conmon_client_subscription_get_rxstatus(sub)),
					 dt_cmStringInstanceID(conmon_client_subscription_get_instance_id(sub), buf, 64));

	for (n = 0; n < CONMON_MAX_NETWORKS; n++)
	{
		if (available & (1 << n))
		{
			sprintf(g_MsgBuf, "%s%c", g_MsgBuf, (active & (1 << n)) ? '+' : '-');
			_kkmsg(MSG_LOG, NULL);
		}
	}
}

void dc_ClientInfo()
{
	char device[BUFSIZ] = { 'X', '2', '2','0'};
	unsigned int process_id = 0;

	// id -> name
	const char *name;
	conmon_instance_id_t instance_id;
	if (!conmon_device_id_from_str(&instance_id.device_id, device))
	{
		_kkmsg(MSG_LOG, "Invalid device id format");
	}

	instance_id.process_id = (conmon_process_id_t)process_id;
	name = conmon_client_device_name_for_instance_id(g_dtClient, &instance_id);

	if (name)
	{
		sprintf(g_MsgBuf, "Device '%s' has instance id %s/%04d", name, device, process_id);
		_kkmsg(MSG_LOG, NULL);
	}
	else
	{
		sprintf(g_MsgBuf, "No known device with instance id %s/%04d", device, process_id);
		_kkmsg(MSG_LOG, NULL);
	}
}

//-----------------------------------------------------------------------------
// TBD below functions:
//
void dc_StateChangedCB(conmon_client_t *client)
{
	sprintf(g_MsgBuf, "Connection state changed - %s", dt_cmStringState(conmon_client_state(client)));
}

void dc_NetworkChangedCB(conmon_client_t *client)
{
	char buf[BUFSIZ];
	sprintf(g_MsgBuf, "Networks changed - %s", dt_cmStringNetworks(conmon_client_get_networks(client), buf, sizeof(buf)));
}

void dc_SocketChangedCB(conmon_client_t *client)
{
	sprintf(g_MsgBuf, "Conmon client sockets changed - %s", conmon_client_get_dante_device_name(client));
}

void dc_DeviceNameChangedCB(conmon_client_t *client)
{
	sprintf(g_MsgBuf, "Dante device name changed - %s", conmon_client_get_dante_device_name(client));
}

void dc_DomainNameChangedCB(conmon_client_t *client)
{
	sprintf(g_MsgBuf, "DNS domain name changed - %s", conmon_client_get_dns_domain_name(client));
}

void dc_SubscriptionChangedCB(conmon_client_t *client, unsigned int num_changes, const conmon_client_subscription_t *const *changes)
{
	aud_utime_t now;
	unsigned int i;
	AUD_UNUSED(client);

	aud_utime_get(&now);
	sprintf(g_MsgBuf, "%u.%u: Subscriptions changed - %d", (unsigned int)now.tv_sec, (unsigned int)now.tv_usec, num_changes);

	for (i = 0; i < num_changes; i++)
	{
		dc_Subscription(changes[i]);
	}
}

