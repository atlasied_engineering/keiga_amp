
/*****************************************************************************
| Copyright (c) Keiga Inc. All Rights Reserved.
|
| Project : amor
| FileName: kk_api.cpp
| Author  : Dennis Lu
| Revise  : 
| Version : 1.8.5.7
| Comment : 
\*****************************************************************************/

#include "stdafx.h"
#include <tchar.h>
//#include "cJSON.h"
#include "ip.h"
#include "dt.h"
#include "kk.h"

kk_msgbuf_t g_MsgBuf;
HWND g_MsgDlg;
int g_MsgIDC;

FILE *fpMsg;
char logdate[10];

//callbacks
static kk_apReiveMsg_fn *kk_ap_cbReiveMsg = NULL;

//-----------------------------------------------------------------------------
//module: alinkon AMP
//
void kk_InitKKAPI()
{
	memset(g_MsgBuf, 0, LEN_MSG);
	g_MsgDlg = NULL;
	g_MsgIDC = 0;
}

void kk_InitAMP()
{
}


bool kk_ampCheckPassword(char *gateway, char *account, char *password)
{
	return true;
}

void kk_dtSendOutMonMsg(int chtype, char *msg)
{
	aud_error_t err;
	conmon_channel_type_t type;

	if (!strlen(msg))
	{
		MessageBox(NULL, "No message data!", "Error", MB_OK);
		return;
	}

	if (chtype == DT_CHTYPE_NONE)
	{
		MessageBox(NULL, "No channel type!", "Error", MB_OK);
		return;
	}
	else if (chtype == DT_CHTYPE_LOCAL) type = CONMON_CHANNEL_TYPE_LOCAL;
	else if (chtype == DT_CHTYPE_BROADCAST) type = CONMON_CHANNEL_TYPE_BROADCAST;
	else
	{
		MessageBox(NULL, "Not yet!", "Error", MB_OK);
		return;
	}

	err = dc_SendMonitoringMsg(type, msg);
}

void kk_dtSendOutConMsg(char *device, char *msg)
{
	aud_error_t err;

	if (!strlen(msg))
	{
		MessageBox(NULL, "No message data!", "Error", MB_OK);
		return;
	}

	if (device && !STRCASECMP(device, "None"))
	{
		MessageBox(NULL, "please connect to gateway for sending data!", "Error", MB_OK);
		return;
	}

	err = dc_SendDeviceConMsg(device, msg);
}

//tools
void kk_InitMsgBox(HWND dlg, int idc)
{
	g_MsgDlg = dlg;
	g_MsgIDC = idc;
}

bool kk_logOpenFile()
{
	char today[10];
	char str[20];
	HANDLE hfile;
	char path[MAX_PATH] = _T("");
	char currentPath[MAX_PATH] = _T("");
	WIN32_FIND_DATA ffd;
	errno_t err;

	_strdate(today);
	strcpy(logdate, today);
	//mbstowcs(str, today, strlen(today) + 1);
	sprintf(str, _T("20%c%c%c%c%c%c-%s"), str[6], str[7], str[0], str[1], str[3], str[4], "kklog.txt");

	_getcwd(currentPath, MAX_PATH);
	sprintf(path, "%s%s%s", currentPath, "\\log\\", str);
	hfile = FindFirstFile(path, &ffd);
	if (hfile != INVALID_HANDLE_VALUE)
	{
		//file exist and append data
		err = fopen_s(&fpMsg, path, "a+");
	}
	else
	{
		//new folder
		sprintf(currentPath, "%s%s", currentPath, "\\log");
		err = _mkdir(currentPath);

		//new file
		err = fopen_s(&fpMsg, path, "a+");
	}
	return TRUE;
}

void kk_logCloseFile()
{
	if (fpMsg) fclose(fpMsg);
}

void kk_logWriteMsg(char *msg)
{
	char str[128];
	char today[10], log[128]; //log[128];
	
	char nowtime[10], oldtime[10];

	_strdate(today);
	if (strcmp(today, logdate))
	{
		kk_logCloseFile();
		kk_logOpenFile();
	}

	_strtime_s(nowtime, 10);
	sprintf(log, "%s %s\n", nowtime, msg);

	fseek(fpMsg, 0, SEEK_SET);
	fwrite(log, strlen(log), 1, fpMsg);
}

void _kkmsg(int id, char *msg)
{
	//if dialog handle is NULL, all messages will be written to log file
	if (!g_MsgDlg) id = MSG_LOG;

	switch (id)
	{
	case MSG_CLEAR:
		SendDlgItemMessage(g_MsgDlg, g_MsgIDC, LB_RESETCONTENT, 0, 0);
		break;
	case MSG_BOX:
		msg ? SendDlgItemMessage(g_MsgDlg, g_MsgIDC, LB_ADDSTRING, 0, (LPARAM)msg) : SendDlgItemMessage(g_MsgDlg, g_MsgIDC, LB_ADDSTRING, 0, (LPARAM)g_MsgBuf);
		break;
	case MSG_LOG:
		//msg ? kk_logWriteMsg(msg) : kk_logWriteMsg(g_MsgBuf);
		break;
	case MSG_HEX:
		//tbd
		break;
	default:
		break;
	}
}

//-----------------------------------------------------------------------------
//module: dante DAPI v4
//
void kk_dtInit() {
	dt_InitDAPI();
}

void kk_dtDBSetConfig(int ifn1, int ifn2) {
	dt_SetDBConfig(ifn1, ifn2);
}
void kk_dtDBGetConfig(int *pifn1, int *pifn2) {
	dt_GetDBConfig(pifn1, pifn2);
}

void kk_dtStart()
{
	
	dt_StartDAPI();
	
	dt_InitConMonClient();
	//dt_InitBrowsing();
	
	dt_SetReceiveMsgCB(kk_dtReceiveMonMsgCB);
	//dt_InitRouting();
}

void kk_dtStop()
{
	dt_StopDAPI();
}

void kk_dtReceiveMonMsgCB(char *name, char *msg, int len, int ch)
{
	char type[LEN_NAME];

	if (ch == DT_CHTYPE_STATUS) strcpy(type, "STATUS");
	else if (ch == DT_CHTYPE_LOCAL) strcpy(type, "LOCA");
	else if (ch == DT_CHTYPE_BROADCAST) strcpy(type, "BROADCAST");

	sprintf(g_MsgBuf, "Receive %s message (%d bytes):",type,len);
	_kkmsg(MSG_BOX, NULL);

	kk_ParseCmd(name, msg, len);

}

void kk_dtShowTXCH(HWND hdlg, int id, char *device)
{
	PDANTENODE pnode;
	PNODECHANNEL pch;

	//reset tx channels
	SendDlgItemMessage(hdlg, id, LB_RESETCONTENT, 0, 0);

	//find device node
	pnode = dt_FindNode(device);
	if (!pnode) return;

	//fill tx labels or channels, then point to the first item
	pch = pnode->ptx;
	while (pch)
	{
		if (strcmp(pch->label, ""))
		{
			SendDlgItemMessage(hdlg, id, LB_ADDSTRING, 0, (LPARAM)pch->label);
		}
		else
		{
			SendDlgItemMessage(hdlg, id, LB_ADDSTRING, 0, (LPARAM)pch->name);
		}
		pch = pch->next_ch;
	}
	SendDlgItemMessage(hdlg, id, LB_SETCURSEL, 0, 0);
}

void kk_dtShowRXCH(HWND hdlg, int id, char *device)
{
	PDANTENODE pnode;
	PNODECHANNEL pch;

	//find device node
	pnode = dt_FindNode(device);
	if (!pnode) return;

	pch = pnode->prx;
	while (pch)
	{
		SendDlgItemMessage(hdlg, id, LB_ADDSTRING, 0, (LPARAM)pch->name);
		pch = pch->next_ch;
	}
	SendDlgItemMessage(hdlg, id, LB_SETCURSEL, 0, 0);
}

void kk_dtShowRX(HWND hdlg, int id, char *tx)
{
	PDANTENODE pnode;

	pnode = g_dtHead;
	while (pnode)
	{
		if (strcmp(pnode->name, tx) != 0)
		{
			if (pnode->active)
			{
				SendDlgItemMessage(hdlg, id, LB_ADDSTRING, 0, (LPARAM)pnode->name);
			}
		}
		pnode = pnode->next_node;
	}
}

void kk_dtSubscribeCH(char *rx, char *rxch, char *tx, char *txch)
{
	dr_SubscribeChannel(rx, rxch, tx, txch);
}

void kk_dtGetFirstDevice(char *name)
{
	PDANTENODE p;

	p = g_dtHead;
	while (p)
	{
		strcpy(name, p->name);
		return;
		/*
		if (p->active)
		{
			strcpy(name, p->name);
			return;
		}
		else {
			p = p->next_node;
		}
		*/
	}
	strcpy(name, "");
}

void kk_dtGetNextDevice(char *name)
{
	PDANTENODE p;

	p = dt_FindNode(name);
	if (!p)
	{
		KKMSG(g_MsgBuf, "ERR- no this node (%s)", name);
		_kkmsg(MSG_BOX, NULL);
		strcpy(name, "");
		return;
	}

	p = p->next_node;
	while (p)
	{
		strcpy(name, p->name);
		return;
		/*
		if (p->active)
		{
			strcpy(name, p->name);
			return;
		}
		else {
			p = p->next_node;
		}
		*/
	}
	strcpy(name, "");
}

//-----------------------------------------------------------------------------
//module: winsocket v2.2
//
void kk_StartWSA()
{
}

void kk_StopWSA()
{
}

//-----------------------------------------------------------------------------
//module: application UI
//

void kk_apSetReceiveMsgCB(kk_apReiveMsg_fn *fn)
{
	kk_ap_cbReiveMsg = fn;
}

void kk_ParseCmd(char* name, char* msg, int len) {
	
	//DO Some thing


	kk_ap_cbReiveMsg(name, msg, len);

	/*
	uint8_t Device[32] = "RM12";

	//Parse CMD to Save Parameter Data


	char* json_data = "{\"Info\":{\"AMP\":\"RM12E\",\"DSP_Version\":\"V18.5.2.1\",\"Dongle_Version\":\"V18.8.28.1\",\"Dongle_Name\":\"Good\",\"DC_Trigger\":true,\"Auto_Off_Switch\":false,\"Auto_Off_Time\":1,\"Auto_On_Switch\":false,\"Auto_On_Sensitivity\":1,\"GEQ_Count\":0,\"Online_People\":1}}";

	//jSon test
	cJSON *root, *jInfo, *jArray;
	char *cAMP = NULL, *cInfo = NULL;
	root = cJSON_Parse(json_data);
	jInfo = cJsonFun.getObjectItem(root, "Info");
	cAMP = cJsonFun.getString(jInfo, "AMP");

	//jSon combine
	cJSON *root_combine = cJSON_CreateObject();
	cJsonFun.putOpt_String(root_combine, "str1", "test1");
	cJsonFun.putOpt_Double(root_combine, "num", 30);
	cJsonFun.putOpt_Boolean(root_combine, "sw", cJSON_True);
	char* json_combine_data = cJSON_Print(root_combine);

	//cArray1 = cJSON_Print(jArray1);
	return;
	*/
}