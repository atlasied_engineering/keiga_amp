﻿#pragma once

#ifndef _INTERNET_COMMUNICATION_H_
#define _INTERNET_COMMUNICATION_H_

#ifdef __cplusplus
extern "C" {
#endif // !__cplusplus



#include <Winsock2.h>
#include <Ws2tcpip.h>
#include <IPHlpApi.h>
#include <stdint.h>
#include <stdio.h>

#pragma comment(lib, "iphlpapi.lib")
#pragma comment(lib, "ws2_32.lib")


typedef struct tagSocketRelation {
	UINT8 type;
	SOCKET mySocket;
	struct sockaddr_in remoteAddr;
}SOCKRELAT, *PSOCKRELAT;

//------------------------------------------------------------------------------
typedef void (*receive_fn) (PSOCKRELAT,unsigned char*, UINT32);

//-------------------------------------------------------------------------------
// Struct
//-------------------------------------------------------------------------------
typedef struct _socket_parameter {
	SOCKET *ConnectSocket;
	PDWORD pThread_id;
} SOCKPARA, *PSOCKPARA;

#ifndef DEFINE_STRUCT_NETWORKINTERFACE
#define DEFINE_STRUCT_NETWORKINTERFACE

typedef struct tagNetworkInterface {
	int	idxIf;			//index of interface
	int type;			//MIB_IF_TYPE_ETHERNET , IF_TYPE_IEEE80211(wireless)
	char ip[17];		//192.168.0.0
	char mac[24];		//FF:FF:FF:FF:FF:FF
	char name[255];		//name
	char desc[255];		//description
	struct tagNetworkInterface* nextNet;
}NETINTERFACE, *PNETINTERFACE;

#endif // !DEFINE_STRUCT_NETWORKINTERFACE
//-------------------------------------------------------------------------------
// Function
// @brief	the all of function return  0 => success      
//										1 => error
//-------------------------------------------------------------------------------
/*----------------------------------Common------------------------------------------*/
//initial Socket DLL
char WSA_Init(LPWSADATA pWSAData);
//Set IP & Port
char Set_Sock_Addr(PSOCKADDR_IN psock_addr,char* ip, int port);
//Create Thread for Recive data with callback
char Create_Sock_Recive_Thread(PDWORD thread_id, SOCKET *sock, DWORD function);
void Close_Sock_Recive_Thread(PDWORD thread_id);
char Close_Socket(SOCKET sock);

//List Network Interface
PNETINTERFACE GetNetworkInterface();
//Get the mac address of a given ip
void GetMacAddress(unsigned char *mac, struct in_addr destip);
/*----------------------------------TCP------------------------------------------*/
//special server 
char Create_TCP_Server(SOCKET *sock, struct sockaddr_in* stServerAddr, int port, int backlog);
char Bind_TCP_Server(SOCKET *sock, struct sockaddr_in* stServerAddr, int port);

//Client Connect to server
char Connect_Server(SOCKET *sock, struct sockaddr_in *stServerAddr);

//OneShot
char Send_OneShot_TCP_Data(struct sockaddr_in stServerAddr, char* send_buffer, int send_len);
char Get_OneShot_TCP_Data(struct sockaddr_in stServerAddr, char* read_buffer, int read_len, int *get_len);
//Basic Function
char Send_TCP_Data(SOCKET sock, char* send_buffer, int send_len);
char Read_TCP_Data(SOCKET sock, char* read_buffer, int read_len, int *get_len);

/*----------------------------------UDP------------------------------------------*/
char SetupBroadcast(SOCKET *sock, int sw);
//special server 
char Create_UDP_Server(SOCKET *sock, struct sockaddr_in* stServerAddr, int port);



//OneShot
char Send_OneShot_UDP_Data(struct sockaddr_in stServerAddr, char* send_buffer, int send_len);
//Send and Get Data OneShot
char SR_OneShot_UDP_Data(struct sockaddr_in stServerAddr, char* send_buffer, int send_len, char* read_buffer, int read_len, int *get_len);
//Basic Function
char Send_UDP_Data(SOCKET sock, char* send_buffer, int send_len, struct  sockaddr_in* server_add);
char Read_UDP_Data(SOCKET sock, char* read_buffer, int read_len, struct  sockaddr_in* external_add, int *get_len);


/*----------------------------------Multicast------------------------------------------*/
int join_source_group(int sd, uint32_t grpaddr, uint32_t srcaddr, uint32_t iaddr);
int leave_source_group(int sd, uint32_t grpaddr, uint32_t srcaddr, uint32_t iaddr);

int join_group(int sd, uint32_t grpaddr, uint32_t iaddr);
int leave_group(int sd, uint32_t grpaddr, uint32_t iaddr);

/*--------------------------------Receive ---------------------------------------*/
void Server_Receiver_init();
void Server_Receiver_Close();
void Server_Receiver_CloseWithSock(SOCKET sock);
void Server_Receiver_Add(SOCKET sock, UINT8 type, receive_fn receiveCb);


#ifdef __cplusplus
}
#endif // !__cplusplus

#endif // !_INTERNET_COMMUNICATION_H_