//#include "stdafx.h"
#include "internet_communication.h"


typedef struct tagSocketMap {
	SOCKET fd;
	UINT8 type;			//SOCK_STREAM , SOCK_DGRAM
	receive_fn receiverCB;
	struct tagSocketMap *next;
}SOCKETMAP;

HANDLE mHThread = NULL;
boolean mCtlReceiveLoop = FALSE;
SOCKETMAP *mSocketMap = NULL;
HANDLE mSocketMapMutex = NULL;
fd_set mMasterfds;
int mFdMax = 0;

void debug_printf(char* type, char* funName);
void debug_printf(char* type, char* funName) {
	//if (0) {
	//	uint8_t str[100];
	//	sprintf(str, "%-10s : %s\r\n", type, funName);
	//	util_appendTextToEditCtrl(GetDlgItem(hMainWnd, IDC_MAIN_EDIT_RECEIVE), (LPCTSTR)str);
	//}
}

int mSocketMap_Size();
SOCKETMAP* mSocketMap_Find(SOCKET sock);
void mSocketMap_Add(SOCKET sock, UINT8 type, receive_fn receiveCb);
void mSocketMap_FreeWithSocket(SOCKET sock);
void mSocketMap_Free();
void mSocketMap_UpdataFd();
DWORD WINAPI Thread_Server_Receiver(LPVOID lpParam);

/*==================================================================================*/
/*										Common										*/
/*==================================================================================*/

char WSA_Init(LPWSADATA pWSAData) {
	if (0 != WSAStartup(MAKEWORD(2, 1), pWSAData))
	{
		//printf("Winsock init faied!\r\n");
		WSACleanup();
		return 1;
	}
	if (LOBYTE(pWSAData->wVersion) != 2 || HIBYTE(pWSAData->wVersion) != 1)
	{
		//printf("the socket version is error!\r\n");
		WSACleanup();
		return 1;
	}
	return 0;
}

//Set IP & Port
char Set_Sock_Addr(PSOCKADDR_IN psock_addr, char* ip, int port) {
	//Setting IPv4
	psock_addr->sin_family = AF_INET;
	//Setting Port
	psock_addr->sin_port = htons(port);
	//Setting IP
	if(strcmp(ip,"0,0,0,0") == 0)
		psock_addr->sin_addr.s_addr = INADDR_ANY;
	else
		inet_pton(AF_INET, ip, &psock_addr->sin_addr);
}
char Create_Sock_Recive_Thread( PDWORD thread_id, SOCKET *sock, DWORD function)
{
	PSOCKPARA pData = (PSOCKPARA)malloc(sizeof(SOCKPARA));
	pData->ConnectSocket = sock;
	pData->pThread_id = thread_id;


	HANDLE hThread = CreateThread(
		NULL,                   // default security attributes
		0,                      // use default stack size  
		function,				// thread function name
		pData,                  // argument to thread function 
		0,                      // use default creation flags 
		thread_id);				// returns the thread identifier 

	if (hThread == NULL)
	{
		MessageBox(NULL, "hThread == NUL", "Error", MB_OK);
		free(pData);
		return 1;
	}
	
	CloseHandle(hThread);
	return 0;
}
void Close_Sock_Recive_Thread(PDWORD thread_id)
{
	*thread_id = -1;
	return;
}

char Close_Socket(SOCKET sock) {
	closesocket(sock);
}

//List Network Interface
PNETINTERFACE GetNetworkInterface() {
	PNETINTERFACE ret = NULL;

	//function: GetAdaptersInfo 
	//refer: https://docs.microsoft.com/en-us/windows/win32/api/iphlpapi/nf-iphlpapi-getadaptersinfo

	/* Declare and initialize variables */

	// It is possible for an adapter to have multiple
	// IPv4 addresses, gateways, and secondary WINS servers
	// assigned to the adapter. 
	//
	// Note that this sample code only prints out the 
	// first entry for the IP address/mask, and gateway, and
	// the primary and secondary WINS server for each adapter. 

	PIP_ADAPTER_INFO pAdapterInfo;
	PIP_ADAPTER_INFO pAdapter = NULL;
	DWORD dwRetVal = 0;
	UINT i;

	ULONG ulOutBufLen = sizeof(IP_ADAPTER_INFO);
	pAdapterInfo = (IP_ADAPTER_INFO *)malloc(sizeof(IP_ADAPTER_INFO));
	if (pAdapterInfo == NULL) {
		//printf("Error allocating memory needed to call GetAdaptersinfo\n");
		return NULL;
	}

	// Make an initial call to GetAdaptersInfo to get
	// the necessary size into the ulOutBufLen variable
	if (GetAdaptersInfo(pAdapterInfo, &ulOutBufLen) == ERROR_BUFFER_OVERFLOW) {
		free(pAdapterInfo);
		pAdapterInfo = (IP_ADAPTER_INFO *)malloc(ulOutBufLen);
		if (pAdapterInfo == NULL) {
			//printf("Error allocating memory needed to call GetAdaptersinfo\n");
			return NULL;
		}
	}

	if ((dwRetVal = GetAdaptersInfo(pAdapterInfo, &ulOutBufLen)) == NO_ERROR) {
		pAdapter = pAdapterInfo;
		while (pAdapter) {
			//create node
			PNETINTERFACE netInterface = (NETINTERFACE *)malloc(sizeof(NETINTERFACE));
			if (!netInterface) {
				//free node
				if (ret) {
					PNETINTERFACE tmp;
					while (ret) {
						tmp = ret->nextNet;
						free(ret);
						ret = tmp;
					}
				}
				return NULL;
			}
			netInterface->nextNet = NULL;

			//set value
			netInterface->idxIf = pAdapter->ComboIndex;
			netInterface->type = pAdapter->Type;
			//memcpy(netInterface->name, pAdapter->AdapterName,
			//	strlen(pAdapter->AdapterName) + 1);
			memcpy(netInterface->desc, pAdapter->Description,
				strlen(pAdapter->Description) + 1);
			memcpy(netInterface->ip, pAdapter->IpAddressList.IpAddress.String,
				strlen(pAdapter->IpAddressList.IpAddress.String) + 1);

			sprintf(netInterface->mac, "");
			for (i = 0; i < pAdapter->AddressLength; i++) {
				if (i == (pAdapter->AddressLength - 1))
					sprintf(netInterface->mac, "%s%.2X", netInterface->mac, (int)pAdapter->Address[i]);
				else
					sprintf(netInterface->mac, "%s%.2X:", netInterface->mac, (int)pAdapter->Address[i]);
			}


			//get alias name
			//refer index : https://docs.microsoft.com/zh-tw/windows/win32/iphlp/what-s-new-in-ip-helper
			//refer index to luid : https://docs.microsoft.com/en-us/windows/win32/api/netioapi/nf-netioapi-convertinterfaceindextoluid
			//refer luid to alias : https://docs.microsoft.com/en-us/windows/win32/api/netioapi/nf-netioapi-convertinterfaceluidtoalias
			NET_LUID pnet_luind;
			int err = ConvertInterfaceIndexToLuid(netInterface->idxIf, &pnet_luind);
			if (err != NO_ERROR) {
				switch (err) {
				case ERROR_FILE_NOT_FOUND: {
					break;
				}
				case ERROR_INVALID_PARAMETER: {
					break;
				}
				}

			}

			ConvertInterfaceLuidToAlias(&pnet_luind, netInterface->name, 255);

			pAdapter = pAdapter->Next;
			//add node
			if (!ret)
				ret = netInterface;
			else {
				PNETINTERFACE p = ret;
				while (p->nextNet) {
					p = p->nextNet;
				}
				p->nextNet = netInterface;
			}

		}
	}
	if (pAdapterInfo)
		free(pAdapterInfo);

	return ret;
}
//Get the mac address of a given ip
void GetMacAddress(unsigned char *mac, struct in_addr destip)
{
	DWORD ret;
	IPAddr srcip;
	ULONG MacAddr[2];
	ULONG PhyAddrLen = 6;  /* default to length of six bytes */
	int i;

	srcip = 0;

	//Send an arp packet
	ret = SendARP((IPAddr)destip.S_un.S_addr, srcip, MacAddr, &PhyAddrLen);

	//Prepare the mac address
	if (PhyAddrLen)
	{
		BYTE *bMacAddr = (BYTE *)& MacAddr;
		for (i = 0; i < (int)PhyAddrLen; i++)
		{
			mac[i] = (char)bMacAddr[i];
		}
	}
}
/*==================================================================================*/
/*										TCP											*/
/*==================================================================================*/

char Create_TCP_Server(SOCKET *sock, struct sockaddr_in* stServerAddr, int port ,int backlog) {
	
	Set_Sock_Addr(stServerAddr, "0,0,0,0", port);

	/* Creat SOCKET */
	*sock = socket(AF_INET, SOCK_STREAM, 0);
	/* bind server port */
	bind(*sock, (struct sockaddr*)stServerAddr, sizeof(struct sockaddr_in));
	/* listen server */
	listen(*sock, backlog);
}

char Bind_TCP_Server(SOCKET *sock, struct sockaddr_in* stServerAddr, int port) {

	Set_Sock_Addr(stServerAddr, "0,0,0,0", port);

	/* Creat SOCKET */
	*sock = socket(AF_INET, SOCK_STREAM, 0);
	/* bind server port */
	bind(*sock, (struct sockaddr*)stServerAddr, sizeof(struct sockaddr_in));
}



char Connect_Server(SOCKET *sock, struct sockaddr_in* stServerAddr) {
	/* Creat SOCKET */
	if (!*sock) {
		*sock = socket(AF_INET, SOCK_STREAM, 0);
	}
	//struct timeval timeo = { 3, 0 };
	//socklen_t len = sizeof(timeo);
	//setsockopt(*sock, SOL_SOCKET, SO_SNDTIMEO, &timeo, len);

	/* connect to Server */
	if(0 != connect(*sock, (SOCKADDR*)stServerAddr, sizeof(struct sockaddr_in))){ 
		return 1;
	}
	return 0;
}

char Send_OneShot_TCP_Data(struct sockaddr_in stServerAddr, char* send_buffer, int send_len) {
	SOCKET sock;
	/* Creat SOCKET */
	sock = socket(AF_INET, SOCK_STREAM, 0);
	/* connect to Server */
	connect(sock, (SOCKADDR*)&stServerAddr, sizeof(struct sockaddr_in));
	///* Get data from Server */
	//if (Read_TCP_Data(uiFdClientsocket, read_buffer, read_len)) {
	//	Close_Socket(uiFdClientsocket);
	//	return 1;
	//}
	/* send data to server */
	if (Send_TCP_Data(sock, send_buffer, send_len)){
		Close_Socket(sock);
		return 1;
	}
	Sleep(100);
	/*Close Socket*/
	Close_Socket(sock);
	Sleep(100);
	return 0;
}

char Get_OneShot_TCP_Data(struct sockaddr_in stServerAddr, char* read_buffer, int read_len, int *get_len){
	SOCKET sock;
	/* Creat SOCKET */
	sock = socket(AF_INET, SOCK_STREAM, 0);
	/* connect to Server */
	connect(sock, (SOCKADDR*)&stServerAddr, sizeof(struct sockaddr_in));
	/* Get data from Server */
	if (Read_TCP_Data(sock, read_buffer, read_len, get_len)) {
		Close_Socket(sock);
		return 1;
	}
	
	/*Close Socket*/
	Sleep(50);
	Close_Socket(sock);
	Sleep(50);
	return 0;
}

char Send_TCP_Data(SOCKET sock, char* send_buffer, int send_len) {
	/* 向伺服器發送數據 */
	int len;
	len = send(sock, send_buffer, send_len, 0);
	if (SOCKET_ERROR == len) {
		return 1;
	}
	else if (0 == len) {
		return 1;
	}
	return 0;
}

char Read_TCP_Data(SOCKET sock, char* read_buffer, int read_len,  int *get_len) {
	*get_len = recv(sock, read_buffer, read_len, 0);
	if (SOCKET_ERROR == *get_len ) {
		return 1;
	}
	return 0;
}
/*==================================================================================*/
/*										UDP											*/
/*==================================================================================*/
char SetupBroadcast(SOCKET *sock, int sw) {
	//sw => 0: Disable Broadcast , 
	//		1: Enable Broadcast

	int broadcast = sw;
	//char broadcast = '1'; // 如果上面這行不能用的話，改用這行
	// 這個 call 就是要讓 sockfd 可以送廣播封包
	if (setsockopt(*sock, SOL_SOCKET, SO_BROADCAST, &broadcast,
		sizeof(broadcast)) == -1) {
		return 1;
		//perror("setsockopt (SO_BROADCAST)");
		//exit(1);
	}

	if (setsockopt(*sock, SOL_SOCKET, SO_REUSEADDR, &broadcast,
		sizeof(broadcast)) == -1) {
		return 1;
		//perror("setsockopt (SO_BROADCAST)");
		//exit(1);
	}
	
	return 0;
}
char Create_UDP_Server(SOCKET *sock, struct sockaddr_in* stServerAddr, int port) {
	Set_Sock_Addr(stServerAddr, "0,0,0,0", port);

	/* Creat SOCKET */
	*sock = socket(AF_INET, SOCK_DGRAM, 0);

	SetupBroadcast(sock, 1);

	/* bind server port */
	bind(*sock, (struct sockaddr*)stServerAddr, sizeof(struct sockaddr_in));

}


char Send_OneShot_UDP_Data(struct sockaddr_in stServerAddr, char* send_buffer, int send_len){
	SOCKET sock;
	/* Creat SOCKET */
	sock = socket(AF_INET, SOCK_DGRAM, 0);
	/* send data to server */
	if (Send_UDP_Data(sock, send_buffer, send_len, &stServerAddr)) {
		Close_Socket(sock);
		return 1;
	}
	/*Close Socket*/
	Sleep(100);
	Close_Socket(sock);
	Sleep(100);
	return 0;
}


//Send and Read Data
//Step 1.Send data
//Step 2.Read Data
//Step 3.Colse
char SR_OneShot_UDP_Data(struct  sockaddr_in stServerAddr, char* send_buffer, int send_len, char* read_buffer, int read_len, int *get_len) {
	SOCKET sock;
	//external IP send data to me
	struct sockaddr_in exIP;
	memset(&exIP, 0, sizeof(exIP));
	/* Creat SOCKET */
	sock = socket(AF_INET, SOCK_DGRAM, 0);

	/* send data to server */
	if (Send_UDP_Data(sock, send_buffer, send_len, &stServerAddr)) {
		Close_Socket(sock);
		return 1;
	}
	/* Read data from server */
	if (Read_UDP_Data(sock, read_buffer, read_len, &exIP, get_len)) {
		Close_Socket(sock);
		return 1;
	}
	/*Close Socket*/
	Sleep(100);
	Close_Socket(sock);
	Sleep(100);
	return 0;
}

char Read_UDP_Data(SOCKET sock, char* read_buffer, int read_len, struct  sockaddr_in* external_add, int *get_len) {
	int iAddrlen = sizeof(struct sockaddr_in);
	*get_len = recvfrom(sock, read_buffer, read_len, 0, (struct sockaddr*) external_add, &iAddrlen);
	if (SOCKET_ERROR == *get_len) {
		return 1;
	}
	return 0;
}

char Send_UDP_Data(SOCKET sock, char* send_buffer, int send_len, struct sockaddr_in* server_add)
{
	/* 向 server_add 發送數據 */
	if (SOCKET_ERROR == sendto(sock, send_buffer, send_len, 0, (struct sockaddr*)server_add, sizeof(struct sockaddr_in))) {
		return 1;
	}
	return 0;
}


/*----------------------------------Multicast------------------------------------------*/
int join_source_group(int sd, uint32_t grpaddr, uint32_t srcaddr, uint32_t iaddr) {
	struct ip_mreq_source imr;

	imr.imr_multiaddr.s_addr = grpaddr;
	imr.imr_sourceaddr.s_addr = srcaddr;
	imr.imr_interface.s_addr = iaddr;
	return setsockopt(sd, IPPROTO_IP, IP_ADD_SOURCE_MEMBERSHIP, (char *)&imr, sizeof(imr));
}
int leave_source_group(int sd, uint32_t grpaddr, uint32_t srcaddr, uint32_t iaddr) {
	struct ip_mreq_source imr;

	imr.imr_multiaddr.s_addr = grpaddr;
	imr.imr_sourceaddr.s_addr = srcaddr;
	imr.imr_interface.s_addr = iaddr;
	return setsockopt(sd, IPPROTO_IP, IP_DROP_SOURCE_MEMBERSHIP, (char *)&imr, sizeof(imr));
}

int join_group(int sd, uint32_t grpaddr, uint32_t iaddr) {
	struct ip_mreq imr;

	imr.imr_multiaddr.s_addr = grpaddr;
	imr.imr_interface.s_addr = iaddr;
	return setsockopt(sd, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char *)&imr, sizeof(imr));
}

int leave_group(int sd, uint32_t grpaddr, uint32_t iaddr) {
	struct ip_mreq imr;

	imr.imr_multiaddr.s_addr = grpaddr;
	imr.imr_interface.s_addr = iaddr;
	return setsockopt(sd, IPPROTO_IP, IP_DROP_MEMBERSHIP, (char *)&imr, sizeof(imr));
}


/*--------------------------------Receive ---------------------------------------*/
int mSocketMap_Size() {
	debug_printf("Lock", __FUNCTION__);
	WaitForSingleObject(mSocketMapMutex, INFINITE);
	
	int retSize = 0;
	SOCKETMAP *p = mSocketMap;
	while (p) {
		retSize++;
		p = p->next;
	}
	debug_printf("Release", __FUNCTION__);
	ReleaseMutex(mSocketMapMutex);
	return retSize;
}
SOCKETMAP* mSocketMap_Find(SOCKET sock) {
	debug_printf("Lock", __FUNCTION__);
	WaitForSingleObject(mSocketMapMutex, INFINITE);
	
	SOCKETMAP *p = mSocketMap;
	while (p) {
		if (p->fd == sock) {
			goto funend_return;
		}
		p = p->next;
	}
funend_return:
	debug_printf("Release", __FUNCTION__);
	ReleaseMutex(mSocketMapMutex);
	return p;
}
void mSocketMap_Add(SOCKET sock, UINT8 type, receive_fn receiveCb) {
	debug_printf("Lock", __FUNCTION__);
	WaitForSingleObject(mSocketMapMutex, INFINITE);
	
	SOCKETMAP *p = mSocketMap;
	SOCKETMAP *obj = NULL;
	if (!mSocketMap) {
		mSocketMap = (SOCKETMAP*)malloc(sizeof(SOCKETMAP));
		mSocketMap->fd = sock;
		mSocketMap->type = type;
		mSocketMap->receiverCB = receiveCb;
		mSocketMap->next = NULL;
		goto funend_return;
	}

	if (p->fd == sock)
		goto funend_return;

	while (p->next) { 
		if (p->fd == sock)
			goto funend_return;
		p = p->next; 
	}

	obj = (SOCKETMAP*)malloc(sizeof(SOCKETMAP));
	obj->fd = sock;
	obj->type = type;
	obj->receiverCB = receiveCb;
	obj->next = NULL;

	p->next = obj;


funend_return:
	debug_printf("Release", __FUNCTION__);
	ReleaseMutex(mSocketMapMutex);
	return;
}
void mSocketMap_FreeWithSocket(SOCKET sock) {
	debug_printf("Lock", __FUNCTION__);
	WaitForSingleObject(mSocketMapMutex, INFINITE);
	
	SOCKETMAP *p = mSocketMap;
	SOCKETMAP *pre = p;
	while (p) {
		if (p->fd == sock) {
			closesocket(p->fd);
			
			if (p == mSocketMap) 
				mSocketMap = p->next;
			else 
				pre->next = p->next;

			free(p);
			break;
		}
		pre = p;
		p = p->next;
	}
	debug_printf("Release", __FUNCTION__);
	ReleaseMutex(mSocketMapMutex);
}
void mSocketMap_Free() {
	debug_printf("Lock", __FUNCTION__);
	WaitForSingleObject(mSocketMapMutex, INFINITE);

	SOCKETMAP *p, *next;
	p = mSocketMap;
	next = mSocketMap;
	while (p) {
		closesocket(p->fd);	//Close Socket

		next = p->next;
		free(p);
		p = next;
	}
	mSocketMap = NULL;
	debug_printf("Release", __FUNCTION__);
	ReleaseMutex(mSocketMapMutex);
}
void mSocketMap_UpdataFd() {
	debug_printf("Lock", __FUNCTION__);
	WaitForSingleObject(mSocketMapMutex, INFINITE);
	
	int maxFd = 0;
	FD_ZERO(&mMasterfds);
	SOCKETMAP *p = mSocketMap;
	while (p) {
		FD_SET(p->fd, &mMasterfds);
		if (p->fd > maxFd)
			maxFd = p->fd;

		p = p->next;
	}
	mFdMax = maxFd;
	debug_printf("Release", __FUNCTION__);
	ReleaseMutex(mSocketMapMutex);
}


void Server_Receiver_init() {
	//Create Mutex for SocketMap
	if (!mSocketMapMutex) {
		mSocketMapMutex = CreateMutex(NULL, FALSE, NULL);
	}

	mSocketMap_Free();
	mSocketMap_UpdataFd();
	mCtlReceiveLoop = FALSE;
	while (mHThread){ Sleep(100); }
	mCtlReceiveLoop = TRUE;
	mHThread = CreateThread(NULL, 0, Thread_Server_Receiver, NULL, 0, NULL);
}
void Server_Receiver_Close() {
	mCtlReceiveLoop = FALSE;
	mSocketMap_Free();
	mSocketMap_UpdataFd();
}
void Server_Receiver_CloseWithSock(SOCKET sock) {
	mSocketMap_FreeWithSocket(sock);
	mSocketMap_UpdataFd();
	//if(mSocketMap_Size() == 0)
	//	mCtlReceiveLoop = FALSE;
}

void Server_Receiver_Add(SOCKET sock,UINT8 type, receive_fn receiveCb) {
	mSocketMap_Add(sock, type, receiveCb);
	mSocketMap_UpdataFd();
}

DWORD WINAPI Thread_Server_Receiver(LPVOID lpParam) {
	char buf[4096]; // 儲存 client 資料的緩衝區
	int nbytes;
	fd_set read_fds;
	struct timeval  timeOut;
	timeOut.tv_sec = 1;	//1 sec
	timeOut.tv_usec = 0000 * 1000; //0.5 sec
	
	FD_ZERO(&read_fds);
	while (mCtlReceiveLoop) {
		if (!mSocketMap) {
			Sleep(500);
			continue;
		}
		memcpy(&read_fds, &mMasterfds, sizeof(mMasterfds));
		
		if (select(mFdMax + 1, &read_fds, NULL, NULL, &timeOut) == -1) {
			break;
		}
		timeOut.tv_sec = 1;
		timeOut.tv_usec = 0000 * 1000;
		
		for (int i = 0; i <= mFdMax; i++) {
			if (FD_ISSET(i, &read_fds)) {
				SOCKETMAP *p = mSocketMap_Find(i);
				if (!p) continue;
				SOCKRELAT sockRelat = {0};
				sockRelat.mySocket = i;
				nbytes = -1;
				switch (p->type) {
				case SOCK_STREAM: { 
					sockRelat.type = SOCK_STREAM;
					nbytes = recv(i, buf, sizeof(buf), 0); break; 
				}
				case SOCK_DGRAM: {
					sockRelat.type = SOCK_DGRAM;
					int iAddrlen = sizeof(struct sockaddr_in);
					nbytes = recvfrom(i, buf, sizeof(buf), 0, (struct sockaddr*) &sockRelat.remoteAddr, &iAddrlen);
					break; }
				}

				if (nbytes<= 0) {
					//faild
					int isNoConnect = FALSE;
					if (nbytes == SOCKET_ERROR) {
						int errSocket = WSAGetLastError();
						switch (errSocket) {
						case WSAENOTCONN: {
							MessageBox(NULL, "WSAENOTCONN", "WSAENOTCONN", MB_OK);
							break;
						}
						case WSAECONNRESET: {
							//SendMessageA(GetDlgItem(hMainWnd, IDC_MAIN_LIST_MESSAGE), LB_ADDSTRING, 0, (LPARAM)"WSAECONNRESET");
							isNoConnect = TRUE;
							break;
						}
						case WSANOTINITIALISED: {
							//SendMessageA(GetDlgItem(hMainWnd, IDC_MAIN_LIST_MESSAGE), LB_ADDSTRING, 0, (LPARAM)"WSANOTINITIALISED");
							isNoConnect = TRUE;
							break;
						}
						case WSAENOTSOCK: {
							//SendMessageA(GetDlgItem(hMainWnd, IDC_MAIN_LIST_MESSAGE), LB_ADDSTRING, 0, (LPARAM)"WSAENOTSOCK");
							// 關閉連線
							//sprintfA(strBuff, "selectserver: socket %d nosock\n", i);
							//SendMessageA(GetDlgItem(hMainWnd, IDC_MAIN_LIST_MESSAGE), LB_ADDSTRING, 0, (LPARAM)strBuff);
							isNoConnect = TRUE;
							break;
						}
						case WSAEWOULDBLOCK: {break; }
						default: {
							//char errStr[40];
							//sprintfA(errStr, "errCode: %d", errSocket);
							//SendMessageA(GetDlgItem(hMainWnd, IDC_MAIN_LIST_MESSAGE), LB_ADDSTRING, 0, (LPARAM)errStr);
							break;
						}
						}

					}
					Server_Receiver_CloseWithSock(i);
				}
				else {
					p->receiverCB(&sockRelat, buf, nbytes);
				}


			}
		}	
	}
	mSocketMap_Free();
	mHThread = NULL;
}