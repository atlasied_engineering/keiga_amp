#include "ipLiveForDante.h"

#include <stdio.h>
#include <stdlib.h>
#include "comm\internet_communication.h"

typedef struct tagLiveListIp {
	char ip[20];		//ip od dante
	char mac[30];		//mac of dante
	int countLive;		//live time
	struct tagLiveListIp *nextIp;
}LIVEIP, *PLIVEIP;

//List of live'ip
PLIVEIP mLiveIpTable = NULL;
int mLiveTimeSec = 60;		//default

static ILD_YES_NO mIsLiveIp(char* ip);
static ILD_YES_NO mIsLiveMac(char* mac);
static ILD_YES_NO mIsLiveIpAndMac(char* ip, char* mac);
static void mRecordLiveIpAndMac(char* ip, char* mac, int liveTimeSec);
DWORD WINAPI mThread_CountLiveIp(LPVOID lpParam);

//Monitoer PTP service
WSADATA mWsa;
SOCKADDR_IN mPtpListen_addr;		//Linsten ptp server
SOCKET mPtpListenSocket = NULL;

void mListPtpServer();
static void mReceiveData_ptp(PSOCKRELAT sock, uint8_t* data, UINT32 len);


static ILD_YES_NO mIsLiveIp(char* ip) {
	PLIVEIP p = mLiveIpTable;
	while (p) {
		if (_stricmp(p->ip, ip) == 0) {
			return ILD_YES;
		}
		p = p->nextIp;
	}
	return ILD_NO;
}
static ILD_YES_NO mIsLiveMac(char* mac) {
	PLIVEIP p = mLiveIpTable;
	while (p) {
		if (_stricmp(p->mac, mac) == 0) {
			return ILD_YES;
		}
		p = p->nextIp;
	}
	return ILD_NO;
}
static ILD_YES_NO mIsLiveIpAndMac(char* ip, char* mac) {
	PLIVEIP p = mLiveIpTable;
	while (p) {

		if (_stricmp(p->ip, ip) == 0 && _stricmp(p->mac, mac) == 0) {
			return ILD_YES;
		}
		p = p->nextIp;
	}
	return ILD_NO;
}
static void mRecordLiveIpAndMac(char* ip, char* mac, int liveTimeSec) {
	if (mIsLiveIpAndMac(ip, mac) == ILD_NO) {
		//add node
		PLIVEIP pNew = (PLIVEIP)malloc(sizeof(LIVEIP));

		//new record ip and mac
		sprintf(pNew->ip, "%s", ip);
		sprintf(pNew->mac, "%s", mac);
		pNew->countLive = liveTimeSec;
		pNew->nextIp = NULL;

		//add record 
		if (!mLiveIpTable) {
			mLiveIpTable = pNew;
		}
		else {
			PLIVEIP pre = mLiveIpTable;
			PLIVEIP cur = mLiveIpTable;
			//insert node by Increase
			while (cur) {
				if (_wcsicmp(cur->ip, pNew->ip) > 0) {
					if (pre == cur) {//add to Head
						pNew->nextIp = mLiveIpTable;
						mLiveIpTable = pNew;
					}
					else {// add to middle
						pNew->nextIp = cur;
						pre->nextIp = pNew;
					}
					break;
				}
				//next node
				pre = cur;
				cur = cur->nextIp;
			}
			//add to last
			if (!cur) {
				pre->nextIp = pNew;
			}
		}
	}
	else {
		//refresh live time
		PLIVEIP p = mLiveIpTable;
		while (p) {
			if (_stricmp(p->ip, ip) == 0 && _stricmp(p->mac, mac) == 0) {
				p->countLive = liveTimeSec;
				break;
			}
			p = p->nextIp;
		}
	}
}
DWORD WINAPI mThread_CountLiveIp(LPVOID lpParam) {
	PLIVEIP pre = NULL;
	PLIVEIP cur = NULL;
	while (1) {
		Sleep(1000);
		pre = mLiveIpTable;
		cur = mLiveIpTable;
		while (cur) {
			//reduced live
			cur->countLive = cur->countLive - 1 < 0 ? 0 : cur->countLive - 1;

			//remove node when count equal 0
			if (cur->countLive == 0) {
				if (mLiveIpTable == cur) {
					mLiveIpTable = cur->nextIp;
					free(cur);
					pre = mLiveIpTable;
					cur = mLiveIpTable;
				}
				else {
					pre->nextIp = cur->nextIp;
					free(cur);
					cur = pre->nextIp;
				}
			}
			else {
				//next node
				pre = cur;
				cur = cur->nextIp;
			}

		}

	}
}

void mListPtpServer() {


	Create_UDP_Server(&mPtpListenSocket, &mPtpListen_addr, 319);
	//listen muticast
	int ret = join_group(mPtpListenSocket, inet_addr("224.0.1.129"), inet_addr("0.0.0.0"));

	//enable Broadcast
	//SetupBroadcast(&g_dhcpListenSocket, 1);
	//add receiver
	Server_Receiver_Add(mPtpListenSocket, SOCK_DGRAM, &mReceiveData_ptp);
}

static void mReceiveData_ptp(PSOCKRELAT sock, uint8_t* data, UINT32 len) {

	typedef uint8_t type48[6];

	typedef struct tagData_PTP {
		uint16_t versionPTP;
		uint16_t versionNetwork;
		char subdomain[16];
		uint8_t msgType;
		uint8_t srcCommunicationTech;
		type48 srcUuid;
		uint16_t srcPortId;
		uint16_t seqId;
		uint8_t control;
		//Flag
		union {
			uint16_t data;
			struct {
				uint16_t PTP_LI61 : 1;
				uint16_t PTP_LI59 : 1;
				uint16_t PTP_Boundary_Clock : 1;
				uint16_t PTP_Assist : 1;
				uint16_t PTP_Ext_Sync : 1;
				uint16_t PTP_Parent_Stats : 1;
				uint16_t PTP_Sync_Burst : 1;
				uint16_t PTP_Reserved : 9;
			}bit;
		}flag;
	}DATAPTP, *PDATAPTP;


	char IP_add[17];
	struct in_addr srcip;
	unsigned char mac[6];
	char strMac[20];
	char show_msg[3000];

	int hasMac = FALSE;
	inet_ntop(AF_INET, &sock->remoteAddr.sin_addr, IP_add, 17);
	inet_pton(AF_INET, IP_add, &(srcip.s_addr));


	PDATAPTP dataPtp = (PDATAPTP)data;


	//Converter NBO(Network Byte Order) to HBO (Host Byte Order)
	dataPtp->versionPTP = ntohs(dataPtp->versionPTP);
	dataPtp->versionNetwork = ntohs(dataPtp->versionNetwork);
	dataPtp->srcPortId = ntohs(dataPtp->srcPortId);
	dataPtp->seqId = ntohs(dataPtp->seqId);
	dataPtp->flag.data = ntohs(dataPtp->flag.data);

	//check version ptp
	if (dataPtp->versionPTP == 1) {
		memcpy(mac, dataPtp->srcUuid, 6);
		sprintf(show_msg, "%s,  mac: %02x:%02x:%02x:%02x:%02x:%02x", IP_add, mac[0], mac[1], mac[2], mac[3], mac[4], mac[5], mac[6]);


		char cMac[20];
		sprintf(cMac, "%02x:%02x:%02x:%02x:%02x:%02x", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5], mac[6]);
		//dmgmt_record_ip(wIp, 60);
		mRecordLiveIpAndMac(IP_add, cMac, mLiveTimeSec);

	}

	return;
}

//initialization
void ild_init() {

	WSA_Init(&mWsa);
	
	DWORD id;

	CreateThread(NULL, 0, mThread_CountLiveIp, 0, 0, &id);

	Server_Receiver_init();
}
//setting live time seconds
void ild_set_live_time(int liveTimeSec) {
	mLiveTimeSec = liveTimeSec;
}
//start
void ild_start() {
	if (mPtpListenSocket != NULL)
		return;

	mListPtpServer();
}
//stop
void ild_stop(){
	PLIVEIP next = mLiveIpTable;
	PLIVEIP cur = mLiveIpTable;

	while (cur) {
		next = cur->nextIp;
		free(cur);
		cur = next;
	}
}

//check
ILD_YES_NO ild_is_live_ip(char* ip) {
	if (mIsLiveIp(ip)) {
		return ILD_YES;
	}else{
		return ILD_NO;
	}
}
ILD_YES_NO ild_is_live_mac(char* mac) {
	if (mIsLiveMac(mac)) {
		return ILD_YES;
	}
	else {
		return ILD_NO;
	}
}

ILD_YES_NO ild_is_live_ipAndMac(char* ip, char* mac) {
	if (mIsLiveIpAndMac(ip,mac)) {
		return ILD_YES;
	}
	else {
		return ILD_NO;
	}
}