//ild = Ip Live For Dante

#ifndef _IP_LIVE_FOR_DANTE_H_
#define _IP_LIVE_FOR_DANTE_H_

#ifdef __cplusplus
extern "C" {
#endif // __cpulspuls
	
#include <stdint.h>
	
	typedef enum tagENUM_ILD_YES_NO {
		ILD_NO,
		ILD_YES,
	}ILD_YES_NO;

	//initialization
	void ild_init();
	//setting live time seconds
	void ild_set_live_time(int liveTimeSec);
	//start
	void ild_start();
	//stop
	void ild_stop();
	//check
	ILD_YES_NO ild_is_live_ip(char* ip);//check ip 
	ILD_YES_NO ild_is_live_mac(char* mac);// check mac
	ILD_YES_NO ild_is_live_ipAndMac(char* ip, char* mac);//check both ip and mac 

#ifdef __cplusplus
}
#endif // __cpulspuls

#endif // !_IP_LIVE_FOR_DANTE_H_
