
#include "stdafx.h"
#include "device_control.h"
#include "device_state.h"
#include "Dante_Discovery.h"
#include "ACC_Command.h"
#include "AL003M.h"
#include "myListView.h"
//#include "Protocol.h"
#include "dt.h"

PDEVICEPARAM g_pDevice;


HWND g_hDeviceControl;
HWND g_hTabControl;
HWND g_hCurrentTab;
boolean g_reflash_ui;
char	g_preset_file_name[100] = { 0 };
char	g_installer_file_name[100] = { 0 };
unsigned char g_mypa[10];

//callbacks
static device_Write_fn *device_cbWrite = NULL;
unsigned char chksum;
COMMANDLEAD g_command_lead;
boolean version_err;
void Uart_Porcess(void);

void Device_SetWriteCB(device_Write_fn *fn) {
	device_cbWrite = fn;
}

void mDevice_Process_Initial(PDEVICEPARAM pDevice) {

	pDevice->device = TRUE;

	for (int i = 0; i < DEVICE_OUT_QUEUE_MAX; i++)//清除所有在command queue未傳的資料
		pDevice->out_queue[i][0] = 0;
	pDevice->queue_index = pDevice->queue_to_send = 0;


	pDevice->queue_index = 0;
	pDevice->queue_to_send = 0;
	pDevice->tick_wait_point = GetTickCount();
}

void Show_Device_Info()
{

	char *device_info = (char*)malloc(500 * sizeof(char));
	char *speaker = (char*)malloc(17 * sizeof(char));
	char *zone = (char*)malloc(17 * sizeof(char));
	char *ip = (char*)malloc(16 * sizeof(char));
	char *mac = (char*)malloc(18 * sizeof(char));
	char *ver = (char*)malloc(12 * sizeof(char));
	char *poe = (char*)malloc(6 * sizeof(char));

	memset(device_info, 0, sizeof(device_info));
	memset(speaker, 0, sizeof(speaker));
	memset(zone, 0, sizeof(zone));
	memset(ip, 0, sizeof(ip));
	memset(mac, 0, sizeof(mac));
	memset(ver, 0, sizeof(ver));
	memset(poe, 0, sizeof(poe));

	for (int i = 0; i < 16; i++) {
		speaker[i] = g_pDevice->pa[AL003M_SPEAKER_1 + i];
	}
	for (int i = 0; i < 16; i++) {
		zone[i] = g_pDevice->pa[AL003M_LOCATION_1 + i];
	}
	sprintf(ver, "%s%d%s%d%s%d%s%d", "V", g_pDevice->pa[AL003M_VER_YEAR], ".", g_pDevice->pa[AL003M_VER_MONTH], ".", g_pDevice->pa[AL003M_VER_DATE], ".", g_pDevice->pa[AL003M_VER_NUMBER]);

	if (g_pDevice->pa[AL003M_T2P_TYPE] & 0x7F) {
		sprintf(poe, "%s", "PoE+");
	}
	else {
		sprintf(poe, "%s", "PoE");
	}

	PDANTENODE pnode;
	//check if the device has exist
	pnode = dt_FindNode(g_pDevice->device_name);
	if (pnode) {
		sprintf(ip, "%d%s%d%s%d%s%d",
			(pnode->devInfo.networks.networks->ip_address) & 0xFF, ".",
			(pnode->devInfo.networks.networks->ip_address >> 8) & 0xFF, ".",
			(pnode->devInfo.networks.networks->ip_address >> 16) & 0xFF, ".",
			(pnode->devInfo.networks.networks->ip_address >> 24) & 0xFF);

		sprintf(mac, "%02X%s%02X%s%02X%s%02X%s%02X%s%02X", pnode->devInfo.networks.networks->mac_address[0], ":", pnode->devInfo.networks.networks->mac_address[1], ":", pnode->devInfo.networks.networks->mac_address[2], ":", pnode->devInfo.networks.networks->mac_address[3], ":", pnode->devInfo.networks.networks->mac_address[4], ":", pnode->devInfo.networks.networks->mac_address[5]);
		sprintf(device_info, "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s", " Device Status", "\n Atlas Model: ", g_preset_file_name, "\n Dante Model: ", pnode->devInfo.modelname, "\n Dante Device Name: ", g_pDevice->device_name, "\n Installer Name: ", speaker, "\n Zone: ", zone, "\n IP Address: ", ip, "\n MAC: ", mac, "\n FW Version: ", ver, "\n Installer File Name: ", g_installer_file_name, "\n PoE: ", poe);
	}
	else {
		sprintf(device_info, "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s", " Device Status", "\n Atlas Model: ", "", "\n Dante Model: ", "", "\n Dante Device Name: ", "", "\n Installer Name: ", "", "\n Zone: ", "", "\n IP Address: ", "", "\n MAC: ", "", "\n FW Version: ", "", "\n Installer File Name: ", "", "\n PoE:", "");
	}

	if (!g_pDevice->state) {
		sprintf(device_info, "%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s%s", " Device Status", "\n Atlas Model: ", "", "\n Dante Model: ", "", "\n Dante Device Name: ", "", "\n Installer Name: ", "", "\n Zone: ", "", "\n IP Address: ", "", "\n MAC: ", "", "\n FW Version: ", "", "\n Installer File Name: ", "", "\n PoE:", "");
	}
	SendMessageA(GetDlgItem(g_hDeviceControl, IDC_STATIC_DEVICE), WM_SETTEXT, 0, (LPARAM)device_info);

	free(device_info);
	free(speaker);
	free(zone);
	free(ip);
	free(mac);
	free(ver);
	free(poe);
}

void Uart_Rx(PDEVICEPARAM pDevice, unsigned char* data_in, unsigned long length)
{
	if (length == 0)
		return;

	if (pDevice->rx_state == UART_READY) {
		if (data_in[0] == ACC_CUST_KEIGA || data_in[0] == 0xFF) {
			chksum = data_in[0];
			g_command_lead._.Customer = data_in[0];
			pDevice->rx_state = UART_HEAD;
			Uart_Rx(pDevice, data_in + 1, length - 1);
		}
	}
	else if (pDevice->rx_state == UART_HEAD) {
		if (data_in[0] == ACC_HEADER_WRITE || data_in[0] == ACC_HEADER_READ || data_in[0] == ACC_HEADER_ACK || data_in[0] == ACC_HEADER_ECHO) {
			g_command_lead._.Head = data_in[0];
			chksum += data_in[0];
			pDevice->rx_state = UART_SOURCE;
			Uart_Rx(pDevice, data_in + 1, length - 1);
		}
		else
			pDevice->rx_state = UART_READY;
	}
	else if (pDevice->rx_state == UART_SOURCE) {
		g_command_lead._.Source = data_in[0];
		chksum += data_in[0];
		pDevice->rx_state = UART_TARGET;
		Uart_Rx(pDevice, data_in + 1, length - 1);
	}
	else if (pDevice->rx_state == UART_TARGET) {
		if (data_in[0] == pDevice->id) {
			g_command_lead._.Target = data_in[0];
			chksum += data_in[0];
			pDevice->rx_state = UART_COMMAND;
			Uart_Rx(pDevice, data_in + 1, length - 1);
		}
		else
			pDevice->rx_state = UART_READY;
	}
	else if (pDevice->rx_state == UART_COMMAND) {
		g_command_lead._.Command = data_in[0];
		chksum += data_in[0];
		pDevice->rx_state = UART_LENGTH_H;
		Uart_Rx(pDevice, data_in + 1, length - 1);
	}
	else if (pDevice->rx_state == UART_LENGTH_H) {
		g_command_lead._.Length = data_in[0] * 256;
		chksum += data_in[0];
		pDevice->rx_state = UART_LENGTH_L;
		Uart_Rx(pDevice, data_in + 1, length - 1);
	}
	else if (pDevice->rx_state == UART_LENGTH_L) {
		g_command_lead._.Length += data_in[0];
		chksum += data_in[0];
		if (g_command_lead._.Length == 0) {
			pDevice->rx_state = UART_FINISH;
			Uart_Porcess();
		}
		else if (g_command_lead._.Length == 1) {
			pDevice->rx_data_index = 0;
			pDevice->rx_state = UART_CHECKSUM;
			Uart_Rx(pDevice, data_in + 1, length - 1);
		}
		else {
			pDevice->rx_data_index = 0;
			pDevice->rx_state = UART_DATA_N;
			Uart_Rx(pDevice, data_in + 1, length - 1);
		}
	}
	else if (pDevice->rx_state == UART_DATA_N) {
		chksum += data_in[0];
		pDevice->rx_data[pDevice->rx_data_index] = data_in[0];
		pDevice->rx_data_index++;
		if (pDevice->rx_data_index == g_command_lead._.Length - 1) {
			pDevice->rx_state = UART_CHECKSUM;
		}
		Uart_Rx(pDevice, data_in + 1, length - 1);
	}
	else if (pDevice->rx_state == UART_CHECKSUM) {
		if (chksum == data_in[0]) {
			pDevice->rx_data[pDevice->rx_data_index] = data_in[0];
			pDevice->rx_state = UART_FINISH;
			Uart_Porcess();
		}
		else
			pDevice->rx_state = UART_READY;
	}
	else if (pDevice->rx_state == UART_FINISH) {
		//doing Uart_process()
	}
	else {
		pDevice->rx_state = UART_READY;
	}
}

void Uart_Porcess(void)
{
	RECT rect;
	rect.left = 18;
	rect.right = 29;
	rect.top = 44;
	rect.bottom = 55;
	g_mypa[MYPA_RECIVE_AC3] = RECIVE_AC3_COLOR_1;
	InvalidateRect(g_hDeviceControl, &rect, TRUE);
	KillTimer(g_hDeviceControl, TIMER_AC3_RECEIVE_DISPLAY);
	SetTimer(g_hDeviceControl, TIMER_AC3_RECEIVE_DISPLAY, TIMER_AC3_RECEIVE_DISPLAY, NULL);
	switch (g_command_lead._.Command) {
	case ACC_CMD_LINALD:
		if (g_command_lead._.Head == ACC_HEADER_ACK) {
			g_mypa[MYPA_LINALD_STATUS] = LINALD_STATUS_FINISHED;
			Show_Device_Info();
			InvalidateRect(g_hDeviceControl, NULL, TRUE);
			
		}
		else {
			if ((g_command_lead._.Length - 1) == AL003M_DATA_COUNT) {
				boolean changed = FALSE;
				for (int i = 0; i < g_command_lead._.Length - 1; i++) {
					if (g_pDevice->pa[i] != g_pDevice->rx_data[i]) {
						changed = TRUE;
						for (int i = 0; i < g_command_lead._.Length - 1; i++)
							g_pDevice->pa[i] = g_pDevice->rx_data[i];
						break;
					}
				}
				
				memset(g_preset_file_name, 0, sizeof(g_preset_file_name));
				for (int i = 0; i < 32; i++) {
					g_preset_file_name[i] = g_pDevice->pa[AL003M_FACTORY_1 + i];
				}

				memset(g_installer_file_name, 0, sizeof(g_installer_file_name));
				for (int i = 0; i < 32; i++) {
					g_installer_file_name[i] = g_pDevice->pa[AL003M_INSTALLER_1 + i];
				}
				Show_Device_Info();

				if (changed) {
					if ((g_pDevice->pa[AL003M_VER_YEAR] * 8760 + g_pDevice->pa[AL003M_VER_MONTH] * 720 + g_pDevice->pa[AL003M_VER_DATE] * 24 + g_pDevice->pa[AL003M_VER_NUMBER]) < (FW_VER_YEAR * 8760 + FW_VER_MONTH * 720 + FW_VER_DATE * 24 + FW_VER_NUMBER)) {
						version_err = TRUE;
						g_pDevice->pa[AL003M_VER_YEAR] = g_pDevice->rx_data[g_command_lead._.Length - 5];
						g_pDevice->pa[AL003M_VER_MONTH] = g_pDevice->rx_data[g_command_lead._.Length - 4];
						g_pDevice->pa[AL003M_VER_DATE] = g_pDevice->rx_data[g_command_lead._.Length - 3];
						g_pDevice->pa[AL003M_VER_NUMBER] = g_pDevice->rx_data[g_command_lead._.Length - 2];
						SendMessage(GetDlgItem(g_hDeviceControl, IDC_STATIC_DEVICE), WM_SETTEXT, 0, (LPARAM)"Firmware Version Error");
						InvalidateRect(g_hDeviceControl, NULL, TRUE);
					}
					else {
						version_err = FALSE;
						Show_Device_Info();
						g_reflash_ui = TRUE;
						InvalidateRect(g_hDeviceControl, NULL, TRUE);
					}
				}

			}
			else {
				version_err = TRUE;
				g_pDevice->pa[AL003M_VER_YEAR] = g_pDevice->rx_data[g_command_lead._.Length - 5];
				g_pDevice->pa[AL003M_VER_MONTH] = g_pDevice->rx_data[g_command_lead._.Length - 4];
				g_pDevice->pa[AL003M_VER_DATE] = g_pDevice->rx_data[g_command_lead._.Length - 3];
				g_pDevice->pa[AL003M_VER_NUMBER] = g_pDevice->rx_data[g_command_lead._.Length - 2];
				SendMessage(GetDlgItem(g_hDeviceControl, IDC_STATIC_DEVICE), WM_SETTEXT, 0, (LPARAM)"FW Version Error");
				InvalidateRect(g_hDeviceControl, NULL, TRUE);
			}
		}
		break;
	case ACC_CMD_RENAME:
	{

		Show_Device_Info();
		g_reflash_ui = TRUE;
		InvalidateRect(g_hDeviceControl, NULL, TRUE);
	}
	break;
	case ACC_CMD_USER_PRESET:
		if (g_mypa[MYPA_USER_PRESET_STATUS] == USER_PRESET_STATUS_UPLOADING)
			g_mypa[MYPA_USER_PRESET_STATUS] = USER_PRESET_STATUS_FINISHED;
		else if (g_mypa[MYPA_USER_RESET_STATUS] == USER_RESET_STATUS_UPLOADING)
			g_mypa[MYPA_USER_RESET_STATUS] = USER_RESET_STATUS_FINISHED;

		g_reflash_ui = TRUE;
		InvalidateRect(g_hDeviceControl, NULL, TRUE);
		break;
	case ACC_CMD_SFACDEF:
		g_mypa[MYPA_LOAD_DEFAULT_STATUS] = LOAD_DEFAULT_STATUS_FINISHED;
		g_reflash_ui = TRUE;
		InvalidateRect(g_hDeviceControl, NULL, TRUE);
		break;
	case ACC_CMD_SRCVOL:
	{
		if (g_command_lead._.Head == ACC_HEADER_ACK) {
			g_pDevice->pa[AL003M_SIGNAL_CH1] = g_pDevice->rx_data[2];
			g_pDevice->pa[AL003M_SIGNAL_CH2] = g_pDevice->rx_data[3];
			if (TabCtrl_GetCurSel(g_hTabControl) == PAGE_LEVEL) {
				InvalidateRect(g_hTabControl, NULL, TRUE);
			}
		}
	}
	break;

	default:
		break;
	}
	g_pDevice->rx_state = UART_READY;
}

void mDevice_Process_Receiver(void* mparam, void* lparam) {
	PDEVICEPARAM pDevice = (PDEVICEPARAM)mparam;
	PDEVICERECV p = (PDEVICERECV)lparam;
	int lenMessage = p->lenMessage;
	unsigned char* buff = p->buffParam;
	int lenBuff = p->sizeParam;

	Uart_Rx(pDevice, buff, lenMessage);
	memset(buff, 0, sizeof(buff));
	char msg[50];

	pDevice->tick_resend_wait_point = GetTickCount();
	pDevice->tick_wait_point = GetTickCount();

}
void Set_Send_Flag(PDEVICEPARAM pDevice, unsigned char command, unsigned char head, int length, ...)
{
	if (!pDevice)
		return;
	va_list va;
	int i, j;
	unsigned char chksum = 0;
	
	int i_temp = pDevice->queue_index;
	i_temp = i_temp++ > (DEVICE_OUT_QUEUE_MAX - 1) ? 0 : i_temp;
	if (i_temp == pDevice->queue_to_send) {
		pDevice->out_queue[pDevice->queue_to_send][0] = 0;
		pDevice->queue_to_send = ++pDevice->queue_to_send >  (DEVICE_OUT_QUEUE_MAX - 1) ? 0 : pDevice->queue_to_send;
		return;//queue overflow
	}


	pDevice->out_queue[pDevice->queue_index][3] = ACC_CUST_KEIGA;
	pDevice->out_queue[pDevice->queue_index][4] = head;
	pDevice->out_queue[pDevice->queue_index][5] = 0xAA;
	pDevice->out_queue[pDevice->queue_index][6] = pDevice->id;
	pDevice->out_queue[pDevice->queue_index][7] = command;
	pDevice->out_queue[pDevice->queue_index][8] = (length + 1) / 256;	//length+1 for checksum byte
	pDevice->out_queue[pDevice->queue_index][9] = (length + 1) % 256;

	va_start(va, length);
	j = 10;
	i = length;
	while (i > 0) {
		pDevice->out_queue[pDevice->queue_index][j] = va_arg(va, unsigned char);// g_pDevice->pa[va_arg(va, unsigned char)];
		i--;
		j++;
	}
	va_end(va);

	//checksum
	pDevice->out_queue[pDevice->queue_index][j] = 0;
	for (i = 3; i < j; i++)
		pDevice->out_queue[pDevice->queue_index][j] += pDevice->out_queue[pDevice->queue_index][i];

	//total bytes to send
	pDevice->out_queue[pDevice->queue_index][1] = (length + 8) / 256;
	pDevice->out_queue[pDevice->queue_index][2] = (length + 8) % 256;
	//flag for ready to send
	pDevice->out_queue[pDevice->queue_index][0] = 1;

	pDevice->queue_index = ++pDevice->queue_index >(DEVICE_OUT_QUEUE_MAX - 1) ? 0 : pDevice->queue_index;
}

void mDevice_Process_Upload(PDEVICEPARAM pDevice, void* lparam) {
	//Delay sometime for Dante
	DWORD waiting_time = GetTickCount();
	while ((GetTickCount() - waiting_time) <  20);
	if (pDevice->out_queue[pDevice->queue_to_send][0] == 1) {
		device_cbWrite(pDevice->manufacturer, pDevice->device_name, &pDevice->out_queue[pDevice->queue_to_send][3], pDevice->out_queue[pDevice->queue_to_send][1] * 256 + pDevice->out_queue[pDevice->queue_to_send][2]);
		pDevice->out_queue[pDevice->queue_to_send][0] = 0;
		pDevice->queue_to_send = ++pDevice->queue_to_send > (DEVICE_OUT_QUEUE_MAX - 1) ? 0 : pDevice->queue_to_send;
	}
}

boolean Device_Check_Modle(PDEVICESTATE pds)
{
	switch (pds->amp_id) {
	case ACC_AMP_AL003M:
		if (strcmp(pds->Model, AL003M_MODEL_NAME) == 0 || strcmp(pds->Model, DA_APX40N_MODEL_NAME) == 0 || strcmp(pds->Model, DA_22SYS_MODEL_NAME) == 0
			|| strcmp(pds->Model, DA_62SYS_MODEL_NAME) == 0 || strcmp(pds->Model, DA_APX_MODEL_NAME) == 0 || strcmp(pds->Model, DA_PM8GD_MODEL_NAME) == 0
			|| strcmp(pds->Model, DA_PD8DG_MODEL_NAME) == 0) {
			if (g_hDeviceControl) {
				ShowWindow(g_hDeviceControl, SW_SHOW);
			}
			else {
				g_hDeviceControl = CreateDialog(hInst, MAKEINTRESOURCE(IDD_DIALOG_MAIN), hMainWnd, DialogProc);
				ShowWindow(g_hDeviceControl, SW_SHOW);
			}
			return TRUE;
		}
		return FALSE;
	}
	return FALSE;
}

DWORD WINAPI Thread_Device(LPVOID lpParam) {

	PDEVICESTATE pds = (PDEVICESTATE)lpParam;

	unsigned long	process_wait_point = 0;
	pds->pDevice = (PDEVICEPARAM)malloc(sizeof(DEVICEPARAM));
	if (pds->pDevice == NULL)
		return 1;
	g_pDevice = pds->pDevice;
	memset(pds->pDevice->device_name, 0, sizeof(pds->pDevice->device_name));
	memset(pds->pDevice->manufacturer, 0, sizeof(pds->pDevice->manufacturer));
	strcpy(pds->pDevice->device_name, pds->Name);
	strcpy(pds->pDevice->manufacturer, pds->Manufacturer);
	pds->pDevice->id = pds->amp_id;
	pds->pDevice->fn_reciver = mDevice_Process_Receiver;

	pds->pDevice->fn_msg = CB_SetListViewItem;

	mDevice_Process_Initial(pds->pDevice);

	while (1) {
		if (pds->pDevice->out_queue[pds->pDevice->queue_to_send][0] == 1) {
			mDevice_Process_Upload(pds->pDevice, NULL);
		}
		pds->pDevice->state = pds->state;

		if (!pds->pDevice->device)
			break;
		Sleep(100);
	}

	if (pds->pDevice) {
		free(pds->pDevice);
		pds->pDevice = NULL;
	}
	return 1;
}

INT_PTR CALLBACK DialogProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_INITDIALOG:
	{
		INITCOMMONCONTROLSEX InitCtrlEx;
		InitCtrlEx.dwSize = sizeof(INITCOMMONCONTROLSEX);
		InitCtrlEx.dwICC = ICC_TAB_CLASSES;
		InitCommonControlsEx(&InitCtrlEx);

//		ild_init();
//		ild_start();

//		ild_set_live_time(10); //10 seconds

		g_hTabControl = GetDlgItem(hDlg, IDC_TAB1);

		TCITEM tcItem;
		tcItem.mask = TCIF_TEXT; // I'm only having text on the tab

		tcItem.pszText = "Output Level";
		TabCtrl_InsertItem(g_hTabControl, PAGE_LEVEL + 1, &tcItem);



		tcItem.pszText = "Dante Input";
		TabCtrl_InsertItem(g_hTabControl, PAGE_DANTE + 1, &tcItem);

		tcItem.pszText = "Filter View";
		TabCtrl_InsertItem(g_hTabControl, PAGE_FREQ_RESP + 1, &tcItem);

		tcItem.pszText = "Masking";
		TabCtrl_InsertItem(g_hTabControl, PAGE_MASKING + 1, &tcItem);

		tcItem.pszText = "Admin";
		TabCtrl_InsertItem(g_hTabControl, PAGE_IP + 1, &tcItem);

		tcItem.pszText = "Installation File";
		TabCtrl_InsertItem(g_hTabControl, PAGE_PRESET + 1, &tcItem);

		TabCtrl_SetCurSel(g_hTabControl, PAGE_LEVEL);
		g_hCurrentTab = CreateDialog(hInst, MAKEINTRESOURCE(TAB_LEVEL), g_hTabControl, LevelProc); // Setting dialog to tab one by default		*/
		SetTimer(hDlg, TIME_LINK_AMP, TIME_LINK_AMP, NULL);
	}
	break;
	case WM_COMMAND:

		break;

	case WM_NOTIFY:
		switch (((LPNMHDR)lParam)->code)
		{
		case TCN_SELCHANGE:
		{
			EndDialog(g_hCurrentTab, 0);

			switch (TabCtrl_GetCurSel(g_hTabControl))
			{
			case PAGE_LEVEL:
				g_hCurrentTab = CreateDialog(hInst, MAKEINTRESOURCE(TAB_LEVEL), g_hTabControl, LevelProc);
				break;
			case PAGE_DANTE:
				g_hCurrentTab = CreateDialog(hInst, MAKEINTRESOURCE(TAB_DANTE), g_hTabControl, DanteProc);
				break;
			case PAGE_FREQ_RESP:
				g_hCurrentTab = CreateDialog(hInst, MAKEINTRESOURCE(TAB_DANTE_VIEWER), g_hTabControl, DanteFilterViewProc);
				break;
			case PAGE_MASKING:
				g_hCurrentTab = CreateDialog(hInst, MAKEINTRESOURCE(TAB_MASKING), g_hTabControl, MaskingProc);
				break;
			case PAGE_IP:
				g_hCurrentTab = CreateDialog(hInst, MAKEINTRESOURCE(TAB_IP), g_hTabControl, IpProc);
				break;
			case PAGE_PRESET:
				g_hCurrentTab = CreateDialog(hInst, MAKEINTRESOURCE(TAB_PRESET), g_hTabControl, PresetProc);
				break;
			}
		}//End case
		break;
		}//End switch
		break;

	case WM_TIMER:
	{
		switch (wParam) {
		case TIME_LINK_AMP:
			break;
		case TIMER_AC3_RECEIVE_DISPLAY:
		{
			KillTimer(hDlg, TIMER_AC3_RECEIVE_DISPLAY);
			RECT rect;
			rect.left = 18;
			rect.right = 29;
			rect.top = 44;
			rect.bottom = 55;
			g_mypa[MYPA_RECIVE_AC3] = RECIVE_AC3_COLOR_0;
			InvalidateRect(hDlg, &rect, TRUE);
		}
		break;
		}
	}
		break;
	case WM_PAINT:
	{
		PAINTSTRUCT 	ps;
		HDC 			hdc;

		hdc = BeginPaint(hDlg, &ps);
		SelectObject(hdc, GetStockObject(DC_BRUSH));
		if (g_pDevice->state && g_pDevice->device_name[0] != 0) {
			if (version_err)
				SetDCBrushColor(hdc, RGB(250, 250, 0));
			else
				SetDCBrushColor(hdc, RGB(0, 180, 0));
		}
		else
			SetDCBrushColor(hdc, RGB(180, 0, 0));
		HPEN hPen = CreatePen(PS_SOLID, 1, RGB(10, 10, 10));
		SelectObject(hdc, hPen);
		Ellipse(hdc, 16, 22, 31, 37);
		DeleteObject(hPen);
		switch (g_mypa[MYPA_RECIVE_AC3]) {
		case RECIVE_AC3_COLOR_1:
			SetDCBrushColor(hdc, RGB(250, 100, 0));
			hPen = CreatePen(PS_SOLID, 1, RGB(10, 10, 10));
			break;
		default:
			SetDCBrushColor(hdc, RGB(240, 240, 240));
			hPen = CreatePen(PS_SOLID, 1, RGB(10, 10, 10));
			break;
		}
		SelectObject(hdc, hPen);
		Ellipse(hdc, 18, 44, 29, 55);
		DeleteObject(hPen);
		EndPaint(hDlg, &ps);
	}
		break;
	case WM_CLOSE:
		DestroyWindow(hDlg);
		g_hDeviceControl = NULL;
		break;

	case WM_DESTROY:
		g_hDeviceControl = NULL;
		g_pDevice->device = FALSE;
		return (INT_PTR)TRUE;
	}
	return FALSE;
}


