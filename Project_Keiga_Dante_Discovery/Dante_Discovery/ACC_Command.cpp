
#include "stdafx.h"
#include "ACC_Command.h"
#ifndef HIBYTE
#define HIBYTE(w)           ((unsigned char )((((int)(w)) >> 8) & 0xff))
#endif
#ifndef LOBYTE
#define LOBYTE(w)           ((unsigned char )(((int)(w)) & 0xff))
#endif

Send_FUNC ACC_SendHandlerFn = 0;

/*****************************************************/
/*Check Sum											*/
/*****************************************************/
unsigned char cCheckSum(unsigned char* data, int len) {
	unsigned char ckSum = 0;
	for (int i = 0; i < len; i++)
		ckSum = LOBYTE(ckSum + data[i]);
	return ckSum;
}

unsigned char stCheckSum(_pComand_Lead cmd,char *data)
{
	unsigned char ckSum = 0;
	int len;
	len = (cmd->Len_H << 8) + cmd->Len_L;
	ckSum = 0;
	ckSum += cmd->Customer;
	ckSum += cmd->Head;
	ckSum += cmd->Source;
	ckSum += cmd->Target;
	ckSum += cmd->Command;
	ckSum += cmd->Len_H;
	ckSum += cmd->Len_L;

	for (int i = 0; i < len-1; i++)
	{
		ckSum += *(data + i);
	}
	return ckSum;
}
/*****************************************************/
/*		transter  output & source					*/
/*****************************************************/
//Output
int Trans_AccToOutput(unsigned char opage, unsigned char oput){
	unsigned char i = 0x01;
	unsigned char count = 1;
	switch (opage) {
	case ACC_PA_ALL_OPAGE_OUT0: { // Output page 0
		switch (oput) {
		case ACC_PA_ALL_OPUT_B1: {	return ACC_PA_ALL_OUTPUT_1;	break; }
		case ACC_PA_ALL_OPUT_B2: {	return ACC_PA_ALL_OUTPUT_N(2);	break; }
		case ACC_PA_ALL_OPUT_B3: {	return ACC_PA_ALL_OUTPUT_N(3);	break; }
		case ACC_PA_ALL_OPUT_B4: {	return ACC_PA_ALL_OUTPUT_N(4);	break; }
		case ACC_PA_ALL_OPUT_B5: {	return ACC_PA_ALL_OUTPUT_N(5);	break; }
		case ACC_PA_ALL_OPUT_B6: {	return ACC_PA_ALL_OUTPUT_N(6);	break; }
		case ACC_PA_ALL_OPUT_B7: {	return ACC_PA_ALL_OUTPUT_N(7);	break; }
		case ACC_PA_ALL_OPUT_B8: {	return ACC_PA_ALL_OUTPUT_N(8);	break; }
		default: {
			return 0xFF;
			break;
		}
		}
		break;
	}
	case ACC_PA_ALL_OPAGE_OUT1: {
		for (	i = 0x01, count=1; i != oput;i =i <<= 1, count++);
		return ACC_PA_ALL_OUTPUT_N( 8*1 + count);
		break;
	}
	case ACC_PA_ALL_OPAGE_OUT2: {
		for (i = 0x01, count = 1;i != oput; i = i <<= 1, count++);
		return ACC_PA_ALL_OUTPUT_N( 8*2 + count);
		break;
	}
	case ACC_PA_ALL_OPAGE_OUT3: {
		for (i = 0x01, count = 1; i != oput; i = i <<= 1, count++);
		return ACC_PA_ALL_OUTPUT_N(8 * 3 + count);
		break;
	}
	case ACC_PA_ALL_OPAGE_OUT4: {
		for (i = 0x01, count = 1; i != oput; i = i <<= 1, count++);
		return ACC_PA_ALL_OUTPUT_N(8 * 4 + count);
		break;
	}
	case ACC_PA_ALL_OPAGE_OUT5: {
		for (i = 0x01, count = 1; i != oput; i = i <<= 1, count++);
		return ACC_PA_ALL_OUTPUT_N(8 * 5 + count);
		break;
	}
	case ACC_PA_ALL_OPAGE_OUT6: {
		for (i = 0x01, count = 1; i != oput; i = i <<= 1, count++);
		return ACC_PA_ALL_OUTPUT_N(8 * 6 + count);
		break;
	}
	case ACC_PA_ALL_OPAGE_SUB: {
		switch (oput) {
		case ACC_PA_ALL_OPUT_B1: {	return ACC_PA_ALL_SUB_1;	break; }
		case ACC_PA_ALL_OPUT_B2: {	return ACC_PA_ALL_SUB_N(2);	break; }
		case ACC_PA_ALL_OPUT_B3: {	return ACC_PA_ALL_SUB_N(3);	break; }
		case ACC_PA_ALL_OPUT_B4: {	return ACC_PA_ALL_SUB_N(4);	break; }
		case ACC_PA_ALL_OPUT_B5: {	return ACC_PA_ALL_SUB_N(5);	break; }
		case ACC_PA_ALL_OPUT_B6: {	return ACC_PA_ALL_SUB_N(6);	break; }
		case ACC_PA_ALL_OPUT_B7: {	return ACC_PA_ALL_SUB_N(7);	break; }
		case ACC_PA_ALL_OPUT_B8: {	return ACC_PA_ALL_SUB_N(8);	break; }
		default: {
			return 0xFF;
			break;
		}
		}
		break;
	}
	default: {
		return 0xFF;
		break;
	}
	}
	return 0xFF;
}
int Trans_OutputToAcc(unsigned char output){
	int result ;
	if (output < ACC_PA_ALL_OUTPUT_1 && ACC_PA_ALL_OUTPUT_MAX < output)
		return 0xFFFF;
	switch (output % 8) {
	case 0: {result = ACC_PA_ALL_OPUT_B1; break; }
	case 1:	{result = ACC_PA_ALL_OPUT_B2; break; }
	case 2:	{result = ACC_PA_ALL_OPUT_B3; break; }
	case 3:	{result = ACC_PA_ALL_OPUT_B4; break; }
	case 4:	{result = ACC_PA_ALL_OPUT_B5; break; }
	case 5:	{result = ACC_PA_ALL_OPUT_B6; break; }
	case 6:	{result = ACC_PA_ALL_OPUT_B7; break; }
	case 7:	{result = ACC_PA_ALL_OPUT_B8; break; }
	}
	switch (output / 8) {
		case 0:	{result += ACC_PA_ALL_OPAGE_OUT0 << 8; break;}
		case 1:	{result += ACC_PA_ALL_OPAGE_OUT1 << 8; break;}
		case 2:	{result += ACC_PA_ALL_OPAGE_OUT2 << 8; break;}
		case 3:	{result += ACC_PA_ALL_OPAGE_OUT3 << 8; break;}
		case 4:	{result += ACC_PA_ALL_OPAGE_OUT4 << 8; break;}
		case 5:	{result += ACC_PA_ALL_OPAGE_OUT5 << 8; break;}
		case 6:	{result += ACC_PA_ALL_OPAGE_OUT6 << 8; break;}
	}
	return result;
}
int Trans_SubToAcc(unsigned char output) {
	int result;
	if (output < ACC_PA_ALL_SUB_1 && ACC_PA_ALL_SUB_MAX < output)
		return 0xFFFF;
	result = ACC_PA_ALL_OPAGE_SUB << 8;
	switch (output) {
	case ACC_PA_ALL_SUB_1:    { result += ACC_PA_ALL_OPUT_B1;    break;	}
	case ACC_PA_ALL_SUB_N(2): {	result += ACC_PA_ALL_OPUT_B2;    break;	}
	case ACC_PA_ALL_SUB_N(3): {	result += ACC_PA_ALL_OPUT_B3;    break;	}
	case ACC_PA_ALL_SUB_N(4): {	result += ACC_PA_ALL_OPUT_B4;    break;	}
	case ACC_PA_ALL_SUB_N(5): {	result += ACC_PA_ALL_OPUT_B5;    break;	}
	case ACC_PA_ALL_SUB_N(6): {	result += ACC_PA_ALL_OPUT_B6;    break;	}
	case ACC_PA_ALL_SUB_N(7): {	result += ACC_PA_ALL_OPUT_B7;    break;	}
	case ACC_PA_ALL_SUB_N(8): {	result += ACC_PA_ALL_OPUT_B8;    break;	}
	}
	return result;
}

/*****************************************************/
/*		SRCPRY										*/
/*****************************************************/
unsigned char SRCPRY_GET_Enable_U16(unsigned char msb, unsigned char lsb){
	unsigned char en;
	int u16_Priority;
	u16_Priority = (msb << 8) | lsb;
	en = ((u16_Priority >> ACC_PA_ALL_SRCPRY_POS$_U16_EN) & ACC_PA_ALL_SRCPRY_MASK$_BIT1);
	return en;
}

unsigned char SRCPRY_GET_Enable_U8(unsigned char msb){
	unsigned char en;
	en = ((msb >> ACC_PA_ALL_SRCPRY_POS$_U8_EN) & ACC_PA_ALL_SRCPRY_MASK$_BIT1);
	return en;
}

//Ex: Setting enable
//SRCPRY_SET_Enable_U8(&MSB_byte, ACC_PA_ALL_SRCPRY_EN$_ON);
//Ex: Setting disable
//SRCPRY_SET_Enable_U8(&MSB_byte, ACC_PA_ALL_SRCPRY_EN$_OFF);
void SRCPRY_SET_Enable_U8(unsigned char *msb, unsigned char en) {
	unsigned char b = *msb;
	b= b & (~(0x01 << ACC_PA_ALL_SRCPRY_POS$_U8_EN));
	b = b | (en << ACC_PA_ALL_SRCPRY_POS$_U8_EN);
	*msb = b;
}

//ex: Get Line from P3
//SRCPRY_GET_LineFromPry_U16(MSB_byte,LSB_byte, ACC_PA_ALL_SRCPRY_POS$_U16_P3 )
unsigned char SRCPRY_GET_LineFromPry_U16(unsigned char msb, unsigned char lsb, unsigned char pos_pry ){
	unsigned char Line;
	unsigned int u16_Priority;
	u16_Priority = (msb << 8) | lsb;
	Line = (u16_Priority >> pos_pry) & ACC_PA_ALL_SRCPRY_MASK$_U16_BIT3;
	return Line;
}

//ex: Set L3 to P2  
//SRCPRY_SET_LineToPry_U16( MSB_byte, LSB_byte,   ACC_PA_ALL_SRCPRY_POS$_U16_P2,  PA_ALL_SRCPRY_LINE$_L3 )
void SRCPRY_SET_LineToPry_U16(unsigned char *msb, unsigned char *lsb, unsigned char pos_pry, unsigned char line ){
	unsigned char Line = (line & ACC_PA_ALL_SRCPRY_MASK$_U16_BIT3);
	unsigned int u16_Priority;
	u16_Priority = ((*msb) << 8) | (*lsb);
	
	u16_Priority = u16_Priority & (~(ACC_PA_ALL_SRCPRY_MASK$_U16_BIT3 << pos_pry));
	u16_Priority = u16_Priority | (Line << pos_pry);
		
	*msb = HIBYTE(u16_Priority);
	*lsb = LOBYTE(u16_Priority);
}








