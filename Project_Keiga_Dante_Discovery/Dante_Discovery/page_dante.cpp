#include "stdafx.h"
#include "device_control.h"
#include "AL003M.h"
#include "ACC_Command.h"
#include "utils.h"

#define TIMER_SEND_CMD			300

#define DELAY_CMD_TYPE_VOL_DANTE	0

static struct tagDelaySendCmd {
	uint16_t type;
	int argc;
	int argv[20];
}mDelaySend;

static unsigned char gain, q;
static unsigned int freq;
static DATAPEQ data_peq;
static DATAHLPF data_hlpf;
static DATAOUTVOL data_vol;

#define PEQ_INDEX_START 5
#define PEQ_INDEX_STOP	7

void Init_Dante_UI(HWND hwnd);
void Change_Dante_Edit_Data(HWND hwnd);

static HWND hParent;
static HWND hEditDante, hEditHPF, hEditC1F, hEditC2F, hEditC3F, hEditC1Q, hEditC2Q, hEditC3Q, hEditC1G, hEditC2G, hEditC3G;
static WNDPROC oldEditProcDante, oldEditProcHPF, oldEditProcC1F, oldEditProcC2F, oldEditProcC3F, oldEditProcC1Q, oldEditProcC2Q, oldEditProcC3Q, oldEditProcC1G, oldEditProcC2G, oldEditProcC3G;



LRESULT CALLBACK DanteEditProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_CHAR:
	{
		// Make sure we only allow specific characters
		if (!((wParam >= '0' && wParam <= '9')
			|| wParam == '.'
			|| wParam == '-'
			|| wParam == VK_RETURN
			|| wParam == VK_DELETE
			|| wParam == VK_BACK))
		{
			return 0;
		}
	}
	break;
	case WM_LBUTTONUP:
		SendMessage(hwnd, EM_SETSEL, 0, -1);
		break;
	case WM_KILLFOCUS:
		if (TabCtrl_GetCurSel(g_hTabControl) == PAGE_DANTE)
			Change_Dante_Edit_Data(hwnd);
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_RETURN:
		case VK_TAB:
			Change_Dante_Edit_Data(hwnd);

			if (wParam == VK_RETURN)
				return 0;
		}
		break;

	}
	if (hwnd == hEditDante)
		return CallWindowProc(oldEditProcDante, hwnd, uMsg, wParam, lParam);
	else if (hwnd == hEditHPF)
		return CallWindowProc(oldEditProcHPF, hwnd, uMsg, wParam, lParam);
	else if (hwnd == hEditC1F)
		return CallWindowProc(oldEditProcC1F, hwnd, uMsg, wParam, lParam);
	else if (hwnd == hEditC2F)
		return CallWindowProc(oldEditProcC2F, hwnd, uMsg, wParam, lParam);
	else if (hwnd == hEditC3F)
		return CallWindowProc(oldEditProcC3F, hwnd, uMsg, wParam, lParam);
	else if (hwnd == hEditC1Q)
		return CallWindowProc(oldEditProcC1Q, hwnd, uMsg, wParam, lParam);
	else if (hwnd == hEditC2Q)
		return CallWindowProc(oldEditProcC2Q, hwnd, uMsg, wParam, lParam);
	else if (hwnd == hEditC3Q)
		return CallWindowProc(oldEditProcC3Q, hwnd, uMsg, wParam, lParam);
	else if (hwnd == hEditC1G)
		return CallWindowProc(oldEditProcC1G, hwnd, uMsg, wParam, lParam);
	else if (hwnd == hEditC2G)
		return CallWindowProc(oldEditProcC2G, hwnd, uMsg, wParam, lParam);
	else if (hwnd == hEditC3G)
		return CallWindowProc(oldEditProcC3G, hwnd, uMsg, wParam, lParam);
	return 0;
}

INT_PTR CALLBACK DanteProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{

	switch (uMsg)
	{
	case WM_INITDIALOG: {
		HFONT hFont = CreateFont((int)20, 0, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, ARABIC_CHARSET,
			OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, "");
		Init_Dante_UI(hDlg);

		hParent = hDlg;

		hEditDante = GetDlgItem(hDlg, IDC_EDIT_DANTE);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_DANTE), (UINT)EM_SETLIMITTEXT, (WPARAM)5, (LPARAM)0);
		oldEditProcDante = (WNDPROC)SetWindowLongPtr(hEditDante, GWLP_WNDPROC, (LONG_PTR)DanteEditProc);

		hEditHPF = GetDlgItem(hDlg, IDC_EDIT_HP_F_DANTE);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_HP_F_DANTE), (UINT)EM_SETLIMITTEXT, (WPARAM)5, (LPARAM)0);
		oldEditProcHPF = (WNDPROC)SetWindowLongPtr(hEditHPF, GWLP_WNDPROC, (LONG_PTR)DanteEditProc);

		hEditC1F = GetDlgItem(hDlg, IDC_EDIT_C1_F_DANTE);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_C1_F_DANTE), (UINT)EM_SETLIMITTEXT, (WPARAM)5, (LPARAM)0);
		oldEditProcC1F = (WNDPROC)SetWindowLongPtr(hEditC1F, GWLP_WNDPROC, (LONG_PTR)DanteEditProc);

		hEditC2F = GetDlgItem(hDlg, IDC_EDIT_C2_F_DANTE);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_C2_F_DANTE), (UINT)EM_SETLIMITTEXT, (WPARAM)5, (LPARAM)0);
		oldEditProcC2F = (WNDPROC)SetWindowLongPtr(hEditC2F, GWLP_WNDPROC, (LONG_PTR)DanteEditProc);

		hEditC3F = GetDlgItem(hDlg, IDC_EDIT_C3_F_DANTE);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_C3_F_DANTE), (UINT)EM_SETLIMITTEXT, (WPARAM)5, (LPARAM)0);
		oldEditProcC3F = (WNDPROC)SetWindowLongPtr(hEditC3F, GWLP_WNDPROC, (LONG_PTR)DanteEditProc);

		hEditC1Q = GetDlgItem(hDlg, IDC_EDIT_C1_Q_DANTE);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_C1_Q_DANTE), (UINT)EM_SETLIMITTEXT, (WPARAM)5, (LPARAM)0);
		oldEditProcC1Q = (WNDPROC)SetWindowLongPtr(hEditC1Q, GWLP_WNDPROC, (LONG_PTR)DanteEditProc);

		hEditC2Q = GetDlgItem(hDlg, IDC_EDIT_C2_Q_DANTE);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_C2_Q_DANTE), (UINT)EM_SETLIMITTEXT, (WPARAM)5, (LPARAM)0);
		oldEditProcC2Q = (WNDPROC)SetWindowLongPtr(hEditC2Q, GWLP_WNDPROC, (LONG_PTR)DanteEditProc);

		hEditC3Q = GetDlgItem(hDlg, IDC_EDIT_C3_Q_DANTE);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_C3_Q_DANTE), (UINT)EM_SETLIMITTEXT, (WPARAM)5, (LPARAM)0);
		oldEditProcC3Q = (WNDPROC)SetWindowLongPtr(hEditC3Q, GWLP_WNDPROC, (LONG_PTR)DanteEditProc);

		hEditC1G = GetDlgItem(hDlg, IDC_EDIT_C1_G_DANTE);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_C1_G_DANTE), (UINT)EM_SETLIMITTEXT, (WPARAM)4, (LPARAM)0);
		oldEditProcC1G = (WNDPROC)SetWindowLongPtr(hEditC1G, GWLP_WNDPROC, (LONG_PTR)DanteEditProc);

		hEditC2G = GetDlgItem(hDlg, IDC_EDIT_C2_G_DANTE);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_C2_G_DANTE), (UINT)EM_SETLIMITTEXT, (WPARAM)4, (LPARAM)0);
		oldEditProcC2G = (WNDPROC)SetWindowLongPtr(hEditC2G, GWLP_WNDPROC, (LONG_PTR)DanteEditProc);

		hEditC3G = GetDlgItem(hDlg, IDC_EDIT_C3_G_DANTE);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_C3_G_DANTE), (UINT)EM_SETLIMITTEXT, (WPARAM)4, (LPARAM)0);
		oldEditProcC3G = (WNDPROC)SetWindowLongPtr(hEditC3G, GWLP_WNDPROC, (LONG_PTR)DanteEditProc);
	}
		break;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDC_CHECK_MUTE_DANTE:
			g_pDevice->pa[AL003M_MUTE_DANTE] = (unsigned char)SendMessageW((HWND)lParam, BM_GETCHECK, 0, 0);
			data_vol.OutputPage._.Byte = 1;
			data_vol.Output._.Byte = 2;
			data_vol.VolumeL = 0xFF;
			data_vol.VolumeR = 0xFF;
			data_vol.LMuteVolH.Volume = 0x7F;
			data_vol.LMuteVolH.Mute = g_pDevice->pa[AL003M_MUTE_DANTE];
			data_vol.RMuteVolH.Byte = 0xFF;
			Set_Send_Flag(g_pDevice, ACC_CMD_OUTVOL, ACC_HEADER_WRITE, 6, data_vol.OutputPage._.Byte, data_vol.Output._.Byte, data_vol.VolumeL, data_vol.VolumeR, data_vol.LMuteVolH.Byte, data_vol.RMuteVolH.Byte);
			break;
		case IDC_CHECK_HP_FILTER_DANTE:
			g_pDevice->pa[AL003M_HIPASS_SWITCH_DANTE] = !(unsigned char)SendMessageW((HWND)lParam, BM_GETCHECK, 0, 0);
			data_hlpf.OutputPage._.Byte = 1;
			data_hlpf.Output._.Byte = 2;
			data_hlpf.Channel.Byte = 1;
			data_hlpf.Type = FILTER_TYPE_HIPASS;
			data_hlpf.EnFreqH.Frequency = 0x7F;
			data_hlpf.FreqL = 0xFF;
			data_hlpf.EnFreqH.Enable = g_pDevice->pa[AL003M_HIPASS_SWITCH_DANTE];
			data_hlpf.Slope = 0xFF;
			Set_Send_Flag(g_pDevice, ACC_CMD_HLPF, ACC_HEADER_WRITE, 7, data_hlpf.OutputPage._.Byte, data_hlpf.Output._.Byte, data_hlpf.Channel.Byte, data_hlpf.Type, data_hlpf.EnFreqH, data_hlpf.FreqL, data_hlpf.Slope);
			break;
		case IDC_CHECK_C1_DANTE:
			data_peq.OutputPage._.Byte = 1;
			data_peq.Output._.Byte = 1;
			data_peq.Index = 5;
			data_peq.EnFreqH.Frequency = 0x7F;
			data_peq.FreqL = 0xFF;
			data_peq.Gain = 0xFF;
			data_peq.qH = 0xFF;
			data_peq.qL = 0xFF;
			data_peq.EnFreqH.Enable = !SendMessage((HWND)lParam, BM_GETCHECK, 0, 0);
			data_peq.Channel.Byte = 1;
			g_pDevice->pa[AL003M_C6_SWITCH_DANTE] = data_peq.EnFreqH.Enable;
			Set_Send_Flag(g_pDevice, ACC_CMD_PEQ, ACC_HEADER_WRITE, 11, data_peq.OutputPage._.Byte, data_peq.Output._.Byte, data_peq.Channel.Byte, data_peq.Mode, data_peq.Index, data_peq.Type, data_peq.Gain, data_peq.EnFreqH, data_peq.FreqL, data_peq.qH, data_peq.qL);
			break;
		case IDC_CHECK_C2_DANTE:
			data_peq.OutputPage._.Byte = 1;
			data_peq.Output._.Byte = 1;
			data_peq.Index = 6;
			data_peq.EnFreqH.Frequency = 0x7F;
			data_peq.FreqL = 0xFF;
			data_peq.Gain = 0xFF;
			data_peq.qH = 0xFF;
			data_peq.qL = 0xFF;
			data_peq.EnFreqH.Enable = !SendMessage((HWND)lParam, BM_GETCHECK, 0, 0);
			data_peq.Channel.Byte = 1;
			g_pDevice->pa[AL003M_C7_SWITCH_DANTE] = data_peq.EnFreqH.Enable;
			Set_Send_Flag(g_pDevice, ACC_CMD_PEQ, ACC_HEADER_WRITE, 11, data_peq.OutputPage._.Byte, data_peq.Output._.Byte, data_peq.Channel.Byte, data_peq.Mode, data_peq.Index, data_peq.Type, data_peq.Gain, data_peq.EnFreqH, data_peq.FreqL, data_peq.qH, data_peq.qL);
			break;
		case IDC_CHECK_C3_DANTE:
			data_peq.OutputPage._.Byte = 1;
			data_peq.Output._.Byte = 1;
			data_peq.Index = 7;
			data_peq.EnFreqH.Frequency = 0x7F;
			data_peq.FreqL = 0xFF;
			data_peq.Gain = 0xFF;
			data_peq.qH = 0xFF;
			data_peq.qL = 0xFF;
			data_peq.EnFreqH.Enable = !SendMessage((HWND)lParam, BM_GETCHECK, 0, 0);
			data_peq.Channel.Byte = 1;
			g_pDevice->pa[AL003M_C8_SWITCH_DANTE] = data_peq.EnFreqH.Enable;
			Set_Send_Flag(g_pDevice, ACC_CMD_PEQ, ACC_HEADER_WRITE, 11, data_peq.OutputPage._.Byte, data_peq.Output._.Byte, data_peq.Channel.Byte, data_peq.Mode, data_peq.Index, data_peq.Type, data_peq.Gain, data_peq.EnFreqH, data_peq.FreqL, data_peq.qH, data_peq.qL);
			break;
		case IDC_COMBO_HP_SLOPE_DANTE:
			if (HIWORD(wParam) == CBN_SELENDOK) {
				g_pDevice->pa[AL003M_HIPASS_SLOPE_DANTE] = (unsigned char)SendMessage((HWND)lParam, (UINT)CB_GETCURSEL, (WPARAM)0, (LPARAM)0);
				data_hlpf.OutputPage._.Byte = 1;
				data_hlpf.Output._.Byte = 2;
				data_hlpf.Channel.Byte = 1;
				data_hlpf.Type = FILTER_TYPE_HIPASS;
				data_hlpf.EnFreqH.Frequency = 0x7F;
				data_hlpf.FreqL = 0xFF;
				data_hlpf.EnFreqH.Enable = g_pDevice->pa[AL003M_HIPASS_SWITCH_DANTE];
				data_hlpf.Slope = g_pDevice->pa[AL003M_HIPASS_SLOPE_DANTE];
				Set_Send_Flag(g_pDevice, ACC_CMD_HLPF, ACC_HEADER_WRITE, 7, data_hlpf.OutputPage._.Byte, data_hlpf.Output._.Byte, data_hlpf.Channel.Byte, data_hlpf.Type, data_hlpf.EnFreqH, data_hlpf.FreqL, data_hlpf.Slope);
			}
			break;
		}

		break;
	case WM_VSCROLL:
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_DANTE) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {

			int value = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_DANTE), TBM_GETPOS, 0, 0) - VOLUME_CHANNEL_MAX) + VOLUME_CHANNEL_MIN;
			g_pDevice->pa[AL003M_VOL_DANTE_H] = value / 256;
			g_pDevice->pa[AL003M_VOL_DANTE] = value % 256;

			char *text = (char*)malloc(10 * sizeof(char));
			memset(text, 0, 10 * sizeof(char));

			value = VOLUME_CHANNEL_MAX - value;
			
			if (value > 0) {
				sprintf(text, "%s%.1f", "-", (double)value / 10);
			}
			else {
				sprintf(text, "%.1f", (double)value / 10);
			}
			SendMessage(GetDlgItem(hDlg, IDC_EDIT_DANTE), WM_SETTEXT, (WPARAM)0, (LPARAM)text);
			
			free(text);

			KillTimer(g_hDeviceControl, TIME_LINK_AMP);
			mDelaySend.type = DELAY_CMD_TYPE_VOL_DANTE;
			KillTimer(hDlg, TIMER_SEND_CMD);
			SetTimer(hDlg, TIMER_SEND_CMD, TIMER_SEND_CMD, NULL);
		}
		break;
	case WM_TIMER: {
		switch (wParam) {
		case TIMER_SEND_CMD: {
			switch (mDelaySend.type) {
			case DELAY_CMD_TYPE_VOL_DANTE: {
				data_vol.OutputPage._.Byte = 1;
				data_vol.Output._.Byte = 2;
				data_vol.VolumeR = 0xFF;
				data_vol.RMuteVolH.Byte = 0xFF;
				data_vol.VolumeL = g_pDevice->pa[AL003M_VOL_DANTE];
				data_vol.LMuteVolH.Volume = g_pDevice->pa[AL003M_VOL_DANTE_H];
				data_vol.LMuteVolH.Mute = g_pDevice->pa[AL003M_MUTE_DANTE];
				Set_Send_Flag(g_pDevice, ACC_CMD_OUTVOL, ACC_HEADER_WRITE, 6, data_vol.OutputPage._.Byte, data_vol.Output._.Byte, data_vol.VolumeL, data_vol.VolumeR, data_vol.LMuteVolH.Byte, data_vol.RMuteVolH.Byte);
				break; }
			}
			KillTimer(hDlg, TIMER_SEND_CMD);
			SetTimer(g_hDeviceControl, TIME_LINK_AMP, TIME_LINK_AMP, NULL);
			break;
		}
		}
		break;
	}
	case WM_NOTIFY:
		break;
	case WM_PAINT:
		if (g_reflash_ui) {
			Init_Dante_UI(hDlg);
			g_reflash_ui = FALSE;
		}
		break;
	case WM_CLOSE:
		return TRUE;

	case WM_DESTROY:
		return TRUE;
	}
	return FALSE;
}


void Init_Dante_UI(HWND hDlg)
{
	if (g_pDevice->pa[AL003M_MUTE_DANTE] == 1)
		SendMessage(GetDlgItem(hDlg, IDC_CHECK_MUTE_DANTE), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
	else
		SendMessage(GetDlgItem(hDlg, IDC_CHECK_MUTE_DANTE), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_DANTE), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(VOLUME_CHANNEL_MIN, VOLUME_CHANNEL_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_DANTE), TBM_SETPOS, (WPARAM)1, abs(g_pDevice->pa[AL003M_VOL_DANTE_H] * 256 + g_pDevice->pa[AL003M_VOL_DANTE] - VOLUME_CHANNEL_MAX));
	int value;
	char *text = (char*)malloc(10 * sizeof(char));
	memset(text, 0, 10 * sizeof(char));
	value = VOLUME_CHANNEL_MAX - (g_pDevice->pa[AL003M_VOL_DANTE_H] * 256 + g_pDevice->pa[AL003M_VOL_DANTE]);
	if (value > 0) {
		sprintf(text, "%s%.1f", "-", (double)value / 10);
	}
	else {
		sprintf(text, "%.1f", (double)value / 10);
	}
	SendMessage(GetDlgItem(hDlg, IDC_EDIT_DANTE), WM_SETTEXT, (WPARAM)0, (LPARAM)text);

	//Filter
	SendMessage(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_DANTE), CB_RESETCONTENT, 0, 0);
	SendMessage(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_DANTE), (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)"6 dB/oct");
	SendMessage(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_DANTE), (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)"12 dB/oct");
	SendMessage(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_DANTE), CB_SETCURSEL, g_pDevice->pa[AL003M_HIPASS_SLOPE_DANTE], 0);
	Set_Freq_Text(hDlg, IDC_EDIT_HP_F_DANTE, (unsigned int)g_pDevice->pa[AL003M_HIPASS_FREQ_H_DANTE] * 256 + g_pDevice->pa[AL003M_HIPASS_FREQ_L_DANTE]);
	SendMessage(GetDlgItem(hDlg, IDC_CHECK_HP_FILTER_DANTE), BM_SETCHECK, (WPARAM)!g_pDevice->pa[AL003M_HIPASS_SWITCH_DANTE], (LPARAM)1);

	//EQ
	SendMessage(GetDlgItem(hDlg, IDC_CHECK_C1_DANTE), BM_SETCHECK, (WPARAM)!g_pDevice->pa[AL003M_C6_SWITCH_DANTE], (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_CHECK_C2_DANTE), BM_SETCHECK, (WPARAM)!g_pDevice->pa[AL003M_C7_SWITCH_DANTE], (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_CHECK_C3_DANTE), BM_SETCHECK, (WPARAM)!g_pDevice->pa[AL003M_C8_SWITCH_DANTE], (LPARAM)1);
	Set_Freq_Text(hDlg, IDC_EDIT_C1_F_DANTE, (unsigned int)g_pDevice->pa[AL003M_C6_FREQ_H_DANTE] * 256 + g_pDevice->pa[AL003M_C6_FREQ_L_DANTE]);
	Set_Freq_Text(hDlg, IDC_EDIT_C2_F_DANTE, (unsigned int)g_pDevice->pa[AL003M_C7_FREQ_H_DANTE] * 256 + g_pDevice->pa[AL003M_C7_FREQ_L_DANTE]);
	Set_Freq_Text(hDlg, IDC_EDIT_C3_F_DANTE, (unsigned int)g_pDevice->pa[AL003M_C8_FREQ_H_DANTE] * 256 + g_pDevice->pa[AL003M_C8_FREQ_L_DANTE]);
	Set_Gain_Text(hDlg, IDC_EDIT_C1_G_DANTE, ((double)g_pDevice->pa[AL003M_C6_GAIN_DANTE] - 40) / 2);
	Set_Gain_Text(hDlg, IDC_EDIT_C2_G_DANTE, ((double)g_pDevice->pa[AL003M_C7_GAIN_DANTE] - 40) / 2);
	Set_Gain_Text(hDlg, IDC_EDIT_C3_G_DANTE, ((double)g_pDevice->pa[AL003M_C8_GAIN_DANTE] - 40) / 2);
	Set_Q_Text(hDlg, IDC_EDIT_C1_Q_DANTE, ((double)(g_pDevice->pa[AL003M_C6_Q_H_DANTE] * 256 + g_pDevice->pa[AL003M_C6_Q_L_DANTE]) / 100));
	Set_Q_Text(hDlg, IDC_EDIT_C2_Q_DANTE, ((double)(g_pDevice->pa[AL003M_C7_Q_H_DANTE] * 256 + g_pDevice->pa[AL003M_C7_Q_L_DANTE]) / 100));
	Set_Q_Text(hDlg, IDC_EDIT_C3_Q_DANTE, ((double)(g_pDevice->pa[AL003M_C8_Q_H_DANTE] * 256 + g_pDevice->pa[AL003M_C8_Q_L_DANTE]) / 100));

	InvalidateRect(hDlg, NULL, TRUE);
}


void Change_Dante_Edit_Data(HWND hwnd)
{
	char number[6];

	if (hwnd == hEditDante) {
		GetDlgItemTextA(hParent, IDC_EDIT_DANTE, number, 6);
		double i = atof(number);
		if (i < -50) {
			i = -50;
			g_pDevice->pa[AL003M_VOL_DANTE_H] = 0;
			g_pDevice->pa[AL003M_VOL_DANTE] = 0;
		}
		else if (i > 0) {
			i = 0;
			g_pDevice->pa[AL003M_VOL_DANTE_H] = VOLUME_CHANNEL_MAX / 256;
			g_pDevice->pa[AL003M_VOL_DANTE] = VOLUME_CHANNEL_MAX % 256;
		}
		else {
			g_pDevice->pa[AL003M_VOL_DANTE_H] = ((int)(i * 10) + VOLUME_CHANNEL_MAX) / 256;
			g_pDevice->pa[AL003M_VOL_DANTE] = ((int)(i * 10) + VOLUME_CHANNEL_MAX) % 256;
		}
		SendMessage(GetDlgItem(hParent, IDC_SLIDER_DANTE), TBM_SETPOS, (WPARAM)1, abs(g_pDevice->pa[AL003M_VOL_DANTE_H] * 256 + g_pDevice->pa[AL003M_VOL_DANTE] - VOLUME_CHANNEL_MAX));

		data_vol.OutputPage._.Byte = 1;
		data_vol.Output._.Byte = 2;
		data_vol.VolumeR = 0xFF;
		data_vol.RMuteVolH.Byte = 0xFF;
		data_vol.VolumeL = g_pDevice->pa[AL003M_VOL_DANTE];
		data_vol.LMuteVolH.Volume = g_pDevice->pa[AL003M_VOL_DANTE_H];
		data_vol.LMuteVolH.Mute = g_pDevice->pa[AL003M_MUTE_DANTE];
		Set_Send_Flag(g_pDevice, ACC_CMD_OUTVOL, ACC_HEADER_WRITE, 6, data_vol.OutputPage._.Byte, data_vol.Output._.Byte, data_vol.VolumeL, data_vol.VolumeR, data_vol.LMuteVolH.Byte, data_vol.RMuteVolH.Byte);
	}
	else if (hwnd == hEditHPF) {
		GetDlgItemTextA(hParent, IDC_EDIT_HP_F_DANTE, number, 6);
		int i = atoi(number);
			if (i < HIPASS_FERQUENCY_MIN) {
				i = HIPASS_FERQUENCY_MIN;
			}
			else if (i > HIPASS_FERQUENCY_MAX) {
				i = (int)HIPASS_FERQUENCY_MAX;
			}
			g_pDevice->pa[AL003M_HIPASS_FREQ_H_DANTE] = (unsigned char)(i / 256);
			g_pDevice->pa[AL003M_HIPASS_FREQ_L_DANTE] = (unsigned char)(i % 256);

			data_hlpf.OutputPage._.Byte = 1;
			data_hlpf.Output._.Byte = 2;
			data_hlpf.Channel.Byte = 1;
			data_hlpf.Type = FILTER_TYPE_HIPASS;
			data_hlpf.EnFreqH.Frequency = g_pDevice->pa[AL003M_HIPASS_FREQ_H_DANTE];
			data_hlpf.FreqL = g_pDevice->pa[AL003M_HIPASS_FREQ_L_DANTE];
			data_hlpf.EnFreqH.Enable = g_pDevice->pa[AL003M_HIPASS_SWITCH_DANTE];
			data_hlpf.Slope = 0xFF;
			Set_Send_Flag(g_pDevice, ACC_CMD_HLPF, ACC_HEADER_WRITE, 7, data_hlpf.OutputPage._.Byte, data_hlpf.Output._.Byte, data_hlpf.Channel.Byte, data_hlpf.Type, data_hlpf.EnFreqH, data_hlpf.FreqL, data_hlpf.Slope);
		}
	else if (hwnd == hEditC1F) {
		GetDlgItemTextA(hParent, IDC_EDIT_C1_F_DANTE, number, 6);
		int i = atoi(number);
		if (i < CEQ_FERQUENCY_MIN) {
			i = CEQ_FERQUENCY_MIN;
			Set_Freq_Text(hParent, IDC_EDIT_C1_F_DANTE, i);
		}
		else if (i > CEQ_FERQUENCY_MAX) {
			i = CEQ_FERQUENCY_MAX;
			Set_Freq_Text(hParent, IDC_EDIT_C1_F_DANTE, i);
		}

		g_pDevice->pa[AL003M_C6_FREQ_H_DANTE] = i / 256;
		g_pDevice->pa[AL003M_C6_FREQ_L_DANTE] = i % 256;

		data_peq.OutputPage._.Byte = 1;
		data_peq.Output._.Byte = 1;
		data_peq.Index = 5;
		data_peq.EnFreqH.Frequency = g_pDevice->pa[AL003M_C6_FREQ_H_DANTE];
		data_peq.FreqL = g_pDevice->pa[AL003M_C6_FREQ_L_DANTE];
		data_peq.Gain = 0xFF;
		data_peq.qH = 0xFF;
		data_peq.qL = 0xFF;
		data_peq.EnFreqH.Enable = g_pDevice->pa[AL003M_C6_SWITCH_DANTE];
		data_peq.Channel.Byte = 1;
		Set_Send_Flag(g_pDevice, ACC_CMD_PEQ, ACC_HEADER_WRITE, 11, data_peq.OutputPage._.Byte, data_peq.Output._.Byte, data_peq.Channel.Byte, data_peq.Mode, data_peq.Index, data_peq.Type, data_peq.Gain, data_peq.EnFreqH, data_peq.FreqL, data_peq.qH, data_peq.qL);
	}
	else if (hwnd == hEditC2F) {
		GetDlgItemTextA(hParent, IDC_EDIT_C2_F_DANTE, number, 6);
		int i = atoi(number);
		if (i < CEQ_FERQUENCY_MIN) {
			i = CEQ_FERQUENCY_MIN;
			Set_Freq_Text(hParent, IDC_EDIT_C2_F_DANTE, i);
		}
		else if (i > CEQ_FERQUENCY_MAX) {
			i = CEQ_FERQUENCY_MAX;
			Set_Freq_Text(hParent, IDC_EDIT_C2_F_DANTE, i);
		}

		g_pDevice->pa[AL003M_C7_FREQ_H_DANTE] = i / 256;
		g_pDevice->pa[AL003M_C7_FREQ_L_DANTE] = i % 256;

		data_peq.OutputPage._.Byte = 1;
		data_peq.Output._.Byte = 1;
		data_peq.Index = 6;
		data_peq.EnFreqH.Frequency = g_pDevice->pa[AL003M_C7_FREQ_H_DANTE];
		data_peq.FreqL = g_pDevice->pa[AL003M_C7_FREQ_L_DANTE];
		data_peq.Gain = 0xFF;
		data_peq.qH = 0xFF;
		data_peq.qL = 0xFF;
		data_peq.EnFreqH.Enable = g_pDevice->pa[AL003M_C7_SWITCH_DANTE];
		data_peq.Channel.Byte = 1;
		Set_Send_Flag(g_pDevice, ACC_CMD_PEQ, ACC_HEADER_WRITE, 11, data_peq.OutputPage._.Byte, data_peq.Output._.Byte, data_peq.Channel.Byte, data_peq.Mode, data_peq.Index, data_peq.Type, data_peq.Gain, data_peq.EnFreqH, data_peq.FreqL, data_peq.qH, data_peq.qL);
	}
	else if (hwnd == hEditC3F) {
		GetDlgItemTextA(hParent, IDC_EDIT_C3_F_DANTE, number, 6);
		int i = atoi(number);
		if (i < CEQ_FERQUENCY_MIN) {
			i = CEQ_FERQUENCY_MIN;
			Set_Freq_Text(hParent, IDC_EDIT_C3_F_DANTE, i);
		}
		else if (i > CEQ_FERQUENCY_MAX) {
			i = CEQ_FERQUENCY_MAX;
			Set_Freq_Text(hParent, IDC_EDIT_C3_F_DANTE, i);
		}

		g_pDevice->pa[AL003M_C8_FREQ_H_DANTE] = i / 256;
		g_pDevice->pa[AL003M_C8_FREQ_L_DANTE] = i % 256;

		data_peq.OutputPage._.Byte = 1;
		data_peq.Output._.Byte = 1;
		data_peq.Index = 7;
		data_peq.EnFreqH.Frequency = g_pDevice->pa[AL003M_C8_FREQ_H_DANTE];
		data_peq.FreqL = g_pDevice->pa[AL003M_C8_FREQ_L_DANTE];
		data_peq.Gain = 0xFF;
		data_peq.qH = 0xFF;
		data_peq.qL = 0xFF;
		data_peq.EnFreqH.Enable = g_pDevice->pa[AL003M_C8_SWITCH_DANTE];
		data_peq.Channel.Byte = 1;
		Set_Send_Flag(g_pDevice, ACC_CMD_PEQ, ACC_HEADER_WRITE, 11, data_peq.OutputPage._.Byte, data_peq.Output._.Byte, data_peq.Channel.Byte, data_peq.Mode, data_peq.Index, data_peq.Type, data_peq.Gain, data_peq.EnFreqH, data_peq.FreqL, data_peq.qH, data_peq.qL);
	}
	else if (hwnd == hEditC1Q) {
		GetDlgItemTextA(hParent, IDC_EDIT_C1_Q_DANTE, number, 6);
		double i = atof(number);
		if (i < 0.01) {
			i = 0.01;
			Set_Q_Text(hParent, IDC_EDIT_C1_Q_DANTE, i);
		}
		else if (i > 40) {
			i = 40;
			Set_Q_Text(hParent, IDC_EDIT_C1_Q_DANTE, i);
		}
		else
			Set_Q_Text(hParent, IDC_EDIT_C1_Q_DANTE, i);

		g_pDevice->pa[AL003M_C6_Q_H_DANTE] = (i * 100) / 256;
		g_pDevice->pa[AL003M_C6_Q_L_DANTE] = (int)(i * 100) % 256;
		data_peq.OutputPage._.Byte = 1;
		data_peq.Output._.Byte = 1;
		data_peq.Index = 5;
		data_peq.Channel.Byte = 1;
		data_peq.EnFreqH.Frequency = 0x7F;
		data_peq.FreqL = 0xFF;
		data_peq.Gain = 0xFF;
		data_peq.qH = g_pDevice->pa[AL003M_C6_Q_H_DANTE];
		data_peq.qL = g_pDevice->pa[AL003M_C6_Q_L_DANTE];
		data_peq.EnFreqH.Enable = g_pDevice->pa[AL003M_C6_SWITCH_DANTE];
		Set_Send_Flag(g_pDevice, ACC_CMD_PEQ, ACC_HEADER_WRITE, 11, data_peq.OutputPage._.Byte, data_peq.Output._.Byte, data_peq.Channel.Byte, data_peq.Mode, data_peq.Index, data_peq.Type, data_peq.Gain, data_peq.EnFreqH, data_peq.FreqL, data_peq.qH, data_peq.qL);
	}
	else if (hwnd == hEditC2Q) {
		GetDlgItemTextA(hParent, IDC_EDIT_C2_Q_DANTE, number, 6);
		double i = atof(number);
		if (i < 0.01) {
			i = 0.01;
			Set_Q_Text(hParent, IDC_EDIT_C2_Q_DANTE, i);
		}
		else if (i > 40) {
			i = 40;
			Set_Q_Text(hParent, IDC_EDIT_C2_Q_DANTE, i);
		}
		else
			Set_Q_Text(hParent, IDC_EDIT_C2_Q_DANTE, i);

		g_pDevice->pa[AL003M_C7_Q_H_DANTE] = (i * 100) / 256;
		g_pDevice->pa[AL003M_C7_Q_L_DANTE] = (int)(i * 100) % 256;
		data_peq.OutputPage._.Byte = 1;
		data_peq.Output._.Byte = 1;
		data_peq.Index = 6;
		data_peq.Channel.Byte = 1;
		data_peq.EnFreqH.Frequency = 0x7F;
		data_peq.FreqL = 0xFF;
		data_peq.Gain = 0xFF;
		data_peq.qH = g_pDevice->pa[AL003M_C7_Q_H_DANTE];
		data_peq.qL = g_pDevice->pa[AL003M_C7_Q_L_DANTE];
		data_peq.EnFreqH.Enable = g_pDevice->pa[AL003M_C7_SWITCH_DANTE];
		Set_Send_Flag(g_pDevice, ACC_CMD_PEQ, ACC_HEADER_WRITE, 11, data_peq.OutputPage._.Byte, data_peq.Output._.Byte, data_peq.Channel.Byte, data_peq.Mode, data_peq.Index, data_peq.Type, data_peq.Gain, data_peq.EnFreqH, data_peq.FreqL, data_peq.qH, data_peq.qL);
	}
	else if (hwnd == hEditC3Q) {
		GetDlgItemTextA(hParent, IDC_EDIT_C3_Q_DANTE, number, 6);
		double i = atof(number);
		if (i < 0.01) {
			i = 0.01;
			Set_Q_Text(hParent, IDC_EDIT_C3_Q_DANTE, i);
		}
		else if (i > 40) {
			i = 40;
			Set_Q_Text(hParent, IDC_EDIT_C3_Q_DANTE, i);
		}
		else
			Set_Q_Text(hParent, IDC_EDIT_C3_Q_DANTE, i);

		g_pDevice->pa[AL003M_C8_Q_H_DANTE] = (i * 100) / 256;
		g_pDevice->pa[AL003M_C8_Q_L_DANTE] = (int)(i * 100) % 256;
		data_peq.OutputPage._.Byte = 1;
		data_peq.Output._.Byte = 1;
		data_peq.Index = 7;
		data_peq.Channel.Byte = 1;
		data_peq.EnFreqH.Frequency = 0x7F;
		data_peq.FreqL = 0xFF;
		data_peq.Gain = 0xFF;
		data_peq.qH = g_pDevice->pa[AL003M_C8_Q_H_DANTE];
		data_peq.qL = g_pDevice->pa[AL003M_C8_Q_L_DANTE];
		data_peq.EnFreqH.Enable = g_pDevice->pa[AL003M_C8_SWITCH_DANTE];
		Set_Send_Flag(g_pDevice, ACC_CMD_PEQ, ACC_HEADER_WRITE, 11, data_peq.OutputPage._.Byte, data_peq.Output._.Byte, data_peq.Channel.Byte, data_peq.Mode, data_peq.Index, data_peq.Type, data_peq.Gain, data_peq.EnFreqH, data_peq.FreqL, data_peq.qH, data_peq.qL);
	}
	else if (hwnd == hEditC1G) {
		GetDlgItemTextA(hParent, IDC_EDIT_C1_G_DANTE, number, 6);
		double i = atof(number);
		if (i < -20) {
			i = -20;
			Set_Gain_Text(hParent, IDC_EDIT_C1_G_DANTE, i);
		}
		else if (i > 20) {
			i = 20;
			Set_Gain_Text(hParent, IDC_EDIT_C1_G_DANTE, i);
		}
		else {
			int j = i * 2;
			i = (double)j / 2;
			Set_Gain_Text(hParent, IDC_EDIT_C1_G_DANTE, i);
		}

		g_pDevice->pa[AL003M_C6_GAIN_DANTE] = i * 2 + 40;
		data_peq.OutputPage._.Byte = 1;
		data_peq.Output._.Byte = 1;
		data_peq.Index = 5;
		data_peq.Channel.Byte = 1;
		data_peq.EnFreqH.Frequency = 0x7F;
		data_peq.FreqL = 0xFF;
		data_peq.Gain = g_pDevice->pa[AL003M_C6_GAIN_DANTE];
		data_peq.qH = 0xFF;
		data_peq.qL = 0xFF;
		data_peq.EnFreqH.Enable = g_pDevice->pa[AL003M_C6_SWITCH_DANTE];
		Set_Send_Flag(g_pDevice, ACC_CMD_PEQ, ACC_HEADER_WRITE, 11, data_peq.OutputPage._.Byte, data_peq.Output._.Byte, data_peq.Channel.Byte, data_peq.Mode, data_peq.Index, data_peq.Type, data_peq.Gain, data_peq.EnFreqH, data_peq.FreqL, data_peq.qH, data_peq.qL);
	}
	else if (hwnd == hEditC2G) {
		GetDlgItemTextA(hParent, IDC_EDIT_C2_G_DANTE, number, 6);
		double i = atof(number);
		if (i < -20) {
			i = -20;
			Set_Gain_Text(hParent, IDC_EDIT_C2_G_DANTE, i);
		}
		else if (i > 20) {
			i = 20;
			Set_Gain_Text(hParent, IDC_EDIT_C2_G_DANTE, i);
		}
		else {
			int j = i * 2;
			i = (double)j / 2;
			Set_Gain_Text(hParent, IDC_EDIT_C2_G_DANTE, i);
		}

		g_pDevice->pa[AL003M_C7_GAIN_DANTE] = i * 2 + 40;
		data_peq.OutputPage._.Byte = 1;
		data_peq.Output._.Byte = 1;
		data_peq.Index = 6;
		data_peq.Channel.Byte = 1;
		data_peq.EnFreqH.Frequency = 0x7F;
		data_peq.FreqL = 0xFF;
		data_peq.Gain = g_pDevice->pa[AL003M_C7_GAIN_DANTE];
		data_peq.qH = 0xFF;
		data_peq.qL = 0xFF;
		data_peq.EnFreqH.Enable = g_pDevice->pa[AL003M_C7_SWITCH_DANTE];
		Set_Send_Flag(g_pDevice, ACC_CMD_PEQ, ACC_HEADER_WRITE, 11, data_peq.OutputPage._.Byte, data_peq.Output._.Byte, data_peq.Channel.Byte, data_peq.Mode, data_peq.Index, data_peq.Type, data_peq.Gain, data_peq.EnFreqH, data_peq.FreqL, data_peq.qH, data_peq.qL);
	}
	else if (hwnd == hEditC3G) {
		GetDlgItemTextA(hParent, IDC_EDIT_C3_G_DANTE, number, 6);
		double i = atof(number);
		if (i < -20) {
			i = -20;
			Set_Gain_Text(hParent, IDC_EDIT_C3_G_DANTE, i);
		}
		else if (i > 20) {
			i = 20;
			Set_Gain_Text(hParent, IDC_EDIT_C3_G_DANTE, i);
		}
		else {
			int j = i * 2;
			i = (double)j / 2;
			Set_Gain_Text(hParent, IDC_EDIT_C3_G_DANTE, i);
		}

		g_pDevice->pa[AL003M_C8_GAIN_DANTE] = i * 2 + 40;
		data_peq.OutputPage._.Byte = 1;
		data_peq.Output._.Byte = 1;
		data_peq.Index = 7;
		data_peq.Channel.Byte = 1;
		data_peq.EnFreqH.Frequency = 0x7F;
		data_peq.FreqL = 0xFF;
		data_peq.Gain = g_pDevice->pa[AL003M_C8_GAIN_DANTE];
		data_peq.qH = 0xFF;
		data_peq.qL = 0xFF;
		data_peq.EnFreqH.Enable = g_pDevice->pa[AL003M_C8_SWITCH_DANTE];
		Set_Send_Flag(g_pDevice, ACC_CMD_PEQ, ACC_HEADER_WRITE, 11, data_peq.OutputPage._.Byte, data_peq.Output._.Byte, data_peq.Channel.Byte, data_peq.Mode, data_peq.Index, data_peq.Type, data_peq.Gain, data_peq.EnFreqH, data_peq.FreqL, data_peq.qH, data_peq.qL);
	}
}