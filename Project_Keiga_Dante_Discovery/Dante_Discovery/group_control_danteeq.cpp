#include "stdafx.h"
#include "group_control.h"
#include "ACC_Command.h"
#include "device_state.h"
#include "utils.h"

HWND hdlgGroupDanteEQ = NULL;
static HWND hParent;
static HWND hEditHPFDante, hEditC1F, hEditC2F, hEditC3F, hEditC1Q, hEditC2Q, hEditC3Q, hEditC1G, hEditC2G, hEditC3G;
static WNDPROC oldEditProcDante, oldEditProcHPFDante, oldEditProcC1F, oldEditProcC2F, oldEditProcC3F, oldEditProcC1Q, oldEditProcC2Q, oldEditProcC3Q, oldEditProcC1G, oldEditProcC2G, oldEditProcC3G;
static DATAPEQ dante_peq_c1 = { 0 };
static DATAPEQ dante_peq_c2 = { 0 };
static DATAPEQ dante_peq_c3 = { 0 };
static DATAHLPF dante_hlpf = { 0 };
static int radio_select = 0;
static int group_query_count = 0;
static unsigned char group_data[512] = { 0 };
static int group_data_length = 0;
void Change_G_Dante_Edit_Data(HWND hwnd);

LRESULT CALLBACK DanteGEditProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_CHAR:
	{
		// Make sure we only allow specific characters
		if (!((wParam >= '0' && wParam <= '9')
			|| wParam == '.'
			|| wParam == '-'
			|| wParam == VK_RETURN
			|| wParam == VK_DELETE
			|| wParam == VK_BACK))
		{
			return 0;
		}
	}
	break;
	case WM_LBUTTONUP:
		SendMessage(hwnd, EM_SETSEL, 0, -1);
		break;
	case WM_KILLFOCUS:
		Change_G_Dante_Edit_Data(hwnd);
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_RETURN:
		case VK_TAB:
			Change_G_Dante_Edit_Data(hwnd);

			if (wParam == VK_RETURN)
				return 0;
		}
		break;

	}
	if (hwnd == hEditHPFDante)
		return CallWindowProc(oldEditProcHPFDante, hwnd, uMsg, wParam, lParam);
	else if (hwnd == hEditC1F)
		return CallWindowProc(oldEditProcC1F, hwnd, uMsg, wParam, lParam);
	else if (hwnd == hEditC2F)
		return CallWindowProc(oldEditProcC2F, hwnd, uMsg, wParam, lParam);
	else if (hwnd == hEditC3F)
		return CallWindowProc(oldEditProcC3F, hwnd, uMsg, wParam, lParam);
	else if (hwnd == hEditC1Q)
		return CallWindowProc(oldEditProcC1Q, hwnd, uMsg, wParam, lParam);
	else if (hwnd == hEditC2Q)
		return CallWindowProc(oldEditProcC2Q, hwnd, uMsg, wParam, lParam);
	else if (hwnd == hEditC3Q)
		return CallWindowProc(oldEditProcC3Q, hwnd, uMsg, wParam, lParam);
	else if (hwnd == hEditC1G)
		return CallWindowProc(oldEditProcC1G, hwnd, uMsg, wParam, lParam);
	else if (hwnd == hEditC2G)
		return CallWindowProc(oldEditProcC2G, hwnd, uMsg, wParam, lParam);
	else if (hwnd == hEditC3G)
		return CallWindowProc(oldEditProcC3G, hwnd, uMsg, wParam, lParam);
	return 0;
}

void Change_G_Dante_Edit_Data(HWND hwnd)
{
	char number[6];

	if (hwnd == hEditHPFDante) {
		GetDlgItemTextA(hParent, IDC_EDIT_HP_F_DANTE, number, 6);
		int i = atoi(number);
		if (i < HIPASS_FERQUENCY_MIN) {
			i = HIPASS_FERQUENCY_MIN;
		}
		else if (i > HIPASS_FERQUENCY_MAX) {
			i = (int)HIPASS_FERQUENCY_MAX;
		}
		dante_hlpf.OutputPage._.Byte = 1;
		dante_hlpf.Output._.Byte = 2;
		dante_hlpf.Channel.Byte = 1;
		dante_hlpf.Type = FILTER_TYPE_HIPASS;
		dante_hlpf.EnFreqH.Frequency = (unsigned char)(i / 256);
		dante_hlpf.FreqL = (unsigned char)(i % 256);
	}
	else if (hwnd == hEditC1F) {
		GetDlgItemTextA(hParent, IDC_EDIT_C1_F_DANTE, number, 6);
		int i = atoi(number);
		if (i < CEQ_FERQUENCY_MIN) {
			i = CEQ_FERQUENCY_MIN;
			Set_Freq_Text(hParent, IDC_EDIT_C1_F_DANTE, i);
		}
		else if (i > CEQ_FERQUENCY_MAX) {
			i = CEQ_FERQUENCY_MAX;
			Set_Freq_Text(hParent, IDC_EDIT_C1_F_DANTE, i);
		}

		dante_peq_c1.OutputPage._.Byte = 1;
		dante_peq_c1.Output._.Byte = 1;
		dante_peq_c1.Index = 5;
		dante_peq_c1.EnFreqH.Frequency = i / 256;
		dante_peq_c1.FreqL = i % 256;
		dante_peq_c1.Channel.Byte = 1;
	}
	else if (hwnd == hEditC2F) {
		GetDlgItemTextA(hParent, IDC_EDIT_C2_F_DANTE, number, 6);
		int i = atoi(number);
		if (i < CEQ_FERQUENCY_MIN) {
			i = CEQ_FERQUENCY_MIN;
			Set_Freq_Text(hParent, IDC_EDIT_C2_F_DANTE, i);
		}
		else if (i > CEQ_FERQUENCY_MAX) {
			i = CEQ_FERQUENCY_MAX;
			Set_Freq_Text(hParent, IDC_EDIT_C2_F_DANTE, i);
		}

		dante_peq_c2.OutputPage._.Byte = 1;
		dante_peq_c2.Output._.Byte = 1;
		dante_peq_c2.Index = 6;
		dante_peq_c2.EnFreqH.Frequency = i / 256;
		dante_peq_c2.FreqL = i % 256;
		dante_peq_c2.Channel.Byte = 1;
	}
	else if (hwnd == hEditC3F) {
		GetDlgItemTextA(hParent, IDC_EDIT_C3_F_DANTE, number, 6);
		int i = atoi(number);
		if (i < CEQ_FERQUENCY_MIN) {
			i = CEQ_FERQUENCY_MIN;
			Set_Freq_Text(hParent, IDC_EDIT_C3_F_DANTE, i);
		}
		else if (i > CEQ_FERQUENCY_MAX) {
			i = CEQ_FERQUENCY_MAX;
			Set_Freq_Text(hParent, IDC_EDIT_C3_F_DANTE, i);
		}
		dante_peq_c3.OutputPage._.Byte = 1;
		dante_peq_c3.Output._.Byte = 1;
		dante_peq_c3.Index = 7;
		dante_peq_c3.EnFreqH.Frequency = i / 256;
		dante_peq_c3.FreqL = i % 256;
		dante_peq_c3.Channel.Byte = 1;
	}
	else if (hwnd == hEditC1Q) {
		GetDlgItemTextA(hParent, IDC_EDIT_C1_Q_DANTE, number, 6);
		double i = atof(number);
		if (i < 0.01) {
			i = 0.01;
			Set_Q_Text(hParent, IDC_EDIT_C1_Q_DANTE, i);
		}
		else if (i > 40) {
			i = 40;
			Set_Q_Text(hParent, IDC_EDIT_C1_Q_DANTE, i);
		}
		else
			Set_Q_Text(hParent, IDC_EDIT_C1_Q_DANTE, i);

		dante_peq_c1.OutputPage._.Byte = 1;
		dante_peq_c1.Output._.Byte = 1;
		dante_peq_c1.Index = 5;
		dante_peq_c1.Channel.Byte = 1;
		dante_peq_c1.qH = (i * 100) / 256;
		dante_peq_c1.qL = (int)(i * 100) % 256;
	}
	else if (hwnd == hEditC2Q) {
		GetDlgItemTextA(hParent, IDC_EDIT_C2_Q_DANTE, number, 6);
		double i = atof(number);
		if (i < 0.01) {
			i = 0.01;
			Set_Q_Text(hParent, IDC_EDIT_C2_Q_DANTE, i);
		}
		else if (i > 40) {
			i = 40;
			Set_Q_Text(hParent, IDC_EDIT_C2_Q_DANTE, i);
		}
		else
			Set_Q_Text(hParent, IDC_EDIT_C2_Q_DANTE, i);

		dante_peq_c2.OutputPage._.Byte = 1;
		dante_peq_c2.Output._.Byte = 1;
		dante_peq_c2.Index = 6;
		dante_peq_c2.Channel.Byte = 1;
		dante_peq_c2.qH = (i * 100) / 256;
		dante_peq_c2.qL = (int)(i * 100) % 256;
	}
	else if (hwnd == hEditC3Q) {
		GetDlgItemTextA(hParent, IDC_EDIT_C3_Q_DANTE, number, 6);
		double i = atof(number);
		if (i < 0.01) {
			i = 0.01;
			Set_Q_Text(hParent, IDC_EDIT_C3_Q_DANTE, i);
		}
		else if (i > 40) {
			i = 40;
			Set_Q_Text(hParent, IDC_EDIT_C3_Q_DANTE, i);
		}
		else
			Set_Q_Text(hParent, IDC_EDIT_C3_Q_DANTE, i);

		dante_peq_c3.OutputPage._.Byte = 1;
		dante_peq_c3.Output._.Byte = 1;
		dante_peq_c3.Index = 7;
		dante_peq_c3.Channel.Byte = 1;
		dante_peq_c3.qH = (i * 100) / 256;
		dante_peq_c3.qL = (int)(i * 100) % 256;
	}
	else if (hwnd == hEditC1G) {
		GetDlgItemTextA(hParent, IDC_EDIT_C1_G_DANTE, number, 6);
		double i = atof(number);
		if (i < -20) {
			i = -20;
			Set_Gain_Text(hParent, IDC_EDIT_C1_G_DANTE, i);
		}
		else if (i > 20) {
			i = 20;
			Set_Gain_Text(hParent, IDC_EDIT_C1_G_DANTE, i);
		}
		else {
			int j = i * 2;
			i = (double)j / 2;
			Set_Gain_Text(hParent, IDC_EDIT_C1_G_DANTE, i);
		}

		dante_peq_c1.OutputPage._.Byte = 1;
		dante_peq_c1.Output._.Byte = 1;
		dante_peq_c1.Index = 5;
		dante_peq_c1.Channel.Byte = 1;
		dante_peq_c1.Gain = i * 2 + 40;
	}
	else if (hwnd == hEditC2G) {
		GetDlgItemTextA(hParent, IDC_EDIT_C2_G_DANTE, number, 6);
		double i = atof(number);
		if (i < -20) {
			i = -20;
			Set_Gain_Text(hParent, IDC_EDIT_C2_G_DANTE, i);
		}
		else if (i > 20) {
			i = 20;
			Set_Gain_Text(hParent, IDC_EDIT_C2_G_DANTE, i);
		}
		else {
			int j = i * 2;
			i = (double)j / 2;
			Set_Gain_Text(hParent, IDC_EDIT_C2_G_DANTE, i);
		}

		dante_peq_c2.OutputPage._.Byte = 1;
		dante_peq_c2.Output._.Byte = 1;
		dante_peq_c2.Index = 6;
		dante_peq_c2.Channel.Byte = 1;
		dante_peq_c2.Gain = i * 2 + 40;
	}
	else if (hwnd == hEditC3G) {
		GetDlgItemTextA(hParent, IDC_EDIT_C3_G_DANTE, number, 6);
		double i = atof(number);
		if (i < -20) {
			i = -20;
			Set_Gain_Text(hParent, IDC_EDIT_C3_G_DANTE, i);
		}
		else if (i > 20) {
			i = 20;
			Set_Gain_Text(hParent, IDC_EDIT_C3_G_DANTE, i);
		}
		else {
			int j = i * 2;
			i = (double)j / 2;
			Set_Gain_Text(hParent, IDC_EDIT_C3_G_DANTE, i);
		}

		dante_peq_c3.OutputPage._.Byte = 1;
		dante_peq_c3.Output._.Byte = 1;
		dante_peq_c3.Index = 7;
		dante_peq_c3.Channel.Byte = 1;
		dante_peq_c3.Gain = i * 2 + 40;
	}
}

void Init_Group_DanteEQ_UI(HWND hDlg)
{
	SendMessage(GetDlgItem(hDlg, IDC_RADIO1), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_RADIO2), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_RADIO3), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_RADIO4), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);

	if (radio_select == 0) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO1), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C1_F_DANTE), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C1_G_DANTE), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C1_Q_DANTE), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_C1_DANTE), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON1), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C2_F_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C2_G_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C2_Q_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_C2_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C3_F_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C3_G_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C3_Q_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_C3_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_HP_F_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_HP_FILTER_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON4), FALSE);
	}
	else if (radio_select == 1) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO2), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C1_F_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C1_G_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C1_Q_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_C1_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C2_F_DANTE), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C2_G_DANTE), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C2_Q_DANTE), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_C2_DANTE), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON2), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C3_F_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C3_G_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C3_Q_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_C3_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_HP_F_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_HP_FILTER_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON4), FALSE);
	}
	else if (radio_select == 2) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO3), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C1_F_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C1_G_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C1_Q_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_C1_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C2_F_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C2_G_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C2_Q_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_C2_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C3_F_DANTE), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C3_G_DANTE), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C3_Q_DANTE), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_C3_DANTE), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON3), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_HP_F_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_HP_FILTER_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON4), FALSE);
	}
	else if (radio_select == 3) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO4), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C1_F_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C1_G_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C1_Q_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_C1_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C2_F_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C2_G_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C2_Q_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_C2_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C3_F_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C3_G_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_C3_Q_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_C3_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_HP_F_DANTE), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_DANTE), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_HP_FILTER_DANTE), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON4), TRUE);
	}

	if (dante_hlpf.EnFreqH.Enable)
		SendMessage(GetDlgItem(hDlg, IDC_CHECK_HP_FILTER_DANTE), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	else
		SendMessage(GetDlgItem(hDlg, IDC_CHECK_HP_FILTER_DANTE), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);

	if (dante_peq_c1.EnFreqH.Enable)
		SendMessage(GetDlgItem(hDlg, IDC_CHECK_C1_DANTE), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	else
		SendMessage(GetDlgItem(hDlg, IDC_CHECK_C1_DANTE), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);

	if (dante_peq_c2.EnFreqH.Enable)
		SendMessage(GetDlgItem(hDlg, IDC_CHECK_C2_DANTE), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	else
		SendMessage(GetDlgItem(hDlg, IDC_CHECK_C2_DANTE), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);

	if (dante_peq_c3.EnFreqH.Enable)
		SendMessage(GetDlgItem(hDlg, IDC_CHECK_C3_DANTE), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
	else
		SendMessage(GetDlgItem(hDlg, IDC_CHECK_C3_DANTE), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);

	//Filter
	SendMessage(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_DANTE), CB_RESETCONTENT, 0, 0);
	SendMessage(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_DANTE), (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)_T("6 dB/oct"));
	SendMessage(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_DANTE), (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)_T("12 dB/oct"));
	SendMessage(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_DANTE), CB_SETCURSEL, dante_hlpf.Slope, 0);
	Set_Freq_Text(hDlg, IDC_EDIT_HP_F_DANTE, (unsigned int)dante_hlpf.EnFreqH.Frequency + dante_hlpf.FreqL);
	SendMessage(GetDlgItem(hDlg, IDC_CHECK_HP_FILTER_DANTE), BM_SETCHECK, (WPARAM)!dante_hlpf.EnFreqH.Enable, (LPARAM)1);

	//EQ
	SendMessage(GetDlgItem(hDlg, IDC_CHECK_C1_DANTE), BM_SETCHECK, (WPARAM)!dante_peq_c1.EnFreqH.Enable, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_CHECK_C2_DANTE), BM_SETCHECK, (WPARAM)!dante_peq_c2.EnFreqH.Enable, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_CHECK_C3_DANTE), BM_SETCHECK, (WPARAM)!dante_peq_c3.EnFreqH.Enable, (LPARAM)1);
	Set_Freq_Text(hDlg, IDC_EDIT_C1_F_DANTE, (unsigned int)dante_peq_c1.EnFreqH.Frequency * 256 + dante_peq_c1.FreqL);
	Set_Freq_Text(hDlg, IDC_EDIT_C2_F_DANTE, (unsigned int)dante_peq_c2.EnFreqH.Frequency * 256 + dante_peq_c2.FreqL);
	Set_Freq_Text(hDlg, IDC_EDIT_C3_F_DANTE, (unsigned int)dante_peq_c3.EnFreqH.Frequency * 256 + dante_peq_c3.FreqL);
	Set_Gain_Text(hDlg, IDC_EDIT_C1_G_DANTE, ((double)dante_peq_c1.Gain - 40) / 2);
	Set_Gain_Text(hDlg, IDC_EDIT_C2_G_DANTE, ((double)dante_peq_c2.Gain - 40) / 2);
	Set_Gain_Text(hDlg, IDC_EDIT_C3_G_DANTE, ((double)dante_peq_c3.Gain - 40) / 2);
	Set_Q_Text(hDlg, IDC_EDIT_C1_Q_DANTE, ((double)(dante_peq_c1.qH * 256 + dante_peq_c1.qL) / 100));
	Set_Q_Text(hDlg, IDC_EDIT_C2_Q_DANTE, ((double)(dante_peq_c2.qH * 256 + dante_peq_c2.qL) / 100));
	Set_Q_Text(hDlg, IDC_EDIT_C3_Q_DANTE, ((double)(dante_peq_c3.qH * 256 + dante_peq_c3.qL) / 100));
}

INT_PTR CALLBACK GroupDanteEQ(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		radio_select = 0;
		Init_Group_DanteEQ_UI(hDlg);
		hParent = hDlg;


		hEditHPFDante = GetDlgItem(hDlg, IDC_EDIT_HP_F_DANTE);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_HP_F_DANTE), (UINT)EM_SETLIMITTEXT, (WPARAM)5, (LPARAM)0);
		oldEditProcHPFDante = (WNDPROC)SetWindowLongPtr(hEditHPFDante, GWLP_WNDPROC, (LONG_PTR)DanteGEditProc);

		hEditC1F = GetDlgItem(hDlg, IDC_EDIT_C1_F_DANTE);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_C1_F_DANTE), (UINT)EM_SETLIMITTEXT, (WPARAM)5, (LPARAM)0);
		oldEditProcC1F = (WNDPROC)SetWindowLongPtr(hEditC1F, GWLP_WNDPROC, (LONG_PTR)DanteGEditProc);

		hEditC2F = GetDlgItem(hDlg, IDC_EDIT_C2_F_DANTE);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_C2_F_DANTE), (UINT)EM_SETLIMITTEXT, (WPARAM)5, (LPARAM)0);
		oldEditProcC2F = (WNDPROC)SetWindowLongPtr(hEditC2F, GWLP_WNDPROC, (LONG_PTR)DanteGEditProc);

		hEditC3F = GetDlgItem(hDlg, IDC_EDIT_C3_F_DANTE);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_C3_F_DANTE), (UINT)EM_SETLIMITTEXT, (WPARAM)5, (LPARAM)0);
		oldEditProcC3F = (WNDPROC)SetWindowLongPtr(hEditC3F, GWLP_WNDPROC, (LONG_PTR)DanteGEditProc);

		hEditC1Q = GetDlgItem(hDlg, IDC_EDIT_C1_Q_DANTE);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_C1_Q_DANTE), (UINT)EM_SETLIMITTEXT, (WPARAM)5, (LPARAM)0);
		oldEditProcC1Q = (WNDPROC)SetWindowLongPtr(hEditC1Q, GWLP_WNDPROC, (LONG_PTR)DanteGEditProc);

		hEditC2Q = GetDlgItem(hDlg, IDC_EDIT_C2_Q_DANTE);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_C2_Q_DANTE), (UINT)EM_SETLIMITTEXT, (WPARAM)5, (LPARAM)0);
		oldEditProcC2Q = (WNDPROC)SetWindowLongPtr(hEditC2Q, GWLP_WNDPROC, (LONG_PTR)DanteGEditProc);

		hEditC3Q = GetDlgItem(hDlg, IDC_EDIT_C3_Q_DANTE);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_C3_Q_DANTE), (UINT)EM_SETLIMITTEXT, (WPARAM)5, (LPARAM)0);
		oldEditProcC3Q = (WNDPROC)SetWindowLongPtr(hEditC3Q, GWLP_WNDPROC, (LONG_PTR)DanteGEditProc);

		hEditC1G = GetDlgItem(hDlg, IDC_EDIT_C1_G_DANTE);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_C1_G_DANTE), (UINT)EM_SETLIMITTEXT, (WPARAM)4, (LPARAM)0);
		oldEditProcC1G = (WNDPROC)SetWindowLongPtr(hEditC1G, GWLP_WNDPROC, (LONG_PTR)DanteGEditProc);

		hEditC2G = GetDlgItem(hDlg, IDC_EDIT_C2_G_DANTE);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_C2_G_DANTE), (UINT)EM_SETLIMITTEXT, (WPARAM)4, (LPARAM)0);
		oldEditProcC2G = (WNDPROC)SetWindowLongPtr(hEditC2G, GWLP_WNDPROC, (LONG_PTR)DanteGEditProc);

		hEditC3G = GetDlgItem(hDlg, IDC_EDIT_C3_G_DANTE);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_C3_G_DANTE), (UINT)EM_SETLIMITTEXT, (WPARAM)4, (LPARAM)0);
		oldEditProcC3G = (WNDPROC)SetWindowLongPtr(hEditC3G, GWLP_WNDPROC, (LONG_PTR)DanteGEditProc);

		break;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDC_RADIO1:
			radio_select = 0;
			Init_Group_DanteEQ_UI(hDlg);
			break;
		case IDC_RADIO2:
			radio_select = 1;
			Init_Group_DanteEQ_UI(hDlg);
			break;
		case IDC_RADIO3:
			radio_select = 2;
			Init_Group_DanteEQ_UI(hDlg);
			break;
		case IDC_RADIO4:
			radio_select = 3;
			Init_Group_DanteEQ_UI(hDlg);
			break;

		case IDC_CHECK_HP_FILTER_DANTE:
			dante_hlpf.OutputPage._.Byte = 1;
			dante_hlpf.Output._.Byte = 2;
			dante_hlpf.Channel.Byte = 1;
			dante_hlpf.Type = FILTER_TYPE_HIPASS;
			dante_hlpf.EnFreqH.Enable = !(unsigned char)SendMessageW((HWND)lParam, BM_GETCHECK, 0, 0);
			break;
		case IDC_CHECK_C1_DANTE:
			dante_peq_c1.OutputPage._.Byte = 1;
			dante_peq_c1.Output._.Byte = 1;
			dante_peq_c1.Index = 5;
			dante_peq_c1.EnFreqH.Enable = !(unsigned char)SendMessage((HWND)lParam, BM_GETCHECK, 0, 0);
			dante_peq_c1.Channel.Byte = 1;
			break;
		case IDC_CHECK_C2_DANTE:
			dante_peq_c2.OutputPage._.Byte = 1;
			dante_peq_c2.Output._.Byte = 1;
			dante_peq_c2.Index = 6;
			dante_peq_c2.EnFreqH.Enable = !(unsigned char)SendMessage((HWND)lParam, BM_GETCHECK, 0, 0);
			dante_peq_c2.Channel.Byte = 1;
			break;
		case IDC_CHECK_C3_DANTE:
			dante_peq_c3.OutputPage._.Byte = 1;
			dante_peq_c3.Output._.Byte = 1;
			dante_peq_c3.Index = 7;
			dante_peq_c3.EnFreqH.Enable = !(unsigned char)SendMessage((HWND)lParam, BM_GETCHECK, 0, 0);
			dante_peq_c3.Channel.Byte = 1;
			break;
		case IDC_COMBO_HP_SLOPE_DANTE:
			if (HIWORD(wParam) == CBN_SELENDOK) {
				dante_hlpf.OutputPage._.Byte = 1;
				dante_hlpf.Output._.Byte = 2;
				dante_hlpf.Channel.Byte = 1;
				dante_hlpf.Type = FILTER_TYPE_HIPASS;
				dante_hlpf.Slope = (unsigned char)SendMessage((HWND)lParam, (UINT)CB_GETCURSEL, (WPARAM)0, (LPARAM)0);
			}
			break;

		case IDC_BUTTON1: {
			unsigned char data[19] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_PEQ,0,12,dante_peq_c1.OutputPage._.Byte, dante_peq_c1.Output._.Byte, dante_peq_c1.Channel.Byte, dante_peq_c1.Mode, dante_peq_c1.Index, dante_peq_c1.Type, dante_peq_c1.Gain, dante_peq_c1.EnFreqH.Byte, dante_peq_c1.FreqL, dante_peq_c1.qH, dante_peq_c1.qL, 0 };
			strncpy((char*)group_data, (char*)data, 19);
			group_data_length = 19;
			send_group_command(data, 19);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
						  break;
		case IDC_BUTTON2: {
			unsigned char data[19] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_PEQ,0,12,dante_peq_c2.OutputPage._.Byte, dante_peq_c2.Output._.Byte, dante_peq_c2.Channel.Byte, dante_peq_c2.Mode, dante_peq_c2.Index, dante_peq_c2.Type, dante_peq_c2.Gain, dante_peq_c2.EnFreqH.Byte, dante_peq_c2.FreqL, dante_peq_c2.qH, dante_peq_c2.qL, 0 };
			strncpy((char*)group_data, (char*)data, 19);
			group_data_length = 19;
			send_group_command(data, 19);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
						  break;
		case IDC_BUTTON3: {
			unsigned char data[19] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_PEQ,0,12,dante_peq_c3.OutputPage._.Byte, dante_peq_c3.Output._.Byte, dante_peq_c3.Channel.Byte, dante_peq_c3.Mode, dante_peq_c3.Index, dante_peq_c3.Type, dante_peq_c3.Gain, dante_peq_c3.EnFreqH.Byte, dante_peq_c3.FreqL, dante_peq_c3.qH, dante_peq_c3.qL, 0 };
			strncpy((char*)group_data, (char*)data, 19);
			group_data_length = 19;
			send_group_command(data, 19);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
		case IDC_BUTTON4: {
			unsigned char data[15] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_HLPF,0,8,dante_hlpf.OutputPage._.Byte, dante_hlpf.Output._.Byte, dante_hlpf.Channel.Byte, dante_hlpf.Type, dante_hlpf.EnFreqH.Byte, dante_hlpf.FreqL, dante_hlpf.Slope, 0 };
			strncpy((char*)group_data, (char*)data, 15);
			group_data_length = 15;
			send_group_command(data, 15);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
						  break;
		}
		break;
	case WM_TIMER:
		switch (wParam) {
		case TIMER_QUERY_ACK:
		{
			group_query_count++;
			int iAck = 0;
			int iGroupChecked = 0;
			PDEVICESTATE p;
			p = g_dsHead;
			while (p) {
				if (p->check) {
					iGroupChecked++;
					if (p->ack == group_data[4]) {
						iAck++;
					}
					else {
						send_group_command(group_data, group_data_length);
					}
				}
				p = p->next_node;
			}
			char *text = (char*)malloc(10 * sizeof(char));
			sprintf(text, "%d%s%d", iAck, "/", iGroupChecked);

			if (group_query_count == 10) {
				if (iAck == iGroupChecked)
					sprintf(text, "%s", "Finished");
				else
					sprintf(text, "%s", "Timeout");
				KillTimer(hDlg, TIMER_QUERY_ACK);
			}
			SendMessage(GetDlgItem(hDlg, IDC_STATIC_GROUP_CMD_PROGRESS), WM_SETTEXT, (WPARAM)0, (LPARAM)text);
			free(text);
			break;
		}
		default:
			break;
		}
		break;
	case WM_CLOSE:
		DestroyWindow(hdlgGroupDanteEQ);
		hdlgGroupDanteEQ = NULL;
		break;
	case WM_DESTROY:
		hdlgGroupDanteEQ = NULL;
		return (INT_PTR)TRUE;
	}

	DefWindowProc(hDlg, message, wParam, lParam);
}

