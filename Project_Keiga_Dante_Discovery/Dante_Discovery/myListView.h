#pragma once
#include "stdafx.h"
/*
typedef struct _LV_ITEM {
	UINT   mask;        // attributes of this data structure
	int    iItem;       // index of the item to which this structure refers
	int    iSubItem;    // index of the subitem to which this structure refers
	UINT   state;       // Specifies the current state of the item
	UINT   stateMask;   // Specifies the bits of the state member that are valid. 
	LPTSTR  pszText;    // Pointer to a null-terminated string
						// that contains the item text 
	int    cchTextMax;  // Size of the buffer pointed to by the pszText member
	int    iImage;      // index of the list view item's icon 
	LPARAM lParam;      // 32-bit value to associate with item 
} MY_LV_ITEM;

typedef struct _LV_COLUMN {
	UINT mask;       // which members of this structure contain valid information
	int fmt;         // alignment of the column heading and the subitem text 
	int cx;          // Specifies the width, in pixels, of the column.
	LPTSTR pszText;  // Pointer to a null-terminated string
					 // that contains the column heading 
	int cchTextMax;  // Specifies the size, in characters, of the buffer
	int iSubItem;    // index of subitem
} MY_LV_COLUMN;
*/
#define COLUMN_DANTE_DEVICE_NAME		0
#define COLUMN_DANTE_MODEL_NAME		1
#define COLUMN_SPEAKER_NAME	2
#define COLUMN_ZONE	3
#define COLUMN_INSTALLER_FILE_NAME	4
#define COLUMN_POE		5
#define COLUMN_STATE	6
#define COLUMN_IP		7
#define COLUMN_MAC		8
#define COLUMN_FW		9
#define COLUMN_UPDATE	10
#define COLUMN_MAX		11


HWND CreateListView(HINSTANCE hInst, HWND hwndParent, int x, int y, int width, int height);
void AddListViewItem(HWND hlistview, int raw, int colume, char* string, BOOLEAN insert);
LRESULT ProcessCustomDraw(LPARAM lParam, HWND hwnd);
