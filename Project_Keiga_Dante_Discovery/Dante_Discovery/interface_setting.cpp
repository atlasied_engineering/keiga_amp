#include "stdafx.h"
#include "dt_extend_api.h"
#include "..\lib\ipLiveForDante.h"
HWND hdlgInterface;

INT_PTR CALLBACK InterfaceProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam) {

	int idx;
	char str[LEN_MSG];
	static int isAnyInterfac;
	static int ifn1, ifn2;
	static WCHAR name_ifn1[100], name_ifn2[100];
	static PNETINTERFACE p = NULL, nInterface = NULL;
	int index;

	switch (message) {
	case WM_INITDIALOG:
		//get index interface
		kk_dtDBGetConfig(&ifn1, &ifn2);
		//get network interface
		nInterface = db_GetNetworkInterface();

		CheckDlgButton(hDlg, IDC_DT_INTERFACE_RBTN_AUTO, FALSE);
		CheckDlgButton(hDlg, IDC_DT_INTERFACE_RBTN_CUSTOMER, FALSE);
		//any radio
		if (ifn1 == 0) {
			isAnyInterfac = TRUE;
			CheckDlgButton(hDlg, IDC_DT_INTERFACE_RBTN_AUTO, TRUE);
			EnableWindow(GetDlgItem(hDlg, IDC_DT_INTERFACE_COMBO_IF1), FALSE);
			EnableWindow(GetDlgItem(hDlg, IDC_DT_INTERFACE_COMBO_IF2), FALSE);
		}
		else {
			isAnyInterfac = FALSE;
			CheckDlgButton(hDlg, IDC_DT_INTERFACE_RBTN_CUSTOMER, TRUE);
			EnableWindow(GetDlgItem(hDlg, IDC_DT_INTERFACE_COMBO_IF1), TRUE);
			EnableWindow(GetDlgItem(hDlg, IDC_DT_INTERFACE_COMBO_IF2), TRUE);

			wsprintfW(name_ifn1, L"");
			wsprintfW(name_ifn2, L"");

			//show
			p = nInterface;
			while (p) {
				if (p->idxIf == ifn1) {
					SendDlgItemMessageW(hDlg, IDC_DT_INTERFACE_COMBO_IF1, CB_RESETCONTENT, 0, 0);
					SendDlgItemMessageW(hDlg, IDC_DT_INTERFACE_COMBO_IF1, CB_ADDSTRING, 0, (LPARAM)p->name);
					SendDlgItemMessageW(hDlg, IDC_DT_INTERFACE_COMBO_IF1, CB_SETCURSEL, 0, 0);
					wsprintfW(name_ifn1, p->name);
					SendDlgItemMessageA(hDlg, IDC_DT_INTERFACE_STATIC_IP1, WM_SETTEXT, 0, (LPARAM)p->ip);
					SendDlgItemMessageA(hDlg, IDC_DT_INTERFACE_STATIC_MAC1, WM_SETTEXT, 0, (LPARAM)p->mac);

				}
				if (p->idxIf == ifn2) {
					SendDlgItemMessageW(hDlg, IDC_DT_INTERFACE_COMBO_IF2, CB_RESETCONTENT, 0, 0);
					SendDlgItemMessageW(hDlg, IDC_DT_INTERFACE_COMBO_IF2, CB_ADDSTRING, 0, (LPARAM)p->name);
					SendDlgItemMessageW(hDlg, IDC_DT_INTERFACE_COMBO_IF2, CB_SETCURSEL, 0, 0);
					wsprintfW(name_ifn2, p->name);
					SendDlgItemMessageA(hDlg, IDC_DT_INTERFACE_STATIC_IP2, WM_SETTEXT, 0, (LPARAM)p->ip);
					SendDlgItemMessageA(hDlg, IDC_DT_INTERFACE_STATIC_MAC2, WM_SETTEXT, 0, (LPARAM)p->mac);
				}

				p = p->nextNet;
			}


		}

		break;
	case WM_COMMAND: {

		switch (LOWORD(wParam)) {

		case IDC_DT_INTERFACE_RBTN_AUTO: {
			isAnyInterfac = TRUE;
			CheckDlgButton(hDlg, IDC_DT_INTERFACE_RBTN_AUTO, TRUE);
			CheckDlgButton(hDlg, IDC_DT_INTERFACE_RBTN_CUSTOMER, FALSE);
			EnableWindow(GetDlgItem(hDlg, IDC_DT_INTERFACE_COMBO_IF1), FALSE);
			EnableWindow(GetDlgItem(hDlg, IDC_DT_INTERFACE_COMBO_IF2), FALSE);
			break;
		}
		case IDC_DT_INTERFACE_RBTN_CUSTOMER: {
			isAnyInterfac = FALSE;
			CheckDlgButton(hDlg, IDC_DT_INTERFACE_RBTN_AUTO, FALSE);
			CheckDlgButton(hDlg, IDC_DT_INTERFACE_RBTN_CUSTOMER, TRUE);
			EnableWindow(GetDlgItem(hDlg, IDC_DT_INTERFACE_COMBO_IF1), TRUE);
			EnableWindow(GetDlgItem(hDlg, IDC_DT_INTERFACE_COMBO_IF2), TRUE);
			break;
		}
		case IDC_DT_INTERFACE_COMBO_IF1: {
			switch (HIWORD(wParam)) {
			case CBN_DROPDOWN: {
				//show
				SendDlgItemMessageW(hDlg, IDC_DT_INTERFACE_COMBO_IF1, CB_RESETCONTENT, 0, 0);
				p = nInterface;
				while (p) {
					SendDlgItemMessageW(hDlg, IDC_DT_INTERFACE_COMBO_IF1, CB_ADDSTRING, 0, (LPARAM)p->name);
					p = p->nextNet;
				}
				index = SendDlgItemMessageW(hDlg, IDC_DT_INTERFACE_COMBO_IF1, CB_FINDSTRINGEXACT, 0, (LPARAM)name_ifn1);
				SendDlgItemMessageW(hDlg, IDC_DT_INTERFACE_COMBO_IF1, CB_SETCURSEL, index, 0);
				break;
			}
			case CBN_SELENDOK: {
				//Get name
				index = SendDlgItemMessageW(hDlg, IDC_DT_INTERFACE_COMBO_IF1, CB_GETCURSEL, 0, 0);
				SendDlgItemMessageW(hDlg, IDC_DT_INTERFACE_COMBO_IF1, CB_GETLBTEXT, index, (LPARAM)name_ifn1);

				p = nInterface;
				while (p) {
					if (wcscmp(p->name, name_ifn1) == 0) {
						ifn1 = p->idxIf;
						SendDlgItemMessageA(hDlg, IDC_DT_INTERFACE_STATIC_IP1, WM_SETTEXT, 0, (LPARAM)p->ip);
						SendDlgItemMessageA(hDlg, IDC_DT_INTERFACE_STATIC_MAC1, WM_SETTEXT, 0, (LPARAM)p->mac);
					}
					p = p->nextNet;
				}
				break;
			}

			}

			break;
		}
		case IDC_DT_INTERFACE_COMBO_IF2: {
			switch (HIWORD(wParam)) {
			case CBN_DROPDOWN: {
				//show
				SendDlgItemMessageW(hDlg, IDC_DT_INTERFACE_COMBO_IF2, CB_RESETCONTENT, 0, 0);
				p = nInterface;
				while (p) {
					SendDlgItemMessageW(hDlg, IDC_DT_INTERFACE_COMBO_IF2, CB_ADDSTRING, 0, (LPARAM)p->name);
					p = p->nextNet;
				}
				index = SendDlgItemMessageW(hDlg, IDC_DT_INTERFACE_COMBO_IF2, CB_FINDSTRINGEXACT, 0, (LPARAM)name_ifn2);
				SendDlgItemMessageW(hDlg, IDC_DT_INTERFACE_COMBO_IF2, CB_SETCURSEL, index, 0);
				break;
			}
			case CBN_SELENDOK: {
				//Get name
				index = SendDlgItemMessageW(hDlg, IDC_DT_INTERFACE_COMBO_IF2, CB_GETCURSEL, 0, 0);
				SendDlgItemMessageW(hDlg, IDC_DT_INTERFACE_COMBO_IF2, CB_GETLBTEXT, index, (LPARAM)name_ifn2);

				p = nInterface;
				while (p) {
					if (wcscmp(p->name, name_ifn2) == 0) {
						ifn2 = p->idxIf;
						SendDlgItemMessageA(hDlg, IDC_DT_INTERFACE_STATIC_IP2, WM_SETTEXT, 0, (LPARAM)p->ip);
						SendDlgItemMessageA(hDlg, IDC_DT_INTERFACE_STATIC_MAC2, WM_SETTEXT, 0, (LPARAM)p->mac);
					}
					p = p->nextNet;
				}

				break;
			}

			}

			break;
		}

		case IDC_DT_INTERFACE_BTN_CONFIRM: {
			//any radio: checked
			if (isAnyInterfac == TRUE) {
				dt_SetDBConfig(0, 0);
			}
			else {	//customer radio: checked
				dt_SetDBConfig(ifn1, ifn2);
			}
			DestroyWindow(hdlgInterface);
			break;
		}
		case IDC_DT_INTERFACE_BTN_EXIT: {
			DestroyWindow(hdlgInterface);
			hdlgInterface = NULL;
			break;
		}
		}
		break;
	}
	case WM_CLOSE:
		DestroyWindow(hdlgInterface);
		hdlgInterface = NULL;
		break;
	case WM_DESTROY: {
		//free
		if (nInterface) {
			p = nInterface->nextNet;
			while (p) {
				p = nInterface->nextNet;
				free(nInterface);
				nInterface = p;
			}
			nInterface = NULL;
		}
		hdlgInterface = NULL;
		kk_dtStop();
		kk_dtStart();

		return (INT_PTR)TRUE;
	}
	}
	DefWindowProc(hDlg, message, wParam, lParam);
}