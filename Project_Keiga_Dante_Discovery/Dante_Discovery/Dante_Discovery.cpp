
#include "stdafx.h"
#include "Dante_Discovery.h"
#include "interface_setting.h"
#include "dante_conmon.h"
#include "ACC_Command.h"
#include "myListView.h"
#include "device_state.h"
#include "utils.h"
#include "update.h"
#include "dt_extend_api.h"
#include "group_control.h"
#include "..\lib\ipLiveForDante.h"
#include "device_control.h"

#define MODE_LOAD	0
#define MODE_SAVE	1

HINSTANCE hInst;
HWND hMainWnd;
char szTitle[MAX_LOADSTRING];
char szWindowClass[MAX_LOADSTRING];
HWND hListView = NULL;
int index_of_double_click;
char g_currentPath[MAX_FILE_PATH] = { 0 };
char g_iniPath[MAX_FILE_PATH] = { 0 };
boolean g_tx_done = TRUE;
static HWND hBtnSearch = NULL;
static HWND hBtnClear = NULL;
static HWND hEditfilter = NULL;
static WNDPROC oldEditFilterProc;
static PDEVICESTATE pds_right_click;
static boolean bReflashlistview;
static boolean bFiltering = FALSE;
static RECT rectFilterIcon = { 490,10,510,30 };

wchar_t* open_csv_file(int mode);
LRESULT CALLBACK DeviceFilterProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

ATOM                MyRegisterClass(HINSTANCE hInstance);
BOOL                InitInstance(HINSTANCE, int);
LRESULT CALLBACK    WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK    About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY wWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ wchar_t*    lpCmdLine,
                     _In_ int       nCmdShow)
{
    UNREFERENCED_PARAMETER(hPrevInstance);
    UNREFERENCED_PARAMETER(lpCmdLine);


	CreateEventA(NULL, FALSE, FALSE, "{797ca43a-5d1e-4d46-a318-83d5774b2448}");
	if (GetLastError() == ERROR_ALREADY_EXISTS) {
		return FALSE;
	}


    LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
    LoadString(hInstance, IDC_DANTE_DISCOVERY, szWindowClass, MAX_LOADSTRING);
    MyRegisterClass(hInstance);
	_getcwd(g_currentPath, MAX_FILE_PATH);
	sprintf(g_iniPath, "%s%s", g_currentPath, "\\Devices_Info.ini");


    if (!InitInstance (hInstance, nCmdShow))
    {
        return FALSE;
    }

    HACCEL hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_DANTE_DISCOVERY));

    MSG msg;


    while (GetMessage(&msg, nullptr, 0, 0))
    {
        if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }
    }

    return (int) msg.wParam;
}



ATOM MyRegisterClass(HINSTANCE hInstance)
{
    WNDCLASSEX wcex;

    wcex.cbSize = sizeof(WNDCLASSEX);

    wcex.style          = CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc    = WndProc;
    wcex.cbClsExtra     = 0;
    wcex.cbWndExtra     = 0;
    wcex.hInstance      = hInstance;
    wcex.hIcon          = LoadIcon(hInstance, MAKEINTRESOURCE(IDI_ICON2));
    wcex.hCursor        = LoadCursor(nullptr, IDC_ARROW);
    wcex.hbrBackground  = (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName   = MAKEINTRESOURCE(IDC_DANTE_DISCOVERY);
    wcex.lpszClassName  = szWindowClass;
    wcex.hIconSm        = LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_ICON1));

    return RegisterClassEx(&wcex);
}

BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   hInst = hInstance;

   hMainWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, MAIN_WIDTH, MAIN_HEIGHT, nullptr, nullptr, hInstance, nullptr);

   if (!hMainWnd)
   {
      return FALSE;
   }
   ShowWindow(hMainWnd, nCmdShow);
   UpdateWindow(hMainWnd);

   return TRUE;
}


DWORD WINAPI Thread_Ping_Device(LPVOID lpParam) {
	PDEVICESTATE p;
	char mac[30] = { 0 };

	ds_GetFirstDevice(mac);
	while (strlen(mac)) {
		p = ds_FindNode(mac);
		if (p) {
			if (p->state != ild_is_live_mac(mac)) {
				g_listchanged = TRUE;
				p->state = ild_is_live_mac(mac);
			}
		}
		ds_GetNextDevice(mac);
	}
	return 1;
}

void ReflashListView(HWND hlistview)
{
	LVITEM LvItem;
	memset(&LvItem, 0, sizeof(LvItem)); // Zero struct's Members

	char mac[18] = { 0 };
	char mac2[18] = { 0 };
	boolean found = FALSE;
	int index = 0;

	bReflashlistview = TRUE;
	ds_GetFirstDevice(mac);
	while (strlen(mac)) {
		index = 0;
		found = FALSE;
		ListView_GetItemText(hlistview, index, COLUMN_MAC, mac2, sizeof(mac2));
		while (strlen(mac2)) {
			if (strcmp(mac, mac2) == 0) {
				found = TRUE;
				PDEVICESTATE p;
				p = ds_FindNode(mac);
				if (p && p->show) {
					ListView_SetItemText(hlistview, index, COLUMN_DANTE_DEVICE_NAME, p->Name);
					ListView_SetItemText(hlistview, index, COLUMN_DANTE_MODEL_NAME, p->Model);
					ListView_SetItemText(hlistview, index, COLUMN_ZONE, p->Zone);
					ListView_SetItemText(hlistview, index, COLUMN_INSTALLER_FILE_NAME, p->InstallerFileName);
					ListView_SetItemText(hlistview, index, COLUMN_POE, p->Poe);
					if (p->findme) {
						ListView_SetItemText(hlistview, index, COLUMN_STATE, "Find Me");
					}else {
						if (p->state) {
							ListView_SetItemText(hlistview, index, COLUMN_STATE, "Online");
						}
						else {
							ListView_SetItemText(hlistview, index, COLUMN_STATE, "Offline");
						}
					}
					ListView_SetItemText(hlistview, index, COLUMN_IP, p->Ip);
					ListView_SetItemText(hlistview, index, COLUMN_MAC, p->Mac);
					ListView_SetItemText(hlistview, index, COLUMN_FW, p->Fw);
					//if(p->pUpdate == NULL)
					//	ListView_SetItemText(hlistview, index, COLUMN_UPDATE, "");
					ListView_SetCheckState(hlistview, index, p->check);

				}
				break;
			}
			memset(mac2, 0, sizeof(mac2));
			index++;
			ListView_GetItemText(hlistview, index, COLUMN_MAC, mac2, sizeof(mac2));
		}
		if (!found) {
			PDEVICESTATE p;
			p = ds_FindNode(mac);
			if (p && p->show) {
				LvItem.iItem = index;
				LvItem.lParam = index;

				for (int j = 0; j < COLUMN_MAX; j++) {
					LvItem.iSubItem = j;       // Put in first coluom
					switch (LvItem.iSubItem) {
					case COLUMN_DANTE_DEVICE_NAME:
						LvItem.mask = LVIF_TEXT | LVIF_PARAM;
						LvItem.pszText = p->Name;
						LvItem.cchTextMax = sizeof(LvItem.pszText);
						ListView_InsertItem(hlistview, &LvItem);
						break;
					case COLUMN_DANTE_MODEL_NAME:
						LvItem.mask = LVIF_TEXT;
						LvItem.pszText = p->Model;
						LvItem.cchTextMax = sizeof(LvItem.pszText);
						ListView_SetItem(hlistview, &LvItem);
						break;
					case COLUMN_SPEAKER_NAME:
						LvItem.mask = LVIF_TEXT;
						LvItem.pszText = p->Speaker;
						LvItem.cchTextMax = sizeof(LvItem.pszText);
						ListView_SetItem(hlistview, &LvItem);
						break;
					case COLUMN_ZONE:
						LvItem.mask = LVIF_TEXT;
						LvItem.pszText = p->Zone;
						LvItem.cchTextMax = sizeof(LvItem.pszText);
						ListView_SetItem(hlistview, &LvItem);
						break;
					case COLUMN_INSTALLER_FILE_NAME:
						LvItem.mask = LVIF_TEXT;
						LvItem.pszText = p->InstallerFileName;
						LvItem.cchTextMax = sizeof(LvItem.pszText);
						ListView_SetItem(hlistview, &LvItem);
						break;
					case COLUMN_POE:
						LvItem.mask = LVIF_TEXT;
						LvItem.pszText = p->Poe;
						LvItem.cchTextMax = sizeof(LvItem.pszText);
						ListView_SetItem(hlistview, &LvItem);
						break;
					case COLUMN_STATE:
						LvItem.mask = LVIF_TEXT;
						if (p->findme) {
							LvItem.pszText = "Find Me";
						}else{
							if (p->state)
								LvItem.pszText = "Online";
							else
								LvItem.pszText = "Offline";
						}
						LvItem.cchTextMax = sizeof(LvItem.pszText);
						ListView_SetItem(hlistview, &LvItem);
						break;
					case COLUMN_IP:
						LvItem.mask = LVIF_TEXT;
						LvItem.pszText = p->Ip;
						LvItem.cchTextMax = sizeof(LvItem.pszText);
						ListView_SetItem(hlistview, &LvItem);
						break;
					case COLUMN_MAC:
						LvItem.mask = LVIF_TEXT;
						LvItem.pszText = p->Mac;
						LvItem.cchTextMax = sizeof(LvItem.pszText);
						ListView_SetItem(hlistview, &LvItem);
						break;
					case COLUMN_FW:
						LvItem.mask = LVIF_TEXT;
						LvItem.pszText = p->Fw;
						LvItem.cchTextMax = sizeof(LvItem.pszText);
						ListView_SetItem(hlistview, &LvItem);
						break;
					case COLUMN_UPDATE:
						LvItem.mask = LVIF_TEXT;
						if (p->Update_State != NULL)
							LvItem.pszText = p->Update_State;
//						else
//							LvItem.pszText = "";
						LvItem.cchTextMax = sizeof(LvItem.pszText);
						ListView_SetItem(hlistview, &LvItem);
						break;
					}
				}
				ListView_SetCheckState(hlistview, LvItem.iItem, p->check);
			}
		}
		ds_GetNextDevice(mac);
	}
	bReflashlistview = FALSE;
}

void SortList(HWND hlistview)
{
	SendMessage(hlistview, LVM_DELETEALLITEMS, 0, 0);

	LVITEM LvItem;
	memset(&LvItem, 0, sizeof(LvItem));

	char mac[18] = { 0 };
	LvItem.iItem = 0;
	LvItem.lParam = 0;

	bReflashlistview = TRUE;
	ds_GetFirstDevice(mac);
	while (strlen(mac)) {
		PDEVICESTATE p;
		p = ds_FindNode(mac);
		if (p) {
			if(!p->show){
				ds_GetNextDevice(mac);
				continue;
			}
			for (int j = 0; j < COLUMN_MAX; j++) {
				LvItem.iSubItem = j;       // Put in first coluom
				switch (LvItem.iSubItem) {
				case COLUMN_DANTE_DEVICE_NAME:
					LvItem.mask = LVIF_TEXT | LVIF_PARAM;
					LvItem.pszText = p->Name;
					LvItem.cchTextMax = sizeof(LvItem.pszText);
					ListView_InsertItem(hlistview, &LvItem);
					break;
				case COLUMN_DANTE_MODEL_NAME:
					LvItem.mask = LVIF_TEXT;
					LvItem.pszText = p->Model;
					LvItem.cchTextMax = sizeof(LvItem.pszText);
					ListView_SetItem(hlistview, &LvItem);
					break;
				case COLUMN_SPEAKER_NAME:
					LvItem.mask = LVIF_TEXT;
					LvItem.pszText = p->Speaker;
					LvItem.cchTextMax = sizeof(LvItem.pszText);
					ListView_SetItem(hlistview, &LvItem);
					break;
				case COLUMN_ZONE:
					LvItem.mask = LVIF_TEXT;
					LvItem.pszText = p->Zone;
					LvItem.cchTextMax = sizeof(LvItem.pszText);
					ListView_SetItem(hlistview, &LvItem);
					break;
				case COLUMN_INSTALLER_FILE_NAME:
					LvItem.mask = LVIF_TEXT;
					LvItem.pszText = p->InstallerFileName;
					LvItem.cchTextMax = sizeof(LvItem.pszText);
					ListView_SetItem(hlistview, &LvItem);
					break;
				case COLUMN_POE:
					LvItem.mask = LVIF_TEXT;
					LvItem.pszText = p->Poe;
					LvItem.cchTextMax = sizeof(LvItem.pszText);
					ListView_SetItem(hlistview, &LvItem);
					break;
				case COLUMN_STATE:
					LvItem.mask = LVIF_TEXT;
					if (p->findme) {
						LvItem.pszText = "Find Me";
					}
					else {
						if (p->state)
							LvItem.pszText = "Online";
						else
							LvItem.pszText = "Offline";
					}
					LvItem.cchTextMax = sizeof(LvItem.pszText);
					ListView_SetItem(hlistview, &LvItem);
					break;
				case COLUMN_IP:
					LvItem.mask = LVIF_TEXT;
					LvItem.pszText = p->Ip;
					LvItem.cchTextMax = sizeof(LvItem.pszText);
					ListView_SetItem(hlistview, &LvItem);
					break;
				case COLUMN_MAC:
					LvItem.mask = LVIF_TEXT;
					LvItem.pszText = p->Mac;
					LvItem.cchTextMax = sizeof(LvItem.pszText);
					ListView_SetItem(hlistview, &LvItem);
					break;
				case COLUMN_FW:
					LvItem.mask = LVIF_TEXT;
					LvItem.pszText = p->Fw;
					LvItem.cchTextMax = sizeof(LvItem.pszText);
					ListView_SetItem(hlistview, &LvItem);
					break;
				case COLUMN_UPDATE:
					LvItem.mask = LVIF_TEXT;
					if (p->Update_State != NULL)
						LvItem.pszText = p->Update_State;
//					else
//						LvItem.pszText = "";
					LvItem.cchTextMax = sizeof(LvItem.pszText);
					ListView_SetItem(hlistview, &LvItem);
					break;
				}
			}
			ListView_SetCheckState(hlistview, LvItem.iItem, p->check);
		}
		LvItem.iItem++;
		LvItem.lParam++;
		ds_GetNextDevice(mac);
	}
	bReflashlistview = FALSE;
}

void OnColumnClick(LPNMLISTVIEW  pLVInfo)
{
	PDEVICESTATE p,p1,p2,p3,p4;
	static int nSortColumn = 0;
	static BOOL bSortAscending = TRUE;
	LPARAM lParamSort;
	int result;
	// get new sort parameters
	if (pLVInfo->iSubItem == nSortColumn)
		bSortAscending = !bSortAscending;
	else
	{
		nSortColumn = pLVInfo->iSubItem;
		bSortAscending = TRUE;
	}

	int size = ds_GetCount();
	if (size < 2)
		return;

	int i, j;
	for (i = 0; i < size; i++) {
		p = g_dsHead;
		for (j = size - 1; j > i; j--) {
			switch (pLVInfo->iSubItem) {
			case COLUMN_DANTE_DEVICE_NAME:
				if (bSortAscending)
					result = lstrcmp(p->Name, p->next_node->Name);
				else
					result = -lstrcmp(p->Name, p->next_node->Name);

				break;
			case COLUMN_DANTE_MODEL_NAME:
				if (bSortAscending)
					result = lstrcmp(p->Model, p->next_node->Model);
				else
					result = -lstrcmp(p->Model, p->next_node->Model);
				break;
			case COLUMN_ZONE:
				if (bSortAscending)
					result = lstrcmp(p->Zone, p->next_node->Zone);
				else
					result = -lstrcmp(p->Zone, p->next_node->Zone);
				break;
			case COLUMN_SPEAKER_NAME:
				if (bSortAscending)
					result = lstrcmp(p->Speaker, p->next_node->Speaker);
				else
					result = -lstrcmp(p->Speaker, p->next_node->Speaker);
				break;
			case COLUMN_INSTALLER_FILE_NAME:
				if (bSortAscending)
					result = p->InstallerFileName > p->next_node->InstallerFileName;
				else
					result = p->InstallerFileName < p->next_node->InstallerFileName;
				break;
			case COLUMN_POE:
				if (bSortAscending)
					result = p->Poe > p->next_node->Poe;
				else
					result = p->Poe < p->next_node->Poe;
				break;
			case COLUMN_STATE:
				if (bSortAscending)
					result = p->state > p->next_node->state;
				else
					result = p->state < p->next_node->state;
				break;
			case COLUMN_IP:
				if (bSortAscending)
					result = lstrcmp(p->Ip, p->next_node->Ip);
				else
					result = -lstrcmp(p->Ip, p->next_node->Ip);
				break;
			case COLUMN_MAC:
				if (bSortAscending)
					result = lstrcmp(p->Mac, p->next_node->Mac);
				else
					result = -lstrcmp(p->Mac, p->next_node->Mac);
				break;
			case COLUMN_FW:
				if (bSortAscending)
					result = lstrcmp(p->Fw, p->next_node->Fw);
				else
					result = -lstrcmp(p->Fw, p->next_node->Fw);
				break;
			}
			if (result > 0) {
				if (p == g_dsHead)
					g_dsHead = p->next_node;
				p1 = p->prev_node;
				p2 = p;
				p3 = p->next_node;
				p4 = p->next_node->next_node;

				if(p1)
					p1->next_node = p3;
				p3->prev_node = p1;
				p3->next_node = p2;
				p2->prev_node = p3;
				p2->next_node = p4;
				if (p4)
					p4->prev_node = p2;
			}else
				p = p->next_node;
		}
	}
	SortList(hListView);
}

void CheckAllItems(BOOL fChecked) {
	for (int nItem = 0; nItem < ListView_GetItemCount(hListView); nItem++) {
		ListView_SetCheckState(hListView, nItem, fChecked);
	}
}

void SetHeaderCheckbox(void) {
	BOOL fChecked = TRUE;
	for (int nItem = 0; nItem < ListView_GetItemCount(hListView); nItem++) {
		if (!ListView_GetCheckState(hListView, nItem)) {
			fChecked = FALSE;
			break;
		}
	}

	HWND header = ListView_GetHeader(hListView);
	HDITEM hdi = { 0 };
	hdi.mask = HDI_FORMAT;
	Header_GetItem(header, 0, &hdi);
	if (fChecked) {
		hdi.fmt |= HDF_CHECKED;
	}
	else {
		hdi.fmt &= ~HDF_CHECKED;
	}
	Header_SetItem(header, 0, &hdi);
}

void CB_SetListViewItem(int index, char* pszText)
{
	ListView_SetItemText(hListView, index, COLUMN_UPDATE, pszText);
}

HWND hwEdit = NULL;
//LVCOLUMN  LvCol;
int iItem, iSubItem;
WNDPROC EOldProc;
LRESULT CALLBACK EditProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_KILLFOCUS:
	{
		LV_DISPINFO lvDispinfo;
		ZeroMemory(&lvDispinfo, sizeof(LV_DISPINFO));
		lvDispinfo.hdr.hwndFrom = hwnd;
		lvDispinfo.hdr.idFrom = GetDlgCtrlID(hwnd);
		lvDispinfo.hdr.code = LVN_ENDLABELEDIT;
		lvDispinfo.item.mask = LVIF_TEXT;
		lvDispinfo.item.iItem = iItem;
		lvDispinfo.item.iSubItem = iSubItem;
		lvDispinfo.item.pszText = NULL;
		char szEditText[255];
		GetWindowText(hwnd, szEditText, 255);
		lvDispinfo.item.pszText = szEditText;
		lvDispinfo.item.cchTextMax = lstrlen(szEditText);            
		ListView_SetItemText(hListView, iItem, iSubItem, lvDispinfo.item.pszText);
		//SendMessage(hMainWnd, WM_NOTIFY, (WPARAM)IDC_LIST, (LPARAM)&lvDispinfo); //the LV ID and the LVs Parent window's HWND            
		DestroyWindow(hwnd);            return 1;        
	}
	}

	return CallWindowProc(EOldProc, hwnd, message, wParam, lParam);
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch (message)
	{
	case WM_CREATE: {
		Update_Device_State();
		
		ild_init();
		ild_start();
		ild_set_live_time(10); //10 seconds

		CreateWindow(WC_STATIC, "Filter :  ", WS_CHILD | WS_VISIBLE | ES_RIGHT, 10, 10, 60, 22, hWnd, NULL, hInst, NULL);
		CreateWindow(WC_COMBOBOX, "", CBS_DROPDOWNLIST | WS_CHILD | WS_VISIBLE, 70, 10, 170, 20, hWnd, (HMENU)IDC_COMBO_FILTER, hInst, NULL);
		SendMessage(GetDlgItem(hWnd, IDC_COMBO_FILTER), CB_ADDSTRING, 0, (LPARAM)"Dante Device Name");
		SendMessage(GetDlgItem(hWnd, IDC_COMBO_FILTER), CB_ADDSTRING, 0, (LPARAM)"Model");
		SendMessage(GetDlgItem(hWnd, IDC_COMBO_FILTER), CB_ADDSTRING, 0, (LPARAM)"Zone");
		SendMessage(GetDlgItem(hWnd, IDC_COMBO_FILTER), CB_ADDSTRING, 0, (LPARAM)"Installer File Name");
		SendMessage(GetDlgItem(hWnd, IDC_COMBO_FILTER), CB_ADDSTRING, 0, (LPARAM)"MAC Address");
		SendMessage(GetDlgItem(hWnd, IDC_COMBO_FILTER), CB_ADDSTRING, 0, (LPARAM)"IP Address");
		//SendMessage(GetDlgItem(hWnd, IDC_COMBO_FILTER), CB_ADDSTRING, 0, (LPARAM)_T("IP"));
		SendMessage(GetDlgItem(hWnd, IDC_COMBO_FILTER), CB_SETCURSEL, 0, 0);
		
		hEditfilter = CreateWindow(WC_EDIT, NULL, WS_CHILD | WS_VISIBLE | WS_BORDER | ES_LEFT, 250, 10, 150, 22, hWnd, NULL, hInst, NULL);
		oldEditFilterProc =(WNDPROC)SetWindowLongPtr(hEditfilter, GWLP_WNDPROC, (LPARAM)DeviceFilterProc);
		hBtnSearch = CreateWindow(WC_BUTTON, "Search", WS_CHILD | WS_VISIBLE, 420, 10, 60, 22, hWnd, (HMENU)IDC_BUTTON_SEARCH, hInst, NULL );
		hListView = CreateListView(hInst, hWnd, 10, 40, LISTVIEW_WIDTH, LISTVIEW_HEIGHT);
		hBtnClear = CreateWindow(WC_BUTTON, "Clear Search List", WS_CHILD | WS_VISIBLE, 520, 10, 180, 22, hWnd, (HMENU)IDC_BUTTON_CLEAR_OFFLINE, hInst, NULL);
		
		ShowWindow(hListView, TRUE);

		if (hListView != NULL) {
			HFONT hFont = CreateFont((int)16, 0, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, GB2312_CHARSET,
				OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, "");
			SendMessage(hListView, WM_SETFONT, (WPARAM)hFont, TRUE);

			LVCOLUMN LvCol;
			// Here we put the info on the Coulom headers
			// this is not data, only name of each header we like
			memset(&LvCol, 0, sizeof(LvCol));                  // Zero Members
			LvCol.mask = LVCF_TEXT | LVCF_WIDTH | LVCF_SUBITEM;    // Type of mask
			SendMessage(hListView, LVM_SETEXTENDEDLISTVIEWSTYLE,
				0, LVS_EX_FULLROWSELECT | LVS_EX_CHECKBOXES);

			LvCol.pszText = "Dante Device Name";                          
			LvCol.cx = 150;                                  
			ListView_InsertColumn(hListView, COLUMN_DANTE_DEVICE_NAME, &LvCol);

			LvCol.pszText = "Model";
			LvCol.cx = 150;
			ListView_InsertColumn(hListView, COLUMN_DANTE_MODEL_NAME, &LvCol);

			LvCol.pszText = "Installer Speaker Name";
			LvCol.cx = 200;
			ListView_InsertColumn(hListView, COLUMN_SPEAKER_NAME, &LvCol);

			LvCol.pszText = "Zone";                            
			LvCol.cx = 150;
			ListView_InsertColumn(hListView, COLUMN_ZONE, &LvCol);

			LvCol.pszText = "Installer File Name";
			LvCol.cx = 200;
			ListView_InsertColumn(hListView, COLUMN_INSTALLER_FILE_NAME, &LvCol);

			LvCol.pszText = "PoE Mode";
			LvCol.cx = 80;
			ListView_InsertColumn(hListView, COLUMN_POE, &LvCol);

			LvCol.pszText = "Status";                            
			LvCol.cx = 80;
			ListView_InsertColumn(hListView, COLUMN_STATE, &LvCol);

			LvCol.pszText = "IP Address";                            
			LvCol.cx = 140;
			ListView_InsertColumn(hListView, COLUMN_IP, &LvCol);

			LvCol.pszText = "MAC Address";                            
			LvCol.cx = 160;
			ListView_InsertColumn(hListView, COLUMN_MAC, &LvCol);

			LvCol.pszText = "FW Version";                            
			LvCol.cx = 120;
			ListView_InsertColumn(hListView, COLUMN_FW, &LvCol);

			LvCol.pszText = "Status Message";
			LvCol.cx = 260;
			ListView_InsertColumn(hListView, COLUMN_UPDATE, &LvCol);

			ReflashListView(hListView);

			kk_apSetReceiveMsgCB(dt_ReceiveCB);
			kk_dtInit();
			kk_dtStop();
			kk_dtStart();
			KillTimer(hWnd, TIMER_SCAN);
			SetTimer(hWnd, TIMER_SCAN, TIMER_SCAN, NULL);
			}
		}
		break;
    case WM_COMMAND:
		{
			int wmId = LOWORD(wParam);
			switch (wmId)
			{
			case ID_FINDME_STARTFINDME: {
				PDANTENODE pnode;
				pnode = dt_FindNode(pds_right_click->Name);
				if (!pnode)
					break;

				unsigned char cmd[13] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,ACC_AMP_ANY,ACC_CMD_CHKSS,0x00,0x06,0xF0,0x00,0x01,0x00,0x00,0x00 };
				if (strcmp(pnode->devInfo.modelname, AL003_MODEL_NAME) == 0)
					cmd[3] = ACC_AMP_AL003;
				else if (strcmp(pnode->devInfo.modelname, ED004_MODEL_NAME) == 0)
					cmd[3] = ACC_AMP_ED004;
				else if (strcmp(pnode->devInfo.modelname, AL003M_MODEL_NAME) == 0)
					cmd[3] = ACC_AMP_AL003M;
				else if (strcmp(pnode->devInfo.modelname, DA_APX40N_MODEL_NAME) == 0)
					cmd[3] = ACC_AMP_AL003M;
				for (int i = 0; i < 12; i++)
					cmd[12] += cmd[i];
				dt_Write_data(pnode->devInfo.mf, pds_right_click->Name, cmd, 13);
				
				pds_right_click->findme = TRUE;
				ListView_SetItemText(hListView, ds_GetIndexByMac(pds_right_click->Mac), COLUMN_STATE, "Find Me");
			}
				break;
			case ID_FINDME_STOPFINDME: {
				
				PDANTENODE pnode;
				pnode = dt_FindNode(pds_right_click->Name);
				if (!pnode)
					break;
				unsigned char cmd[13] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,ACC_AMP_ANY,ACC_CMD_CHKSS,0x00,0x06,0xF0,0x00,0x00,0x00,0x00,0x00 };
				if (strcmp(pnode->devInfo.modelname, AL003_MODEL_NAME) == 0)
					cmd[3] = ACC_AMP_AL003;
				else if (strcmp(pnode->devInfo.modelname, ED004_MODEL_NAME) == 0)
					cmd[3] = ACC_AMP_ED004;
				else if (strcmp(pnode->devInfo.modelname, AL003M_MODEL_NAME) == 0)
					cmd[3] = ACC_AMP_AL003M;
				else if (strcmp(pnode->devInfo.modelname, DA_APX40N_MODEL_NAME) == 0)
					cmd[3] = ACC_AMP_AL003M;

				for (int i = 0; i < 12; i++)
					cmd[12] += cmd[i];

				dt_Write_data(pnode->devInfo.mf, pds_right_click->Name, cmd, 13);
				
				pds_right_click->findme = FALSE;
				if (pds_right_click->state) {
					ListView_SetItemText(hListView, ds_GetIndexByMac(pds_right_click->Mac), COLUMN_STATE, "Online");
				}
				else {
					ListView_SetItemText(hListView, ds_GetIndexByMac(pds_right_click->Mac), COLUMN_STATE, "Offline");
				}
			}
				break;
			case ID_UPDATE_STOPUPDATE: {
				PDEVICESTATE p = pds_right_click;
				if (p == NULL)
					break;
				pds_right_click->pUpdate->update = FALSE;
				sprintf(pds_right_click->Update_State, "%s", "FW Update - Stoped");
				CB_SetListViewItem(ds_GetIndexByMac(p->Mac), pds_right_click->Update_State);
			}
				break;
			case IDM_ABOUT:
				DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
				break;
			case IDM_EXIT:
				DestroyWindow(hWnd);
				break;
			case IDM_INTERFACE:
				if (hdlgInterface) {
					ShowWindow(hdlgInterface, SW_SHOW);
				}
				else {
					hdlgInterface = CreateDialog(hInst, MAKEINTRESOURCE(IDD_DT_INTERFACE), hWnd, InterfaceProc);
					ShowWindow(hdlgInterface, SW_SHOW);
				}
				break;
			case IDM_SCAN: {
				char mac[18] = { 0 };
				ds_GetFirstDevice(mac);
				while (strlen(mac)) {
					PDEVICESTATE p;
					p = ds_FindNode(mac);
					if (p) {
						p->state = 0;
					}
					ds_GetNextDevice(mac);
				}
				g_listchanged = TRUE;
				kk_dtStop();
				kk_dtStart();
				KillTimer(hWnd, TIMER_SCAN);
				SetTimer(hWnd, TIMER_SCAN, TIMER_SCAN, NULL);
			}
				break;
			case ID_CONTROL_GROUPCONTROLLOGIN:
			{
				PDEVICESTATE p;
				DWORD id;
				char model[33] = { 0 };

				DWORD error = 0;

				p = g_dsHead;
				while (p) {
					if (p->check) {
						if (model[0] == 0)
							strcpy(model, p->Model);
						else {
							if (strcmp(model, p->Model) != 0) {
								error |= 0x01;
								break;
							}
						}
					}
					p = p->next_node;
				}
				if (error & 0x01) {
					MessageBox(hWnd, "Grouped Control require the selected devices to all have the same Model", "Error", MB_OK | MB_ICONWARNING);
					break;
				}
				else {
					if (hGroupLogin) {
						ShowWindow(hGroupLogin, SW_SHOW);
					}
					else {
						hGroupLogin = CreateDialog(hInst, MAKEINTRESOURCE(IDD_GROUP_LOGIN), hWnd, GroupLoginProc);
						ShowWindow(hGroupLogin, SW_SHOW);
					}
				}
			}
				break;
			case ID_CONTROL_GROUPMASTERLEVELCONTROL:
			{
				PDEVICESTATE p;
				DWORD id;
				char model[33] = { 0 };
				DWORD same_model = 1;

				p = g_dsHead;
				while (p) {
					if (p->check) {
						if (model[0] == 0)
							strcpy(model, p->Model);
						else {
							if (strcmp(model, p->Model) != 0) {
								same_model = 0;
								break;
							}
						}
					}
					p = p->next_node;
				}
				if (same_model == 0) {
					MessageBox(hWnd, "Different models been selected.", "Error", MB_OK | MB_ICONWARNING);

				}
				else {
					if (hdlgGroupMasterLevel) {
						ShowWindow(hdlgGroupMasterLevel, SW_SHOW);
					}
					else {
						hdlgGroupMasterLevel = CreateDialog(hInst, MAKEINTRESOURCE(IDD_GROUP_MASTER_LEVEL), hWnd, GroupMasterLevel);
						ShowWindow(hdlgGroupMasterLevel, SW_SHOW);
					}
				}
			}
				break;
			case IDM_UPDATE_FW: {
				select_file();
				if (!g_update)
					break;
				PDEVICESTATE p;
				DWORD id;
				Update_SetWriteCB(dt_Write_data);

				p = g_dsHead;
				while (p) {
					if (p->check) {
						CreateThread(NULL, 0, Thread_Update, p, 0, &id);
					}
					p = p->next_node;
				}
			}
				break;

			case IDM_UPDATE_FACTORY: 
			{
				wchar_t path[2048] = { 0 };
				wsprintfW(path, L"%s", select_preset_file(PRESET_TYPE_ALL));

				if (path == NULL || wcslen(path) == 0)
					break;

				unsigned char *buff;
				long buff_size = 0;

				buff = (unsigned char*)read_file(path, &buff_size);
				unsigned char checksum = 0;
				for (int i = 0; i < buff_size - 1; i++)
					checksum += buff[i];

				if (buff_size < 11 || buff[0] != ACC_CUST_KEIGA || buff[6] != PRESET_TYPE_ALL || checksum != buff[buff_size - 1]) {
					free(buff);
					MessageBox(hWnd, "Load file error.", "", MB_OK | MB_ICONWARNING);
					break;
				}

				if (MessageBox(hWnd, "Load Factory Preset File ?", "Load File", MB_ICONQUESTION | MB_YESNO) == IDYES) {
					PDEVICESTATE p;
					DWORD id;
					p = g_dsHead;
					while (p) {
						if (p->check) {
							if (p->amp_id == buff[1]) {
									unsigned char cmd[520] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,p->amp_id,ACC_CMD_LINALD,(buff_size - 10) / 256,(buff_size - 10) % 256 };//7+512+1
									cmd[519] = 0;
									for (int i = 0; i < 7; i++)
										cmd[519] += cmd[i];
									for (int i = 7; i < 519; i++) {
										cmd[i] = buff[i + 3];
										cmd[519] += cmd[i];
									}
									dt_Write_data(p->Manufacturer, p->Name, cmd, 520);
									p->upload_preset_status = PRESET_UPLOADING;
									CB_SetListViewItem(ds_GetIndexByMac(p->Mac), "Uploading...");
							}
							else {
								CB_SetListViewItem(ds_GetIndexByMac(p->Mac), "Err_Amp_Id");
							}
						}
						p = p->next_node;
					}
					KillTimer(hWnd, TIMER_PRESET_TIMEOUT);
					SetTimer(hWnd, TIMER_PRESET_TIMEOUT, TIMER_PRESET_TIMEOUT, NULL);
				}

			}
			break;

			case IDM_UPDATE_INSTALLER: {
				wchar_t path[2048] = { 0 };
				wsprintfW(path, L"%s", select_preset_file(PRESET_TYPE_INSTALLER));

				if (path == NULL || wcslen(path) == 0)
					break;
				unsigned char *buff;
				long buff_size = 0;
				buff = (unsigned char*)read_file(path, &buff_size);
				unsigned char checksum = 0;
				for (int i = 0; i < buff_size-1; i++)
					checksum += buff[i];

				if (buff_size < 11 || buff[0] != ACC_CUST_KEIGA || buff[6] != PRESET_TYPE_INSTALLER || checksum != buff[buff_size - 1]) {
					free(buff);
					MessageBox(hWnd, "Load file error.", "", MB_OK | MB_ICONWARNING);
					break;
				}

				if (MessageBox(hWnd, "Load Installer Preset File ?", "Load File", MB_ICONQUESTION | MB_YESNO) == IDYES) {
					PDEVICESTATE p;
					DWORD id;
					p = g_dsHead;
					while (p) {
						if (p->check) {
							if (p->amp_id == buff[1]) {
								unsigned char cmd[1000] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,p->amp_id,ACC_CMD_USER_PRESET,(buff_size - 10 + 1) / 256,(buff_size - 10 + 1) % 256 ,PRESET_INDEX_INDEX_DATA };
								cmd[buff_size - 3] = 0;
								for (int i = 0; i < 8; i++)
									cmd[buff_size - 3] += cmd[i];
								for (int i = 8; i < buff_size - 3; i++) {
									cmd[i] = buff[i + 2];
									cmd[buff_size - 3] += cmd[i];
								}
								dt_Write_data(p->Manufacturer, p->Name, cmd, buff_size - 2);
								p->upload_preset_status = PRESET_UPLOADING;
								CB_SetListViewItem(ds_GetIndexByMac(p->Mac), "Uploading...");
							}
							else {
								CB_SetListViewItem(ds_GetIndexByMac(p->Mac), "Err_Amp_Id");
							}
						}
						p = p->next_node;
					}
					KillTimer(hWnd, TIMER_PRESET_TIMEOUT);
					SetTimer(hWnd, TIMER_PRESET_TIMEOUT, TIMER_PRESET_TIMEOUT, NULL);
				}
			}
			break;
			case ID_START_EXPORTTOCSVFILE: 
			{
				wchar_t path[2048] = { 0 };
				wsprintfW(path, L"%s", open_csv_file(MODE_SAVE));

				if (path == NULL || wcslen(path) == 0)
					break;

				FILE *fp;
				fp = _wfopen(path, L"wb");
				fseek(fp, 0, SEEK_SET);
				fwrite("Dante Device Name,", 36, 1, fp);
				fwrite("Model,", 12, 1, fp);
				fwrite("Installer Speaker Name,", 46, 1, fp);
				fwrite("Zone,", 10, 1, fp);
				fwrite("Installer File Name,", 40, 1, fp);
				fwrite("PoE Mode,", 18, 1, fp);
				fwrite("Status,", 14, 1, fp);
				fwrite("IP Address,", 22, 1, fp);
				fwrite("MAC Address,", 24, 1, fp);
				fwrite("FW Version,", 22, 1, fp);
				fwrite("Status Message\n", 32, 1, fp);

				PDEVICESTATE p;
				p = g_dsHead;
				while (p) {
					fwrite(p->Name, sizeof(p->Name), 1, fp);
					fwrite(",", 2, 1, fp);
					fwrite(p->Model, sizeof(p->Model), 1, fp);
					fwrite(",", 2, 1, fp);
					fwrite(p->Speaker, sizeof(p->Speaker), 1, fp);
					fwrite(",", 2, 1, fp);
					fwrite(p->Zone, sizeof(p->Zone), 1, fp);
					fwrite(",", 2, 1, fp);
					fwrite(p->InstallerFileName, sizeof(p->InstallerFileName), 1, fp);
					fwrite(",", 2, 1, fp);
					fwrite(p->Poe, sizeof(p->Poe), 1, fp);
					fwrite(",", 2, 1, fp);
					if (p->state)
						fwrite("Online", 12, 1, fp);
					else
						fwrite("Offline", 14, 1, fp);
					fwrite(",", 2, 1, fp);
					fwrite(p->Ip, sizeof(p->Ip), 1, fp);
					fwrite(",", 2, 1, fp);
					fwrite(p->Mac, sizeof(p->Mac), 1, fp);
					fwrite(",", 2, 1, fp);
					fwrite(p->Fw, sizeof(p->Fw), 1, fp);
					fwrite(",", 2, 1, fp);
					if (p->Update_State != NULL)
						fwrite(p->Update_State, sizeof(p->Update_State), 1, fp);
					else
						fwrite(" ", 2, 1, fp);
					fwrite("\n", 4, 1, fp);
					p = p->next_node;
				}
				fclose(fp);
			}
			break;
			case ID_START_EXPORTDEVICESPROFILE:
				ds_ExportProfileData();
				break;
			case ID_START_IMPORTDEVICESPROFILE:
				ds_ImportProfileData();
				break;
			case IDC_BUTTON_SEARCH: {
				bFiltering = TRUE;
				InvalidateRect(hWnd, &rectFilterIcon, TRUE);

				char comboText[20];
				char inputText[100];
				FILTERDATA fData = { NULL };
				GetWindowText(GetDlgItem(hWnd, IDC_COMBO_FILTER), comboText, 20);
				GetWindowText(hEditfilter, inputText, 100);
				
				if (strcmp(inputText, "") == 0) {
					ds_FilterDevice(FILTER_DEVICE_ALL, NULL);
				}
				if (strcmp(comboText, "MAC Address") == 0) {
					strcpy(fData.mac, inputText);
					ds_FilterDevice(FILTER_DEVICE_MAC, &fData);
				}
				if (strcmp(comboText, "Dante Device Name") == 0) {
					strcpy(fData.nameContain, inputText);
					ds_FilterDevice(FILTER_DEVICE_NAMECONTAIN, &fData);
				}
				if (strcmp(comboText, "Zone") == 0) {
					strcpy(fData.zone, inputText);
					ds_FilterDevice(FILTER_DEVICE_ZONE, &fData);
				}
				if (strcmp(comboText, "Model") == 0) {
					strcpy(fData.model, inputText);
					ds_FilterDevice(FILTER_DEVICE_MODEL, &fData);
				}
				if (strcmp(comboText, "IP Address") == 0) {
					strcpy(fData.ip, inputText);
					ds_FilterDevice(FILTER_DEVICE_IP, &fData);
				}
				SendMessage(hListView, LVM_DELETEALLITEMS, 0, 0);
				g_listchanged = TRUE;
				break;
			}
			
			case IDC_BUTTON_CLEAR_OFFLINE:
			{
				if (MessageBox(hWnd, "All devices will be removed from the list including Offline devices", "Clear Search List", MB_ICONQUESTION | MB_YESNO) == IDYES) {
					PDEVICESTATE p, removep;
					PDANTENODE pnode, prev;

					prev = NULL;
					pnode = g_dtHead;

					p = g_dsHead;
					while (p)
					{
						if (!p->state) {
							prev = NULL;
							pnode = g_dtHead;
							while (pnode && strcmp(pnode->name, p->Name) != 0)
							{
								prev = pnode;
								pnode = pnode->next_node;
							}

							if (pnode) {
								if (pnode == g_dtHead)
								{
									//first node
									g_dtHead = pnode->next_node;
									free(pnode);
								}
								else
								{
									if (!pnode->next_node)
										//tail node
										prev->next_node = NULL;
									else
										//middle node
										prev->next_node = pnode->next_node;
									free(pnode);
								}
							}

							removep = p;
							if (p->next_node == NULL) {
								p->prev_node->next_node = NULL;
								free(removep);
								p = NULL;
							}
							else {
								p = p->next_node;
								free(removep);
							}
							removep = NULL;
						}
						else
							p = p->next_node;
					}

					SendMessage(hListView, LVM_DELETEALLITEMS, 0, 0);
					g_listchanged = TRUE;
				}
				break;
			}
			default:
				return DefWindowProc(hWnd, message, wParam, lParam);
			}
		}
		break;
    case WM_PAINT:
	{
		PAINTSTRUCT ps;
		HDC hdc = BeginPaint(hWnd, &ps);
		SelectObject(hdc, GetStockObject(DC_BRUSH));
		HPEN hPen = CreatePen(PS_DOT, 1, RGB(150, 150, 150));
		SetDCBrushColor(hdc, RGB(255, 255, 255));
		SelectObject(hdc, hPen);
		Ellipse(hdc, rectFilterIcon.left, rectFilterIcon.top, rectFilterIcon.right, rectFilterIcon.bottom);
		DeleteObject(hPen);

		hPen = CreatePen(PS_DOT, 1, RGB(255, 255, 255));
		if(bFiltering)
			SetDCBrushColor(hdc, RGB(10, 200, 10));
		else
			SetDCBrushColor(hdc, RGB(240, 240, 240));
		SelectObject(hdc, hPen);
		Ellipse(hdc, rectFilterIcon.left+2, rectFilterIcon.top+2, rectFilterIcon.right-2, rectFilterIcon.bottom-2);
		DeleteObject(hPen);
		EndPaint(hWnd, &ps);
	}
	break;
	case WM_TIMER:
		switch (wParam) {
		case TIMER_SCAN: {
			if (!g_update) {
				CreateThread(NULL, 0, Thread_Ping_Device, NULL, 0, NULL);
				
				if (g_check_ready) {
					Update_Device_State();

					if (g_listchanged) {
						SendMessage(hMainWnd, WM_COMMAND, IDC_BUTTON_SEARCH, (LPARAM)hBtnSearch);
						ReflashListView(hListView);					
						bFiltering = FALSE;
						InvalidateRect(hWnd, &rectFilterIcon, TRUE);
						g_listchanged = FALSE;
					}
				}
			}
			else {
				boolean update = FALSE;
				PDEVICESTATE p;
				p = g_dsHead;
				while (p) {
					if (p->pUpdate!= NULL) {
						if (p->pUpdate->update) {
							update = TRUE;
							break;
						}
					}
					p = p->next_node;
				}
				g_update = update;
				if (!g_update) {
					if (g_update_file_data != NULL) {
						free(g_update_file_data);
						g_update_file_data = NULL;
					}
					kk_dtStop();
					kk_dtStart();
				}
			}
		}
		return 0;
		case TIMER_PRESET_TIMEOUT:
		{
			PDEVICESTATE p;
			p = g_dsHead;
			while (p) {
				if (p->upload_preset_status == PRESET_UPLOADING) {
					p->upload_preset_status = PRESET_TIMEOUT;
					CB_SetListViewItem(ds_GetIndexByMac(p->Mac), "Upload Timeout");
				}
				p = p->next_node;
			}
			KillTimer(hWnd, TIMER_PRESET_TIMEOUT);
		}
		return 0;
		}
		break;
	case WM_NOTIFY:
		if ((((LPNMHDR)lParam)->hwndFrom) == hListView) {
			switch (((LPNMHDR)lParam)->code)
			{
			case NM_DBLCLK:
			{
				LPNMITEMACTIVATE lpNMItem = (LPNMITEMACTIVATE)lParam;

				if (ListView_GetCheckState(hListView, lpNMItem->iItem))
					break;

				index_of_double_click = lpNMItem->iItem;

				if (hDeviceLogin) {
					DestroyWindow(hDeviceLogin);
					hDeviceLogin = NULL;
				}
				hDeviceLogin = CreateDialog(hInst, MAKEINTRESOURCE(IDD_LOGIN), hMainWnd, LoginProc);
				if (hDeviceLogin)
					ShowWindow(hDeviceLogin, SW_SHOW);
				break;
			}
			case NM_RCLICK: {
				LPNMITEMACTIVATE lpNMItem = (LPNMITEMACTIVATE)lParam;

//				if (ListView_GetCheckState(hListView, lpNMItem->iItem))
//					break;
				LVITEM lvitem;
				char Text[255] = { 0 };
				memset(&lvitem, 0, sizeof(lvitem));
				lvitem.mask = LVIF_TEXT;
				lvitem.iSubItem = COLUMN_MAC;
				lvitem.pszText = Text;
				lvitem.cchTextMax = 255;
				lvitem.iItem = lpNMItem->iItem;
				SendMessage(hListView, LVM_GETITEMTEXT, lpNMItem->iItem, (LPARAM)&lvitem);

				pds_right_click = ds_FindNode(lvitem.pszText);
				if (!pds_right_click)
					break;
				POINT cursor; // Getting the cursor position
				GetCursorPos(&cursor);
				// Creating the po-up menu list
				if (pds_right_click->pUpdate) {
					if (pds_right_click->pUpdate->update) {
						TrackPopupMenu((HMENU)GetSubMenu(LoadMenu(hInst, MAKEINTRESOURCE(IDR_MENU3)), 0), TPM_LEFTALIGN | TPM_RIGHTBUTTON, cursor.x, cursor.y, 0, hWnd, NULL);
					}
				}
				else if (!pds_right_click->findme) {
					TrackPopupMenu((HMENU)GetSubMenu(LoadMenu(hInst, MAKEINTRESOURCE(IDR_MENU1)), 0), TPM_LEFTALIGN | TPM_RIGHTBUTTON, cursor.x, cursor.y, 0, hWnd, NULL);
				}
				else {
					TrackPopupMenu((HMENU)GetSubMenu(LoadMenu(hInst, MAKEINTRESOURCE(IDR_MENU2)), 0), TPM_LEFTALIGN | TPM_RIGHTBUTTON, cursor.x, cursor.y, 0, hWnd, NULL);
				}
			}
			break;
			/*
			case NM_CLICK: {
				if (hwEdit != NULL) { SendMessage(hwEdit, WM_KILLFOCUS, 0, 0); };

					LPNMITEMACTIVATE lpNMItem = (LPNMITEMACTIVATE)lParam;
					LVITEM lvitem;
					char Text[255] = { 0 };
					memset(&lvitem, 0, sizeof(lvitem));
					lvitem.mask = LVIF_TEXT;
					lvitem.iSubItem = ((NMLISTVIEW*)lParam)->iSubItem;
					lvitem.pszText = Text;
					lvitem.cchTextMax = 255;
					lvitem.iItem = lpNMItem->iItem;
					SendMessage(hListView, LVM_GETITEMTEXT, lpNMItem->iItem, (LPARAM)&lvitem);

					LPNMITEMACTIVATE itemclicado = (LPNMITEMACTIVATE)lParam;
					RECT subitemrect;
					ListView_GetSubItemRect(hListView, itemclicado->iItem, itemclicado->iSubItem, LVIR_BOUNDS, &subitemrect);
					int altura = subitemrect.bottom - subitemrect.top;
					int largura = subitemrect.right - subitemrect.left;
					if (itemclicado->iSubItem == 0) { largura = largura / 2; };
					hwEdit = CreateWindowEx(WS_EX_CLIENTEDGE, "EDIT", lvitem.pszText, WS_CHILD | WS_VISIBLE | ES_WANTRETURN, subitemrect.left, subitemrect.top, largura, altura, hListView, 0, GetModuleHandle(NULL), NULL);
					if (hwEdit == NULL)  
						MessageBox(hListView, "Could not create edit box.", "Error", MB_OK | MB_ICONERROR);
					else {
						HFONT hFont = CreateFont((int)16, 0, 0, 0, FW_NORMAL, FALSE, FALSE, FALSE, GB2312_CHARSET,
							OUT_TT_PRECIS, CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY, DEFAULT_PITCH | FF_DONTCARE, "");
						SendMessage(hwEdit, WM_SETFONT, (WPARAM)hFont, TRUE);
						SetFocus(hwEdit);
						EOldProc = (WNDPROC)SetWindowLong(hwEdit, GWL_WNDPROC, (LONG)EditProc);
					}
					iItem = itemclicado->iItem;
					iSubItem = itemclicado->iSubItem;

				return 0;
				break;
			}
			*/
			case NM_CUSTOMDRAW:
			{
				return ProcessCustomDraw(lParam, hListView);
				return TRUE;
			}
			break;
			case LVN_ENDLABELEDIT:
			{
				LPNMLVDISPINFOA pdi = (LPNMLVDISPINFOA)lParam;
				if (pdi->item.iSubItem == 5) {

				}
				break;
			}
			case LVN_COLUMNCLICK:
			{
				OnColumnClick((LPNMLISTVIEW)lParam);
				break;
			}
			case LVN_ITEMCHANGED:
			{
				LPNMLISTVIEW pnmlv = (LPNMLISTVIEW)lParam;

				if (pnmlv->uChanged & LVIF_STATE && !bReflashlistview) {
					PDEVICESTATE p;
					p = ds_FindNodeByIndex(pnmlv->iItem);
					p->check = ListView_GetCheckState(hListView, pnmlv->iItem);
					boolean b = p->check;
				}
				return 0;
				break;
			}
			}
		}
		break;
	case WM_DESTROY: {
		kk_dtStop();
		ild_stop();
		char mac[18] = { 0 };
		ds_GetFirstDevice(mac);
		while (strlen(mac)) {
			ds_NodeRemove(mac);
			ds_GetFirstDevice(mac);
		}
		if (g_update_file_data != NULL) {
			free(g_update_file_data);
			g_update_file_data = NULL;
		}
		PostQuitMessage(0);
	}
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}

// [關於] 方塊的訊息處理常式。
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
    UNREFERENCED_PARAMETER(lParam);
    switch (message)
    {
    case WM_INITDIALOG:
        return (INT_PTR)TRUE;

    case WM_COMMAND:
        if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
        {
            EndDialog(hDlg, LOWORD(wParam));

            return (INT_PTR)TRUE;
        }
        break;
    }
    return (INT_PTR)FALSE;
}





LRESULT CALLBACK DeviceFilterProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam) {
	static boolean passedCtrl = FALSE;
	switch (uMsg)
	{
	case WM_CHAR:
	{
		if (passedCtrl)
			break;

		// Make sure we only allow specific characters
		if (!((wParam >= '0' && wParam <= '9')
			|| wParam == '.'
			|| wParam == '-'
			|| wParam == ':'
			|| (wParam >= 'a' && wParam <= 'z')
			|| (wParam >= 'A' && wParam <= 'Z')
			|| wParam == VK_RETURN
			|| wParam == VK_DELETE
			|| wParam == VK_BACK))
		{
			return 0;
		}
		PostMessage(hMainWnd, WM_COMMAND, IDC_BUTTON_SEARCH, 0);
	}
	break;

	case WM_GETDLGCODE: {
		if (wParam == VK_TAB) {
			LRESULT res;
			res = CallWindowProc(oldEditFilterProc, hwnd, uMsg, wParam, lParam);
			res &= ~DLGC_WANTALLKEYS;
			return res;
		}
		if (wParam == VK_RETURN) {
		}
		return CallWindowProc(oldEditFilterProc, hwnd, uMsg, wParam, lParam);
	}
	case WM_LBUTTONUP:
		SendMessage(hwnd, EM_SETSEL, 0, -1);
		break;
	case WM_KILLFOCUS:
		break;
	case WM_KEYDOWN: {
		switch (wParam)
		{
		case VK_CONTROL: {
			passedCtrl = TRUE;
			break;
		}
		case VK_RETURN:
			SendMessage(hMainWnd, WM_COMMAND, IDC_BUTTON_SEARCH, 0);
			if (wParam == VK_RETURN)
				return 0;
		}
		break;
	}
	case WM_KEYUP: {passedCtrl = FALSE; break; }
	}
	return CallWindowProc(oldEditFilterProc, hwnd, uMsg, wParam, lParam);
}

wchar_t* open_csv_file(int mode)
{
	OPENFILENAMEW ofn;
	wchar_t szFile[2048] = { 0 };
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = hMainWnd;
	ofn.lpstrFile = szFile;
	ofn.lpstrDefExt = L"csv";
	ofn.nMaxFile = sizeof(szFile);
	ofn.lpstrFilter = L"CSV file(*.csv)\0*.csv\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = NULL;

	 
	if (mode == MODE_LOAD) {
		ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
		if (GetOpenFileNameW(&ofn))
			return ofn.lpstrFile;
	}
	else {
		ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_OVERWRITEPROMPT;
		if (GetSaveFileNameW(&ofn))
			return ofn.lpstrFile;
	}
	return NULL;
}


