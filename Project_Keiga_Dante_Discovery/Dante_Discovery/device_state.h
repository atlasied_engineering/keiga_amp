#ifndef _DEVICE_STATE_
#define _DEVICE_STATE_

#pragma once

#include "stdafx.h"
#include "update.h"
#include "device_control.h"
#include "dante_conmon.h"

#define MAX_DEVICE_STATE	1024

#define KEIGA_COMPANY_NAME						"Keiga Electronic Inc"
#define ATLASIED_COMPANY_NAME					"AtlasIED"

#define SD001_MODEL_NAME						"SD001"
#define ED003_MODEL_NAME						"ED003"
#define ED004_MODEL_NAME						"ED004"
#define ED005_MODEL_NAME						"ED005"
#define AL003_MODEL_NAME						"AL003"
#define AL003M_MODEL_NAME						"AL003M PoE Amp"
#define DA_APX40N_MODEL_NAME					"DA-APX40N Speaker"
#define DA_22SYS_MODEL_NAME					"DA-22SYS Speaker"
#define DA_62SYS_MODEL_NAME					"DA-62SYS Speaker"
#define DA_APX_MODEL_NAME						"DA-APX Speaker"
#define DA_PM8GD_MODEL_NAME					"DA-PM8GD Speaker"
#define DA_PD8DG_MODEL_NAME					"DA-PD8DG Speaker"

#define SD001_MODEL_NAME_W						L"SD001"
#define ED003_MODEL_NAME_W						L"ED003"
#define ED004_MODEL_NAME_W						L"ED004"
#define ED005_MODEL_NAME_W						L"ED005"
#define AL003_MODEL_NAME_W						L"AL003"
#define AL003M_MODEL_NAME_W						L"AL003M PoE Amp"
#define DA_APX40N_MODEL_NAME_W					L"DA-APX40N Speaker"
#define DA_22SYS_MODEL_NAME_W					L"DA-22SYS Speaker"
#define DA_62SYS_MODEL_NAME_W					L"DA-62SYS Speaker"
#define DA_APX_MODEL_NAME_W						L"DA-APX Speaker"
#define DA_PM8GD_MODEL_NAME_W					L"DA-PM8GD Speaker"
#define DA_PD8DG_MODEL_NAME_W					L"DA-PD8DG Speaker"

#define AL003_MONO_PRESET_ID				1
	#define ATLASIED_POE_SPEAKER_PRESET_ID		1
	#define DA_APX40N_POE_SPEAKER_PRESET_ID		2

#define FILTER_DEVICE_ALL			(1u << 0)
#define FILTER_DEVICE_MODEL			(1u << 1)
#define FILTER_DEVICE_COMPANY		(1u << 2)
#define FILTER_DEVICE_NAMECONTAIN	(1u << 3)	//contain name
#define FILTER_DEVICE_ZONE			(1u << 4)	//contain name
//#define FILTER_DEVICE_SPEAKER		(1u << 5)	//contain name
#define FILTER_DEVICE_MAC			(1u << 5)	//contain name
#define FILTER_DEVICE_IP			(1u << 6)	//contain name


#define ERROR_SHORT_DAT				0x01
#define ERROR_SHORT_CLK				0x02
#define ERROR_CLASSD				0x04
#define ERROR_TEMPERATURE			0x08
#define ERROR_GPIO_IN				0x10

#define PRESET_NONE					0
#define PRESET_UPLOADING			1
#define PRESET_FINISHED				2
#define PRESET_TIMEOUT				3

#define FW_YEAR			508
#define FW_MONTH		509
#define FW_DATE			510
#define FW_NUMBER		511
#define	USER_DATA_COUNT	512

#define USER_FACTORY_1_AL003M	309
#define USER_INSTALLER_1_AL003M	341

#define PRESET_TYPE_ALL				0
#define PRESET_TYPE_FACTORY			1
#define PRESET_TYPE_INSTALLER		2

typedef struct tagDeviceState
{
	char Mac[18];
	char Name[33];
	char Model[33];
	char Zone[18];
	char Speaker[18];
	char InstallerFileName[33];
	char Ip[18];
	char Fw[18];
	char Poe[6];
	char Manufacturer[33];
	char Update_State[100];
	unsigned char fw_ver[4];
	unsigned char amp_id;
	unsigned char channel_mode;
	unsigned char ch1_mute;
	unsigned char ch2_mute;
	unsigned char bypass_group_control;
	char username[16];
	char password[16];
	CARGO cargo;
	int state;
	int upload_preset_status;
	int ack;
	boolean check;
	boolean show;		//show to listview
	boolean findme;
	PUPDATEPARAM pUpdate = NULL;
	PDEVICEPARAM pDevice = NULL;
	struct tagDeviceState *next_node;
	struct tagDeviceState *prev_node;
}DEVICESTATE, *PDEVICESTATE;

typedef struct tagFilteData {
	char model[100];
	char company[100];
	char nameContain[100];
	char zone[100];
	char speaker[100];
	char mac[19];
	char ip[19];
}FILTERDATA, *PFILTERDATA;

extern PDEVICESTATE g_dsHead;
extern DEVICESTATE g_ds[];
extern boolean g_check_ready;
extern boolean g_listchanged;
void Update_Device_State(void);
void ds_GetProfileData(char* path);
void ds_ImportProfileData(void);
void ds_ExportProfileData(void);
int ds_GetCount(void);
PDEVICESTATE ds_FindNode(char* mac);
PDEVICESTATE ds_FindNodeByIndex(int index);
int ds_GetIndexByMac(char* mac);
void ds_NodeAdd(char* mac);
void ds_NodeRemove(char* mac);
void ds_GetFirstDevice(char *mac);
void ds_GetNextDevice(char *mac);
void ds_FilterDevice(uint32_t filteType, PFILTERDATA pFilterData);
#endif // _DEVICE_STATE_