#pragma once
#include "stdafx.h"

#define UPDATE_MODE_BOOT		0		//update mcu in the time of bootloader 
#define UPDATE_MODE_AP			1		//update mcu in the time of app

#define UPDATE_MAX_FILE_SIZE 0x20000;//129536//66560
#define UPDATE_PTN_FILE_SIZE_BOOT 512 //Partition of hex on bootloader  
#define UPDATE_PTN_FILE_SIZE_AP 256  //Partition of hex on application


//#define CMD_RH_RESET								0xF0
#define CMD_BOOTLOAD							0xF1
#define CMD_UPDATE_SECTION						0xF2
#define CMD_UPDATE_PAGE							0xF3
#define CMD_UPDATE_PAGE_LCM						0xF4
#define CMD_UPDATE_PAGE_EX_MODLUE				0xF5

#define	OUT_QUEUE_MAX				1000

#define	UPDATE_OUT_QUEUE_MAX		10
#define	UPDATE_OUT_LENGTH			300

typedef boolean upload_Write_fn(char* manufacturer, char* name, unsigned char* outputData, const unsigned int sizeBuffer);

typedef void upload_Receive_fn(void* mparamr, void* lparamr);

typedef void upload_Msg_fn(int index, char* pszText);

typedef struct tagUpdateDataReceiver {
	int lenMessage;			//length of message
	int sizeParam;			//size of buffer of paramer
	unsigned char* buffParam;	//buffer of paramer
}UPDATERECV,*PUPDATERECV;

typedef struct tagVersionFW {
	int yy;
	int mm;
	int dd;
	int vv;
}VERSIONFW;

typedef struct tagUpdateDataParam {
	char			device_name[33] = { 0 };
	char			manufacturer[33] = { 0 };
	char			fw[18] = { 0 };
	char			update_msg[100] = { 0 };
	int				index_in_listview;
	boolean			update;
	int				update_page;
	unsigned int	send_hex_data_flag;
	unsigned int	send_hex_data_sum;
	int				update_state;
	int				update_progress;
	boolean 		update_file_result;
	unsigned long	tick_wait_point;
	unsigned long	tick_count;
	unsigned long	tick_resend_wait_point;
	unsigned char	out_queue[UPDATE_OUT_QUEUE_MAX][UPDATE_OUT_LENGTH];
	int				queue_index = 0;
	int				queue_to_send = 0;
	upload_Receive_fn *fn_reciver = NULL;
	upload_Msg_fn *fn_msg = NULL;
}UPDATEPARAM, *PUPDATEPARAM;

typedef enum {} UpdateState_AP;

// Enumerate of the Status of the Application Update 
#define ESAU_Init				0
#define ESAU_SetVerToZero		1
#define ESAU_PreAmpData			2
#define ESAU_PreAmpDone			3
#define ESAU_LcmData			4
#define ESAU_LcmDone			5
#define ESAU_ExModuleData		6
#define ESAU_ExModuleDone		7
#define ESAU_GetCksum_PreAmp	8
#define ESAU_GetCksum_Lcm		9
#define ESAU_GetCksum_ExModule	10
#define ESAU_SetVer				11
#define ESAU_Restart			12
#define ESAU_Finished			13
#define ESAU_Failure			14
#define ESAU_MAX				15
#define ESAU_GetResume			16

extern unsigned char*	g_update_file_data;

void select_file(void);
wchar_t* select_preset_file(int type);
boolean load_hex(wchar_t* file);

void Update_SetWriteCB(upload_Write_fn *fn);
DWORD WINAPI Thread_Update(LPVOID lpParam);