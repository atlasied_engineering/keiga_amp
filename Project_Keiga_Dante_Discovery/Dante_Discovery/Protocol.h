

#ifndef _PROTOCOL_H_
#define _PROTOCOL_H_

#pragma once

#define MAX_PARAM					512
#define MAX_FILE_PATH				32767
//7.7.2017 by Andy
#define MAX_RENAME					500

#define MAX_MODEL					100

#define TRG_ALL						0x00
#define TRG_DA260					0x11
#define LEN_DA260					59
#define LEN_DA260_EQ				10

#define FW_VER_YEAR					21
#define FW_VER_MONTH				5
#define FW_VER_DATE					25
#define FW_VER_NUMBER				1

#define PRESET_TYPE_ALL				0
#define PRESET_TYPE_FACTORY			1
#define PRESET_TYPE_INSTALLER		2

#ifndef _RXSTATE_
#define _RXSTATE_
typedef enum tagRxState {
	UART_READY, UART_HEAD, UART_SOURCE, UART_TARGET, UART_COMMAND, UART_LENGTH_H, UART_LENGTH_L, UART_DATA_N, UART_CHECKSUM, UART_FINISH
} RXSTATE;
#endif

typedef struct tagCommandLead
{
	union
	{
		unsigned char Byte[7];
		struct {
			unsigned char Customer;
			unsigned char Head;
			unsigned char Source;
			unsigned char Target;
			unsigned char Command;
			unsigned short Length;
		};
	}_;
}COMMANDLEAD, *PCOMMANDLEAD;

typedef struct tagSource_Page
{
	union
	{
		unsigned char Byte;
		struct {
			unsigned char RCA_Page0 : 1;
			unsigned char RCA_Page1 : 1;
			unsigned char Reserve1 : 1;
			unsigned char Reserve2 : 1;
			unsigned char Reserve3 : 1;
			unsigned char Wireless : 1;
			unsigned char DD_Page : 1;//DBay & Dante
			unsigned char Optical_Page : 1;
		};
	}_;
}SOURCEPAGE, *PSOURCEPAGE;

typedef struct tagSource
{
	union
	{
		unsigned char Byte;
		struct {
			unsigned char RCA1 : 1;
			unsigned char RCA2 : 1;
			unsigned char RCA3 : 1;
			unsigned char RCA4 : 1;
			unsigned char RCA5 : 1;
			unsigned char RCA6 : 1;
			unsigned char RCA7 : 1;
			unsigned char RCA8 : 1;
		};
		struct {
			unsigned char RCA9 : 1;
			unsigned char RCA10 : 1;
			unsigned char RCA11 : 1;
			unsigned char RCA12 : 1;
			unsigned char RCA13 : 1;
			unsigned char RCA14 : 1;
			unsigned char RCA15 : 1;
			unsigned char RCA16 : 1;
		};
		struct {
			unsigned char Wireless1 : 1;
			unsigned char Wireless2 : 1;
			unsigned char Wireless3 : 1;
			unsigned char Wireless4 : 1;
			unsigned char Wireless5 : 1;
			unsigned char Wireless6 : 1;
			unsigned char Wireless7 : 1;
			unsigned char Wireless8 : 1;
		};
		struct {
			unsigned char Dante1 : 1;
			unsigned char Dante2 : 1;
			unsigned char Dante3 : 1;
			unsigned char Dante4 : 1;
			unsigned char DBay1 : 1;
			unsigned char DBay2 : 1;
			unsigned char DBay3 : 1;
			unsigned char DBay4 : 1;
		};
		struct {
			unsigned char Coax1 : 1;
			unsigned char Coax2 : 1;
			unsigned char Coax3 : 1;
			unsigned char Coax4 : 1;
			unsigned char Optical1 : 1;
			unsigned char Optical2 : 1;
			unsigned char Optical3 : 1;
			unsigned char Optical4 : 1;
		};
	}_;
}SOURCE, *PSOURCE;

typedef struct tagOutputPage
{
	union
	{
		unsigned char Byte;
		struct {
			unsigned char Out_Page0 : 1;
			unsigned char Out_Page1 : 1;
			unsigned char Out_Page2 : 1;
			unsigned char Out_Page3 : 1;
			unsigned char Out_Page4 : 1;
			unsigned char Out_Page5 : 1;
			unsigned char Out_Page6 : 1;
			unsigned char Subwoofer_Page : 1;
		};
	}_;
}OUTPUTPAGE, *POUTPUTPAGE;

typedef struct tagOutput
{
	union
	{
		unsigned char Byte;
		struct {
			unsigned char Output1 : 1;
			unsigned char Output2 : 1;
			unsigned char Output3 : 1;
			unsigned char Output4 : 1;
			unsigned char Output5 : 1;
			unsigned char Output6 : 1;
			unsigned char Output7 : 1;
			unsigned char Output8 : 1;
		};
		struct {
			unsigned char Output9 : 1;
			unsigned char Output10 : 1;
			unsigned char Output11 : 1;
			unsigned char Output12 : 1;
			unsigned char Output13 : 1;
			unsigned char Output14 : 1;
			unsigned char Output15 : 1;
			unsigned char Output16 : 1;
		};
		struct {
			unsigned char Output17 : 1;
			unsigned char Output18 : 1;
			unsigned char Output19 : 1;
			unsigned char Output20 : 1;
			unsigned char Output21 : 1;
			unsigned char Output22 : 1;
			unsigned char Output23 : 1;
			unsigned char Output24 : 1;
		};
		struct {
			unsigned char Output25 : 1;
			unsigned char Output26 : 1;
			unsigned char Output27 : 1;
			unsigned char Output28 : 1;
			unsigned char Output29 : 1;
			unsigned char Output30 : 1;
			unsigned char Output31 : 1;
			unsigned char Output32 : 1;
		};
		struct {
			unsigned char Output33 : 1;
			unsigned char Output34 : 1;
			unsigned char Output35 : 1;
			unsigned char Output36 : 1;
			unsigned char Output37 : 1;
			unsigned char Output38 : 1;
			unsigned char Output39 : 1;
			unsigned char Output40 : 1;
		};
		struct {
			unsigned char Output41 : 1;
			unsigned char Output42 : 1;
			unsigned char Output43 : 1;
			unsigned char Output44 : 1;
			unsigned char Output45 : 1;
			unsigned char Output46 : 1;
			unsigned char Output47 : 1;
			unsigned char Output48 : 1;
		};
		struct {
			unsigned char Output49 : 1;
			unsigned char Output50 : 1;
			unsigned char Output51 : 1;
			unsigned char Output52 : 1;
			unsigned char Output53 : 1;
			unsigned char Output54 : 1;
			unsigned char Output55 : 1;
			unsigned char Output56 : 1;
		};
		struct {
			unsigned char SubOut1 : 1;
			unsigned char SubOut2 : 1;
			unsigned char SubOut3 : 1;
			unsigned char SubOut4 : 1;
			unsigned char SubOut5 : 1;
			unsigned char SubOut6 : 1;
			unsigned char SubOut7 : 1;
			unsigned char SubOut8 : 1;
		};
	}_;
}OUTPUT, *POUTPUT;

typedef struct tagDataMVOL
{
	union
	{
		unsigned char Byte;
		struct {
			unsigned char Volume : 7;
			unsigned char Mute : 1;
		};
	}Master;

	unsigned char Limit;

}DATAMVOL, *PDATAMVOL;

typedef struct tagDataSRCVOL
{
	SOURCEPAGE SourcePage;
	SOURCE Source;
	unsigned char VolumeL;
	unsigned char VolumeR;
}DATASRCVOL, *PDATASRCVOL;

typedef struct tagDataOUTVOL
{
	OUTPUTPAGE OutputPage;
	OUTPUT Output;
	unsigned char VolumeL;
	unsigned char VolumeR;
	union
	{
		unsigned char Byte;
		struct {
			unsigned char Volume : 7;
			unsigned char Mute : 1;
		};
	}LMuteVolH;
	union
	{
		unsigned char Byte;
		struct {
			unsigned char Volume : 7;
			unsigned char Mute : 1;
		};
	}RMuteVolH;
}DATAOUTVOL, *PDATAOUTVOL;

typedef struct tagDataSRCTOUT
{
	OUTPUTPAGE OutputPage;
	OUTPUT Output;
	SOURCEPAGE SourcePage;
	SOURCE Source;
}DATASRCTOUT, *PDATASRCOUT;

typedef struct tagDataGEQ
{
	OUTPUTPAGE OutputPage;
	OUTPUT Output;
	union
	{
		unsigned char Byte;
		struct {
			unsigned char L : 1;
			unsigned char R : 1;
			unsigned char Reserve1 : 1;
			unsigned char Reserve2 : 1;
			unsigned char Reserve3 : 1;
			unsigned char Reserve4 : 1;
			unsigned char Reserve5 : 1;
			unsigned char Reserve6 : 1;
		};
	}Channel;
	unsigned char Mode;
	unsigned char Index;
	unsigned char Gain;
}DATAGEQ, *PDATAGEQ;

typedef struct tagDataPEQ
{
	OUTPUTPAGE OutputPage;
	OUTPUT Output;
	union
	{
		unsigned char Byte;
		struct {
			unsigned char L : 1;
			unsigned char R : 1;
			unsigned char Reserve1 : 1;
			unsigned char Reserve2 : 1;
			unsigned char Reserve3 : 1;
			unsigned char Reserve4 : 1;
			unsigned char Reserve5 : 1;
			unsigned char Reserve6 : 1;
		};
	}Channel;
	unsigned char Mode;
	unsigned char Index;
	unsigned char Type;
	unsigned char Gain;
	union
	{
		unsigned char Byte;
		struct {
			unsigned char Frequency : 7;
			unsigned char Enable : 1;
		};
	}EnFreqH;
	unsigned char FreqL;
	unsigned char qH;
	unsigned char qL;
}DATAPEQ, *PDATAPEQ;

typedef struct tagDataHLPF
{
	OUTPUTPAGE OutputPage;
	OUTPUT Output;
	union
	{
		unsigned char Byte;
		struct {
			unsigned char L : 1;
			unsigned char R : 1;
			unsigned char Reserve1 : 1;
			unsigned char Reserve2 : 1;
			unsigned char Reserve3 : 1;
			unsigned char Reserve4 : 1;
			unsigned char Reserve5 : 1;
			unsigned char Reserve6 : 1;
		};
	}Channel;
	unsigned char Type;
	union
	{
		unsigned char Byte;
		struct {
			unsigned char Frequency : 7;
			unsigned char Enable : 1;
		};
	}EnFreqH;
	unsigned char FreqL;
	unsigned char Slope;
}DATAHLPF, *PDATAHLPF;



//////////////////////////////////////////////////////////////////
//AL003 Mono
//////////////////////////////////////////////////////////////////
#define USER_VOL_MASTER								0
#define USER_VOL_CH1									1
#define USER_VOL_CH2									2
#define USER_VOL_DANTE								3
#define USER_VOL_MASKING_IN						4
#define USER_VOL_MASKING_OUT					5
#define USER_MUTE											6
#define USER_MUTE_CH1									7
#define USER_MUTE_CH2									8
#define USER_MUTE_DANTE								9
#define USER_MUTE_MASKING							10
#define USER_INPUT_GAIN_DANTE					11
#define USER_HIPASS_SWITCH_FACTORY		12
#define USER_HIPASS_FREQ_H_FACTORY		13
#define USER_HIPASS_FREQ_L_FACTORY		14
#define USER_HIPASS_SLOPE_FACTORY			15
#define USER_LOPASS_SWITCH_FACTORY		16
#define USER_LOPASS_FREQ_H_FACTORY		17
#define USER_LOPASS_FREQ_L_FACTORY		18
#define USER_LOPASS_SLOPE_FACTORY			19
#define USER_HIPASS_SWITCH_DANTE			20
#define USER_HIPASS_FREQ_H_DANTE			21
#define USER_HIPASS_FREQ_L_DANTE			22
#define USER_HIPASS_SLOPE_DANTE				23
#define USER_LOPASS_SWITCH_DANTE			24
#define USER_LOPASS_FREQ_H_DANTE			25
#define USER_LOPASS_FREQ_L_DANTE			26
#define USER_LOPASS_SLOPE_DANTE				27
#define USER_HIPASS_SWITCH_MASKING		28
#define USER_HIPASS_FREQ_H_MASKING		29
#define USER_HIPASS_FREQ_L_MASKING		30
#define USER_HIPASS_SLOPE_MASKING			31
#define USER_LOPASS_SWITCH_MASKING		32
#define USER_LOPASS_FREQ_H_MASKING		33
#define USER_LOPASS_FREQ_L_MASKING		34
#define USER_LOPASS_SLOPE_MASKING			35
#define USER_C1_SWITCH_FACTORY				36
#define USER_C2_SWITCH_FACTORY				37
#define USER_C3_SWITCH_FACTORY				38
#define USER_C4_SWITCH_FACTORY				39
#define USER_C5_SWITCH_FACTORY				40
#define USER_C6_SWITCH_DANTE					41
#define USER_C7_SWITCH_DANTE					42
#define USER_C8_SWITCH_DANTE					43
#define USER_C9_SWITCH_DANTE					44
#define USER_C10_SWITCH_DANTE					45
#define USER_C11_SWITCH_MASKING				46
#define USER_C12_SWITCH_MASKING				47
#define USER_C13_SWITCH_MASKING				48
#define USER_C14_SWITCH_MASKING				49
#define USER_C15_SWITCH_MASKING				50
#define USER_C16_SWITCH_MASKING				51
#define USER_C17_SWITCH_MASKING				52
#define USER_C18_SWITCH_MASKING				53
#define USER_C19_SWITCH_MASKING				54
#define USER_C20_SWITCH_MASKING				55
#define USER_C21_SWITCH_MASKING				56
#define USER_C22_SWITCH_MASKING				57
#define USER_C23_SWITCH_MASKING				58
#define USER_C24_SWITCH_MASKING				59
#define USER_C25_SWITCH_MASKING				60
#define USER_C26_SWITCH_MASKING				61
#define USER_C27_SWITCH_MASKING				62
#define USER_C28_SWITCH_MASKING				63
#define USER_C29_SWITCH_MASKING				64
#define USER_C30_SWITCH_MASKING				65
#define USER_C1_GAIN_FACTORY					66
#define USER_C2_GAIN_FACTORY					67
#define USER_C3_GAIN_FACTORY					68
#define USER_C4_GAIN_FACTORY					69
#define USER_C5_GAIN_FACTORY					70
#define USER_C6_GAIN_DANTE						71
#define USER_C7_GAIN_DANTE						72
#define USER_C8_GAIN_DANTE						73
#define USER_C9_GAIN_DANTE						74
#define USER_C10_GAIN_DANTE						75
#define USER_C11_GAIN_MASKING					76
#define USER_C12_GAIN_MASKING					77
#define USER_C13_GAIN_MASKING					78
#define USER_C14_GAIN_MASKING					79
#define USER_C15_GAIN_MASKING					80
#define USER_C16_GAIN_MASKING					81
#define USER_C17_GAIN_MASKING					82
#define USER_C18_GAIN_MASKING					83
#define USER_C19_GAIN_MASKING					84
#define USER_C20_GAIN_MASKING					85
#define USER_C21_GAIN_MASKING					86
#define USER_C22_GAIN_MASKING					87
#define USER_C23_GAIN_MASKING					88
#define USER_C24_GAIN_MASKING					89
#define USER_C25_GAIN_MASKING					90
#define USER_C26_GAIN_MASKING					91
#define USER_C27_GAIN_MASKING					92
#define USER_C28_GAIN_MASKING					93
#define USER_C29_GAIN_MASKING					94
#define USER_C30_GAIN_MASKING					95
#define USER_C1_Q_H_FACTORY				    96
#define USER_C2_Q_H_FACTORY				    97
#define USER_C3_Q_H_FACTORY				    98
#define USER_C4_Q_H_FACTORY				    99
#define USER_C5_Q_H_FACTORY				    100
#define USER_C6_Q_H_DANTE					    101
#define USER_C7_Q_H_DANTE					    102
#define USER_C8_Q_H_DANTE					    103
#define USER_C9_Q_H_DANTE					    104
#define USER_C10_Q_H_DANTE				    105
#define USER_C11_Q_H_MASKING			    106
#define USER_C12_Q_H_MASKING			    107
#define USER_C13_Q_H_MASKING			    108
#define USER_C14_Q_H_MASKING			    109
#define USER_C15_Q_H_MASKING			    110
#define USER_C16_Q_H_MASKING			    111
#define USER_C17_Q_H_MASKING			    112
#define USER_C18_Q_H_MASKING			    113
#define USER_C19_Q_H_MASKING			    114
#define USER_C20_Q_H_MASKING			    115
#define USER_C21_Q_H_MASKING			    116
#define USER_C22_Q_H_MASKING			    117
#define USER_C23_Q_H_MASKING			    118
#define USER_C24_Q_H_MASKING			    119
#define USER_C25_Q_H_MASKING			    120
#define USER_C26_Q_H_MASKING			    121
#define USER_C27_Q_H_MASKING			    122
#define USER_C28_Q_H_MASKING			    123
#define USER_C29_Q_H_MASKING			    124
#define USER_C30_Q_H_MASKING			    125
#define USER_C1_Q_L_FACTORY				    126
#define USER_C2_Q_L_FACTORY				    127
#define USER_C3_Q_L_FACTORY				    128
#define USER_C4_Q_L_FACTORY				    129
#define USER_C5_Q_L_FACTORY				    130
#define USER_C6_Q_L_DANTE					    131
#define USER_C7_Q_L_DANTE					    132
#define USER_C8_Q_L_DANTE					    133
#define USER_C9_Q_L_DANTE					    134
#define USER_C10_Q_L_DANTE				    135
#define USER_C11_Q_L_MASKING			    136
#define USER_C12_Q_L_MASKING			    137
#define USER_C13_Q_L_MASKING			    138
#define USER_C14_Q_L_MASKING			    139
#define USER_C15_Q_L_MASKING			    140
#define USER_C16_Q_L_MASKING			    141
#define USER_C17_Q_L_MASKING			    142
#define USER_C18_Q_L_MASKING			    143
#define USER_C19_Q_L_MASKING			    144
#define USER_C20_Q_L_MASKING			    145
#define USER_C21_Q_L_MASKING			    146
#define USER_C22_Q_L_MASKING			    147
#define USER_C23_Q_L_MASKING			    148
#define USER_C24_Q_L_MASKING			    149
#define USER_C25_Q_L_MASKING			    150
#define USER_C26_Q_L_MASKING			    151
#define USER_C27_Q_L_MASKING			    152
#define USER_C28_Q_L_MASKING			    153
#define USER_C29_Q_L_MASKING			    154
#define USER_C30_Q_L_MASKING			    155
#define USER_C1_FREQ_H_FACTORY				156
#define USER_C2_FREQ_H_FACTORY				157
#define USER_C3_FREQ_H_FACTORY				158
#define USER_C4_FREQ_H_FACTORY				159
#define USER_C5_FREQ_H_FACTORY				160
#define USER_C6_FREQ_H_DANTE					161
#define USER_C7_FREQ_H_DANTE					162
#define USER_C8_FREQ_H_DANTE					163
#define USER_C9_FREQ_H_DANTE					164
#define USER_C10_FREQ_H_DANTE					165
#define USER_C11_FREQ_H_MASKING				166
#define USER_C12_FREQ_H_MASKING				167
#define USER_C13_FREQ_H_MASKING				168
#define USER_C14_FREQ_H_MASKING				169
#define USER_C15_FREQ_H_MASKING				170
#define USER_C16_FREQ_H_MASKING				171
#define USER_C17_FREQ_H_MASKING				172
#define USER_C18_FREQ_H_MASKING				173
#define USER_C19_FREQ_H_MASKING				174
#define USER_C20_FREQ_H_MASKING				175
#define USER_C21_FREQ_H_MASKING				176
#define USER_C22_FREQ_H_MASKING				177
#define USER_C23_FREQ_H_MASKING				178
#define USER_C24_FREQ_H_MASKING				179
#define USER_C25_FREQ_H_MASKING				180
#define USER_C26_FREQ_H_MASKING				181
#define USER_C27_FREQ_H_MASKING				182
#define USER_C28_FREQ_H_MASKING				183
#define USER_C29_FREQ_H_MASKING				184
#define USER_C30_FREQ_H_MASKING				185
#define USER_C1_FREQ_L_FACTORY				186
#define USER_C2_FREQ_L_FACTORY				187
#define USER_C3_FREQ_L_FACTORY				188
#define USER_C4_FREQ_L_FACTORY				189
#define USER_C5_FREQ_L_FACTORY				190
#define USER_C6_FREQ_L_DANTE					191
#define USER_C7_FREQ_L_DANTE					192
#define USER_C8_FREQ_L_DANTE					193
#define USER_C9_FREQ_L_DANTE					194
#define USER_C10_FREQ_L_DANTE					195
#define USER_C11_FREQ_L_MASKING				196
#define USER_C12_FREQ_L_MASKING				197
#define USER_C13_FREQ_L_MASKING				198
#define USER_C14_FREQ_L_MASKING				199
#define USER_C15_FREQ_L_MASKING				200
#define USER_C16_FREQ_L_MASKING				201
#define USER_C17_FREQ_L_MASKING				202
#define USER_C18_FREQ_L_MASKING				203
#define USER_C19_FREQ_L_MASKING				204
#define USER_C20_FREQ_L_MASKING				205
#define USER_C21_FREQ_L_MASKING				206
#define USER_C22_FREQ_L_MASKING				207
#define USER_C23_FREQ_L_MASKING				208
#define USER_C24_FREQ_L_MASKING				209
#define USER_C25_FREQ_L_MASKING				210
#define USER_C26_FREQ_L_MASKING				211
#define USER_C27_FREQ_L_MASKING				212
#define USER_C28_FREQ_L_MASKING				213
#define USER_C29_FREQ_L_MASKING				214
#define USER_C30_FREQ_L_MASKING				215
#define USER_T2_LIMIT_ENABLE		      216
#define USER_T2_LIMIT					 	      217
#define USER_T2_LIMIT_PBTL			      218
#define USER_T2P_LIMIT_ENABLE		      219
#define USER_T2P_LIMIT					      220
#define USER_T2P_LIMIT_PBTL			      221
#define USER_T2PP_LIMIT_ENABLE	      222
#define USER_T2PP_LIMIT					      223
#define USER_T2PP_LIMIT_PBTL		      224
#define USER_T2_C_LIMIT_ENABLE	      225
#define USER_T2_C_LIMIT					      226
#define USER_T2_C_RECOVERY			      227
#define USER_T2P_C_LIMIT_ENABLE	      228
#define USER_T2P_C_LIMIT				      229
#define USER_T2P_C_RECOVERY			      230
#define USER_T2PP_C_LIMIT_ENABLE      231
#define USER_T2PP_C_LIMIT				      232
#define USER_T2PP_C_RECOVERY		      233												      
#define USER_THRESHOLD_DANTE		      234
#define USER_KNEE_DANTE				 	      235
#define USER_RATIO_H_DANTE			      236
#define USER_RATIO_L_DANTE			      237
#define USER_ATTACK_H_DANTE			      238
#define USER_ATTACK_L_DANTE			      239
#define USER_RELEASE_H_DANTE		      240
#define USER_RELEASE_L_DANTE		      241
#define USER_HOLD_H_DANTE				      242
#define USER_HOLD_L_DANTE				      243										      
#define USER_4O_THRESHOLD_DANTE	      244
#define USER_4O_KNEE_DANTE			      245
#define USER_4O_RATIO_H_DANTE		      246
#define USER_4O_RATIO_L_DANTE		      247
#define USER_4O_ATTACK_H_DANTE	      248
#define USER_4O_ATTACK_L_DANTE	      249
#define USER_4O_RELEASE_H_DANTE	      250
#define USER_4O_RELEASE_L_DANTE	      251
#define USER_4O_HOLD_H_DANTE		      252
#define USER_4O_HOLD_L_DANTE		      253											      
#define USER_GPIO_STATUS				      254
#define USER_PBTL_MODE					      255
#define USER_T2P_TYPE						      256
#define USER_OHM								      257
#define USER_STATE_LED1					      258
#define USER_STATE_LED2					      259					      
#define USER_LOCATION_1					      260
#define USER_LOCATION_2					      261
#define USER_LOCATION_3					      262
#define USER_LOCATION_4					      263
#define USER_LOCATION_5					      264
#define USER_LOCATION_6					      265
#define USER_LOCATION_7					      266
#define USER_LOCATION_8					      267
#define USER_LOCATION_9					      268
#define USER_LOCATION_10				      269
#define USER_LOCATION_11				      270
#define USER_LOCATION_12				      271
#define USER_LOCATION_13				      272
#define USER_LOCATION_14				      273
#define USER_LOCATION_15				      274
#define USER_LOCATION_16				      275
#define USER_TEMP_PROTECT_DB				  276
#define USER_TEMP_PROTECT_START				  277
#define USER_TEMP_PROTECT_H					  278
#define USER_TEMP_PROTECT_L					  279					
#define USER_SPEAKER_1							  280
#define USER_SPEAKER_2							  281
#define USER_SPEAKER_3							  282
#define USER_SPEAKER_4							  283
#define USER_SPEAKER_5							  284
#define USER_SPEAKER_6							  285
#define USER_SPEAKER_7							  286
#define USER_SPEAKER_8							  287
#define USER_SPEAKER_9							  288
#define USER_SPEAKER_10						  289
#define USER_SPEAKER_11						  290
#define USER_SPEAKER_12							291
#define USER_SPEAKER_13							292
#define USER_SPEAKER_14							293
#define USER_SPEAKER_15							294
#define USER_SPEAKER_16							295
#define USER_SIGNAL_CH1							296
#define USER_SIGNAL_CH2							297
#define USER_VOL_CH1_H							298
#define USER_VOL_CH2_H							299
#define USER_VOL_DANTE_H						300
#define USER_FINDME_TIME						301
#define USER_FINDME_TIME_H						302
#define USER_VOL_FINDME							303
#define USER_INVERT_CH2							304
#define USER_SWITCH_MASKING_EQ					305
#define USER_T2_LIMIT_CH2			 			306
#define USER_T2P_LIMIT_CH2		 				307
#define USER_T2PP_LIMIT_CH2		 				308
#define USER_FACTORY_1						  309
#define USER_FACTORY_2						  310
#define USER_FACTORY_3						  311
#define USER_FACTORY_4						  312
#define USER_FACTORY_5						  313
#define USER_FACTORY_6						  314
#define USER_FACTORY_7						  315
#define USER_FACTORY_8						  316
#define USER_FACTORY_9						  317
#define USER_FACTORY_10						  318
#define USER_FACTORY_11						  319
#define USER_FACTORY_12						  320
#define USER_FACTORY_13						  321
#define USER_FACTORY_14						  322
#define USER_FACTORY_15						  323
#define USER_FACTORY_16						  324
#define USER_FACTORY_17						  325
#define USER_FACTORY_18						  326
#define USER_FACTORY_19						  327
#define USER_FACTORY_20						  328
#define USER_FACTORY_21						  329
#define USER_FACTORY_22						  330
#define USER_FACTORY_23						  331
#define USER_FACTORY_24						  332
#define USER_FACTORY_25						  333
#define USER_FACTORY_26						  334
#define USER_FACTORY_27						  335
#define USER_FACTORY_28						  336
#define USER_FACTORY_29						  337
#define USER_FACTORY_30						  338
#define USER_FACTORY_31						  339
#define USER_FACTORY_32						  340
#define USER_INSTALLER_1						341
#define USER_INSTALLER_2						342
#define USER_INSTALLER_3						343
#define USER_INSTALLER_4						344
#define USER_INSTALLER_5						345
#define USER_INSTALLER_6						346
#define USER_INSTALLER_7						347
#define USER_INSTALLER_8						348
#define USER_INSTALLER_9						349
#define USER_INSTALLER_10						350
#define USER_INSTALLER_11						351
#define USER_INSTALLER_12						352
#define USER_INSTALLER_13						353
#define USER_INSTALLER_14						354
#define USER_INSTALLER_15						355
#define USER_INSTALLER_16						356
#define USER_INSTALLER_17						357
#define USER_INSTALLER_18						358
#define USER_INSTALLER_19						359
#define USER_INSTALLER_20						360
#define USER_INSTALLER_21						361
#define USER_INSTALLER_22						362
#define USER_INSTALLER_23						363
#define USER_INSTALLER_24						364
#define USER_INSTALLER_25						365
#define USER_INSTALLER_26						366
#define USER_INSTALLER_27						367
#define USER_INSTALLER_28						368
#define USER_INSTALLER_29						369
#define USER_INSTALLER_30						370
#define USER_INSTALLER_31						371
#define USER_INSTALLER_32						372
#define USER_T2_LIMIT_H				 			373
#define USER_T2P_LIMIT_H			 			374
#define USER_T2PP_LIMIT_H			 			375
#define USER_T2_LIMIT_CH2_H		 				376
#define USER_T2P_LIMIT_CH2_H	 				377
#define USER_T2PP_LIMIT_CH2_H	 				378
#define USER_T2_LIMIT_PBTL_H	 				379
#define USER_T2P_LIMIT_PBTL_H	 				380
#define USER_T2PP_LIMIT_PBTL_H 					381
#define USER_RESERVE103							382
#define USER_RESERVE104							383
#define USER_RESERVE105							384
#define USER_RESERVE106							385
#define USER_RESERVE107							386
#define USER_RESERVE108							387
#define USER_RESERVE109							388
#define USER_RESERVE110						  389
#define USER_RESERVE111						  390
#define USER_RESERVE112							391
#define USER_RESERVE113							392
#define USER_RESERVE114							393
#define USER_RESERVE115							394
#define USER_RESERVE116							395
#define USER_RESERVE117							396
#define USER_RESERVE118							397
#define USER_RESERVE119							398
#define USER_RESERVE120						  399
#define USER_RESERVE121							400
#define USER_RESERVE122							401
#define USER_RESERVE123							402
#define USER_RESERVE124							403
#define USER_RESERVE125							404
#define USER_RESERVE126							405
#define USER_RESERVE127							406
#define USER_RESERVE128							407
#define USER_RESERVE129							408
#define USER_RESERVE130						  409
#define USER_RESERVE131						  410
#define USER_RESERVE132						  411
#define USER_RESERVE133						  412
#define USER_RESERVE134						  413
#define USER_RESERVE135						  414
#define USER_RESERVE136						  415
#define USER_RESERVE137						  416
#define USER_RESERVE138						  417
#define USER_RESERVE139						  418
#define USER_RESERVE140						  419
#define USER_RESERVE141						  420
#define USER_RESERVE142						  421
#define USER_RESERVE143						  422
#define USER_RESERVE144						  423
#define USER_RESERVE145						  424
#define USER_RESERVE146						  425
#define USER_RESERVE147						  426
#define USER_RESERVE148						  427
#define USER_RESERVE149						  428
#define USER_RESERVE150						  429
#define USER_RESERVE151						  430
#define USER_RESERVE152						  431
#define USER_RESERVE153						  432
#define USER_RESERVE154						  433
#define USER_RESERVE155						  434
#define USER_RESERVE156						  435
#define USER_RESERVE157						  436
#define USER_RESERVE158						  437
#define USER_RESERVE159						  438
#define USER_RESERVE160						  439
#define USER_RESERVE161						  440
#define USER_RESERVE162							441
#define USER_RESERVE163							442
#define USER_RESERVE164							443
#define USER_RESERVE165							444
#define USER_RESERVE166							445
#define USER_RESERVE167							446
#define USER_RESERVE168							447
#define USER_RESERVE169							448
#define USER_RESERVE170						  449
#define USER_RESERVE171							450
#define USER_RESERVE172							451
#define USER_RESERVE173							452
#define USER_RESERVE174							453
#define USER_RESERVE175							454
#define USER_RESERVE176							455
#define USER_RESERVE177							456
#define USER_RESERVE178							457
#define USER_RESERVE179							458
#define USER_RESERVE180						  459
#define USER_RESERVE181						  460
#define USER_RESERVE182						  461
#define USER_RESERVE183						  462
#define USER_RESERVE184						  463
#define USER_RESERVE185						  464
#define USER_RESERVE186						  465
#define USER_RESERVE187						  466
#define USER_RESERVE188						  467
#define USER_RESERVE189						  468
#define USER_RESERVE190						  469
#define USER_RESERVE191						  470
#define USER_RESERVE192						  471
#define USER_RESERVE193						  472
#define USER_RESERVE194						  473
#define USER_RESERVE195						  474
#define USER_RESERVE196						  475
#define USER_RESERVE197						  476
#define USER_RESERVE198						  477
#define USER_RESERVE199						  478
#define USER_RESERVE200						  479
#define USER_RESERVE201						  480
#define USER_RESERVE202						  481
#define USER_RESERVE203						  482
#define USER_RESERVE204						  483
#define USER_RESERVE205						  484
#define USER_RESERVE206						  485
#define USER_RESERVE207						  486
#define USER_RESERVE208						  487
#define USER_RESERVE209						  488
#define USER_RESERVE210						  489
#define USER_RESERVE211						  490
#define USER_RESERVE212						  491
#define USER_RESERVE213						  492
#define USER_RESERVE214						  493
#define USER_RESERVE215						  494
#define USER_RESERVE216						  495
#define USER_RESERVE217						  496
#define USER_RESERVE218						  497
#define USER_RESERVE219						  498
#define USER_RESERVE220						  499
#define USER_RESERVE221						  500
#define USER_RESERVE222						  501
#define USER_RESERVE223						  502
#define USER_RESERVE224						  503
#define USER_RESERVE225						  504
#define USER_RESERVE226						  505
#define USER_RESERVE227						  506
#define USER_RESERVE228						  507
#define USER_VER_YEAR								508
#define USER_VER_MONTH				      509
#define USER_VER_DATE					      510
#define USER_VER_NUMBER				      511
//////////////////////////////////////
#define USER_DATA_COUNT						 		512



///AL003


#define USER_PINK_IN_CH1						6
#define USER_PINK_IN_CH2						7
#define USER_PINK_OUT_CH1						8
#define USER_PINK_OUT_CH2						9
#define USER_PINK_MUTE_CH1					10
#define USER_PINK_MUTE_CH2					11
#define USER_INVERT_CH1							12
#define USER_DELAY_CH1							14
#define USER_DELAY_CH2							15
#define USER_HIPASS_SWITCH_CH1			16
#define USER_HIPASS_FREQ_H_CH1			17
#define USER_HIPASS_FREQ_L_CH1			18
#define USER_HIPASS_SLOPE_CH1				19
#define USER_LOPASS_SWITCH_CH1			20
#define USER_LOPASS_FREQ_H_CH1			21
#define USER_LOPASS_FREQ_L_CH1			22
#define USER_LOPASS_SLOPE_CH1				23
#define USER_POST_GAIN_CH1					24
#define USER_HIPASS_SWITCH_CH2			25
#define USER_HIPASS_FREQ_H_CH2			26
#define USER_HIPASS_FREQ_L_CH2			27
#define USER_HIPASS_SLOPE_CH2				28
#define USER_LOPASS_SWITCH_CH2			29
#define USER_LOPASS_FREQ_H_CH2			30
#define USER_LOPASS_FREQ_L_CH2			31
#define USER_LOPASS_SLOPE_CH2				32
#define USER_POST_GAIN_CH2					33
#define USER_C1_SWITCH_CH1					34
#define USER_C2_SWITCH_CH1					35
#define USER_C3_SWITCH_CH1					36
#define USER_C4_SWITCH_CH1					37
#define USER_C5_SWITCH_CH1					38
#define USER_C6_SWITCH_CH1					39
#define USER_C7_SWITCH_CH1					40
#define USER_C8_SWITCH_CH1					41
#define USER_C9_SWITCH_CH1					42
#define USER_C10_SWITCH_CH1					43
#define USER_C1_GAIN_CH1						44
#define USER_C2_GAIN_CH1						45
#define USER_C3_GAIN_CH1						46
#define USER_C4_GAIN_CH1						47
#define USER_C5_GAIN_CH1						48
#define USER_C6_GAIN_CH1						49
#define USER_C7_GAIN_CH1						50
#define USER_C8_GAIN_CH1						51
#define USER_C9_GAIN_CH1						52
#define USER_C10_GAIN_CH1						53
#define USER_C1_Q_H_CH1							54
#define USER_C2_Q_H_CH1							55
#define USER_C3_Q_H_CH1							56
#define USER_C4_Q_H_CH1							57
#define USER_C5_Q_H_CH1							58
#define USER_C6_Q_H_CH1							59
#define USER_C7_Q_H_CH1							60
#define USER_C8_Q_H_CH1							61
#define USER_C9_Q_H_CH1							62
#define USER_C10_Q_H_CH1						63
#define USER_C1_Q_L_CH1							64
#define USER_C2_Q_L_CH1							65
#define USER_C3_Q_L_CH1							66
#define USER_C4_Q_L_CH1							67
#define USER_C5_Q_L_CH1							68
#define USER_C6_Q_L_CH1							69
#define USER_C7_Q_L_CH1							70
#define USER_C8_Q_L_CH1							71
#define USER_C9_Q_L_CH1							72
#define USER_C10_Q_L_CH1						73
#define USER_C1_FREQ_H_CH1					74
#define USER_C2_FREQ_H_CH1					75
#define USER_C3_FREQ_H_CH1					76
#define USER_C4_FREQ_H_CH1					77
#define USER_C5_FREQ_H_CH1					78
#define USER_C6_FREQ_H_CH1					79
#define USER_C7_FREQ_H_CH1					80
#define USER_C8_FREQ_H_CH1					81
#define USER_C9_FREQ_H_CH1					82
#define USER_C10_FREQ_H_CH1					83
#define USER_C1_FREQ_L_CH1					84
#define USER_C2_FREQ_L_CH1					85
#define USER_C3_FREQ_L_CH1					86
#define USER_C4_FREQ_L_CH1					87
#define USER_C5_FREQ_L_CH1					88
#define USER_C6_FREQ_L_CH1					89
#define USER_C7_FREQ_L_CH1					90
#define USER_C8_FREQ_L_CH1					91
#define USER_C9_FREQ_L_CH1					92
#define USER_C10_FREQ_L_CH1					93
#define USER_C1_SWITCH_CH2					94
#define USER_C2_SWITCH_CH2					95
#define USER_C3_SWITCH_CH2					96
#define USER_C4_SWITCH_CH2					97
#define USER_C5_SWITCH_CH2					98
#define USER_C6_SWITCH_CH2					99
#define USER_C7_SWITCH_CH2					100
#define USER_C8_SWITCH_CH2					101
#define USER_C9_SWITCH_CH2					102
#define USER_C10_SWITCH_CH2					103
#define USER_C1_GAIN_CH2						104
#define USER_C2_GAIN_CH2						105
#define USER_C3_GAIN_CH2						106
#define USER_C4_GAIN_CH2						107
#define USER_C5_GAIN_CH2						108
#define USER_C6_GAIN_CH2						109
#define USER_C7_GAIN_CH2						110
#define USER_C8_GAIN_CH2						111
#define USER_C9_GAIN_CH2						112
#define USER_C10_GAIN_CH2						113
#define USER_C1_Q_H_CH2							114
#define USER_C2_Q_H_CH2							115
#define USER_C3_Q_H_CH2							116
#define USER_C4_Q_H_CH2							117
#define USER_C5_Q_H_CH2							118
#define USER_C6_Q_H_CH2							119
#define USER_C7_Q_H_CH2							120
#define USER_C8_Q_H_CH2							121
#define USER_C9_Q_H_CH2							122
#define USER_C10_Q_H_CH2						123
#define USER_C1_Q_L_CH2							124
#define USER_C2_Q_L_CH2							125
#define USER_C3_Q_L_CH2							126
#define USER_C4_Q_L_CH2							127
#define USER_C5_Q_L_CH2							128
#define USER_C6_Q_L_CH2							129
#define USER_C7_Q_L_CH2							130
#define USER_C8_Q_L_CH2							131
#define USER_C9_Q_L_CH2							132
#define USER_C10_Q_L_CH2						133
#define USER_C1_FREQ_H_CH2					134
#define USER_C2_FREQ_H_CH2					135
#define USER_C3_FREQ_H_CH2					136
#define USER_C4_FREQ_H_CH2					137
#define USER_C5_FREQ_H_CH2					138
#define USER_C6_FREQ_H_CH2					139
#define USER_C7_FREQ_H_CH2					140
#define USER_C8_FREQ_H_CH2					141
#define USER_C9_FREQ_H_CH2					142
#define USER_C10_FREQ_H_CH2					143
#define USER_C1_FREQ_L_CH2				  144
#define USER_C2_FREQ_L_CH2				  145
#define USER_C3_FREQ_L_CH2					146
#define USER_C4_FREQ_L_CH2					147
#define USER_C5_FREQ_L_CH2					148
#define USER_C6_FREQ_L_CH2				  149
#define USER_C7_FREQ_L_CH2				  150
#define USER_C8_FREQ_L_CH2					151
#define USER_C9_FREQ_L_CH2					152
#define USER_T2P_LIMIT_CH1					157
#define USER_T2_LIMIT_CH1					 	161
#define USER_SENSITIVITY_CH1			 	170
#define USER_SENSITIVITY_CH2			 	171
#define USER_THRESHOLD_CH1				 	172
#define USER_KNEE_CH1				 			 	173
#define USER_RATIO_H_CH1					 	174
#define USER_RATIO_L_CH1					 	175
#define USER_ATTACK_H_CH1					 	176
#define USER_ATTACK_L_CH1					 	177
#define USER_RELEASE_H_CH1				 	178
#define USER_RELEASE_L_CH1				 	179
#define USER_HOLD_H_CH1						 	180
#define USER_HOLD_L_CH1						 	181
#define USER_THRESHOLD_CH2				 	182
#define USER_KNEE_CH2				 			 	183
#define USER_RATIO_H_CH2					 	184
#define USER_RATIO_L_CH2					 	185
#define USER_ATTACK_H_CH2					 	186
#define USER_ATTACK_L_CH2					 	187
#define USER_RELEASE_H_CH2				 	188
#define USER_RELEASE_L_CH2				 	189
#define USER_HOLD_H_CH2						 	190
#define USER_HOLD_L_CH2						 	191
#define USER_4O_THRESHOLD_CH1			 	192
#define USER_4O_KNEE_CH1				 	 	193
#define USER_4O_RATIO_H_CH1				 	194
#define USER_4O_RATIO_L_CH1				 	195
#define USER_4O_ATTACK_H_CH1			 	196
#define USER_4O_ATTACK_L_CH1			 	197
#define USER_4O_RELEASE_H_CH1			 	198
#define USER_4O_RELEASE_L_CH1			 	199
#define USER_4O_HOLD_H_CH1				 	200
#define USER_4O_HOLD_L_CH1				 	201
#define USER_4O_THRESHOLD_CH2			 	202
#define USER_4O_KNEE_CH2				 	 	203
#define USER_4O_RATIO_H_CH2				 	204
#define USER_4O_RATIO_L_CH2				 	205
#define USER_4O_ATTACK_H_CH2			 	206
#define USER_4O_ATTACK_L_CH2			 	207
#define USER_4O_RELEASE_H_CH2			 	208
#define USER_4O_RELEASE_L_CH2			 	209
#define USER_4O_HOLD_H_CH2				 	210
#define USER_4O_HOLD_L_CH2				 	211
#define USER_OHM_CH1							 	212
#define USER_OHM_CH2							 	213


///////////////////////////////////////////
// command define
#define COMMAND_IR_KEY					0x00
#define COMMAND_MVOL					0x08
#define COMMAND_SRCVOL					0x09
#define COMMAND_OUTVOL					0x0A
#define COMMAND_SRCTOUT					0x20
#define COMMAND_GEQ						0xA4
#define COMMAND_PEQ						0x4A
#define COMMAND_HLPF					0x22
#define COMMAND_DTIME					0x2B
#define COMMAND_PLIM					0x2C
#define COMMAND_PHSHT					0x29
#define COMMAND_WUM						0x2D
#define COMMAND_NITM					0x2E
#define COMMAND_CHKSS					0x07
// data[0](index) : 0xA0 Switch bridge mode; data[4]: PBTL Mode; 0x00 = disable, 0x01 = enable
//                : 0xF0 Chime; data[1] = count; data[2] = start/stop(1/0);
//				  : 0xF1 Chime Level; data[1] = level(0~50)
//				  : 0xF2 Chime Duration; data[1]~data[2] = duration(s)
//                :xx 0xF1 Speaker wave; data[3]: 0x00 = CH1, 0x01 = CH2; data[4]: 0x00 = stop, 0x01 = start
//                :xx 0xF2 Detect mode; data[4]: 0x00 = stop, 0x01 = start
//				  :	0xD0 LED control; data[1]: LED number; data[2]: LED level: 0~250 , 0x00 = 0ff
//                       LED number = 0x01, 0x02, 0x04.. 0x80, total 8 LED
#define COMMAND_SFACDEF					0x34
#define COMMAND_LINALD					0xE3
#define COMMAND_USER_PRESET				0xEC
	#define PRESET_INDEX_INDEX_DATA		0x00
	#define PRESET_INDEX_TO_DEFAULT		0x01
//data[0](index) : 0x00 installer preset;
//               : 0x01 parameter to default
#define COMMAND_SWRESET					0xF0
#define COMMAND_CFS						0xF8
#define COMMAND_SGFS					0xF9
#define COMMAND_SSEN					0x10
#define COMMAND_RENAME					0xE9
// data[0] : PBTL Mode ; 0x00 = disable, 0x01 = enable, 0xFF = ignore
//#define COMMAND_PBTL					0xC1

//data[0] : USER_TEMP_PROTECT_H
//data[1] : USER_TEMP_PROTECT_L
//data[2] : USER_TEMP_PROTECT_DB
//data[3] : USER_TEMP_PROTECT_START
#define COMMAND_TEMPERATURE				0xC2

//#define COMMAND_CHIME					0xC3
#define COMMAND_LOCATION				0xC4
#endif //_PROTOCOL_H_