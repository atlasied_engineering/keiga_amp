#ifndef _DEVICE_CONTROL_
#define _DEVICE_CONTROL_
#pragma once
#include "stdafx.h"

#define FW_VER_YEAR					21
#define FW_VER_MONTH				9
#define FW_VER_DATE					24
#define FW_VER_NUMBER				1

#define	DEVICE_OUT_QUEUE_MAX		10
#define	DEVICE_OUT_LENGTH			300

typedef boolean device_Write_fn(char* manufacturer, char* name, unsigned char* outputData, const unsigned int sizeBuffer);
void Device_SetWriteCB(device_Write_fn *fn);

typedef void device_Receive_fn(void* mparamr, void* lparamr);

typedef void device_Msg_fn(int index, char* pszText);

typedef struct tagDeviceDataReceiver {
	int lenMessage;			//length of message
	int sizeParam;			//size of buffer of paramer
	unsigned char* buffParam;	//buffer of paramer
}DEVICERECV, *PDEVICERECV;

#ifndef _RXSTATE_
#define _RXSTATE_
typedef enum tagRxState {
	UART_READY, UART_HEAD, UART_SOURCE, UART_TARGET, UART_COMMAND, UART_LENGTH_H, UART_LENGTH_L, UART_DATA_N, UART_CHECKSUM, UART_FINISH
} RXSTATE;
#endif

typedef struct tagDeviceDataParam {
	char			device_name[32] = { 0 };
	char			manufacturer[33] = { 0 };
	unsigned char	id;
	boolean			device;
	int				state;
	int				index_in_listview;
	RXSTATE			rx_state;
	int				rx_data_index;
	unsigned char	rx_data[1024];
	unsigned long	tick_wait_point;
	unsigned long	tick_count;
	unsigned long	tick_resend_wait_point;
	unsigned char	out_queue[DEVICE_OUT_QUEUE_MAX][DEVICE_OUT_LENGTH];
	int				queue_index = 0;
	int				queue_to_send = 0;
	unsigned char	pa[512];
	device_Receive_fn *fn_reciver = NULL;
	device_Msg_fn *fn_msg = NULL;
}DEVICEPARAM, *PDEVICEPARAM;


extern PDEVICEPARAM g_pDevice;

void Set_Send_Flag(PDEVICEPARAM pDevice, unsigned char command, unsigned char head, int length, ...);
DWORD WINAPI Thread_Device(LPVOID lpParam);



typedef struct Parametric_
{
	POINT	point;
	RECT	rect;
	RECT	rect_l_q;
	RECT	rect_r_q;
	boolean moving;
	boolean moving_l_q;
	boolean moving_r_q;
	double	hz;
	double	db;
	double	q;
	boolean enable;
}Parametric;

typedef struct Filter_
{
	POINT	point_high;
	POINT	point_low;
	RECT	rect_high;
	RECT	rect_low;
	boolean moving_high;
	boolean moving_low;
	double	hz_high;
	double	hz_low;
	double	db;
	int		slope_high;
	int		slope_low;
	int		type;
	boolean show_high;
	boolean show_low;
}Filter;

#define TIME_LINK_AMP			5000
#define TIMER_AC3_RECEIVE_DISPLAY	50

#define PAGE_LEVEL				0
#define PAGE_DANTE				1
#define PAGE_FREQ_RESP			2
#define PAGE_MASKING			3
#define PAGE_IP					4
#define PAGE_PRESET				5

#define	VOLUME_MASTER_MAX		50
#define	VOLUME_MASTER_MIN		0
#define VOLUME_CHANNEL_MAX		500
#define VOLUME_CHANNEL_MIN		0
#define SIGNAL_NONE								0
#define SIGNAL_DETECT							1
#define SIGNAL_CLIP								2

#define VOLUME_CHANNEL_H_DEF						VOLUME_CHANNEL_MAX / 256
#define VOLUME_CHANNEL_L_DEF						VOLUME_CHANNEL_MAX % 256

#define VOLUME_MASKING_MAX						33//  -17db
#define VOLUME_MASKING_MIN						0	//  -50db

#define DB_MASKING_MAX				80//+20.0
#define DB_MASKING_MIN				0//-20.0

#define VOLUME_FINDME_MAX					50	//0dB
#define VOLUME_FINDME_MIN					0		//-50dB
#define VOLUME_FINDME_DEF					30	//-20dB

//0x0002BF20 = 180000(ms) = 3mins
//0x000DBBA0 = 900000(ms) = 15mins
//0x001B7740 = 1800000(ms) = 30mins
#define DURATION_FINDME					0xA0
#define DURATION_FINDME_8				0xBB
#define DURATION_FINDME_16				0x0D
#define DURATION_FINDME_24				0x00

#define HIPASS_FERQUENCY_MIN		10
#define HIPASS_FERQUENCY_MAX		(pow(10, 4.375))//16000

#define LOPASS_FERQUENCY_MIN		10
#define LOPASS_FERQUENCY_MAX		(pow(10, 4.375))//16000

#define CEQ_FERQUENCY_MIN			10
#define CEQ_FERQUENCY_MAX			23000//(pow(10, 4.375))//16000

#define CEQ_Q_MIN					1
#define CEQ_Q_MAX					4000

#define FILTER_TYPE_ALLPASS			0
#define FILTER_TYPE_HIPASS			1
#define FILTER_TYPE_LOWPASS			2

#define FILTER_FREQUENCY_MIN		40
#define FILTER_FREQUENCY_MAX		500

#define FREQ_MIN 30
#define FREQ_MAX 16000

INT_PTR CALLBACK LoginProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK DialogProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK LevelProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK DanteProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK DanteFilterViewProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK MaskingProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK IpProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK PresetProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);
extern HWND hDeviceLogin;
extern HWND g_hDeviceControl;
extern HWND g_hTabControl;
extern HWND g_hCurrentTab;
extern boolean g_reflash_ui;
extern char	g_preset_file_name[100];
extern char	g_installer_file_name[100];

#define MYPA_USER_PRESET_STATUS	0
#define USER_PRESET_STATUS_NONE			0
#define USER_PRESET_STATUS_UPLOADING	1
#define USER_PRESET_STATUS_FINISHED		2
#define USER_PRESET_STATUS_TIMEOUT		3
#define USER_PRESET_STATUS_SAVED		4
#define USER_PRESET_STATUS_SAVE_ERR		5
#define USER_PRESET_STATUS_LOAD_ERR		6
#define USER_PRESET_STATUS_NO_DEVICE	7
#define USER_PRESET_STATUS_CHECKSUM_ERR	8

#define MYPA_LINALD_STATUS					1
#define LINALD_STATUS_NONE			0
#define LINALD_STATUS_UPLOADING		1
#define LINALD_STATUS_FINISHED		2
#define LINALD_STATUS_TIMEOUT		3
#define LINALD_STATUS_SAVED			4
#define LINALD_STATUS_SAVE_ERR		5
#define LINALD_STATUS_LOAD_ERR		6
#define LINALD_STATUS_NO_DEVICE		7
#define LINALD_STATUS_CHECKSUM_ERR	8

#define MYPA_LOAD_DEFAULT_STATUS		2
#define LOAD_DEFAULT_STATUS_NONE		0
#define LOAD_DEFAULT_STATUS_UPLOADING	1
#define LOAD_DEFAULT_STATUS_FINISHED	2
#define LOAD_DEFAULT_STATUS_TIMEOUT		3

#define MYPA_USER_RESET_STATUS	3
#define USER_RESET_STATUS_NONE			0
#define USER_RESET_STATUS_UPLOADING	1
#define USER_RESET_STATUS_FINISHED		2
#define USER_RESET_STATUS_TIMEOUT		3

#define MYPA_PRESET_ID_0				3
#define MYPA_PRESET_ID_1				4
#define MYPA_RECIVE_AC3					5
#define RECIVE_AC3_COLOR_0		0
#define RECIVE_AC3_COLOR_1		1
#define MYPA_6					6
#define MYPA_7					7
#define MYPA_8					8
#define MYPA_9					9
extern unsigned char g_mypa[10];

#endif