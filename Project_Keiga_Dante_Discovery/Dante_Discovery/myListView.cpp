#include "stdafx.h"

#include "myListView.h"


HWND CreateListView(HINSTANCE hInst, HWND hwndParent, int x, int y, int width, int height)
{
	INITCOMMONCONTROLSEX icex;           // Structure for control initialization.
	icex.dwICC = ICC_LISTVIEW_CLASSES;
	InitCommonControlsEx(&icex);

	RECT rcClient;                       // The parent window's client area.

	GetClientRect(hwndParent, &rcClient);

	// Create the list-view window in report view with label editing enabled.
	HWND hWndListView = CreateWindow(WC_LISTVIEW,
		"",
		WS_CHILD | LVS_REPORT | LVS_EDITLABELS | LVS_SINGLESEL,
		x, y,
		width,
		height,
		hwndParent,
		NULL,
		hInst,
		NULL);

	//SendMessage(hWndListView, LVM_SETEXTENDEDLISTVIEWSTYLE, LVS_EX_FULLROWSELECT, LVS_EX_FULLROWSELECT);

	return (hWndListView);

}

void AddListViewItem(HWND hlistview, int raw, int colume, char* string, BOOLEAN insert)
{
	LV_ITEM LvItem;
	memset(&LvItem, 0, sizeof(LvItem)); // Zero struct's Members
										//  Setting properties Of members:
	LvItem.mask = LVIF_TEXT;   // Text Style
	LvItem.cchTextMax = 256; // Max size of test
	LvItem.iItem = raw;
	LvItem.iSubItem = colume;
	LvItem.pszText = string;
	if (insert)
		SendMessage(hlistview, LVM_INSERTITEM, 0, (LPARAM)&LvItem);
	else
		SendMessage(hlistview, LVM_SETITEM, 0, (LPARAM)&LvItem);

	SendMessage(hlistview, LVM_SETCOLUMNWIDTH, colume, (LPARAM)150);
}

LRESULT ProcessCustomDraw(LPARAM lParam, HWND hwnd)
{
	LPNMLVCUSTOMDRAW lplvcd = (LPNMLVCUSTOMDRAW)lParam;
	int iSelect;

	switch (lplvcd->nmcd.dwDrawStage)
	{
		
	case CDDS_PREPAINT: //Before the paint cycle begins
						//request notifications for individual listview items
		return CDRF_NOTIFYITEMDRAW;

	case CDDS_ITEMPREPAINT: //Before an item is drawn

		if (((int)lplvcd->nmcd.dwItemSpec % 2) == 0)
		{
			//customize item appearance
			lplvcd->clrTextBk = RGB(245, 245, 245);
			return  CDRF_NOTIFYSUBITEMDRAW;
		}
		else {
			lplvcd->clrTextBk = RGB(255, 255, 255);

			return CDRF_NOTIFYSUBITEMDRAW;
		}
		break;
		
		//Before a subitem is drawn
	case CDDS_SUBITEM | CDDS_ITEMPREPAINT: {
		if (lplvcd->iSubItem == COLUMN_STATE) {
			char *text = (char*)malloc(sizeof(char) * 256);
			ListView_GetItemText(hwnd, lplvcd->nmcd.dwItemSpec, lplvcd->iSubItem, text, 256);
			if (strcmp("Online", text) == 0)
				lplvcd->clrText = RGB(0, 127, 14);
			else if (strcmp("Offline", text) == 0)
				lplvcd->clrText = RGB(200, 0, 0);
			else if(strcmp("Find Me", text) == 0)
				lplvcd->clrText = RGB(0, 148, 255);
			free(text);
		}
		else {
			lplvcd->clrText = RGB(0, 0, 0);
		}
	}
	return CDRF_DODEFAULT;
	}
}
