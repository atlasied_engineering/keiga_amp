#include "stdafx.h"
#include <math.h>
#include "device_control.h"
#include "AL003M.h"
#include "ACC_Command.h"

#define CH1_X						153
#define CH1_Y						300
#define CH2_X						225
#define CH2_Y						300

#define CH_MODE_X					245
#define CH_MODE_Y					130
#define CIRCLE_R					15

#define TIMER_SEND_CMD			300

#define DELAY_CMD_TYPE_VOL_MASTER	0
#define DELAY_CMD_TYPE_VOL_CH1		1
#define DELAY_CMD_TYPE_VOL_CH2		2

static struct tagDelaySendCmd {
	uint16_t type;
	int argc;
	int argv[20];
}mDelaySend;

void Init_Level_UI(HWND hwnd);
void Set_ChSlider_Bar(HWND hwnd, unsigned int id, int progress);
void Set_ChSlider_Text(HWND hwnd, unsigned int id, unsigned int vol);
void Change_Level_Edit_Data(HWND hwnd);
static DATAOUTVOL data_outvol;
static DATAMVOL data_mvol;
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         
static HWND hParent;
static HWND hEditCH1, hEditCH2, hEditMaster,hEditPinkCh1, hEditPinkCh2;
static WNDPROC oldEditCH1, oldEditCH2, oldEditMaster, oldEditPinkCh1, oldEditPinkCh2;
static HWND hfocus=0;

LRESULT CALLBACK LevelEditProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_CHAR:
	{
		// Make sure we only allow specific characters
		if (!((wParam >= '0' && wParam <= '9')
			|| wParam == '.'
			|| wParam == '-'
			|| wParam == VK_RETURN
			|| wParam == VK_DELETE
			|| wParam == VK_BACK))
		{
			return 0;
		}
	}
	break;
	case WM_LBUTTONUP:
		SendMessage(hwnd, EM_SETSEL, 0, -1);
		break;

	case WM_KILLFOCUS:
		if (TabCtrl_GetCurSel(g_hTabControl) == PAGE_LEVEL)
			Change_Level_Edit_Data(hwnd);
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_RETURN:
		case VK_TAB:
			Change_Level_Edit_Data(hwnd);

			if (wParam == VK_RETURN)
				return 0;
		}
		break;

	}
	if (hwnd == hEditCH1)
		return CallWindowProc(oldEditCH1, hwnd, uMsg, wParam, lParam);
	else if (hwnd == hEditCH2)
		return CallWindowProc(oldEditCH2, hwnd, uMsg, wParam, lParam);
	else if (hwnd == hEditMaster)
		return CallWindowProc(oldEditMaster, hwnd, uMsg, wParam, lParam);
	else if (hwnd == hEditPinkCh1)
		return CallWindowProc(oldEditPinkCh1, hwnd, uMsg, wParam, lParam);
	else if (hwnd == hEditPinkCh2)
		return CallWindowProc(oldEditPinkCh2, hwnd, uMsg, wParam, lParam);
	return 0;
}


INT_PTR CALLBACK LevelProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_INITDIALOG:
		Init_Level_UI(hDlg);
		hParent = hDlg;

		hEditCH1 = GetDlgItem(hDlg, IDC_EDIT_CH1);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_CH1), (UINT)EM_SETLIMITTEXT, (WPARAM)5, (LPARAM)0);
		oldEditCH1 = (WNDPROC)SetWindowLongPtr(hEditCH1, GWLP_WNDPROC, (LONG_PTR)LevelEditProc);

		hEditCH2 = GetDlgItem(hDlg, IDC_EDIT_CH2);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_CH2), (UINT)EM_SETLIMITTEXT, (WPARAM)5, (LPARAM)0);
		oldEditCH2 = (WNDPROC)SetWindowLongPtr(hEditCH2, GWLP_WNDPROC, (LONG_PTR)LevelEditProc);

		hEditMaster = GetDlgItem(hDlg, IDC_EDIT_MASTER);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_MASTER), (UINT)EM_SETLIMITTEXT, (WPARAM)3, (LPARAM)0);
		oldEditMaster = (WNDPROC)SetWindowLongPtr(hEditMaster, GWLP_WNDPROC, (LONG_PTR)LevelEditProc);

		return TRUE;;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDC_CHECK_MUTE:
			if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_MUTE), BM_GETCHECK, 0, 0) == BST_CHECKED)
				g_pDevice->pa[AL003M_MUTE] = 1;
			else
				g_pDevice->pa[AL003M_MUTE] = 0;
			data_mvol.Master.Mute = g_pDevice->pa[AL003M_MUTE];
			data_mvol.Master.Volume = 0x7F;
			data_mvol.Limit = 0xFF;
			Set_Send_Flag(g_pDevice, ACC_CMD_MVOL, ACC_HEADER_WRITE, 2, data_mvol.Master, data_mvol.Limit);
			break;
		case IDC_CHECK_MUTE_CH1:
			if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_MUTE_CH1), BM_GETCHECK, 0, 0) == BST_CHECKED)
				g_pDevice->pa[AL003M_MUTE_CH1] = 1;
			else
				g_pDevice->pa[AL003M_MUTE_CH1] = 0;
			Set_Send_Flag(g_pDevice, ACC_CMD_OUTVOL, ACC_HEADER_WRITE, 6, 0x01, 0x01, 0xFF, 0xFF, 0x7F | (g_pDevice->pa[AL003M_MUTE_CH1]<<7), 0x7F | (g_pDevice->pa[AL003M_MUTE_CH2] << 7));
			break;
		case IDC_CHECK_MUTE_CH2:
			if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_MUTE_CH2), BM_GETCHECK, 0, 0) == BST_CHECKED)
				g_pDevice->pa[AL003M_MUTE_CH2] = 1;
			else
				g_pDevice->pa[AL003M_MUTE_CH2] = 0;
			Set_Send_Flag(g_pDevice, ACC_CMD_OUTVOL, ACC_HEADER_WRITE, 6, 0x01, 0x01, 0xFF, 0xFF, 0x7F | (g_pDevice->pa[AL003M_MUTE_CH1] << 7), 0x7F | (g_pDevice->pa[AL003M_MUTE_CH2] << 7));
			break;
		case IDC_CHECK_INVERT_CH2:
			if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_INVERT_CH2), BM_GETCHECK, 0, 0) == BST_CHECKED)
				g_pDevice->pa[AL003M_INVERT_CH2] = 1;
			else
				g_pDevice->pa[AL003M_INVERT_CH2] = 0;
			Set_Send_Flag(g_pDevice, ACC_CMD_PHSHF, ACC_HEADER_WRITE, 4, 0x01, 0x01, 0x02, g_pDevice->pa[AL003M_INVERT_CH2]);
			break;
		}
		break;
	case WM_VSCROLL:

		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASTER) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			int value = SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASTER), TBM_GETPOS, 0, 0);
			g_pDevice->pa[AL003M_VOL_MASTER] = abs(value - VOLUME_MASTER_MAX) + VOLUME_MASTER_MIN;
			Set_ChSlider_Text(hDlg, IDC_EDIT_MASTER, g_pDevice->pa[AL003M_VOL_MASTER]);
			KillTimer(g_hDeviceControl, TIME_LINK_AMP);
			mDelaySend.type = DELAY_CMD_TYPE_VOL_MASTER;
			KillTimer(hDlg, TIMER_SEND_CMD);
			SetTimer(hDlg, TIMER_SEND_CMD, TIMER_SEND_CMD, NULL);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_CH1)) {
			int value = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_CH1), TBM_GETPOS, 0, 0) - VOLUME_CHANNEL_MAX) + VOLUME_CHANNEL_MIN;
			g_pDevice->pa[AL003M_VOL_CH1_H] = value / 256;
			g_pDevice->pa[AL003M_VOL_CH1] = value % 256;
			Set_ChSlider_Text(hDlg, IDC_EDIT_CH1, g_pDevice->pa[AL003M_VOL_CH1_H] * 256 + g_pDevice->pa[AL003M_VOL_CH1]);
			KillTimer(g_hDeviceControl, TIME_LINK_AMP);
			mDelaySend.type = DELAY_CMD_TYPE_VOL_CH1;
			KillTimer(hDlg, TIMER_SEND_CMD);
			SetTimer(hDlg, TIMER_SEND_CMD, TIMER_SEND_CMD, NULL);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_CH2)) {
			int value = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_CH2), TBM_GETPOS, 0, 0) - VOLUME_CHANNEL_MAX) + VOLUME_CHANNEL_MIN;
			g_pDevice->pa[AL003M_VOL_CH2_H] = value / 256;
			g_pDevice->pa[AL003M_VOL_CH2] = value % 256;
			Set_ChSlider_Text(hDlg, IDC_EDIT_CH2, g_pDevice->pa[AL003M_VOL_CH2_H] * 256 + g_pDevice->pa[AL003M_VOL_CH2]);
			KillTimer(g_hDeviceControl, TIME_LINK_AMP);
			mDelaySend.type = DELAY_CMD_TYPE_VOL_CH2;
			KillTimer(hDlg, TIMER_SEND_CMD);		
			SetTimer(hDlg, TIMER_SEND_CMD, TIMER_SEND_CMD, NULL);
		}
		break;
	case WM_TIMER: {
		switch (wParam) {
		case TIMER_SEND_CMD: {
			switch (mDelaySend.type) {
			case DELAY_CMD_TYPE_VOL_MASTER: {
				data_mvol.Master.Mute = g_pDevice->pa[AL003M_MUTE];
				data_mvol.Master.Volume = g_pDevice->pa[AL003M_VOL_MASTER];
				data_mvol.Limit = 0xFF;
				Set_Send_Flag(g_pDevice, ACC_CMD_MVOL, ACC_HEADER_WRITE, 2, data_mvol.Master, data_mvol.Limit);
				break; }
			case DELAY_CMD_TYPE_VOL_CH1: {
				Set_Send_Flag(g_pDevice, ACC_CMD_OUTVOL, ACC_HEADER_WRITE, 6, 0x01, 0x01, g_pDevice->pa[AL003M_VOL_CH1], 0xFF, g_pDevice->pa[AL003M_VOL_CH1_H] | (g_pDevice->pa[AL003M_MUTE_CH1] << 7), 0x7F | (g_pDevice->pa[AL003M_MUTE_CH2] << 7));
				break; }
			case DELAY_CMD_TYPE_VOL_CH2: {
				Set_Send_Flag(g_pDevice, ACC_CMD_OUTVOL, ACC_HEADER_WRITE, 6, 0x01, 0x01, 0xFF, g_pDevice->pa[AL003M_VOL_CH2], 0x7F | (g_pDevice->pa[AL003M_MUTE_CH1] << 7), g_pDevice->pa[AL003M_VOL_CH2_H] | (g_pDevice->pa[AL003M_MUTE_CH2] << 7));
				break; }

			}
			KillTimer(hDlg, TIMER_SEND_CMD);
			SetTimer(g_hDeviceControl, TIME_LINK_AMP, TIME_LINK_AMP, NULL);
			break;
		}
		}
		break;
	}
	case WM_PAINT: {
		if (g_reflash_ui) {
			Init_Level_UI(hDlg);
			g_reflash_ui = FALSE;
		}

		PAINTSTRUCT 	ps;
		HDC 			hdc;

		hdc = BeginPaint(hDlg, &ps);
		HPEN hPen = CreatePen(PS_SOLID, 1, RGB(10, 10, 10));
		SelectObject(hdc, hPen);

		RECT r;
		POINT pt;

		SelectObject(hdc, GetStockObject(DC_BRUSH));
		//Dante
		if (g_pDevice->pa[AL003M_SIGNAL_CH1] == SIGNAL_NONE) {
			SetDCBrushColor(hdc, RGB(180, 180, 180));
		}
		else if (g_pDevice->pa[AL003M_SIGNAL_CH1] == SIGNAL_DETECT) {
			SetDCBrushColor(hdc, RGB(0, 180, 0));

		}
		else if (g_pDevice->pa[AL003M_SIGNAL_CH1] == (SIGNAL_DETECT | SIGNAL_CLIP)) {
			SetDCBrushColor(hdc, RGB(180, 0, 0));
		}
		GetWindowRect(GetDlgItem(hDlg, IDC_STATIC_DT1), &r);
		pt.x = r.left;
		pt.y = r.top;
		ScreenToClient(hDlg, &pt);
		Ellipse(hdc, pt.x, pt.y, pt.x + CIRCLE_R, pt.y + CIRCLE_R);

		if (g_pDevice->pa[AL003M_SIGNAL_CH2] == SIGNAL_NONE) {
			SetDCBrushColor(hdc, RGB(180, 180, 180));
		}
		else if (g_pDevice->pa[AL003M_SIGNAL_CH2] == SIGNAL_DETECT) {
			SetDCBrushColor(hdc, RGB(0, 180, 0));

		}
		else if (g_pDevice->pa[AL003M_SIGNAL_CH2] == (SIGNAL_DETECT | SIGNAL_CLIP)) {
			SetDCBrushColor(hdc, RGB(180, 0, 0));
		}
		//1ch mode, CH2 always gray
		if (g_pDevice->pa[AL003M_PBTL_MODE])
			SetDCBrushColor(hdc, RGB(180, 180, 180));
		GetWindowRect(GetDlgItem(hDlg, IDC_STATIC_DT2), &r);
		pt.x = r.left;
		pt.y = r.top;
		ScreenToClient(hDlg, &pt);
		Ellipse(hdc, pt.x, pt.y, pt.x + CIRCLE_R, pt.y + CIRCLE_R);

		//Masking
		if (g_pDevice->pa[AL003M_MUTE_MASKING] == 0 && g_pDevice->pa[AL003M_SIGNAL_CH1] != SIGNAL_CLIP) {
			SetDCBrushColor(hdc, RGB(0, 180, 0));

		}
		else if (g_pDevice->pa[AL003M_MUTE_MASKING] == 0 && g_pDevice->pa[AL003M_SIGNAL_CH1] == SIGNAL_CLIP) {
			SetDCBrushColor(hdc, RGB(180, 0, 0));
		}
		else {
			SetDCBrushColor(hdc, RGB(180, 180, 180));
		}
		GetWindowRect(GetDlgItem(hDlg, IDC_STATIC_DT3), &r);
		pt.x = r.left;
		pt.y = r.top;
		ScreenToClient(hDlg, &pt);
		Ellipse(hdc, pt.x, pt.y, pt.x + CIRCLE_R, pt.y + CIRCLE_R);

		if (g_pDevice->pa[AL003M_MUTE_MASKING] == 0 && g_pDevice->pa[AL003M_SIGNAL_CH2] != SIGNAL_CLIP) {
			SetDCBrushColor(hdc, RGB(0, 180, 0));

		}
		else if (g_pDevice->pa[AL003M_MUTE_MASKING] == 0 && g_pDevice->pa[AL003M_SIGNAL_CH2] == SIGNAL_CLIP) {
			SetDCBrushColor(hdc, RGB(180, 0, 0));
		}
		else {
			SetDCBrushColor(hdc, RGB(180, 180, 180));
		}

		//1ch mode, CH2 always gray
		if (g_pDevice->pa[AL003M_PBTL_MODE])
			SetDCBrushColor(hdc, RGB(180, 180, 180));

		GetWindowRect(GetDlgItem(hDlg, IDC_STATIC_DT4), &r);
		pt.x = r.left;
		pt.y = r.top;
		ScreenToClient(hDlg, &pt);
		Ellipse(hdc, pt.x, pt.y, pt.x + CIRCLE_R, pt.y + CIRCLE_R);


		if (g_pDevice->pa[AL003M_PBTL_MODE] == 1) {
			SetDCBrushColor(hdc, RGB(255, 216, 0));
		}
		else {
			SetDCBrushColor(hdc, RGB(180, 180, 180));
		}
		GetWindowRect(GetDlgItem(hDlg, IDC_STATIC_DT5), &r);
		pt.x = r.left;
		pt.y = r.top;
		ScreenToClient(hDlg, &pt);
		Ellipse(hdc, pt.x, pt.y, pt.x + CIRCLE_R, pt.y + CIRCLE_R);

		DeleteObject(hPen);
		EndPaint(hDlg, &ps);
		/*
		PAINTSTRUCT 	ps;
		HDC 			hdc;

		hdc = BeginPaint(hDlg, &ps);
		HPEN hPen = CreatePen(PS_SOLID, 1, RGB(10, 10, 10));
		SelectObject(hdc, hPen);

		SelectObject(hdc, GetStockObject(DC_BRUSH));
		if (g_pDevice->pa[AL003M_SIGNAL_CH1] == SIGNAL_DETECT) {
			SetDCBrushColor(hdc, RGB(0, 180, 0));

		}
		else if (g_pDevice->pa[AL003M_SIGNAL_CH1] == SIGNAL_CLIP) {
			SetDCBrushColor(hdc, RGB(180, 0, 0));
		}
		else {
			SetDCBrushColor(hdc, RGB(180, 180, 180));
		}
		Ellipse(hdc, CH1_X, CH1_Y, CH1_X + CIRCLE_R, CH1_Y + CIRCLE_R);

		if (g_pDevice->pa[AL003M_SIGNAL_CH2] == SIGNAL_DETECT) {
			SetDCBrushColor(hdc, RGB(0, 180, 0));

		}
		else if (g_pDevice->pa[AL003M_SIGNAL_CH2] == SIGNAL_CLIP) {
			SetDCBrushColor(hdc, RGB(180, 0, 0));
		}
		else {
			SetDCBrushColor(hdc, RGB(180, 180, 180));
		}
		Ellipse(hdc, CH2_X, CH2_Y, CH2_X + CIRCLE_R, CH2_Y + CIRCLE_R);

		if (g_pDevice->pa[AL003M_PBTL_MODE] == 1) {
			SetDCBrushColor(hdc, RGB(0, 180, 0));
		}
		else {
			SetDCBrushColor(hdc, RGB(180, 180, 180));
		}
		Ellipse(hdc, CH_MODE_X, CH_MODE_Y, CH_MODE_X + CIRCLE_R, CH_MODE_Y + CIRCLE_R);

		DeleteObject(hPen);
		EndPaint(hDlg, &ps);
		*/
	}
		break;
	case WM_CLOSE:
		return TRUE;

	case WM_DESTROY:
		return TRUE;
	}
	return FALSE;
}

void Init_Level_UI(HWND hDlg)
{
	if (g_pDevice->pa[AL003M_MUTE] == 1)
		SendMessage(GetDlgItem(hDlg, IDC_CHECK_MUTE), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
	else
		SendMessage(GetDlgItem(hDlg, IDC_CHECK_MUTE), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);

	if (g_pDevice->pa[AL003M_MUTE_CH1] == 1)
		SendMessage(GetDlgItem(hDlg, IDC_CHECK_MUTE_CH1), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
	else
		SendMessage(GetDlgItem(hDlg, IDC_CHECK_MUTE_CH1), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);

	if (g_pDevice->pa[AL003M_MUTE_CH2] == 1)
		SendMessage(GetDlgItem(hDlg, IDC_CHECK_MUTE_CH2), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
	else
		SendMessage(GetDlgItem(hDlg, IDC_CHECK_MUTE_CH2), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);

	if (g_pDevice->pa[AL003M_INVERT_CH2] == 1)
		SendMessage(GetDlgItem(hDlg, IDC_CHECK_INVERT_CH2), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
	else
		SendMessage(GetDlgItem(hDlg, IDC_CHECK_INVERT_CH2), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASTER), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(VOLUME_MASTER_MIN, VOLUME_MASTER_MAX));// pa[DATA_VOLUME_LIMIT]));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_CH1), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(VOLUME_CHANNEL_MIN, VOLUME_CHANNEL_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_CH2), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(VOLUME_CHANNEL_MIN, VOLUME_CHANNEL_MAX));

	Set_ChSlider_Bar(hDlg, IDC_SLIDER_MASTER, g_pDevice->pa[AL003M_VOL_MASTER]);
	Set_ChSlider_Text(hDlg, IDC_EDIT_MASTER, g_pDevice->pa[AL003M_VOL_MASTER]);

	Set_ChSlider_Bar(hDlg, IDC_SLIDER_CH1, g_pDevice->pa[AL003M_VOL_CH1_H] * 256 + g_pDevice->pa[AL003M_VOL_CH1]);
	Set_ChSlider_Text(hDlg, IDC_EDIT_CH1, g_pDevice->pa[AL003M_VOL_CH1_H] * 256 + g_pDevice->pa[AL003M_VOL_CH1]);
	Set_ChSlider_Bar(hDlg, IDC_SLIDER_CH2, g_pDevice->pa[AL003M_VOL_CH2_H] * 256 + g_pDevice->pa[AL003M_VOL_CH2]);
	Set_ChSlider_Text(hDlg, IDC_EDIT_CH2, g_pDevice->pa[AL003M_VOL_CH2_H] * 256 + g_pDevice->pa[AL003M_VOL_CH2]);

	SendMessage(GetDlgItem(hDlg, IDC_RADIO_GPIO_OPEN), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_RADIO_GPIO_CLOSED), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	if (!g_pDevice->pa[AL003M_GPIO_STATUS]) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO_GPIO_OPEN), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
	}
	else {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO_GPIO_CLOSED), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
	}

	//Mono to disable CH2 controls
	if (g_pDevice->pa[AL003M_PBTL_MODE]) {
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_CH2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_CH2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_MUTE_CH2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_INVERT_CH2), FALSE);
	}else {
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_CH2), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_CH2), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_MUTE_CH2), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_INVERT_CH2), TRUE);
	}

	data_outvol.OutputPage._.Out_Page0 = 1;
	data_outvol.Output._.Output1 = 1;

}

void Set_ChSlider_Bar(HWND hwnd, unsigned int id, int progress)
{
	int position;

	switch (id)
	{
	case IDC_SLIDER_MASTER:
		position = abs(progress - VOLUME_MASTER_MAX) + VOLUME_MASTER_MIN;
		SendMessage(GetDlgItem(hwnd, IDC_SLIDER_MASTER), TBM_SETPOS, (WPARAM)1, position);
		break;
	case IDC_SLIDER_CH1:
	case IDC_SLIDER_CH2:
		position = abs(progress - VOLUME_CHANNEL_MAX);
		SendMessage(GetDlgItem(hwnd, id), TBM_SETPOS, (WPARAM)1, position);
		break;
	}
}

void Set_ChSlider_Text(HWND hwnd, unsigned int id, unsigned int vol)
{
	int value;
	char *text = (char*)malloc(10 * sizeof(char));
	memset(text, 0, 10 * sizeof(char));

	switch (id)
	{
	case IDC_EDIT_MASTER:
		
		value = VOLUME_MASTER_MAX - vol;
		if (value > 0) {
			sprintf(text, "%s%d","-", value);
		}
		else {
			sprintf(text,"%d", value);
		}

		SendMessage(GetDlgItem(hwnd, id), WM_SETTEXT, (WPARAM)0, (LPARAM)text);

		break;
	case IDC_EDIT_CH1:
	case IDC_EDIT_CH2:
		value = VOLUME_CHANNEL_MAX - vol;
		if (value > 0) {
			sprintf(text, "%s%.1f", "-", (double)value/10);
		}
		else {
			sprintf(text, "%.1f", (double)value / 10);
		}
		SendMessage(GetDlgItem(hwnd, id), WM_SETTEXT, (WPARAM)0, (LPARAM)text);
		break;
	}
	free(text);
}

void Change_Level_Edit_Data(HWND hwnd)
{
	char number[6];
	if (hwnd == hEditCH1) {
		GetDlgItemTextA(hParent, IDC_EDIT_CH1, number, 6);
		double i = atof(number);
		if (i < -50) {
			i = -50;
			g_pDevice->pa[AL003M_VOL_CH1] = 0;
			g_pDevice->pa[AL003M_VOL_CH1_H] = 0;
		}
		else if (i > 0) {
			i = 0;
			g_pDevice->pa[AL003M_VOL_CH1] = VOLUME_CHANNEL_MAX % 256;
			g_pDevice->pa[AL003M_VOL_CH1_H] = VOLUME_CHANNEL_MAX / 256;
		}
		else {
			g_pDevice->pa[AL003M_VOL_CH1] = ((int)(i * 10) + VOLUME_CHANNEL_MAX) % 256;
			g_pDevice->pa[AL003M_VOL_CH1_H] = (i * 10 + VOLUME_CHANNEL_MAX) / 256;
		}
		Set_ChSlider_Text(hParent, IDC_EDIT_CH1, g_pDevice->pa[AL003M_VOL_CH1_H] * 256 + g_pDevice->pa[AL003M_VOL_CH1]);
		Set_ChSlider_Bar(hParent, IDC_SLIDER_CH1, g_pDevice->pa[AL003M_VOL_CH1_H] * 256 + g_pDevice->pa[AL003M_VOL_CH1]);
		Set_Send_Flag(g_pDevice, ACC_CMD_OUTVOL, ACC_HEADER_WRITE, 6, 0x01, 0x01, g_pDevice->pa[AL003M_VOL_CH1], 0xFF, g_pDevice->pa[AL003M_VOL_CH1_H] | (g_pDevice->pa[AL003M_MUTE_CH1] << 7), 0x7F | (g_pDevice->pa[AL003M_MUTE_CH2] << 7));
	}
	else if (hwnd == hEditCH2) {
		GetDlgItemTextA(hParent, IDC_EDIT_CH2, number, 6);
		double i = atof(number);
		if (i < -50) {
			i = -50;
			g_pDevice->pa[AL003M_VOL_CH2] = 0;
			g_pDevice->pa[AL003M_VOL_CH2_H] = 0;
		}
		else if (i > 0) {
			i = 0;
			g_pDevice->pa[AL003M_VOL_CH2] = VOLUME_CHANNEL_MAX % 256;
			g_pDevice->pa[AL003M_VOL_CH2_H] = VOLUME_CHANNEL_MAX / 256;
		}
		else {
			g_pDevice->pa[AL003M_VOL_CH2] = ((int)(i * 10) + VOLUME_CHANNEL_MAX) % 256;
			g_pDevice->pa[AL003M_VOL_CH2_H] = (i * 10 + VOLUME_CHANNEL_MAX) / 256;
		}
		Set_ChSlider_Text(hParent, IDC_EDIT_CH2, g_pDevice->pa[AL003M_VOL_CH2_H] * 256 + g_pDevice->pa[AL003M_VOL_CH2]);
		Set_ChSlider_Bar(hParent, IDC_SLIDER_CH2, g_pDevice->pa[AL003M_VOL_CH2_H] * 256 + g_pDevice->pa[AL003M_VOL_CH2]);
		Set_Send_Flag(g_pDevice, ACC_CMD_OUTVOL, ACC_HEADER_WRITE, 6, 0x01, 0x01, 0xFF, g_pDevice->pa[AL003M_VOL_CH2], 0x7F | (g_pDevice->pa[AL003M_MUTE_CH1] << 7), g_pDevice->pa[AL003M_VOL_CH2_H] | (g_pDevice->pa[AL003M_MUTE_CH2] << 7));
	}
	else if (hwnd == hEditMaster) {
		GetDlgItemTextA(hParent, IDC_EDIT_MASTER, number, 4);
		int i = atoi(number);
		if (i < -50) {
			i = -50;
			g_pDevice->pa[AL003M_VOL_MASTER] = 0;
		}
		else if (i > 0) {
			i = 0;
			g_pDevice->pa[AL003M_VOL_MASTER] = 50;
		}
		else {
			g_pDevice->pa[AL003M_VOL_MASTER] = i + VOLUME_MASTER_MAX;
		}
		Set_ChSlider_Text(hParent, IDC_EDIT_MASTER, g_pDevice->pa[AL003M_VOL_MASTER]);
		Set_ChSlider_Bar(hParent, IDC_SLIDER_MASTER, g_pDevice->pa[AL003M_VOL_MASTER]);
		data_mvol.Master.Mute = g_pDevice->pa[AL003M_MUTE];
		data_mvol.Master.Volume = g_pDevice->pa[AL003M_VOL_MASTER];
		data_mvol.Limit = 0xFF;
		Set_Send_Flag(g_pDevice, ACC_CMD_MVOL, ACC_HEADER_WRITE, 2, data_mvol.Master, data_mvol.Limit);
	}
}
