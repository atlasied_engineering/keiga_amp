#include "stdafx.h"
#include "utils.h"

#define A	0.707


boolean cmpArray(unsigned char* a1, unsigned char* a2, int len);
int ascii_to_hex(char c);

boolean cmpArray(unsigned char* a1, unsigned char* a2, int len)
{
	for (int i = 0; i < len; i++) {
		if (a1[i] != a2[i])
			return FALSE;
	}
	return TRUE;
}

int ascii_to_hex(char c)
{
	int num = (int)c;
	if (num < 58 && num > 47)
	{
		return num - 48;
	}
	if (num < 103 && num > 96)
	{
		return num - 87;
	}
	if (num < 71 && num > 64)
	{
		return num - 55;
	}
	return num;
}


unsigned char str_to_byte(char* c)
{
	return ascii_to_hex(c[0]) * 16 + ascii_to_hex(c[1]);
}

unsigned char str_to_dec(char* c)
{
	return ascii_to_hex(c[0]) * 10 + ascii_to_hex(c[1]);
}


int dB_to_Y(double dB, double max_db, double min_db, RECT rectFrame)
{
	//dB : 24 ~ -30
	//Y :�@80 ~ 323

	double d = max_db - dB;// 54 - (dB + 30);
	double dY = (double)(rectFrame.bottom - rectFrame.top) / (double)(max_db - min_db);
	int Y = (int)(rectFrame.top + dY*d);
	return Y;
}

double Y_to_dB(int y, double max_db, double min_db, RECT rectFrame)
{
	//dB : 24 ~ -30
	//Y :�@80 ~ 323
	int y_height = rectFrame.bottom - y;
	double ddB = (double)(max_db - min_db) / (double)(rectFrame.bottom - rectFrame.top);// 54 - (dB + 30);
	double dB = min_db + (double)(ddB*y_height);
	return dB;
}

int Hz_to_X(double Hz, double max_hz, double min_hz, RECT rectFrame) {
	//Hz : 10 ~ 16002
	//X : 40 ~ 580

	double dL = log10(max_hz) - log10(min_hz);
	double d = log10(Hz) - log10(min_hz);

	double dX = (double)(rectFrame.right - rectFrame.left) / dL;
	int X = (int)(rectFrame.left + dX*d);
	return X;
}

double X_to_Hz(int x, double max_hz, double min_hz, RECT rectFrame) {
	//Hz : 10 ~ 16002
	//X : 40 ~ 580
	int x_width = x - rectFrame.left;
	double dL = log10(max_hz) - log10(min_hz);
	double dHz = dL / (double)(rectFrame.right - rectFrame.left);

	double Hz = pow(10, (double)(x_width*dHz + 1));

	return Hz;
}

double First_Order_LowPass(double f0, double f)
{
	if (f0 == 0)
		return 0;
	return 20 * log10(f0 / sqrt(f * f + f0 * f0));
}

double First_Order_HighPass(double f0, double f)
{
	if (f0 == 0)
		return 0;
	return 20 * log10(f / sqrt(f * f + f0 * f0));
}

double Second_Order_LowPass(double f0, double f, int slope)
{
	if (f0 == 0)
		return 0;
	switch (slope) {
	case FILTER_SLPOE_12:
		return 20 * log10((f0*f0) / sqrt(pow(f0, 4) - pow(f*f0, 2)*(2 - (1 / pow(0.707, 2))) + pow(f, 4)));
	case FILTER_SLPOE_18:
		return 20 * log10((f0*f0) / sqrt(pow(f0, 4) - pow(f*f0, 2)*(2 - (1 / pow(1.0, 2))) + pow(f, 4)));
	case FILTER_SLPOE_24:
		return 20 * log10((f0*f0) / sqrt(pow(f0, 4) - pow(f*f0, 2)*(2 - (1 / pow(0.54, 2))) + pow(f, 4))) + 20 * log10((f0*f0) / sqrt(pow(f0, 4) - pow(f*f0, 2)*(2 - (1 / pow(1.31, 2))) + pow(f, 4)));
	case FILTER_SLPOE_30:
		return 20 * log10((f0*f0) / sqrt(pow(f0, 4) - pow(f*f0, 2)*(2 - (1 / pow(0.54, 2))) + pow(f, 4))) + 20 * log10((f0*f0) / sqrt(pow(f0, 4) - pow(f*f0, 2)*(2 - (1 / pow(1.85, 2))) + pow(f, 4)));
	case FILTER_SLPOE_36:
		return 20 * log10((f0*f0) / sqrt(pow(f0, 4) - pow(f*f0, 2)*(2 - (1 / pow(0.54, 2))) + pow(f, 4))) + 20 * log10((f0*f0) / sqrt(pow(f0, 4) - pow(f*f0, 2)*(2 - (1 / pow(1.85, 2))) + pow(f, 4))) + 20 * log10((f0*f0) / sqrt(pow(f0, 4) - pow(f*f0, 2)*(2 - (1 / pow(0.707, 2))) + pow(f, 4)));
	}
	//return 20 * log10((f0*f0) / sqrt(pow(f0, 4) - pow(f*f0, 2)*(2 - (1 / pow(Q, 2))) + pow(f, 4)));
	return 0;
}

double Second_Order_HighPass(double f0, double f, int slope)
{
	if (f0 == 0)
		return 0;
	switch (slope) {
	case FILTER_SLPOE_12:
		return 20 * log10((f*f) / sqrt(pow(f0, 4) - pow(f*f0, 2)*(2 - (1 / pow(0.707, 2))) + pow(f, 4)));
	case FILTER_SLPOE_18:
		return 20 * log10((f*f) / sqrt(pow(f0, 4) - pow(f*f0, 2)*(2 - (1 / pow(1.0, 2))) + pow(f, 4)));
	case FILTER_SLPOE_24:
		return 20 * log10((f*f) / sqrt(pow(f0, 4) - pow(f*f0, 2)*(2 - (1 / pow(0.54, 2))) + pow(f, 4))) + 20 * log10((f*f) / sqrt(pow(f0, 4) - pow(f*f0, 2)*(2 - (1 / pow(1.31, 2))) + pow(f, 4)));
	case FILTER_SLPOE_30:
		return 20 * log10((f*f) / sqrt(pow(f0, 4) - pow(f*f0, 2)*(2 - (1 / pow(0.54, 2))) + pow(f, 4))) + 20 * log10((f*f) / sqrt(pow(f0, 4) - pow(f*f0, 2)*(2 - (1 / pow(1.85, 2))) + pow(f, 4)));
	case FILTER_SLPOE_36:
		return 20 * log10((f*f) / sqrt(pow(f0, 4) - pow(f*f0, 2)*(2 - (1 / pow(0.54, 2))) + pow(f, 4))) + 20 * log10((f*f) / sqrt(pow(f0, 4) - pow(f*f0, 2)*(2 - (1 / pow(1.85, 2))) + pow(f, 4))) + 20 * log10((f*f) / sqrt(pow(f0, 4) - pow(f*f0, 2)*(2 - (1 / pow(0.707, 2))) + pow(f, 4)));
	}

	//return 20 * log10((f*f) / sqrt(pow(f0, 4) - pow(f*f0, 2)*(2 - (1 / pow(Q, 2))) + pow(f, 4)));
	return 0;
}

double Equalizer(double f0, double f, double gain, double q)
{
	double a = pow(10, gain / 20);
	return 20 * log10(sqrt(pow(q, 2)*a*pow(f0, 4) + pow(q, 2)*a*pow(f, 4) + pow(f0, 2)*(pow(f, 2)*pow(a, 2) - 2 * pow(q, 2)*pow(f, 2)*a)) / sqrt(pow(q, 2)*a*pow(f0, 4) + pow(q, 2)*a*pow(f, 4) + pow(f0, 2)*(pow(f, 2) - 2 * pow(q, 2)*pow(f, 2)*a)));
}

double dB_add_dB(double dB1, double dB2)
{
	return 20 * log10(pow(10, (dB1 / 20)) + pow(10, (dB2 / 20)));
}

double dB_sub_dB(double dB1, double dB2)
{
	return dB1 + dB2;// 20 * log10(pow(10, (dB1 / 20)) * pow(10, (dB2 / 20)));
}

boolean util_isSameStr_noSen(char* str1, char *str2) {
	boolean isSame = FALSE;
	int i = 0;
	char* s1 = (char*)malloc(sizeof(char)* (strlen(str1) + 1));
	char* s2 = (char*)malloc(sizeof(char)* (strlen(str2) + 1));
	
	strcpy(s1, str1);
	strcpy(s2, str2);

	for (i = 0; s1[i]; i++) { s1[i] = towupper(s1[i]); }
	for (i = 0; s2[i]; i++) { s2[i] = towupper(s2[i]); }

	isSame = strcmp(s1, s2) == 0 ? TRUE : FALSE;

	free(s1);
	free(s2);
	return isSame;
}
boolean util_isContainStr_noSen(char* strSrc, char *strSub) {
	boolean isContain = FALSE;

	boolean isSame = FALSE;
	int i = 0;
	char* src = (char*)malloc(sizeof(char)* (strlen(strSrc) + 1));
	char* sub = (char*)malloc(sizeof(char)* (strlen(strSub) + 1));

	strcpy(src, strSrc);
	strcpy(sub, strSub);

	for (i = 0; src[i]; i++) { src[i] = towupper(src[i]); }
	for (i = 0; sub[i]; i++) { sub[i] = towupper(sub[i]); }

	isContain = strstr(src, sub) != NULL ? TRUE: FALSE;

	free(src);
	free(sub);

	return isContain;
}

void CenterWindow(HWND hwnd) {

	RECT rc = { 0 };

	GetWindowRect(hwnd, &rc);
	int win_w = rc.right - rc.left;
	int win_h = rc.bottom - rc.top;

	int screen_w = GetSystemMetrics(SM_CXSCREEN);
	int screen_h = GetSystemMetrics(SM_CYSCREEN);

	SetWindowPos(hwnd, HWND_TOP, (screen_w - win_w) / 2,
		(screen_h - win_h) / 2, 0, 0, SWP_NOSIZE);
}

void CenterInParentWindow(HWND hwnd, HWND hParent) {

	RECT rc = { 0 };
	RECT rcParent = { 0 };

	GetWindowRect(hwnd, &rc);
	GetWindowRect(hParent, &rcParent);

	SetWindowPos(hwnd, HWND_TOP, rcParent.left + (rcParent.right - rcParent.left - rc.right + rc.left) / 2,
		rcParent.top + (rcParent.bottom - rcParent.top - rc.bottom + rc.top) / 2, 0, 0, SWP_NOSIZE);
}


void Set_Freq_Text(HWND hwnd, unsigned int id, unsigned int freq)
{
	char *text = (char*)malloc(7 * sizeof(char));
	memset(text, 0, 7 * sizeof(char));
	sprintf(text, "%d", freq);
	SendMessage(GetDlgItem(hwnd, id), WM_SETTEXT, (WPARAM)0, (LPARAM)text);
	free(text);
}

void Set_Gain_Text(HWND hwnd, unsigned int id, double gain)
{
	char *text = (char*)malloc(7 * sizeof(char));
	memset(text, 0, 7 * sizeof(char));
	sprintf(text, "%.1f", gain);
	SendMessage(GetDlgItem(hwnd, id), WM_SETTEXT, (WPARAM)0, (LPARAM)text);
	free(text);
}

void Set_Gain_Text_Double(HWND hwnd, unsigned int id, double gain)
{
	char *text = (char*)malloc(7 * sizeof(char));
	memset(text, 0, 7 * sizeof(char));
	sprintf(text, "%.1f", gain);
	SendMessage(GetDlgItem(hwnd, id), WM_SETTEXT, (WPARAM)0, (LPARAM)text);
	free(text);
}

void Set_Q_Text(HWND hwnd, unsigned int id, double q)
{
	char *text = (char*)malloc(8 * sizeof(char));
	memset(text, 0, 8 * sizeof(char));
	sprintf(text, "%.2f", q);
	SendMessage(GetDlgItem(hwnd, id), WM_SETTEXT, (WPARAM)0, (LPARAM)text);
	free(text);
}

void Set_Double2f_Text(HWND hwnd, unsigned int id, double q)
{
	char *text = (char*)malloc(8 * sizeof(char));
	memset(text, 0, 8 * sizeof(char));
	sprintf(text, "%.2f", q);
	SendMessage(GetDlgItem(hwnd, id), WM_SETTEXT, (WPARAM)0, (LPARAM)text);
	free(text);
}

void Set_Int_Text(HWND hwnd, unsigned int id, int i)
{
	char *text = (char*)malloc(8 * sizeof(char));
	memset(text, 0, 8 * sizeof(char));
	sprintf(text, "%d", i);
	SendMessage(GetDlgItem(hwnd, id), WM_SETTEXT, (WPARAM)0, (LPARAM)text);
	free(text);
}

char* read_file(wchar_t* filepath, long* lSize)
{
	FILE * pFile;
	//long lSize;
	char* buffer;


	pFile = _wfopen(filepath, L"rb");
	if (pFile == NULL) {
		MessageBoxW(NULL, filepath, L"Open File Error", MB_OK);
		return NULL;
	}

	// obtain file size:
	fseek(pFile, 0, SEEK_END);
	*lSize = ftell(pFile);
	rewind(pFile);

	// allocate memory to contain the whole file:
	buffer = (char*)malloc(sizeof(char)**lSize);
	if (buffer == NULL) {
		char msg[100];
		sprintf(msg, "%s%d", "malloc memory size ", sizeof(char)**lSize, " error");
		MessageBox(NULL, msg, "Error", MB_OK);
		return NULL;
	}

	fread(buffer, 1, *lSize, pFile);
	fclose(pFile);

	return buffer;
}