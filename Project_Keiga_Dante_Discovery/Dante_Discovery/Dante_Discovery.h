#pragma once
#include "stdafx.h"
#define MAX_LOADSTRING 100
#define MAIN_WIDTH	1700
#define MAIN_HEIGHT 600
#define LISTVIEW_WIDTH 1650
#define LISTVIEW_HEIGHT 500
#define IDC_BUTTON_SEARCH	(1000+1)
#define IDC_COMBO_FILTER	(1000+2)
#define IDC_BUTTON_CLEAR_OFFLINE	(1000+3)

#define TIMER_SCAN	5000
#define TIMER_PRESET_TIMEOUT	10000
#define TIMER_RESET_WAIT 5000
#define TIMER_QUERY_ACK 500

extern HINSTANCE hInst;
extern HWND hMainWnd;
extern HWND hListView;
extern int index_of_double_click;
void CB_SetListViewItem(int index, char* pszText);


