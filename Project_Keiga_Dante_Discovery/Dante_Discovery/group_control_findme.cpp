#include "stdafx.h"
#include "group_control.h"
#include "ACC_Command.h"
#include "device_state.h"
#include "Dante_Discovery.h"
#include "myListView.h"

HWND hdlgGroupFindMe = NULL;
static bool findme = FALSE;
static unsigned char findme_time_h = 900 / 256;
static unsigned char findme_time_l = 900 % 256;
static unsigned char findme_vol = 0;
static int radio_select = 0;
static int group_query_count = 0;
static unsigned char group_data[512] = { 0 };
static int group_data_length = 0;


void Init_Group_FindMe_UI(HWND hDlg)
{
	SendMessage(GetDlgItem(hDlg, IDC_RADIO1), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_RADIO2), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_RADIO3), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);


	if (radio_select == 0) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO1), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_START_FINDME), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_STOP_FINDME), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_RADIO_FINDME_TIME_1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_RADIO_FINDME_TIME_2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_RADIO_FINDME_TIME_3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_FINDME_LEVEL), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON2), FALSE);
	}
	else if (radio_select == 1) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO2), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_START_FINDME), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_STOP_FINDME), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_RADIO_FINDME_TIME_1), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_RADIO_FINDME_TIME_2), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_RADIO_FINDME_TIME_3), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON1), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_FINDME_LEVEL), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON2), FALSE);
	}
	else if (radio_select == 2) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO3), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_START_FINDME), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_STOP_FINDME), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_RADIO_FINDME_TIME_1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_RADIO_FINDME_TIME_2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_RADIO_FINDME_TIME_3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_FINDME_LEVEL), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON2), TRUE);
	}

	char *text = (char*)malloc(30 * sizeof(char));
	//Find Me
	SendMessage(GetDlgItem(hDlg, IDC_RADIO_FINDME_TIME_1), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_RADIO_FINDME_TIME_2), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_RADIO_FINDME_TIME_3), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);

	switch (findme_time_l + (findme_time_h << 8)) {
	case 180:
		SendMessage(GetDlgItem(hDlg, IDC_RADIO_FINDME_TIME_1), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		break;
	case 900:
		SendMessage(GetDlgItem(hDlg, IDC_RADIO_FINDME_TIME_2), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		break;
	case 1800:
		SendMessage(GetDlgItem(hDlg, IDC_RADIO_FINDME_TIME_3), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
	}

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_FINDME_LEVEL), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(VOLUME_FINDME_MIN, VOLUME_FINDME_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_FINDME_LEVEL), TBM_SETPOS, (WPARAM)1, findme_vol);

	int value = VOLUME_FINDME_MAX - findme_vol;
	if (value > 0) {
		sprintf(text, "%s%d", "-", value);
	}
	else {
		sprintf(text, "%d", value);
	}
	SendMessage(GetDlgItem(hDlg, IDC_STATIC_FINDME_LEVEL), WM_SETTEXT, (WPARAM)0, (LPARAM)text);

	free(text);
}

INT_PTR CALLBACK GroupFindMe(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		radio_select = 0;
		Init_Group_FindMe_UI(hDlg);
		break;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDC_RADIO1:
			radio_select = 0;
			Init_Group_FindMe_UI(hDlg);
			break;
		case IDC_RADIO2:
			radio_select = 1;
			Init_Group_FindMe_UI(hDlg);
			break;
		case IDC_RADIO3:
			radio_select = 2;
			Init_Group_FindMe_UI(hDlg);
			break;
		case IDC_RADIO_FINDME_TIME_1:
			findme_time_l = 0xB4;
			findme_time_h = 0x00;
			break;
		case IDC_RADIO_FINDME_TIME_2:
			findme_time_l = 0x84;
			findme_time_h = 0x03;
			break;
		case IDC_RADIO_FINDME_TIME_3:
			findme_time_l = 0x08;
			findme_time_h = 0x07;
			break;
		case IDC_BUTTON1: {
			unsigned char data[13] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_CHKSS,0,6,0xF2, findme_time_l, findme_time_h, 0, 0,0 };
			strncpy((char*)group_data, (char*)data, 13);
			group_data_length = 13;
			send_group_command(data, 13);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
						  break;
		case IDC_BUTTON2: {
			unsigned char data[13] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_CHKSS,0,6,0xF1, findme_vol, 0, 0, 0,0 };
			strncpy((char*)group_data, (char*)data, 13);
			group_data_length = 13;
			send_group_command(data, 13);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
						  break;

		case IDC_BUTTON_START_FINDME: {
			unsigned char data[13] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,ACC_AMP_ANY,ACC_CMD_CHKSS,0x00,0x06,0xF0,0x00,0x01,0x00,0x00,0x00 };
			send_group_command(data, 13);
			findme = TRUE;
			PDEVICESTATE p;
			p = g_dsHead;
			while (p) {
				if (p->check) {
					if (findme) {
						ListView_SetItemText(hListView, ds_GetIndexByMac(p->Mac), COLUMN_STATE, "Find Me");
					}
					else {
						if (p->state) {
							ListView_SetItemText(hListView, ds_GetIndexByMac(p->Mac), COLUMN_STATE, "Online");
						}
						else {
							ListView_SetItemText(hListView, ds_GetIndexByMac(p->Mac), COLUMN_STATE, "Offline");
						}
					}
				}
				p = p->next_node;
			}
		}
									  break;
		case IDC_BUTTON_STOP_FINDME: {
			unsigned char data[13] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,ACC_AMP_ANY,ACC_CMD_CHKSS,0x00,0x06,0xF0,0x00,0x00,0x00,0x00,0x00 };
			send_group_command(data, 13);
			findme = FALSE;
			PDEVICESTATE p;
			p = g_dsHead;
			while (p) {
				if (p->check) {
					if (findme) {
						ListView_SetItemText(hListView, ds_GetIndexByMac(p->Mac), COLUMN_STATE, "Find Me");
					}
					else {
						if (p->state) {
							ListView_SetItemText(hListView, ds_GetIndexByMac(p->Mac), COLUMN_STATE, "Online");
						}
						else {
							ListView_SetItemText(hListView, ds_GetIndexByMac(p->Mac), COLUMN_STATE, "Offline");
						}
					}
				}
				p = p->next_node;
			}
		}
									 break;
		}
		break;
	case WM_HSCROLL:
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_FINDME_LEVEL) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			char *text = (char*)malloc(10 * sizeof(char));
			findme_vol = SendMessage(GetDlgItem(hDlg, IDC_SLIDER_FINDME_LEVEL), TBM_GETPOS, 0, 0);

			int value = VOLUME_FINDME_MAX - findme_vol;
			if (value > 0) {
				sprintf(text, "%s%d", "-", value);
			}
			else {
				sprintf(text, "%d", value);
			}
			SendMessage(GetDlgItem(hDlg, IDC_STATIC_FINDME_LEVEL), WM_SETTEXT, (WPARAM)0, (LPARAM)text);
			free(text);
		}
		break;
	case WM_TIMER:
		switch (wParam) {
		case TIMER_QUERY_ACK:
		{
			group_query_count++;
			int iAck = 0;
			int iGroupChecked = 0;
			PDEVICESTATE p;
			p = g_dsHead;
			while (p) {
				if (p->check) {
					iGroupChecked++;
					if (p->ack == group_data[4]) {
						iAck++;
					}
					else {
						send_group_command(group_data, group_data_length);
					}
				}
				p = p->next_node;
			}
			char *text = (char*)malloc(10 * sizeof(char));
			sprintf(text, "%d%s%d", iAck, "/", iGroupChecked);

			if (group_query_count == 10) {
				if (iAck == iGroupChecked)
					sprintf(text, "%s", "Finished");
				else
					sprintf(text, "%s", "Timeout");
				KillTimer(hDlg, TIMER_QUERY_ACK);
			}
			SendMessage(GetDlgItem(hDlg, IDC_STATIC_GROUP_CMD_PROGRESS), WM_SETTEXT, (WPARAM)0, (LPARAM)text);
			free(text);
			break;
		}
		default:
			break;
		}
		break;
	case WM_CLOSE:
		DestroyWindow(hdlgGroupFindMe);
		hdlgGroupFindMe = NULL;
		break;
	case WM_DESTROY:
		hdlgGroupFindMe = NULL;
		return (INT_PTR)TRUE;

	}

	DefWindowProc(hDlg, message, wParam, lParam);
}
