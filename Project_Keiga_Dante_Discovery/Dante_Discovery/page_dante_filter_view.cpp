#include "stdafx.h"
#include "device_control.h"
#include "AL003M.h"
#include "utils.h"

#define MAX_DANTE_PARAMETRIC_EQ 3

#define FR_CH1	0
#define FR_CH2	1

#define FR_PEQ1		0
#define FR_PEQ2		1
#define FR_PEQ3		2
#define FR_PEQ4		3
#define FR_PEQ5		4
#define FR_PEQ6		5
#define FR_PEQ7		6
#define FR_PEQ8		7
#define FR_PEQ9		8
#define FR_PEQ10	9

#define FR_GEQ1	4
#define FR_GEQ2	5
#define FR_GEQ3	6
#define FR_GEQ4	7
#define FR_GEQ5	8

#define FR_HPF	0
#define FR_LPF	1

#define FR_CH		0
#define FR_FILTER	1
#define FR_PEQ		2
#define FR_GEQ		3

void Init_Dante_Filter_View_UI(HWND hwnd);
void DrawDanteFreqResponse(Parametric* p, Filter fr, HDC hdc, RECT rectFrame, double max_dB, double min_dB, double max_hz, double min_hz);

static Parametric eq[3];
static Filter	filter;
static RECT rect;
static boolean is_zoom;
static POINT start_point, end_point;
static double old_db_min, old_db_max, old_freq_max, old_freq_min;
static double db_min, db_max, freq_max, freq_min;
INT_PTR CALLBACK DanteFilterViewProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{

	switch (uMsg)
	{
	case WM_INITDIALOG:
		db_min = -25; db_max = 25; freq_max = 23000; freq_min = 10;
		old_db_min = -25; old_db_max = 25; old_freq_max = 23000; old_freq_min = 10;

		Init_Dante_Filter_View_UI(hDlg);
		break;
	case WM_COMMAND:
		break;

	case WM_NOTIFY:
		break;
	case WM_PAINT: {
		if (g_reflash_ui) {
			Init_Dante_Filter_View_UI(hDlg);
			g_reflash_ui = FALSE;
		}

		PAINTSTRUCT 	ps;
		HDC 			hdc;

		hdc = BeginPaint(hDlg, &ps);
		DrawDanteFreqResponse(eq, filter, hdc, rect, db_max, db_min, freq_max, freq_min);

		if (is_zoom && PtInRect(&rect, end_point)) {
			HPEN hpen = CreatePen(PS_SOLID, 1, RGB(100, 200, 100));
			SelectObject(hdc, hpen);
			MoveToEx(hdc, start_point.x, start_point.y, NULL);
			LineTo(hdc, end_point.x, start_point.y);
			LineTo(hdc, end_point.x, end_point.y);
			LineTo(hdc, start_point.x, end_point.y);
			LineTo(hdc, start_point.x, start_point.y);
			DeleteObject(hpen);
		}

		EndPaint(hDlg, &ps);

	}
				   break;
	case WM_CLOSE:
		return TRUE;

	case WM_DESTROY:
		return TRUE;
	}
	return FALSE;
}


void Init_Dante_Filter_View_UI(HWND hDlg)
{
	GetWindowRect(hDlg, &rect);

	int width, height;
	height = rect.bottom - rect.top;
	width = rect.right - rect.left;
	rect.bottom = height * 0.9;// 0.68
	rect.top = height * 0.1;//0.02617;
	rect.left = width * 0.05;
	rect.right = width * 0.983;

	eq[0].enable = g_pDevice->pa[AL003M_C6_SWITCH_DANTE];
	eq[0].hz = (g_pDevice->pa[AL003M_C6_FREQ_H_DANTE] & 0x7F) * 256 + g_pDevice->pa[AL003M_C6_FREQ_L_DANTE];
	eq[0].db = ((double)(g_pDevice->pa[AL003M_C6_GAIN_DANTE] - 40) / 2);
	eq[0].q = ((double)(g_pDevice->pa[AL003M_C6_Q_H_DANTE] * 256 + g_pDevice->pa[AL003M_C6_Q_L_DANTE]) / 100);
	eq[1].enable = g_pDevice->pa[AL003M_C7_SWITCH_DANTE];
	eq[1].hz = (g_pDevice->pa[AL003M_C7_FREQ_H_DANTE] & 0x7F) * 256 + g_pDevice->pa[AL003M_C7_FREQ_L_DANTE];
	eq[1].db = ((double)(g_pDevice->pa[AL003M_C7_GAIN_DANTE] - 40) / 2);
	eq[1].q = ((double)(g_pDevice->pa[AL003M_C7_Q_H_DANTE] * 256 + g_pDevice->pa[AL003M_C7_Q_L_DANTE]) / 100);
	eq[2].enable = g_pDevice->pa[AL003M_C8_SWITCH_DANTE];
	eq[2].hz = (g_pDevice->pa[AL003M_C8_FREQ_H_DANTE] & 0x7F) * 256 + g_pDevice->pa[AL003M_C8_FREQ_L_DANTE];
	eq[2].db = ((double)(g_pDevice->pa[AL003M_C8_GAIN_DANTE] - 40) / 2);
	eq[2].q = ((double)(g_pDevice->pa[AL003M_C8_Q_H_DANTE] * 256 + g_pDevice->pa[AL003M_C8_Q_L_DANTE]) / 100);


	filter.show_high = g_pDevice->pa[AL003M_HIPASS_SWITCH_DANTE];
	filter.show_low = g_pDevice->pa[AL003M_LOPASS_SWITCH_DANTE];
	filter.hz_high = g_pDevice->pa[AL003M_HIPASS_FREQ_H_DANTE] * 256 + g_pDevice->pa[AL003M_HIPASS_FREQ_L_DANTE];
	filter.hz_low = g_pDevice->pa[AL003M_LOPASS_FREQ_H_DANTE] * 256 + g_pDevice->pa[AL003M_LOPASS_FREQ_L_DANTE];
	filter.slope_high = g_pDevice->pa[AL003M_HIPASS_SLOPE_DANTE];
	filter.slope_low = g_pDevice->pa[AL003M_LOPASS_SLOPE_DANTE];
	filter.db = 0;

	InvalidateRect(hDlg, NULL, TRUE);
}

int GetFilter1stHigh(Filter* f)
{
	switch (f->slope_high) {
	case FILTER_SLPOE_12:
	case FILTER_SLPOE_24:
	case FILTER_SLPOE_36:
		return 0;
	case FILTER_SLPOE_30:
	case FILTER_SLPOE_18:
		return 1;
	}
	return 0;
}

int GetFilter1stLow(Filter* f)
{
	switch (f->slope_low) {
	case FILTER_SLPOE_12:
	case FILTER_SLPOE_24:
	case FILTER_SLPOE_36:
		return 0;
	case FILTER_SLPOE_30:
	case FILTER_SLPOE_18:
		return 1;
	}
	return 0;
}

void DrawDanteFreqResponse(Parametric* p, Filter ft, HDC hdc, RECT rectFrame, double max_dB, double min_dB, double max_hz, double min_hz)
{
	double dB = 0;
	double final_dB = 0;
	HPEN hPen;
	RECT rect;
	int db_grad_range = 1;
	boolean line_break = FALSE;

	SetBkColor(hdc, RGB(240, 240, 240));
	//draw frame
	hPen = CreatePen(PS_SOLID, 2, RGB(50, 50, 50));
	SelectObject(hdc, hPen);
	MoveToEx(hdc, rectFrame.left, rectFrame.top, NULL);
	LineTo(hdc, rectFrame.right, rectFrame.top);
	LineTo(hdc, rectFrame.right, rectFrame.bottom);
	LineTo(hdc, rectFrame.left, rectFrame.bottom);
	LineTo(hdc, rectFrame.left, rectFrame.top - 1);
	DeleteObject(hPen);

	HFONT hfont = CreateFont(FONT_SIZE, 0, 0, 0, FW_NORMAL, 0, 0, 0, 0, 0, 0, 0, 0, "111");
	SetTextColor(hdc, RGB(100, 100, 100));
	double j;
	char text[6];
	rect.left = rectFrame.left - 40;
	rect.right = rectFrame.left - 5;
	//draw row & dB text
	int m;
	double y;
	double gap = (double)(rectFrame.bottom - rectFrame.top) / (double)((max_dB - min_dB) / db_grad_range);
	for (y = rectFrame.bottom, m = 0; y >= rectFrame.top; y -= gap, m++) {


		MoveToEx(hdc, rectFrame.left, dB_to_Y(min_dB + m * db_grad_range, max_dB, min_dB, rectFrame), NULL);
		if ((m % 5) == 0) {
			hPen = CreatePen(PS_SOLID, 1, RGB(100, 100, 100));
			SelectObject(hdc, hPen);
			LineTo(hdc, rectFrame.right, dB_to_Y(min_dB + m * db_grad_range, max_dB, min_dB, rectFrame));
			DeleteObject(hPen);

			SelectObject(hdc, hfont);
			rect.top = y - gap*0.75;
			rect.bottom = rect.top + 20;
			sprintf(text, "%d", (int)(min_dB + m * db_grad_range));
			DrawText(hdc, text, -1, &rect, DT_RIGHT);
		}
		else {
			hPen = CreatePen(PS_SOLID, 1, RGB(100, 100, 100));
			SelectObject(hdc, hPen);
			LineTo(hdc, rectFrame.left + DB_GRAD_LEN, dB_to_Y(min_dB + m * db_grad_range, max_dB, min_dB, rectFrame));
			DeleteObject(hPen);
		}
	}
	//draw column
	for (int i = 0, m = 1; i < 28; i++, m++) {

		MoveToEx(hdc, Hz_to_X(freq_line[i], max_hz, min_hz, rectFrame), rectFrame.top, NULL);
		if ((m % 9) == 0) {
			hPen = CreatePen(PS_SOLID, 1, RGB(100, 100, 100));
			SelectObject(hdc, hPen);
			LineTo(hdc, Hz_to_X(freq_line[i], max_hz, min_hz, rectFrame), rectFrame.bottom);
			DeleteObject(hPen);
		}
		else {
			hPen = CreatePen(PS_SOLID, 1, RGB(100, 100, 100));
			SelectObject(hdc, hPen);
			LineTo(hdc, Hz_to_X(freq_line[i], max_hz, min_hz, rectFrame), rectFrame.bottom);
			DeleteObject(hPen);
		}
	}

	//draw text
	rect.left = 0;
	rect.top = dB_to_Y(0, max_dB, min_dB, rectFrame);
	rect.bottom = rect.top + 20;
	DrawText(hdc, "dB", -1, &rect, DT_LEFT);

	if (max_hz > 300) {
		rect.top = rectFrame.bottom + 5;
		rect.bottom = rectFrame.bottom + 25;
		rect.left = Hz_to_X(20, max_hz, min_hz, rectFrame) - 20;
		rect.right = Hz_to_X(20, max_hz, min_hz, rectFrame) + 20;
		DrawText(hdc, "20", -1, &rect, DT_CENTER);
		rect.left = Hz_to_X(200, max_hz, min_hz, rectFrame) - 20;
		rect.right = Hz_to_X(200, max_hz, min_hz, rectFrame) + 20;
		DrawText(hdc, "200", -1, &rect, DT_CENTER);
		rect.left = Hz_to_X(2000, max_hz, min_hz, rectFrame) - 20;
		rect.right = Hz_to_X(2000, max_hz, min_hz, rectFrame) + 20;
		DrawText(hdc, "2K", -1, &rect, DT_CENTER);
		rect.left = Hz_to_X(20000, max_hz, min_hz, rectFrame) - 20;
		rect.right = Hz_to_X(20000, max_hz, min_hz, rectFrame) + 20;
		DrawText(hdc, "20K", -1, &rect, DT_CENTER);
	}
	else {
		rect.top = rectFrame.bottom + 5;
		rect.bottom = rectFrame.bottom + 25;
		rect.left = Hz_to_X(20, max_hz, min_hz, rectFrame) - 20;
		rect.right = Hz_to_X(20, max_hz, min_hz, rectFrame) + 20;
		DrawText(hdc, "20", -1, &rect, DT_CENTER);
		rect.left = Hz_to_X(40, max_hz, min_hz, rectFrame) - 20;
		rect.right = Hz_to_X(40, max_hz, min_hz, rectFrame) + 20;
		DrawText(hdc, "40", -1, &rect, DT_CENTER);
		rect.left = Hz_to_X(100, max_hz, min_hz, rectFrame) - 20;
		rect.right = Hz_to_X(100, max_hz, min_hz, rectFrame) + 20;
		DrawText(hdc, "100", -1, &rect, DT_CENTER);
		rect.left = Hz_to_X(200, max_hz, min_hz, rectFrame) - 20;
		rect.right = Hz_to_X(200, max_hz, min_hz, rectFrame) + 20;
		DrawText(hdc, "200", -1, &rect, DT_CENTER);

	}
	rect.top = rectFrame.bottom + 15;
	rect.bottom = rectFrame.bottom + 35;
	rect.left = Hz_to_X(10, max_hz, min_hz, rectFrame);
	rect.right = Hz_to_X(10, max_hz, min_hz, rectFrame) + 20;
	DrawText(hdc, "Hz", -1, &rect, DT_CENTER);
	DeleteObject(hfont);

	//draw ch1 line

		hPen = CreatePen(PS_SOLID, 2, RGB(100, 160, 200));
		SelectObject(hdc, hPen);
		for (double f = 1; f <= 4.75; f += 0.01) {
			final_dB = 0;

			for (int i = 0; i < MAX_DANTE_PARAMETRIC_EQ; i++) {
				if (p[i].enable)
					dB = Equalizer(p[i].hz, pow(10, f), p[i].db, p[i].q);
				else
					dB = 0;
				final_dB = final_dB + dB;
			}


			dB = (Second_Order_HighPass(ft.hz_high, pow(10, f), ft.slope_high) + First_Order_HighPass(ft.hz_high, pow(10, f)) * GetFilter1stHigh(&ft)) * ft.show_high;
			final_dB = final_dB + dB;
			dB = (Second_Order_LowPass(ft.hz_low, pow(10, f), ft.slope_low) + First_Order_LowPass(ft.hz_low, pow(10, f)) * GetFilter1stLow(&ft)) * ft.show_low;
			final_dB = final_dB + dB + ft.db;

			if (Hz_to_X(pow(10, f), max_hz, min_hz, rectFrame) <= rectFrame.right && dB_to_Y(final_dB, max_dB, min_dB, rectFrame) <= rectFrame.bottom
				&& Hz_to_X(pow(10, f), max_hz, min_hz, rectFrame) >= rectFrame.left && dB_to_Y(final_dB, max_dB, min_dB, rectFrame) >= rectFrame.top) {
				MoveToEx(hdc, Hz_to_X(pow(10, f), max_hz, min_hz, rectFrame), dB_to_Y(final_dB, max_dB, min_dB, rectFrame), NULL);
				line_break = FALSE;
				break;
			}
		}

		for (double f = 1; f <= 4.75; f += 0.01) {
			final_dB = 0;

			for (int i = 0; i < MAX_DANTE_PARAMETRIC_EQ; i++) {
				if (p[i].enable)
					dB = Equalizer(p[i].hz, pow(10, f), p[i].db, p[i].q);
				else
					dB = 0;
				final_dB = final_dB + dB;
			}


			final_dB += (Second_Order_HighPass(ft.hz_high, pow(10, f), ft.slope_high) + First_Order_HighPass(ft.hz_high, pow(10, f)) * GetFilter1stHigh(&ft)) * ft.show_high;
			final_dB += (Second_Order_LowPass(ft.hz_low, pow(10, f), ft.slope_low) + First_Order_LowPass(ft.hz_low, pow(10, f)) * GetFilter1stLow(&ft)) * ft.show_low;
			final_dB += ft.db;


			if (Hz_to_X(pow(10, f), max_hz, min_hz, rectFrame) <= rectFrame.right && dB_to_Y(final_dB, max_dB, min_dB, rectFrame) <= rectFrame.bottom
				&& Hz_to_X(pow(10, f), max_hz, min_hz, rectFrame) >= rectFrame.left && dB_to_Y(final_dB, max_dB, min_dB, rectFrame) >= rectFrame.top) {
				if (line_break) {
					MoveToEx(hdc, Hz_to_X(pow(10, f), max_hz, min_hz, rectFrame), dB_to_Y(final_dB, max_dB, min_dB, rectFrame), NULL);
					line_break = FALSE;
				}
				else
					LineTo(hdc, Hz_to_X(pow(10, f), max_hz, min_hz, rectFrame), dB_to_Y(final_dB, max_dB, min_dB, rectFrame));
			}
			else {
				line_break = TRUE;
			}
		}
		DeleteObject(hPen);

}