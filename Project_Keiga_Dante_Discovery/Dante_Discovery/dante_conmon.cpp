
#include "stdafx.h"
#include "dante_conmon.h"
#include "Dante_Discovery.h"
#include "ACC_Command.h"
#include "device_state.h"
#include "myListView.h"
#include "AL003M.h"
#include "dt_extend_api.h"

void dt_parseAcc(uint8_t* bytes, int len, PCARGO pcargo) {
	uint8_t acc_lenh = bytes[ACC_CMD_LENH_POS];
	uint8_t acc_lenl = bytes[ACC_CMD_LENL_POS];
	int acc_len = (acc_lenh << 8) | acc_lenl;
	switch (bytes[ACC_CMD_CMD_POS])
	{
	case ACC_CMD_CARGO: {
		pcargo->pos_id = ACC_CMD_DATA_FIRST_POS;
		pcargo->pos_sign = pcargo->pos_id + 1;
		pcargo->pos_totalLen = pcargo->pos_sign + 1;
		pcargo->pos_addrData = pcargo->pos_totalLen + 2;
		pcargo->pos_Data = pcargo->pos_addrData + 2;
		if ((GetTickCount() - pcargo->timeout) > 1500) { //The trading id will be discarded, if timed out. 
			pcargo->pre_id = NULL;
			pcargo->crago_count = 0;
		}
		//目前有其他交易 ID
		if (!(pcargo->pre_id == NULL || pcargo->pre_id == bytes[pcargo->pos_id]))
			break;

		//新的交易 ID
		if (pcargo->pre_id == NULL) {
			pcargo->crago_count = 0;
			pcargo->pre_id = bytes[pcargo->pos_id];
		}

		//start timeout
		pcargo->timeout = GetTickCount();

		//Save Data to buffer
		pcargo->pos_addrBuffer = (bytes[pcargo->pos_addrData] << 8) + bytes[pcargo->pos_addrData + 1];
		uint8_t* ptr = pcargo->crago + pcargo->pos_addrBuffer;

		pcargo->data_len = acc_len - 7;  // 7 = id + sign + total_len + address + sum
		for (int i = 0; i < pcargo->data_len; i++) {
			*ptr++ = bytes[pcargo->pos_Data + i];
			pcargo->crago_count++;
		}

		//接收完畢
		pcargo->data_totalLen = (int)(bytes[pcargo->pos_totalLen] << 8) | bytes[pcargo->pos_totalLen + 1];
		if (pcargo->crago_count == pcargo->data_totalLen) {
			memcpy(pcargo->buff, pcargo->crago, pcargo->crago_count);
			pcargo->buff_len = pcargo->crago_count;
			pcargo->pre_id = NULL;
			pcargo->crago_count = 0;
			//stop timeout
		}
		break;
	}
	default: {
		memcpy(pcargo->buff, bytes, len);
		pcargo->buff_len = len;
	}
	}
}

void dt_ReceiveCB(char *name, char *msg, int len) {
	bool isAccFormat = NO;
	if (len >= ACC_CMD_FOMT_LEN) {
		int lenH = (unsigned char)msg[ACC_CMD_LENH_POS];
		int lenL = (unsigned char)msg[ACC_CMD_LENL_POS];
		int lenCmd = lenH * 256 + lenL;
		lenCmd += ACC_CMD_FOMT_LEN;
		if (len == lenCmd) {
			if (len == ACC_CMD_FOMT_LEN)
				isAccFormat = YES;
			else {
				int ckSum = 0;
				for (int i = 0; i < lenCmd - 1; i++) {
					ckSum += (int)msg[i];
					ckSum = LOBYTE(ckSum);
				}
				if (ckSum == LOBYTE(msg[lenCmd - 1]))
					isAccFormat = YES;
			}
		}
	}

	if (isAccFormat != YES) {
		free(name);
		free(msg);
		return;
	}
	char mac[18] = { 0 };
	PDEVICESTATE p;
	p = g_dsHead;
	while (p) {
		if (strcmp(p->Name, name) == 0) {

			dt_parseAcc((uint8_t*)msg, len, &p->cargo);
			if (p->pUpdate != NULL) {
				UPDATERECV data = { p->cargo.buff_len, 1024, p->cargo.buff };
				p->pUpdate->fn_reciver(p->pUpdate, &data);
				free(name);
				free(msg);
				return;
			}
			else {
				if (p->cargo.buff[ACC_CMD_HEAD_POS] == ACC_HEADER_ACK) {
					p->ack = p->cargo.buff[ACC_CMD_CMD_POS];
				}
				switch (p->cargo.buff[ACC_CMD_CMD_POS]) {
				case ACC_CMD_LINALD: {
					if (p->cargo.buff[ACC_CMD_HEAD_POS] == ACC_HEADER_ACK) {
						p->upload_preset_status = PRESET_FINISHED;
						CB_SetListViewItem(ds_GetIndexByMac(p->Mac), "Upload Finished");
					}

					p->amp_id = p->cargo.buff[ACC_CMD_AMP_POS];
					if (p->amp_id == ACC_AMP_AL003M) {
						char wInstaller[33] = { 0 };
						for (int i = 0; i < 32; i++)
							wInstaller[i] = p->cargo.buff[ACC_CMD_DATA_FIRST_POS + USER_INSTALLER_1_AL003M + i];
						if (strcmp(p->InstallerFileName, wInstaller) != 0) {
							sprintf(p->InstallerFileName, "%s", wInstaller);
							g_listchanged = TRUE;
						}
					}
					char wPoe[6] = { 0 };
					if(p->cargo.buff[ACC_CMD_DATA_FIRST_POS + AL003M_T2P_TYPE] & 0x7F)
						sprintf(wPoe, "%s", "PoE+");
					else
						sprintf(wPoe, "%s", "PoE");
					if (strcmp(p->Poe, wPoe) != 0) {
						sprintf(p->Poe, "%s", wPoe);
						g_listchanged = TRUE;
					}


					char wFw[18] = { 0 };
					sprintf(wFw, "%d%s%d%s%d%s%d", p->cargo.buff[ACC_CMD_DATA_FIRST_POS + FW_YEAR], ".", p->cargo.buff[7 + FW_MONTH], ".", p->cargo.buff[7 + FW_DATE], ".", p->cargo.buff[7 + FW_NUMBER]);
					if (strcmp(p->Fw, wFw) != 0) {
						sprintf(p->Fw, "%s", wFw);
						g_listchanged = TRUE;
					}

					char zone[17] = { 0 };
					for (int i = 0; i < 16; i++)
						zone[i] = p->cargo.buff[ACC_CMD_DATA_FIRST_POS + AL003M_LOCATION_1 + i];
					if (strcmp(p->Zone, zone) != 0) {
						memset(p->Zone, 0, sizeof(p->Zone));
						strcpy(p->Zone, zone);
						g_listchanged = TRUE;
					}

					char speaker[17] = { 0 };
					for (int i = 0; i < 16; i++)
						speaker[i] = p->cargo.buff[ACC_CMD_DATA_FIRST_POS + AL003M_SPEAKER_1 + i];
					if (strcmp(p->Speaker, speaker) != 0) {
						memset(p->Speaker, 0, sizeof(p->Speaker));
						strcpy(p->Speaker, speaker);
						g_listchanged = TRUE;
					}

					for (int i = 0; i < 16; i++)
						p->username[i] = p->cargo.buff[ACC_CMD_DATA_FIRST_POS + AL003M_USERNAME_1 + i];


					for (int i = 0; i < 16; i++)
						p->password[i] = p->cargo.buff[ACC_CMD_DATA_FIRST_POS + AL003M_PASSWORD_1 + i];
					if (p->password[0] == 0) {
						p->password[0] = p->Mac[9];
						p->password[1] = p->Mac[10];
						p->password[2] = p->Mac[12];
						p->password[3] = p->Mac[13];
						p->password[4] = p->Mac[15];
						p->password[5] = p->Mac[16];
					}

					p->channel_mode = p->cargo.buff[ACC_CMD_DATA_FIRST_POS + AL003M_PBTL_MODE];

					p->ch1_mute = p->cargo.buff[ACC_CMD_DATA_FIRST_POS + AL003M_MUTE_CH1];
					p->ch2_mute = p->cargo.buff[ACC_CMD_DATA_FIRST_POS + AL003M_MUTE_CH2];

					if (p->upload_preset_status != PRESET_NONE) {
						switch (p->upload_preset_status) {
						case PRESET_UPLOADING:
							CB_SetListViewItem(ds_GetIndexByMac(p->Mac), "Uploading...");
							break;
						case PRESET_FINISHED:
							CB_SetListViewItem(ds_GetIndexByMac(p->Mac), "Upload Finished");
							p->upload_preset_status = PRESET_NONE;
							break;
						case PRESET_TIMEOUT:
							CB_SetListViewItem(ds_GetIndexByMac(p->Mac), "Upload Timeout");
							break;
						}
					}
					else {
				//		CB_SetListViewItem(ds_GetIndexByMac(p->Mac), "");
					}
				}
									 break;
				case ACC_CMD_USER_PRESET:
					if (p->cargo.buff[1] == ACC_HEADER_ACK) {
						p->upload_preset_status = PRESET_FINISHED;
						CB_SetListViewItem(ds_GetIndexByMac(p->Mac), "Upload Finished");
						p->upload_preset_status = PRESET_NONE;
						if (g_mypa[MYPA_USER_RESET_STATUS] == USER_RESET_STATUS_UPLOADING)
							g_mypa[MYPA_USER_RESET_STATUS] = USER_RESET_STATUS_FINISHED;
					}
					break;
				default:
					break;
				}

				if (p->pDevice != NULL) {
					DEVICERECV data = { p->cargo.buff_len, 1024, p->cargo.buff };
					p->pDevice->fn_reciver(p->pDevice, &data);
				}
			}
		}
		p = p->next_node;
	}
	free(name);
	free(msg);
}

boolean dt_Write_data(char* manufacturer, char* name, unsigned char* outputData, const unsigned int sizeBuffer) {
	if (!g_tx_done)
		return FALSE;
	g_tx_done = FALSE;

	static int send_count = 0;
	static bool isBusy = FALSE;
	send_count++;

	if (sizeBuffer < 400) {
		char* txMsg = (char*)malloc((sizeBuffer * 3 + 1) * sizeof(char));
		ZeroMemory(txMsg, sizeof(txMsg));
		for (int i = 0; i < sizeBuffer; i++) {
			sprintf(txMsg, "%s%02x ", txMsg, outputData[i]);
		}
		dc_SendDeviceConMsg_Manufacturer(manufacturer, name, txMsg);
		free(txMsg);
	}
	else { //split data to send
		dt_Write_Split_data(manufacturer, name, outputData, sizeBuffer, 400);
	}
	return TRUE;
}

void dt_Write_Split_data(char* manufacturer, char* name, unsigned char* outputData, const uint16_t sizeBuffer, const uint16_t maxSize) {
	const uint8_t pos_id = ACC_CMD_DATA_FIRST_POS;
	const uint8_t pos_sign = pos_id + 1;
	const uint8_t pos_totalLen = pos_sign + 1;
	const uint8_t pos_addrData = pos_totalLen + 2;
	const uint8_t pos_Data = pos_addrData + 2;
	DWORD waiting_time = GetTickCount(); // range: every 1 -> 10ms~16ms
	int count = 0, ckSum = 0, n = 0;
	uint8_t *ptr = NULL;
	char* txMSG = (char*)malloc(sizeof(char) * (maxSize + 15) * 3);

	//char txMSG[(DATA_MAX_BYTE + 15) * 3];

	uint8_t data[CONMON_MESSAGE_MAX_BODY_SIZE];

	unsigned char id = (rand() % 255) + 1;

	//Fix AccFormat
	data[ACC_CMD_CUST_POS] = ACC_CUST_KEIGA;
	data[ACC_CMD_HEAD_POS] = ACC_HEADER_WRITE;
	data[ACC_CMD_CMDER_POS] = ACC_CMDER_AMOR;
	data[ACC_CMD_AMP_POS] = 0x7c;
	data[ACC_CMD_CMD_POS] = ACC_CMD_CARGO;

	data[pos_id] = id;	//identification
	data[pos_sign] = ACC_PA_ALL_CARGO_SIGN$_NO_SIGN;
	data[pos_totalLen] = HIBYTE(sizeBuffer);	//total Len
	data[pos_totalLen + 1] = LOBYTE(sizeBuffer);
	//Fix AccFormat  - end

	ptr = outputData;
	while (count < sizeBuffer) {

		//Set write address
		data[pos_addrData] = HIBYTE(count);	//address H
		data[pos_addrData + 1] = LOBYTE(count);	//address L

		for (n = 0; n < maxSize && !(count == sizeBuffer); n++) {
			data[pos_Data + n] = *ptr++;
			count++;
		}

		//calculate Cmd length
		data[ACC_CMD_LENH_POS] = HIBYTE(n + ACC_CMD_WLEN_CARGO_BASE); // 7 = id + sign + total_len + address + sum
		data[ACC_CMD_LENL_POS] = LOBYTE(n + ACC_CMD_WLEN_CARGO_BASE);

		n = ACC_CMD_FOMT_LEN + n + ACC_CMD_WLEN_CARGO_BASE;  //All Data to be send

															 //checkSum
		ckSum = 0;
		for (int i = 0; i < n - 1; i++) {
			ckSum += data[i];
			ckSum &= 0xFF;
		}
		data[n - 1] = ckSum;


		//Transform to KK_API format
		ZeroMemory(txMSG, sizeof(txMSG));
		for (int i = 0; i < n; i++) {
			sprintf(txMSG, "%s%02x ", txMSG, (unsigned char)data[i]);
		}
		//*note: change to your sent interface

		//Send Data to Device  
		dc_SendDeviceConMsg_Manufacturer(manufacturer, name, txMSG);
	}
	free(txMSG);
}