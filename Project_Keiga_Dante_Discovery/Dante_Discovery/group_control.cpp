#include "stdafx.h"
#include "group_control.h"
#include "device_state.h"

unsigned char gc_is_ch;//data is ch1 level(1)�B ch2 level(2) or others(0)

void Set_G_ChSlider_Bar(HWND hwnd, unsigned int id, int progress)
{
	int position;

	switch (id)
	{
	case IDC_SLIDER_G_MASTER:
		position = abs(progress - VOLUME_MASTER_MAX) + VOLUME_MASTER_MIN;
		SendMessage(GetDlgItem(hwnd, id), TBM_SETPOS, (WPARAM)1, position);
		break;
	case IDC_SLIDER_G_CH1:
		position = abs(progress - VOLUME_CHANNEL_MAX);
		SendMessage(GetDlgItem(hwnd, id), TBM_SETPOS, (WPARAM)1, position);
		break;
	case IDC_SLIDER_G_CH2:
		position = abs(progress - VOLUME_CHANNEL_MAX);
		SendMessage(GetDlgItem(hwnd, id), TBM_SETPOS, (WPARAM)1, position);
		break;
	case IDC_SLIDER_G_DANTE:
		position = abs(progress - VOLUME_CHANNEL_MAX);
		SendMessage(GetDlgItem(hwnd, id), TBM_SETPOS, (WPARAM)1, position);
		break;
	case IDC_SLIDER_G_MASKING:
		position = abs(progress - VOLUME_MASKING_MAX);
		SendMessage(GetDlgItem(hwnd, id), TBM_SETPOS, (WPARAM)1, position);
		break;
	default:
		break;
	}
}

void Set_G_ChSlider_Text(HWND hwnd, unsigned int id, int vol)
{
	int value;
	char *text = (char*)malloc(10 * sizeof(char));
	memset(text, 0, 10 * sizeof(char));

	switch (id)
	{
	case IDC_EDIT_G_MASTER:

		value = VOLUME_MASTER_MAX - vol;
		if (value > 0) {
			sprintf(text, "%s%d", "-", value);
		}
		else {
			sprintf(text, "%d", value);
		}
		SendMessage(GetDlgItem(hwnd, id), WM_SETTEXT, (WPARAM)0, (LPARAM)text);

		break;
	case IDC_EDIT_G_CH1:
		value = VOLUME_CHANNEL_MAX - vol;
		if (value > 0) {
			sprintf(text, "%s%.1f", "-", (double)value / 10);
		}
		else {
			sprintf(text, "%.1f", (double)value / 10);
		}
		SendMessage(GetDlgItem(hwnd, id), WM_SETTEXT, (WPARAM)0, (LPARAM)text);
		break;
	case IDC_EDIT_G_CH2:
		value = VOLUME_CHANNEL_MAX - vol;
		if (value > 0) {
			sprintf(text, "%s%.1f", "-", (double)value / 10);
		}
		else {
			sprintf(text, "%.1f", (double)value / 10);
		}
		SendMessage(GetDlgItem(hwnd, id), WM_SETTEXT, (WPARAM)0, (LPARAM)text);
		break;
	case IDC_EDIT_G_DANTE:
		value = VOLUME_CHANNEL_MAX - vol;
		if (value > 0) {
			sprintf(text, "%s%.1f", "-", (double)value / 10);
		}
		else {
			sprintf(text, "%.1f", (double)value / 10);
		}
		SendMessage(GetDlgItem(hwnd, id), WM_SETTEXT, (WPARAM)0, (LPARAM)text);
		break;
	case IDC_EDIT_G_MASKING:
		value = 50 - vol;
		if (value > 0) {
			sprintf(text, "%s%d","-", value);
		}
		else {
			sprintf(text, "%d", value);
		}
		SendMessage(GetDlgItem(hwnd, id), WM_SETTEXT, (WPARAM)0, (LPARAM)text);
		break;
	default:
		break;
	}
	free(text);
}


void send_group_command(unsigned char *cmd, unsigned int length)
{
	PDEVICESTATE p;

	p = g_dsHead;
	while (p) {
		if (p->check) {
			if (p->channel_mode == 1 && gc_is_ch == 2)//bypass in 1 ch mode send ch2 data
				p->ack = cmd[4];
			else {
				if (gc_is_ch == 1) {
					if (!p->ch2_mute)
						cmd[12] = 0x7F;
				}
				if (gc_is_ch == 2) {
					if (!p->ch1_mute)
						cmd[11] = 0x7F;
				}
				p->ack = 0;
				cmd[length - 1] = 0;
				cmd[3] = p->amp_id;
				for (int i = 0; i < length - 1; i++)
					cmd[length - 1] += cmd[i];
				dt_Write_data(p->Manufacturer, p->Name, cmd, length);
			}
		}
		p = p->next_node;
	}
}
