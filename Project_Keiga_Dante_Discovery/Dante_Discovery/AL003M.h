#pragma once
#ifndef _AL003M_H_
#define _AL003M_H_

#pragma once
//////////////////////////////////////////////////////////////////
//AL003 Mono
//////////////////////////////////////////////////////////////////
#define AL003M_VOL_MASTER								0
#define AL003M_VOL_CH1									1
#define AL003M_VOL_CH2									2
#define AL003M_VOL_DANTE								3
#define AL003M_VOL_MASKING_IN						4
#define AL003M_VOL_MASKING_OUT					5
#define AL003M_MUTE											6
#define AL003M_MUTE_CH1									7
#define AL003M_MUTE_CH2									8
#define AL003M_MUTE_DANTE								9
#define AL003M_MUTE_MASKING							10
#define AL003M_INPUT_GAIN_DANTE					11
#define AL003M_HIPASS_SWITCH_FACTORY		12
#define AL003M_HIPASS_FREQ_H_FACTORY		13
#define AL003M_HIPASS_FREQ_L_FACTORY		14
#define AL003M_HIPASS_SLOPE_FACTORY			15
#define AL003M_LOPASS_SWITCH_FACTORY		16
#define AL003M_LOPASS_FREQ_H_FACTORY		17
#define AL003M_LOPASS_FREQ_L_FACTORY		18
#define AL003M_LOPASS_SLOPE_FACTORY			19
#define AL003M_HIPASS_SWITCH_DANTE			20
#define AL003M_HIPASS_FREQ_H_DANTE			21
#define AL003M_HIPASS_FREQ_L_DANTE			22
#define AL003M_HIPASS_SLOPE_DANTE				23
#define AL003M_LOPASS_SWITCH_DANTE			24
#define AL003M_LOPASS_FREQ_H_DANTE			25
#define AL003M_LOPASS_FREQ_L_DANTE			26
#define AL003M_LOPASS_SLOPE_DANTE				27
#define AL003M_HIPASS_SWITCH_MASKING		28
#define AL003M_HIPASS_FREQ_H_MASKING		29
#define AL003M_HIPASS_FREQ_L_MASKING		30
#define AL003M_HIPASS_SLOPE_MASKING			31
#define AL003M_LOPASS_SWITCH_MASKING		32
#define AL003M_LOPASS_FREQ_H_MASKING		33
#define AL003M_LOPASS_FREQ_L_MASKING		34
#define AL003M_LOPASS_SLOPE_MASKING			35
#define AL003M_C1_SWITCH_FACTORY				36
#define AL003M_C2_SWITCH_FACTORY				37
#define AL003M_C3_SWITCH_FACTORY				38
#define AL003M_C4_SWITCH_FACTORY				39
#define AL003M_C5_SWITCH_FACTORY				40
#define AL003M_C6_SWITCH_DANTE					41
#define AL003M_C7_SWITCH_DANTE					42
#define AL003M_C8_SWITCH_DANTE					43
#define AL003M_C9_SWITCH_DANTE					44
#define AL003M_C10_SWITCH_DANTE					45
#define AL003M_C11_SWITCH_MASKING				46
#define AL003M_C12_SWITCH_MASKING				47
#define AL003M_C13_SWITCH_MASKING				48
#define AL003M_C14_SWITCH_MASKING				49
#define AL003M_C15_SWITCH_MASKING				50
#define AL003M_C16_SWITCH_MASKING				51
#define AL003M_C17_SWITCH_MASKING				52
#define AL003M_C18_SWITCH_MASKING				53
#define AL003M_C19_SWITCH_MASKING				54
#define AL003M_C20_SWITCH_MASKING				55
#define AL003M_C21_SWITCH_MASKING				56
#define AL003M_C22_SWITCH_MASKING				57
#define AL003M_C23_SWITCH_MASKING				58
#define AL003M_C24_SWITCH_MASKING				59
#define AL003M_C25_SWITCH_MASKING				60
#define AL003M_C26_SWITCH_MASKING				61
#define AL003M_C27_SWITCH_MASKING				62
#define AL003M_C28_SWITCH_MASKING				63
#define AL003M_C29_SWITCH_MASKING				64
#define AL003M_C30_SWITCH_MASKING				65
#define AL003M_C1_GAIN_FACTORY					66
#define AL003M_C2_GAIN_FACTORY					67
#define AL003M_C3_GAIN_FACTORY					68
#define AL003M_C4_GAIN_FACTORY					69
#define AL003M_C5_GAIN_FACTORY					70
#define AL003M_C6_GAIN_DANTE						71
#define AL003M_C7_GAIN_DANTE						72
#define AL003M_C8_GAIN_DANTE						73
#define AL003M_C9_GAIN_DANTE						74
#define AL003M_C10_GAIN_DANTE						75
#define AL003M_C11_GAIN_MASKING					76
#define AL003M_C12_GAIN_MASKING					77
#define AL003M_C13_GAIN_MASKING					78
#define AL003M_C14_GAIN_MASKING					79
#define AL003M_C15_GAIN_MASKING					80
#define AL003M_C16_GAIN_MASKING					81
#define AL003M_C17_GAIN_MASKING					82
#define AL003M_C18_GAIN_MASKING					83
#define AL003M_C19_GAIN_MASKING					84
#define AL003M_C20_GAIN_MASKING					85
#define AL003M_C21_GAIN_MASKING					86
#define AL003M_C22_GAIN_MASKING					87
#define AL003M_C23_GAIN_MASKING					88
#define AL003M_C24_GAIN_MASKING					89
#define AL003M_C25_GAIN_MASKING					90
#define AL003M_C26_GAIN_MASKING					91
#define AL003M_C27_GAIN_MASKING					92
#define AL003M_C28_GAIN_MASKING					93
#define AL003M_C29_GAIN_MASKING					94
#define AL003M_C30_GAIN_MASKING					95
#define AL003M_C1_Q_H_FACTORY				    96
#define AL003M_C2_Q_H_FACTORY				    97
#define AL003M_C3_Q_H_FACTORY				    98
#define AL003M_C4_Q_H_FACTORY				    99
#define AL003M_C5_Q_H_FACTORY				    100
#define AL003M_C6_Q_H_DANTE					    101
#define AL003M_C7_Q_H_DANTE					    102
#define AL003M_C8_Q_H_DANTE					    103
#define AL003M_C9_Q_H_DANTE					    104
#define AL003M_C10_Q_H_DANTE				    105
#define AL003M_C11_Q_H_MASKING			    106
#define AL003M_C12_Q_H_MASKING			    107
#define AL003M_C13_Q_H_MASKING			    108
#define AL003M_C14_Q_H_MASKING			    109
#define AL003M_C15_Q_H_MASKING			    110
#define AL003M_C16_Q_H_MASKING			    111
#define AL003M_C17_Q_H_MASKING			    112
#define AL003M_C18_Q_H_MASKING			    113
#define AL003M_C19_Q_H_MASKING			    114
#define AL003M_C20_Q_H_MASKING			    115
#define AL003M_C21_Q_H_MASKING			    116
#define AL003M_C22_Q_H_MASKING			    117
#define AL003M_C23_Q_H_MASKING			    118
#define AL003M_C24_Q_H_MASKING			    119
#define AL003M_C25_Q_H_MASKING			    120
#define AL003M_C26_Q_H_MASKING			    121
#define AL003M_C27_Q_H_MASKING			    122
#define AL003M_C28_Q_H_MASKING			    123
#define AL003M_C29_Q_H_MASKING			    124
#define AL003M_C30_Q_H_MASKING			    125
#define AL003M_C1_Q_L_FACTORY				    126
#define AL003M_C2_Q_L_FACTORY				    127
#define AL003M_C3_Q_L_FACTORY				    128
#define AL003M_C4_Q_L_FACTORY				    129
#define AL003M_C5_Q_L_FACTORY				    130
#define AL003M_C6_Q_L_DANTE					    131
#define AL003M_C7_Q_L_DANTE					    132
#define AL003M_C8_Q_L_DANTE					    133
#define AL003M_C9_Q_L_DANTE					    134
#define AL003M_C10_Q_L_DANTE				    135
#define AL003M_C11_Q_L_MASKING			    136
#define AL003M_C12_Q_L_MASKING			    137
#define AL003M_C13_Q_L_MASKING			    138
#define AL003M_C14_Q_L_MASKING			    139
#define AL003M_C15_Q_L_MASKING			    140
#define AL003M_C16_Q_L_MASKING			    141
#define AL003M_C17_Q_L_MASKING			    142
#define AL003M_C18_Q_L_MASKING			    143
#define AL003M_C19_Q_L_MASKING			    144
#define AL003M_C20_Q_L_MASKING			    145
#define AL003M_C21_Q_L_MASKING			    146
#define AL003M_C22_Q_L_MASKING			    147
#define AL003M_C23_Q_L_MASKING			    148
#define AL003M_C24_Q_L_MASKING			    149
#define AL003M_C25_Q_L_MASKING			    150
#define AL003M_C26_Q_L_MASKING			    151
#define AL003M_C27_Q_L_MASKING			    152
#define AL003M_C28_Q_L_MASKING			    153
#define AL003M_C29_Q_L_MASKING			    154
#define AL003M_C30_Q_L_MASKING			    155
#define AL003M_C1_FREQ_H_FACTORY				156
#define AL003M_C2_FREQ_H_FACTORY				157
#define AL003M_C3_FREQ_H_FACTORY				158
#define AL003M_C4_FREQ_H_FACTORY				159
#define AL003M_C5_FREQ_H_FACTORY				160
#define AL003M_C6_FREQ_H_DANTE					161
#define AL003M_C7_FREQ_H_DANTE					162
#define AL003M_C8_FREQ_H_DANTE					163
#define AL003M_C9_FREQ_H_DANTE					164
#define AL003M_C10_FREQ_H_DANTE					165
#define AL003M_C11_FREQ_H_MASKING				166
#define AL003M_C12_FREQ_H_MASKING				167
#define AL003M_C13_FREQ_H_MASKING				168
#define AL003M_C14_FREQ_H_MASKING				169
#define AL003M_C15_FREQ_H_MASKING				170
#define AL003M_C16_FREQ_H_MASKING				171
#define AL003M_C17_FREQ_H_MASKING				172
#define AL003M_C18_FREQ_H_MASKING				173
#define AL003M_C19_FREQ_H_MASKING				174
#define AL003M_C20_FREQ_H_MASKING				175
#define AL003M_C21_FREQ_H_MASKING				176
#define AL003M_C22_FREQ_H_MASKING				177
#define AL003M_C23_FREQ_H_MASKING				178
#define AL003M_C24_FREQ_H_MASKING				179
#define AL003M_C25_FREQ_H_MASKING				180
#define AL003M_C26_FREQ_H_MASKING				181
#define AL003M_C27_FREQ_H_MASKING				182
#define AL003M_C28_FREQ_H_MASKING				183
#define AL003M_C29_FREQ_H_MASKING				184
#define AL003M_C30_FREQ_H_MASKING				185
#define AL003M_C1_FREQ_L_FACTORY				186
#define AL003M_C2_FREQ_L_FACTORY				187
#define AL003M_C3_FREQ_L_FACTORY				188
#define AL003M_C4_FREQ_L_FACTORY				189
#define AL003M_C5_FREQ_L_FACTORY				190
#define AL003M_C6_FREQ_L_DANTE					191
#define AL003M_C7_FREQ_L_DANTE					192
#define AL003M_C8_FREQ_L_DANTE					193
#define AL003M_C9_FREQ_L_DANTE					194
#define AL003M_C10_FREQ_L_DANTE					195
#define AL003M_C11_FREQ_L_MASKING				196
#define AL003M_C12_FREQ_L_MASKING				197
#define AL003M_C13_FREQ_L_MASKING				198
#define AL003M_C14_FREQ_L_MASKING				199
#define AL003M_C15_FREQ_L_MASKING				200
#define AL003M_C16_FREQ_L_MASKING				201
#define AL003M_C17_FREQ_L_MASKING				202
#define AL003M_C18_FREQ_L_MASKING				203
#define AL003M_C19_FREQ_L_MASKING				204
#define AL003M_C20_FREQ_L_MASKING				205
#define AL003M_C21_FREQ_L_MASKING				206
#define AL003M_C22_FREQ_L_MASKING				207
#define AL003M_C23_FREQ_L_MASKING				208
#define AL003M_C24_FREQ_L_MASKING				209
#define AL003M_C25_FREQ_L_MASKING				210
#define AL003M_C26_FREQ_L_MASKING				211
#define AL003M_C27_FREQ_L_MASKING				212
#define AL003M_C28_FREQ_L_MASKING				213
#define AL003M_C29_FREQ_L_MASKING				214
#define AL003M_C30_FREQ_L_MASKING				215
#define AL003M_T2_LIMIT_ENABLE		      216
#define AL003M_T2_LIMIT					 	      217
#define AL003M_T2_LIMIT_PBTL			      218
#define AL003M_T2P_LIMIT_ENABLE		      219
#define AL003M_T2P_LIMIT					      220
#define AL003M_T2P_LIMIT_PBTL			      221
#define AL003M_T2PP_LIMIT_ENABLE	      222
#define AL003M_T2PP_LIMIT					      223
#define AL003M_T2PP_LIMIT_PBTL		      224
#define AL003M_T2_C_LIMIT_ENABLE	      225
#define AL003M_T2_C_LIMIT					      226
#define AL003M_T2_C_RECOVERY			      227
#define AL003M_T2P_C_LIMIT_ENABLE	      228
#define AL003M_T2P_C_LIMIT				      229
#define AL003M_T2P_C_RECOVERY			      230
#define AL003M_T2PP_C_LIMIT_ENABLE      231
#define AL003M_T2PP_C_LIMIT				      232
#define AL003M_T2PP_C_RECOVERY		      233												      
#define AL003M_THRESHOLD_DANTE		      234
#define AL003M_KNEE_DANTE				 	      235
#define AL003M_RATIO_H_DANTE			      236
#define AL003M_RATIO_L_DANTE			      237
#define AL003M_ATTACK_H_DANTE			      238
#define AL003M_ATTACK_L_DANTE			      239
#define AL003M_RELEASE_H_DANTE		      240
#define AL003M_RELEASE_L_DANTE		      241
#define AL003M_HOLD_H_DANTE				      242
#define AL003M_HOLD_L_DANTE				      243										      
#define AL003M_4O_THRESHOLD_DANTE	      244
#define AL003M_4O_KNEE_DANTE			      245
#define AL003M_4O_RATIO_H_DANTE		      246
#define AL003M_4O_RATIO_L_DANTE		      247
#define AL003M_4O_ATTACK_H_DANTE	      248
#define AL003M_4O_ATTACK_L_DANTE	      249
#define AL003M_4O_RELEASE_H_DANTE	      250
#define AL003M_4O_RELEASE_L_DANTE	      251
#define AL003M_4O_HOLD_H_DANTE		      252
#define AL003M_4O_HOLD_L_DANTE		      253											      
#define AL003M_GPIO_STATUS				      254
#define AL003M_PBTL_MODE					      255
#define AL003M_T2P_TYPE						      256
#define AL003M_OHM								      257
#define AL003M_STATE_LED1					      258
#define AL003M_STATE_LED2					      259					      
#define AL003M_LOCATION_1					      260
#define AL003M_LOCATION_2					      261
#define AL003M_LOCATION_3					      262
#define AL003M_LOCATION_4					      263
#define AL003M_LOCATION_5					      264
#define AL003M_LOCATION_6					      265
#define AL003M_LOCATION_7					      266
#define AL003M_LOCATION_8					      267
#define AL003M_LOCATION_9					      268
#define AL003M_LOCATION_10				      269
#define AL003M_LOCATION_11				      270
#define AL003M_LOCATION_12				      271
#define AL003M_LOCATION_13				      272
#define AL003M_LOCATION_14				      273
#define AL003M_LOCATION_15				      274
#define AL003M_LOCATION_16				      275
#define AL003M_TEMP_PROTECT_DB				  276
#define AL003M_TEMP_PROTECT_START				  277
#define AL003M_TEMP_PROTECT_H					  278
#define AL003M_TEMP_PROTECT_L					  279					
#define AL003M_SPEAKER_1							  280
#define AL003M_SPEAKER_2							  281
#define AL003M_SPEAKER_3							  282
#define AL003M_SPEAKER_4							  283
#define AL003M_SPEAKER_5							  284
#define AL003M_SPEAKER_6							  285
#define AL003M_SPEAKER_7							  286
#define AL003M_SPEAKER_8							  287
#define AL003M_SPEAKER_9							  288
#define AL003M_SPEAKER_10						  289
#define AL003M_SPEAKER_11						  290
#define AL003M_SPEAKER_12							291
#define AL003M_SPEAKER_13							292
#define AL003M_SPEAKER_14							293
#define AL003M_SPEAKER_15							294
#define AL003M_SPEAKER_16							295
#define AL003M_SIGNAL_CH1							296
#define AL003M_SIGNAL_CH2							297
#define AL003M_VOL_CH1_H							298
#define AL003M_VOL_CH2_H							299
#define AL003M_VOL_DANTE_H						300
#define AL003M_FINDME_TIME						301
#define AL003M_FINDME_TIME_H						302
#define AL003M_VOL_FINDME							303
#define AL003M_INVERT_CH2							304
#define AL003M_SWITCH_MASKING_EQ					305
#define AL003M_T2_LIMIT_CH2			 			306
#define AL003M_T2P_LIMIT_CH2		 				307
#define AL003M_T2PP_LIMIT_CH2		 				308
#define AL003M_FACTORY_1						  309
#define AL003M_FACTORY_2						  310
#define AL003M_FACTORY_3						  311
#define AL003M_FACTORY_4						  312
#define AL003M_FACTORY_5						  313
#define AL003M_FACTORY_6						  314
#define AL003M_FACTORY_7						  315
#define AL003M_FACTORY_8						  316
#define AL003M_FACTORY_9						  317
#define AL003M_FACTORY_10						  318
#define AL003M_FACTORY_11						  319
#define AL003M_FACTORY_12						  320
#define AL003M_FACTORY_13						  321
#define AL003M_FACTORY_14						  322
#define AL003M_FACTORY_15						  323
#define AL003M_FACTORY_16						  324
#define AL003M_FACTORY_17						  325
#define AL003M_FACTORY_18						  326
#define AL003M_FACTORY_19						  327
#define AL003M_FACTORY_20						  328
#define AL003M_FACTORY_21						  329
#define AL003M_FACTORY_22						  330
#define AL003M_FACTORY_23						  331
#define AL003M_FACTORY_24						  332
#define AL003M_FACTORY_25						  333
#define AL003M_FACTORY_26						  334
#define AL003M_FACTORY_27						  335
#define AL003M_FACTORY_28						  336
#define AL003M_FACTORY_29						  337
#define AL003M_FACTORY_30						  338
#define AL003M_FACTORY_31						  339
#define AL003M_FACTORY_32						  340
#define AL003M_INSTALLER_1						341
#define AL003M_INSTALLER_2						342
#define AL003M_INSTALLER_3						343
#define AL003M_INSTALLER_4						344
#define AL003M_INSTALLER_5						345
#define AL003M_INSTALLER_6						346
#define AL003M_INSTALLER_7						347
#define AL003M_INSTALLER_8						348
#define AL003M_INSTALLER_9						349
#define AL003M_INSTALLER_10						350
#define AL003M_INSTALLER_11						351
#define AL003M_INSTALLER_12						352
#define AL003M_INSTALLER_13						353
#define AL003M_INSTALLER_14						354
#define AL003M_INSTALLER_15						355
#define AL003M_INSTALLER_16						356
#define AL003M_INSTALLER_17						357
#define AL003M_INSTALLER_18						358
#define AL003M_INSTALLER_19						359
#define AL003M_INSTALLER_20						360
#define AL003M_INSTALLER_21						361
#define AL003M_INSTALLER_22						362
#define AL003M_INSTALLER_23						363
#define AL003M_INSTALLER_24						364
#define AL003M_INSTALLER_25						365
#define AL003M_INSTALLER_26						366
#define AL003M_INSTALLER_27						367
#define AL003M_INSTALLER_28						368
#define AL003M_INSTALLER_29						369
#define AL003M_INSTALLER_30						370
#define AL003M_INSTALLER_31						371
#define AL003M_INSTALLER_32						372
#define AL003M_USERNAME_1						  373
#define AL003M_USERNAME_2						  374
#define AL003M_USERNAME_3						  375
#define AL003M_USERNAME_4						  376
#define AL003M_USERNAME_5						  377
#define AL003M_USERNAME_6						  378
#define AL003M_USERNAME_7						  379
#define AL003M_USERNAME_8						  380
#define AL003M_USERNAME_9						  381
#define AL003M_USERNAME_10						382
#define AL003M_USERNAME_11						383
#define AL003M_USERNAME_12						384
#define AL003M_USERNAME_13						385
#define AL003M_USERNAME_14						386
#define AL003M_USERNAME_15						387
#define AL003M_USERNAME_16						388
#define AL003M_PASSWORD_1						  389
#define AL003M_PASSWORD_2						  390
#define AL003M_PASSWORD_3						  391
#define AL003M_PASSWORD_4						  392
#define AL003M_PASSWORD_5						  393
#define AL003M_PASSWORD_6						  394
#define AL003M_PASSWORD_7						  395
#define AL003M_PASSWORD_8						  396
#define AL003M_PASSWORD_9						  397
#define AL003M_PASSWORD_10					  398
#define AL003M_PASSWORD_11					  399
#define AL003M_PASSWORD_12					  400
#define AL003M_PASSWORD_13					  401
#define AL003M_PASSWORD_14					  402
#define AL003M_PASSWORD_15					  403
#define AL003M_PASSWORD_16					  404
#define AL003M_T2_LIMIT_H							405
#define AL003M_T2P_LIMIT_H						406
#define AL003M_T2PP_LIMIT_H						407
#define AL003M_T2_LIMIT_CH2_H					408
#define AL003M_T2P_LIMIT_CH2_H				409
#define AL003M_T2PP_LIMIT_CH2_H				410
#define AL003M_T2_LIMIT_PBTL_H				411
#define AL003M_T2P_LIMIT_PBTL_H				412
#define AL003M_T2PP_LIMIT_PBTL_H					413
#define AL003M_VOL_MASKING_1CH_4O						  414
#define AL003M_VOL_MASKING_2CH_4O						  415
#define AL003M_RESERVE137						  416
#define AL003M_RESERVE138						  417
#define AL003M_RESERVE139						  418
#define AL003M_RESERVE140						  419
#define AL003M_RESERVE141						  420
#define AL003M_RESERVE142						  421
#define AL003M_RESERVE143						  422
#define AL003M_RESERVE144						  423
#define AL003M_RESERVE145						  424
#define AL003M_RESERVE146						  425
#define AL003M_RESERVE147						  426
#define AL003M_RESERVE148						  427
#define AL003M_RESERVE149						  428
#define AL003M_RESERVE150						  429
#define AL003M_RESERVE151						  430
#define AL003M_RESERVE152						  431
#define AL003M_RESERVE153						  432
#define AL003M_RESERVE154						  433
#define AL003M_RESERVE155						  434
#define AL003M_RESERVE156						  435
#define AL003M_RESERVE157						  436
#define AL003M_RESERVE158						  437
#define AL003M_RESERVE159						  438
#define AL003M_RESERVE160						  439
#define AL003M_RESERVE161						  440
#define AL003M_RESERVE162							441
#define AL003M_RESERVE163							442
#define AL003M_RESERVE164							443
#define AL003M_RESERVE165							444
#define AL003M_RESERVE166							445
#define AL003M_RESERVE167							446
#define AL003M_RESERVE168							447
#define AL003M_RESERVE169							448
#define AL003M_RESERVE170						  449
#define AL003M_RESERVE171							450
#define AL003M_RESERVE172							451
#define AL003M_RESERVE173							452
#define AL003M_RESERVE174							453
#define AL003M_RESERVE175							454
#define AL003M_RESERVE176							455
#define AL003M_RESERVE177							456
#define AL003M_RESERVE178							457
#define AL003M_RESERVE179							458
#define AL003M_RESERVE180						  459
#define AL003M_RESERVE181						  460
#define AL003M_RESERVE182						  461
#define AL003M_RESERVE183						  462
#define AL003M_RESERVE184						  463
#define AL003M_RESERVE185						  464
#define AL003M_RESERVE186						  465
#define AL003M_RESERVE187						  466
#define AL003M_RESERVE188						  467
#define AL003M_RESERVE189						  468
#define AL003M_RESERVE190						  469
#define AL003M_RESERVE191						  470
#define AL003M_RESERVE192						  471
#define AL003M_RESERVE193						  472
#define AL003M_RESERVE194						  473
#define AL003M_RESERVE195						  474
#define AL003M_RESERVE196						  475
#define AL003M_RESERVE197						  476
#define AL003M_RESERVE198						  477
#define AL003M_RESERVE199						  478
#define AL003M_RESERVE200						  479
#define AL003M_RESERVE201						  480
#define AL003M_RESERVE202						  481
#define AL003M_RESERVE203						  482
#define AL003M_RESERVE204						  483
#define AL003M_RESERVE205						  484
#define AL003M_RESERVE206						  485
#define AL003M_RESERVE207						  486
#define AL003M_RESERVE208						  487
#define AL003M_RESERVE209						  488
#define AL003M_RESERVE210						  489
#define AL003M_RESERVE211						  490
#define AL003M_RESERVE212						  491
#define AL003M_RESERVE213						  492
#define AL003M_RESERVE214						  493
#define AL003M_RESERVE215						  494
#define AL003M_RESERVE216						  495
#define AL003M_RESERVE217						  496
#define AL003M_RESERVE218						  497
#define AL003M_RESERVE219						  498
#define AL003M_RESERVE220						  499
#define AL003M_RESERVE221						  500
#define AL003M_RESERVE222						  501
#define AL003M_RESERVE223						  502
#define AL003M_RESERVE224						  503
#define AL003M_RESERVE225						  504
#define AL003M_RESERVE226						  505
#define AL003M_RESERVE227						  506
#define AL003M_RESERVE228						  507
#define AL003M_VER_YEAR								508
#define AL003M_VER_MONTH				      509
#define AL003M_VER_DATE					      510
#define AL003M_VER_NUMBER				      511

#define AL003M_DATA_COUNT					512
#define AL003M_PRESET_COUNT				283

#endif //_AL003M_H_