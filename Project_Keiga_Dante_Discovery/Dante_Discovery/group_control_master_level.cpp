#include "stdafx.h"
#include "group_control.h"
#include "ACC_Command.h"
#include "device_state.h"

HWND hdlgGroupMasterLevel = NULL;
static HWND hParent;
static HWND hEditMasterLevel;
static WNDPROC oldEditMasterLevel;
static _Data_MVOL data_master_level = { 0 };
static int radio_select = 0;
static int group_query_count = 0;
static unsigned char group_data[512] = { 0 };
static int group_data_length = 0;
void Change_G_Master_Level_Edit_Data(HWND hwnd);

LRESULT CALLBACK MasterLevelGEditProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_CHAR:
	{
		// Make sure we only allow specific characters
		if (!((wParam >= '0' && wParam <= '9')
			|| wParam == '.'
			|| wParam == '-'
			|| wParam == VK_RETURN
			|| wParam == VK_DELETE
			|| wParam == VK_BACK))
		{
			return 0;
		}
	}
	break;
	case WM_LBUTTONUP:
		SendMessage(hwnd, EM_SETSEL, 0, -1);
		break;
#ifdef EDIT_COLOR
	case WM_SETFOCUS:
	{
		for (int i = 0; i < 3; i++) {
			if (editcolor[i].hwnd == hwnd) {
				editcolor[i].color = 0xFF00;
				break;
			}
		}
	}
	break;
#endif
	case WM_KILLFOCUS:
		Change_G_Master_Level_Edit_Data(hwnd);
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_RETURN:
		case VK_TAB:
			Change_G_Master_Level_Edit_Data(hwnd);

			if (wParam == VK_RETURN)
				return 0;
		}
		break;

	}
	if (hwnd == hEditMasterLevel)
		return CallWindowProc(oldEditMasterLevel, hwnd, uMsg, wParam, lParam);
	return 0;
}

void Change_G_Master_Level_Edit_Data(HWND hwnd)
{
	char number[6];

	if (hwnd == hEditMasterLevel) {
		GetDlgItemTextA(hParent, IDC_EDIT_G_MASTER, number, 4);
		int i = atoi(number);
		if (i < -50) {
			i = -50;
			data_master_level.Master.bVolume = 0;
		}
		else if (i > 0) {
			i = 0;
			data_master_level.Master.bVolume = 50;
		}
		else {
			data_master_level.Master.bVolume = i + VOLUME_MASTER_MAX;
		}
		Set_G_ChSlider_Text(hParent, IDC_EDIT_G_MASTER, data_master_level.Master.bVolume);
		Set_G_ChSlider_Bar(hParent, IDC_SLIDER_G_MASTER, data_master_level.Master.bVolume);
	}
}

void Init_Group_Master_Level_UI(HWND hDlg)
{
	if (data_master_level.Master.bMute == 1)
		SendMessage(GetDlgItem(hDlg, IDC_CHECK_G_MUTE), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
	else
		SendMessage(GetDlgItem(hDlg, IDC_CHECK_G_MUTE), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);


	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_G_MASTER), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(VOLUME_MASTER_MIN, VOLUME_MASTER_MAX));
	Set_G_ChSlider_Bar(hDlg, IDC_SLIDER_G_MASTER, data_master_level.Master.bVolume);
	Set_G_ChSlider_Text(hDlg, IDC_EDIT_G_MASTER, data_master_level.Master.bVolume);
}

INT_PTR CALLBACK GroupMasterLevel(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		Init_Group_Master_Level_UI(hDlg);

		hParent = hDlg;

		hEditMasterLevel = GetDlgItem(hDlg, IDC_EDIT_G_MASTER);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_G_MASTER), (UINT)EM_SETLIMITTEXT, (WPARAM)3, (LPARAM)0);
		oldEditMasterLevel = (WNDPROC)SetWindowLongPtr(hEditMasterLevel, GWLP_WNDPROC, (LONG_PTR)MasterLevelGEditProc);

		data_master_level.Limit = 0xFF;
		break;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDC_CHECK_G_MUTE:
			if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_G_MUTE), BM_GETCHECK, 0, 0) == BST_CHECKED)
				data_master_level.Master.bMute = 1;
			else
				data_master_level.Master.bMute = 0;
			//data_mvol.Master.bVolume = 0x7F;
			//send_group_command(ACC_CMD_MVOL);
			break;
		case IDC_BUTTON_RESET_MASTER: {
			//data_mvol.Master.bVolume = 10;

			//Set_ChSlider_Bar(hDlg, IDC_SLIDER_G_MASTER, data_mvol.Master.bVolume);
			//Set_ChSlider_Text(hDlg, IDC_EDIT_G_MASTER, data_mvol.Master.bVolume);
			unsigned char data[10] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_MVOL,0,3,data_master_level.Master.Byte,0xFF,0 };
			strncpy((char*)group_data, (char*)data, 10);
			group_data_length = 10;
			send_group_command(data, 10);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
									  break;
		}
		break;
	case WM_VSCROLL:
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_G_MASTER) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			int value = SendMessage(GetDlgItem(hDlg, IDC_SLIDER_G_MASTER), TBM_GETPOS, 0, 0);

			data_master_level.Master.bVolume = abs(value - VOLUME_MASTER_MAX) + VOLUME_MASTER_MIN;

			Set_G_ChSlider_Bar(hDlg, IDC_SLIDER_G_MASTER, data_master_level.Master.bVolume);
			Set_G_ChSlider_Text(hDlg, IDC_EDIT_G_MASTER, data_master_level.Master.bVolume);
			//send_group_command(ACC_CMD_MVOL);
		}
		break;
	case WM_TIMER:
		switch (wParam) {
		case TIMER_QUERY_ACK:
		{
			group_query_count++;
			int iAck = 0;
			int iGroupChecked = 0;
			PDEVICESTATE p;
			p = g_dsHead;
			while (p) {
				if (p->check) {
					iGroupChecked++;
					if (p->ack == group_data[4]) {
						iAck++;
					}
					else {
						send_group_command(group_data, group_data_length);
					}
				}
				p = p->next_node;
			}
			char *text = (char*)malloc(10 * sizeof(char));
			sprintf(text, "%d%s%d", iAck, "/", iGroupChecked);

			if (group_query_count == 10) {
				if (iAck == iGroupChecked)
					sprintf(text, "%s", "Finished");
				else
					sprintf(text, "%s", "Timeout");
				KillTimer(hDlg, TIMER_QUERY_ACK);
			}
			SendMessage(GetDlgItem(hDlg, IDC_STATIC_GROUP_CMD_PROGRESS), WM_SETTEXT, (WPARAM)0, (LPARAM)text);
			free(text);
			break;
		}
		default:
			break;
		}
		break;
	case WM_CLOSE:
		DestroyWindow(hdlgGroupMasterLevel);
		hdlgGroupMasterLevel = NULL;
		break;
	case WM_DESTROY:
		hdlgGroupMasterLevel = NULL;
		return (INT_PTR)TRUE;

	}

	DefWindowProc(hDlg, message, wParam, lParam);
}


