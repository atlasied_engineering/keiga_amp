#pragma once
#ifndef _ACC_COMMAND_H_
#define _ACC_COMMAND_H_

/**************************************************************************//*
* @file     ACC_Command.h
* @version  V1.0
* $Revision: 0 $
* $Date: 19/02/14	(YY/MM/DD/)
* @brief    ACC Command Define
*
* @note
*
******************************************************************************/

/*************************************************************************/
/*************************************************************************/
/*****					   CMD	Format								*****/
/*************************************************************************/
/*************************************************************************/


/*************************************************************************/
//Customer
/*************************************************************************/
#define ACC_CUST_ANY									0xFF
#define ACC_CUST_KEIGA									0x55

/*************************************************************************/
//Header
/*************************************************************************/
#define ACC_HEADER_WRITE									0x10
#define ACC_HEADER_ACK										0x20
#define ACC_HEADER_READ										0x11
#define ACC_HEADER_ECHO										0x01

/*************************************************************************/
//Commander
/*************************************************************************/
#define ACC_CMDER_AMOR										0xA0
#define ACC_CMDER_BLE										0xA8
#define ACC_CMDER_COMMON									0xAA

/*************************************************************************/
//Amplifier
/*************************************************************************/
#define ACC_AMP_ANY											0xFF
#define ACC_AMP_COMMON										0xAA
#define ACC_AMP_ALL											0xFF
#define ACC_AMP_ALINKON										0x01
#define ACC_AMP_D21											0x02
#define ACC_AMP_D31											0x03
#define ACC_AMP_SB21										0x06
#define ACC_AMP_G31											0x07
#define ACC_AMP_G21											0x08
#define ACC_AMP_D21P										0x09
#define ACC_AMP_GA150										0x10
#define ACC_AMP_GA300										0x13
#define ACC_AMP_GA600										0x16
#define ACC_AMP_J21											0x22
#define ACC_AMP_J31											0x23						
#define ACC_AMP_GA470_Z1									0x47
#define ACC_AMP_GA470_Z2									0x48
#define ACC_AMP_M4											0x0A
#define ACC_AMP_M3											0x0B
#define ACC_AMP_M2											0x0C
#define ACC_AMP_ASP01										0x0D
#define ACC_AMP_M2A											0x0E
#define ACC_AMP_KRH1002										0x54
#define ACC_AMP_KRH1003										0x55
#define ACC_AMP_KRH1004										0x56
#define ACC_AMP_KRX1000										0x61
#define ACC_AMP_JS001										0x0F
#define ACC_AMP_RM12										0x7C
#define ACC_AMP_ED003										0xD3
#define ACC_AMP_ED004										0xD4
#define ACC_AMP_ED005										0xD5
#define ACC_AMP_SD001										0xD6
#define ACC_AMP_XC001										0xD7
#define ACC_AMP_AL003										0xD8
#define ACC_AMP_AL003M										0xDB
/*************************************************************************/
/*				Keiga command format Len								 */
/*	7 = Customer, Head, Source, target, Commad, Len_H, Len_L,			 */
/*************************************************************************/
#define ACC_CMD_FOMT_LEN									0x07

#define ACC_CMD_CUST_POS									0x00
#define ACC_CMD_HEAD_POS									0x01
#define ACC_CMD_CMDER_POS									0x02
#define ACC_CMD_AMP_POS										0x03
#define ACC_CMD_CMD_POS										0x04
#define ACC_CMD_LENH_POS									0x05
#define ACC_CMD_LENL_POS									0x06
#define ACC_CMD_DATA_FIRST_POS								0x07

/*************************************************************************/
/*				Keiga command ACC Command ckSum							 */
/*************************************************************************/
#define ACC_CMD_CKSUM										0x00

/*************************************************************************/
//					Keiga    Command
/*************************************************************************/
#define ACC_CMD_OUTLV	   							0x04
#define ACC_CMD_OUTM	   							0x05
#define ACC_CMD_CHKBTL	   							0x06
#define ACC_CMD_CHKSS	   							0x07
#define ACC_CMD_MVOL	   							0x08
#define ACC_CMD_SRCVOL	   							0x09
#define ACC_CMD_OUTVOL	   							0x0A
#define ACC_CMD_BCSTSP								0x0B
#define ACC_CMD_BCSTVOL								0x0C
#define ACC_CMD_GRPSP								0x0D
#define ACC_CMD_GRPVOL	  							0x0E
#define ACC_CMD_GRPMEMS   							0x0F
#define ACC_CMD_SSEN	   							0x10
#define ACC_CMD_SRCTYP	   							0x1F
#define ACC_CMD_SRCTOUT	   							0x20
#define ACC_CMD_LSRCTOUT	   						0x21
#define ACC_CMD_HLPF	   							0x22
#define ACC_CMD_IRPKG								0x23
#define ACC_CMD_LBLCROL	   							0x26
#define ACC_CMD_PHSHF	   							0x29
#define ACC_CMD_DTIME	   							0x2B
#define ACC_CMD_PLIM	   							0x2C
#define ACC_CMD_WUM		   							0x2D
#define ACC_CMD_NITM	   							0x2E
#define ACC_CMD_ATKTI	   							0x2F
#define ACC_CMD_RELTI	   							0x30
#define ACC_CMD_SFACDEF	   							0x34
#define ACC_CMD_POWOC	   							0x35
#define ACC_CMD_SRCPRY								0x36
#define ACC_CMD_PEQ		 							0x4A
#define ACC_CMD_MTE		   							0x52
#define ACC_CMD_FWVER	   							0xA1
#define ACC_CMD_GEQ		 							0xA4
#define ACC_CMD_ACCT								0xB0
#define ACC_CMD_CARGO								0xD1
#define ACC_CMD_LINKAMP	   							0xE0
#define ACC_CMD_LINUGEQ	   							0xE1
#define ACC_CMD_LINUPEQ	   							0xE2
#define ACC_CMD_LINALD	   							0xE3
#define ACC_CMD_DDGCB	   							0xE4
#define ACC_CMD_DDPCB	   							0xE5
#define ACC_CMD_RENAME	   							0xE9
#define ACC_CMD_LOG		   							0xEA
#define ACC_CMD_LEC		   							0xEB
#define ACC_CMD_USER_PRESET							0xEC
	#define PRESET_INDEX_INDEX_DATA		0x00
	#define PRESET_INDEX_TO_DEFAULT		0x01
#define ACC_CMD_SWRESET	   							0xF0
#define ACC_CMD_EEBLOAD	   							0xF1
#define ACC_CMD_SPDAMP	   							0xF3
#define ACC_CMD_SPDLCM	   							0xF4
#define ACC_CMD_CFS									0xF8
#define ACC_CMD_SGFS								0xF9

/*************************************************************************/
/*				Keiga command Write Len									*/
/*		資料長度 data + Sum :  Data, Cksum								*/
/*************************************************************************/
#define ACC_CMD_WLEN_OUTLV	   							0x04
#define ACC_CMD_WLEN_OUTM	   							0x04
#define ACC_CMD_WLEN_CHKBTL	   							0x02
//#define ACC_CMD_WLEN_CHKSS	   								no define
#define ACC_CMD_WLEN_MVOL	   							0x03
#define ACC_CMD_WLEN_SRCVOL	   							0x05
#define ACC_CMD_WLEN_OUTVOL	   							0x05
#define ACC_CMD_WLEN_BCSTSP								0x04
#define ACC_CMD_WLEN_BCSTVOL							0x02
#define ACC_CMD_WLEN_GRPSP								0x04
#define ACC_CMD_WLEN_GRPVOL	  							0x03
#define ACC_CMD_WLEN_GRPMEMS   							0x04
#define ACC_CMD_WLEN_SSEN	   							0x04
#define ACC_CMD_WLEN_SRCTYP	   							0x03
#define ACC_CMD_WLEN_SRCTOUT	   						0x05
#define ACC_CMD_WLEN_LSRCTOUT	   						0x04
#define ACC_CMD_WLEN_HLPF	   							0x08
#define ACC_CMD_WLEN_IRPKG								0x03
#define ACC_CMD_WLEN_LBLCROL	   						0x02
#define ACC_CMD_WLEN_PHSHF	   							0x05
#define ACC_CMD_WLEN_DTIME	   							0x05
#define ACC_CMD_WLEN_PLIM	   							0x06
#define ACC_CMD_WLEN_WUM		   						0x04
#define ACC_CMD_WLEN_NITM	   							0x02
#define ACC_CMD_WLEN_ATKTI	   							0x05
#define ACC_CMD_WLEN_RELTI	   							0x05
#define ACC_CMD_WLEN_SFACDEF	   						0x01
#define ACC_CMD_WLEN_POWOC	   							0x02
#define ACC_CMD_WLEN_SRCPRY								0x03
#define ACC_CMD_WLEN_PEQ		 						0x0B
//#define ACC_CMD_WLEN_MTE		   								no define
//#define ACC_CMD_WLEN_FWVER	   								no define
#define ACC_CMD_WLEN_GEQ		 						0x07
#define ACC_CMD_WLEN_ACCT_FOR_CHECK						0x23
#define ACC_CMD_WLEN_ACCT_FOR_CHANGE					0x03
#define ACC_CMD_WLEN_ACCT_FOR_MODIFY					0x28
#define ACC_CMD_WLEN_CARGO_BASE							0x07	// 7 + dataCargo[??] 						
//#define ACC_CMD_WLEN_LINKAMP	   						0x1C1	<--	by Moulds  (ex:RM12)	
//#define ACC_CMD_WLEN_LINUGEQ	   						0x0C	<--	by Moulds  (ex:RM12)
//#define ACC_CMD_WLEN_LINUPEQ	   						0x34	<--	by Moulds  (ex:RM12)
//#define ACC_CMD_WLEN_LINALD	   						0x32D	<--	by Moulds  (ex:RM12)
#define ACC_CMD_WLEN_DDGCB	   							0x02
#define ACC_CMD_WLEN_DDPCB	   							0x02
#define ACC_CMD_WLEN_RENAME	   							0x13
#define ACC_CMD_WLEN_LOG		   						0x12
#define ACC_CMD_WLEN_LEC		   						0x06
#define ACC_CMD_WLEN_SWRESET	   						0x01
#define ACC_CMD_WLEN_EEBLOAD	   						0x02
#define ACC_CMD_WLEN_SPDAMP	   							0x201
#define ACC_CMD_WLEN_SPDLCM	   							0x201
#define ACC_CMD_WLEN_CFS								0x07	
#define ACC_CMD_WLEN_SGFS_BASE							0x05			//range 37~8165 bytes => 5+ ( Length * 32 )

/*************************************************************************/
/*					Keiga command Read Len								*/
/*				資料長度 data + Sum :  Data, Cksum						*/
/*************************************************************************/
#define ACC_CMD_RLEN_OUTLV	   							0x03
#define ACC_CMD_RLEN_OUTM	   							0x03
#define ACC_CMD_RLEN_CHKBTL	   							0x01
#define ACC_CMD_RLEN_CHKSS	   							0x02
#define ACC_CMD_RLEN_MVOL	   							0x01
#define ACC_CMD_RLEN_SRCVOL	   							0x03
#define ACC_CMD_RLEN_OUTVOL	   							0x03
#define ACC_CMD_RLEN_BCSTSP								0x01
#define ACC_CMD_RLEN_BCSTVOL							0x01
#define ACC_CMD_RLEN_GRPSP								0x02
#define ACC_CMD_RLEN_GRPVOL	  							0x02
#define ACC_CMD_RLEN_GRPMEMS   							0x02
#define ACC_CMD_RLEN_SSEN	   							0x03
#define ACC_CMD_RLEN_SRCTYP	   							0x02
#define ACC_CMD_RLEN_SRCTOUT	   						0x03
#define ACC_CMD_RLEN_LSRCTOUT	   						0x03
#define ACC_CMD_RLEN_HLPF	   							0x05
#define ACC_CMD_RLEN_IRPKG								0x02
#define ACC_CMD_RLEN_LBLCROL	   						0x01
#define ACC_CMD_RLEN_PHSHF	   							0x04
#define ACC_CMD_RLEN_DTIME	   							0x04
#define ACC_CMD_RLEN_PLIM	   							0x04
#define ACC_CMD_RLEN_WUM		   						0x01
#define ACC_CMD_RLEN_NITM	   							0x01
#define ACC_CMD_RLEN_ATKTI	   							0x04
#define ACC_CMD_RLEN_RELTI	   							0x04
//#define ACC_CMD_RLEN_SFACDEF	   								no defined
//#define ACC_CMD_RLEN_POWOC   									no defined
#define ACC_CMD_RLEN_SRCPRY								0x01
#define ACC_CMD_RLEN_PEQ		 						0x06
#define ACC_CMD_RLEN_MTE		   						0x01
#define ACC_CMD_RLEN_FWVER	   							0x02
#define ACC_CMD_RLEN_GEQ		 						0x06
//#define ACC_CMD_RLEN_ACCT								0x13    no defined
//#define ACC_CMD_RLEN_LINKAMP	   						0x01	<--	by Moulds  (ex:RM12)
//#define ACC_CMD_RLEN_LINUGEQ	   						0x02	<--	by Moulds  (ex:RM12)
//#define ACC_CMD_RLEN_LINUPEQ	   						0x02	<--	by Moulds  (ex:RM12)
#define ACC_CMD_RLEN_LINALD	   							0x01	
//#define ACC_CMD_RLEN_DDGCB	   								no defined
//#define ACC_CMD_RLEN_DDPCB	   								no defined
#define ACC_CMD_RLEN_RENAME	   							0x03
#define ACC_CMD_RLEN_LOG		   						0x02
#define ACC_CMD_RLEN_LEC		   						0x02
//#define ACC_CMD_RLEN_SWRESET	   								no defined
//#define ACC_CMD_RLEN_EEBLOAD	   								no defined
//#define ACC_CMD_RLEN_SPDAMP	   								no defined
//#define ACC_CMD_RLEN_SPDLCM	   								no defined
#define ACC_CMD_RLEN_CFS								0x03
#define ACC_CMD_RLEN_SGFS								0x05


/*************************************************************************/
/*				Keiga command Read receive  Len (echo Len)    			*/
/*					資料長度 data + Sum :  Data, Cksum					*/
/*************************************************************************/
#define ACC_CMD_ELEN_OUTLV	   							0x04
#define ACC_CMD_ELEN_OUTM	   							0x04
#define ACC_CMD_ELEN_CHKBTL	   							0x02
#define ACC_CMD_ELEN_CHKSS	   							0x03
#define ACC_CMD_ELEN_MVOL	   							0x03
#define ACC_CMD_ELEN_SRCVOL	   							0x05
#define ACC_CMD_ELEN_OUTVOL	   							0x05
#define ACC_CMD_ELEN_BCSTSP								0x04
#define ACC_CMD_ELEN_BCSTVOL							0x02
#define ACC_CMD_ELEN_GRPSP								0x04
#define ACC_CMD_ELEN_GRPVOL	  							0x03
#define ACC_CMD_ELEN_GRPMEMS   							0x04
#define ACC_CMD_ELEN_SSEN	   							0x04
#define ACC_CMD_ELEN_SRCTYP								0x03
#define ACC_CMD_ELEN_SRCTOUT	   						0x05
#define ACC_CMD_ELEN_LSRCTOUT	   						0x04
#define ACC_CMD_ELEN_HLPF	   							0x08
#define ACC_CMD_ELEN_IRPKG								0x03
#define ACC_CMD_ELEN_LBLCROL	   						0x02
#define ACC_CMD_ELEN_PHSHF	   							0x05
#define ACC_CMD_ELEN_DTIME	   							0x05
#define ACC_CMD_ELEN_PLIM	   							0x06
#define ACC_CMD_ELEN_WUM		   						0x04
#define ACC_CMD_ELEN_NITM	   							0x02
#define ACC_CMD_ELEN_ATKTI	   							0x05
#define ACC_CMD_ELEN_RELTI	   							0x05
//#define ACC_CMD_ELEN_SFACDEF	   								no defined
//#define ACC_CMD_ELEN_POWOC	   								no defined
#define ACC_CMD_ELEN_SRCPRY								0x03
#define ACC_CMD_ELEN_PEQ		 						0x0B
//#define ACC_CMD_ELEN_MTE		   								no defined
#define ACC_CMD_ELEN_FWVER	   							0x07
#define ACC_CMD_ELEN_GEQ		 						0x07
//#define ACC_CMD_ELEN_ACCT								0x04    no defined
#define ACC_CMD_ELEN_ACCT_FOR_CHECK						0x13
#define ACC_CMD_ELEN_ACCT_FOR_CHANGE					0x03
#define ACC_CMD_ELEN_ACCT_FOR_MODIFY					0x18
#define ACC_CMD_ELEN_CARGO								0x07	
//#define ACC_CMD_ELEN_LINKAMP	   						0x1C1	<--	by Moulds  (ex:RM12)
//#define ACC_CMD_ELEN_LINUGEQ	   						0x0C	<--	by Moulds  (ex:RM12)
//#define ACC_CMD_ELEN_LINUPEQ	   						0x34	<--	by Moulds  (ex:RM12)
//#define ACC_CMD_ELEN_LINALD	   						0x32D	<--	by Moulds  (ex:RM12)
//#define ACC_CMD_ELEN_DDGCB	   								no defined
//#define ACC_CMD_ELEN_DDPCB	   								no defined
#define ACC_CMD_ELEN_RENAME	   							0x13
#define ACC_CMD_ELEN_LOG		   						0x12
#define ACC_CMD_ELEN_LEC		   						0x06
//#define ACC_CMD_ELEN_SWRESET	   								no defined
//#define ACC_CMD_ELEN_EEBLOAD	   								no defined
//#define ACC_CMD_ELEN_SPDAMP	   								no defined
//#define ACC_CMD_ELEN_SPDLCM	   								no defined
#define ACC_CMD_ELEN_CFS								0x07
//#define ACC_CMD_ELEN_SGFS										//range 37~8165 byts  => 5+ ( Length * 32 )

//for RM12 



/*************************************************************************/
/*************************************************************************/
/*****					CMD	Parameter for BootLoad					*****/
/*************************************************************************/
/*************************************************************************/


/*************************************************************************/
/*	SWRESET					:Software Rese    							*/
/*************************************************************************/

/*************************************************************************/
/*	EEBLOAD					:Entry/Exit Bootload 						 */
/*************************************************************************/
//D1
#define ACC_PA_EEBLOAD_DATA$_B								'B' //Enter Bootload
#define ACC_PA_EEBLOAD_DATA$_E								'E' //Exit Bootload
#define ACC_PA_EEBLOAD_DATA$_L								'L' //Enter LCM Bootload
#define ACC_PA_EEBLOAD_DATA$_S								'S' //Check Sum

/*************************************************************************/
/*	SPDPRE					:Send Page Data pre							*/
/*************************************************************************/
//D1
#define ACC_PA_SPDPRE_PAGE$_MIN								0x00 //128
#define ACC_PA_SPDPRE_PAGE$_MAX								0x80 //128


/*************************************************************************/
/*	SPDLCM					:Send Page Data  LCM						  */
/*************************************************************************/
//D1
#define ACC_PA_SPDLCM_PAGE$_MIN								0x00 //128
#define ACC_PA_SPDLCM_PAGE$_MAX								0x80 //128


/*************************************************************************/
/*************************************************************************/
/*****					CMD Parameter for Control					*****/
/*************************************************************************/
/*************************************************************************/

/*========================================================================*/
/*						Global Define Parameter							*/
/*========================================================================*/
//Global Ignore
#define ACC_PA_GLOBAL_IGNORE								0xFF	//IGNORE

//Global Volume Setting
#define ACC_PA_GLOBAL_VOL_MIN								0x00	// -30 db
#define ACC_PA_GLOBAL_VOL_MAX								0x1E	//	 0 db


//Global Volume Channel
#define ACC_PA_GLOBAL_CH_L								(0x00 | 1 << 0)	// Left Channel
#define ACC_PA_GLOBAL_CH_R								(0x00 | 1 << 1)	// Right Channel
#define ACC_PA_GLOBAL_CH_LR								(ACC_PA_GLOBAL_CH_L | ACC_PA_GLOBAL_CH_R)	// Left/Right Channel

//Global EQ_0DB
#define ACC_PA_GLOBAL_EQ_MIN_DB							0
#define ACC_PA_GLOBAL_EQ_MAX_DB							20
#define ACC_PA_GLOBAL_EQ_0_DB							10


/*========================================================================*/
/*						Byte Format										*/
/*========================================================================*/
//Mute & Volume  Mske
#define ACC_PA_FMAT_VOL_MUTE_Msk					(0x00 | 1<<7 )		//Mute				Bit[7]
#define ACC_PA_FMAT_VOL_RANG_Msk							0x7F		//Volume			Bit[0~6]

//En & FreqH  Mske
#define ACC_PA_FMAT_FREQ_EN_Msk						(0x00 | 1<<7 )		//Enable			Bit[7]
#define ACC_PA_FMAT_FREQ_HZ_Msk								0x7F		//Freq High Byte	Bit[0~6]

//Auto Off
#define ACC_PA_FMAT_AUTOF_SLP_EN_Msk				(0x00 | 1<<7 )		//Sleep Switch		Bit[7]
#define ACC_PA_FMAT_AUTOF_WAK_EN_Msk				(0x00 | 1<<6 )		//Wake up Switch	Bit[6]
#define ACC_PA_FMAT_AUTOF_TIME_Msk							0x3F		//Sleep Time		Bit[0~5]

//BTL Status
#define ACC_PA_FMAT_BTL_O8_Msk							(0x00 | 1<<7)		//Output 8 BTL Status
#define ACC_PA_FMAT_BTL_O7_Msk							(0x00 | 1<<6)		//Output 7 BTL Status
#define ACC_PA_FMAT_BTL_O6_Msk							(0x00 | 1<<5)		//Output 6 BTL Status
#define ACC_PA_FMAT_BTL_O5_Msk							(0x00 | 1<<4)		//Output 5 BTL Status
#define ACC_PA_FMAT_BTL_O4_Msk							(0x00 | 1<<3)		//Output 4 BTL Status
#define ACC_PA_FMAT_BTL_O3_Msk							(0x00 | 1<<2)		//Output 3 BTL Status
#define ACC_PA_FMAT_BTL_O2_Msk							(0x00 | 1<<1)		//Output 2 BTL Status
#define ACC_PA_FMAT_BTL_O1_Msk							(0x00 | 1<<0)		//Output 1 BTL Status

/*========================================================================*/
//						Common Parameter
/*========================================================================*/
//	SPAGE :Source Page
#define ACC_PA_ALL_SPAGE_RCA0					(0x00 | 1<<0 )
#define ACC_PA_ALL_SPAGE_RCA1					(0x00 | 1<<1 )
#define ACC_PA_ALL_SPAGE_RESERVE2				(0x00 | 1<<2 )
#define ACC_PA_ALL_SPAGE_RESERVE3				(0x00 | 1<<3 )
#define ACC_PA_ALL_SPAGE_RESERVE4				(0x00 | 1<<4 )
#define ACC_PA_ALL_SPAGE_WIRELESS				(0x00 | 1<<5 )
#define ACC_PA_ALL_SPAGE_DBAY_DANTE				(0x00 | 1<<6 )
#define ACC_PA_ALL_SPAGE_OPTICAL_COAX			(0x00 | 1<<7 )

//SRC : Source
//RCA0 Page: RCA1~8,  RCA1 Page: RCA9~16,  Wireless: Wirless1~8,  D-Bay/Dante:  [4~7]=D-Bay1~4, [0~3]:Dante1~4,
//Optical/Coax: [4~7]: Optical1~4,  [0~3]: Coax 1~4
#define ACC_PA_ALL_SRC_B1						(0x00 | 1<<0 )	  //Bit[0] RCA:1,9,		WF:1,		Dante:1,	Coax:1 
#define ACC_PA_ALL_SRC_B2						(0x00 | 1<<1 )	  //Bit[1] RCA:2,10,	WF:2,		Dante:2,	Coax:2 
#define ACC_PA_ALL_SRC_B3						(0x00 | 1<<2 )	  //Bit[2] RCA:3,11,	WF:3,		Dante:3,	Coax:3 
#define ACC_PA_ALL_SRC_B4						(0x00 | 1<<3 )	  //Bit[3] RCA:4,12,	WF:4,		Dante:4,	Coax:4 
#define ACC_PA_ALL_SRC_B5						(0x00 | 1<<4 )	  //Bit[4] RCA:5,13,	WF:5,		D-Bay:1,	Optical:1 
#define ACC_PA_ALL_SRC_B6						(0x00 | 1<<5 )	  //Bit[5] RCA:6,14,	WF:6,		D-Bay:2,	Optical:2 
#define ACC_PA_ALL_SRC_B7						(0x00 | 1<<6 )	  //Bit[6] RCA:7,15,	WF:7,		D-Bay:3,	Optical:3 
#define ACC_PA_ALL_SRC_B8						(0x00 | 1<<7 )	  //Bit[7] RCA:8,16,	WF:8,		D-Bay:4,	Optical:4 

// OPAGE :Output Page
#define ACC_PA_ALL_OPAGE_OUT0					(0x00 | 1<<0 )
#define ACC_PA_ALL_OPAGE_OUT1					(0x00 | 1<<1 )
#define ACC_PA_ALL_OPAGE_OUT2					(0x00 | 1<<2 )
#define ACC_PA_ALL_OPAGE_OUT3					(0x00 | 1<<3 )
#define ACC_PA_ALL_OPAGE_OUT4					(0x00 | 1<<4 )
#define ACC_PA_ALL_OPAGE_OUT5					(0x00 | 1<<5 )
#define ACC_PA_ALL_OPAGE_OUT6					(0x00 | 1<<6 )
#define ACC_PA_ALL_OPAGE_SUB					(0x00 | 1<<7 )

// OPUT :Output
//Output 1~8,9~16, 17~24, 25~32, 33~40, 41~48, 49~57, Sub1~Sub8
#define ACC_PA_ALL_OPUT_B1						(0x00 | 1<<0 )
#define ACC_PA_ALL_OPUT_B2						(0x00 | 1<<1 )
#define ACC_PA_ALL_OPUT_B3						(0x00 | 1<<2 )
#define ACC_PA_ALL_OPUT_B4						(0x00 | 1<<3 )
#define ACC_PA_ALL_OPUT_B5						(0x00 | 1<<4 )
#define ACC_PA_ALL_OPUT_B6						(0x00 | 1<<5 )
#define ACC_PA_ALL_OPUT_B7						(0x00 | 1<<6 )
#define ACC_PA_ALL_OPUT_B8						(0x00 | 1<<7 )

//Output Numbering
#define ACC_PA_ALL_OUTPUT_1						0x00
#define ACC_PA_ALL_OUTPUT_N(x)					(( x) + ACC_PA_ALL_OUTPUT_1 -1  )
#define ACC_PA_ALL_OUTPUT_MAX					( 57 + ACC_PA_ALL_OUTPUT_1 -1  )
#define ACC_PA_ALL_SUB_1						0x00
#define ACC_PA_ALL_SUB_N(x)						((x) + ACC_PA_ALL_SUB_1 -1  )
#define ACC_PA_ALL_SUB_MAX						( 8  + ACC_PA_ALL_SUB_1 -1  )




/*************************************************************************/
/*	OUTLV					:Output Level								 */
/*************************************************************************/
//Common
//D1: OPAGE
//D2: OPUT
//D3: Ouput Level
#define ACC_PA_ALL_OUTLV_LV$_70V						0x01		// 70V
#define ACC_PA_ALL_OUTLV_LV$_100V						0x02		// 100V
#define ACC_PA_ALL_OUTLV_LV$_OHM						0x10		//  4Ω/8Ω
//---------------------------------------Specific-------------
//RH1004
//D3:
#define ACC_PA_RH1004_OUTLV_LV$_70V						0x01		// 70V
#define ACC_PA_RH1004_OUTLV_LV$_100V					0x02		// 100V
#define ACC_PA_RH1004_OUTLV_LV$_OHM						0x10		//  4Ω/8Ω

/*************************************************************************/
/*	OUTM					:Output Mode								  */
/*************************************************************************/
//Common
//D1: OPAGE
//D2: OPUT
//D3: 
#define ACC_PA_ALL_OUTM_MODE$_STEREO					0x00
#define ACC_PA_ALL_OUTM_MODE$_BI_AMP 					0x01
#define ACC_PA_ALL_OUTM_MODE$_SUB						0x02
#define ACC_PA_ALL_OUTM_MODE$_SUB_MIX					0x03
#define ACC_PA_ALL_OUTM_MODE$_MONO						0x04
#define ACC_PA_ALL_OUTM_MODE$_MONO_MIX					0x05
#define ACC_PA_ALL_OUTM_MODE$_BI_TW						0x06
#define ACC_PA_ALL_OUTM_MODE$_BI_WF						0x07


/*************************************************************************/
/*	CHKBTL					:Check Bridge-Tied Load 					 */
/*************************************************************************/
//Common
//D1 : BTL Status <Byte Format>

/*************************************************************************/
/*	CHKSS					:Check System Status						*/
/*************************************************************************/
//Common
//D1: Index
#define ACC_PA_ALL_CHKSS_IDX$_POW_STAT						0x01	//Power Status				
#define ACC_PA_ALL_CHKSS_IDX$_DCPOW_STAT					0x02	//DC12 Power Status	
#define ACC_PA_ALL_CHKSS_IDX$_SLP_STAT						0x03	//Sleep Status	
#define ACC_PA_ALL_CHKSS_IDX$_BTL_STAT						0x04	//Bridge-Tied Load Status
#define ACC_PA_ALL_CHKSS_IDX$_LOK_STAT						0x0A	//LOCK Status	
#define ACC_PA_ALL_CHKSS_IDX$_AMP_T1						0x10	//AMP Temperature 1
#define ACC_PA_ALL_CHKSS_IDX$_AMP_T2						0x12	//AMP Temperature 2
#define ACC_PA_ALL_CHKSS_IDX$_AMP_T3						0x14	//AMP Temperature 3
#define ACC_PA_ALL_CHKSS_IDX$_AMP_T4						0x16	//AMP Temperature 4
#define ACC_PA_ALL_CHKSS_IDX$_AMP_T5						0x18	//AMP Temperature 5
#define ACC_PA_ALL_CHKSS_IDX$_AMP_T6						0x1A	//AMP Temperature 6
#define ACC_PA_ALL_CHKSS_IDX$_PSU_T1						0x20	//Power Supply Temperature 1
#define ACC_PA_ALL_CHKSS_IDX$_PSU_T2						0x22	//Power Supply Temperature 2
#define ACC_PA_ALL_CHKSS_IDX$_DSP_1							0x30	//DSP 1 Status
#define ACC_PA_ALL_CHKSS_IDX$_DSP_2							0x32	//DSP 2 Status
#define ACC_PA_ALL_CHKSS_IDX$_DSP_3							0x34	//DSP 3 Status
#define ACC_PA_ALL_CHKSS_IDX$_DSP_4							0x36	//DSP 4 Status
#define ACC_PA_ALL_CHKSS_IDX$_DSP_5							0x38	//DSP 5 Status
#define ACC_PA_ALL_CHKSS_IDX$_DSP_6							0x3A	//DSP 6 Status
#define ACC_PA_ALL_CHKSS_IDX$_GEQ_CNT						0x50	//Carry Board 的GEQ mode 數量
#define ACC_PA_ALL_CHKSS_IDX$_PEQ_CNT						0x52	//Carry Board 的PEQ mode 數量

//D1: by Index 	
#define ACC_PA_ALL_CHKSS_BYIDX$_POW_ON						0x01	//Power On	
#define ACC_PA_ALL_CHKSS_BYIDX$_POW_OFF						0x00	//Power Off

#define ACC_PA_ALL_CHKSS_BYIDX$_DCPOW_ON					0x01	//DC12 Power On
#define ACC_PA_ALL_CHKSS_BYIDX$_DCPOW_OFF					0x00	//DC12 Power Off

#define ACC_PA_ALL_CHKSS_BYIDX$_LOK_ON						0x01	//Lock On
#define ACC_PA_ALL_CHKSS_BYIDX$_LOK_OFF						0x00	//Lock Off

#define ACC_PA_ALL_CHKSS_BYIDX$_DSP_OK						0x01	//Normal Conditions
#define ACC_PA_ALL_CHKSS_BYIDX$_DSP_FAIL					0x00	//Abnormal Conditions

#define ACC_PA_ALL_CHKSS_BYIDX$_SAME						0x01	//the data is same with Carry Board
#define ACC_PA_ALL_CHKSS_BYIDX$_DIFF						0x00	//the data is different with Carry Board

/*************************************************************************/
/*	MVOL					:Master Volume			(R/W)				*/
/*	Models: ALL															*/
/*************************************************************************/
//Common
//D1	(Use Format: Mute & Volume  Mske)
#define ACC_PA_ALL_MVOL_VOL$_MIN							0x00	//-50 db
#define ACC_PA_ALL_MVOL_VOL$_MAX							0x32	//	0 db			
//D2
#define ACC_PA_ALL_MVOL_LIM$_MIN							0x14	//(-30 db)
#define ACC_PA_ALL_MVOL_LIM$_MAX							0x32	//(0   db)

//#define ACC_PA_ALL_MVOL_LIM$_IGG							0xFF	//Ignore

/*************************************************************************/
/*	SRCVOL					:Source Volume								 */
/*	Models: ALL															 */
/*************************************************************************/
//Common
//D1: SPAGE
//D2: SRC
//D3:   
#define ACC_PA_ALL_SRCVOL_VOL_L$_MIN			ACC_PA_GLOBAL_VOL_MIN	//  -30 db
#define ACC_PA_ALL_SRCVOL_VOL_L$_MAX			ACC_PA_GLOBAL_VOL_MAX	//	  0 db

//D4  
#define ACC_PA_ALL_SRCVOL_VOL_R$_MIN			ACC_PA_GLOBAL_VOL_MIN	//  -30 db
#define ACC_PA_ALL_SRCVOL_VOL_R$_MAX			ACC_PA_GLOBAL_VOL_MAX	//    0 db

/*************************************************************************/
/*	OUTVOL					:Output Volume								*/	
/*  Models: ALL															*/
/*************************************************************************/
//Common
//D1: OPAGE
//D2: OPUT
//D3: Left Volume  (Use Format: Mute & Volume  Mske)
#define ACC_PA_ALL_OUTVOL_VOL_L$_MIN			ACC_PA_GLOBAL_VOL_MIN	//  -30 db
#define ACC_PA_ALL_OUTVOL_VOL_L$_MAX			ACC_PA_GLOBAL_VOL_MAX	//	  0 db

//D4: Right Volume (Use Format: Mute & Volume  Mske)
#define ACC_PA_ALL_OUTVOL_VOL_R$_MIN			ACC_PA_GLOBAL_VOL_MIN	//  -30 db
#define ACC_PA_ALL_OUTVOL_VOL_R$_MAX			ACC_PA_GLOBAL_VOL_MAX	//    0 db

/*************************************************************************/
/*	BCSTSP					:Broadcast Setting  Parameter				 */
/*	Models: ALL															 */
/*************************************************************************/
//Common
//D1
#define ACC_PA_ALL_BCSTSP_EN$_ON							0x01
#define ACC_PA_ALL_BCSTSP_EN$_OFF							0x00
//D2: SPAGE
//D3: SRC

/*************************************************************************/
/*	BCSTVOL					:Broadcast Volume							*/
/*************************************************************************/
//Common
//D1: (Use Format: Mute & Volume  Mske)
#define ACC_PA_ALL_BCSTVOL_VOL$_MIN							0x00	//  -50 db
#define ACC_PA_ALL_BCSTVOL_VOL$_MAX							0x32	//    0 db

/*************************************************************************/
/*	GRPSP					:Group Setting Parameter					*/
/*************************************************************************/
//Common	
//D1: 
#define ACC_PA_ALL_GRPSP_INDEX$_GRP1						0x01	//Group1
#define ACC_PA_ALL_GRPSP_INDEX$_GRP2						0x02	//Group2
#define ACC_PA_ALL_GRPSP_INDEX$_GRP3						0x03	//Group3
//D2: SPAGE
//D3: SRC

/*************************************************************************/
/*	GRPVOL					:Group Volume								*/
/*************************************************************************/
//Common	
//D1: 
#define ACC_PA_ALL_GRPVOL_INDEX$_GRP1						0x01	//Group1
#define ACC_PA_ALL_GRPVOL_INDEX$_GRP2						0x02	//Group2
#define ACC_PA_ALL_GRPVOL_INDEX$_GRP3						0x03	//Group3
//D3: (Use Format: Mute & Volume  Mske)
#define ACC_PA_ALL_GRPVOL_VOL$_MIN							0x00	//  -50 db
#define ACC_PA_ALL_GRPVOL_VOL$_MAX							0x32	//    0 db

/*************************************************************************/
/*	GRPMEMS					:Group Member Settings						*/
/*************************************************************************/
//Common	
//D1: 
#define ACC_PA_ALL_GRPMEMS_INDEX$_INDIVI					0x00	//Individual		
#define ACC_PA_ALL_GRPMEMS_INDEX$_GRP1						0x01	//Group1
#define ACC_PA_ALL_GRPMEMS_INDEX$_GRP2						0x02	//Group2
#define ACC_PA_ALL_GRPMEMS_INDEX$_GRP3						0x03	//Group3
//D2:OPAGE
//D3:OPUT

/*************************************************************************/
/*	SSEN					:Source Sensitivity							 */
/*	Models: ALL															 */
/*************************************************************************/
//Common
//D1: SPAGE
//D2: SRC
//D3: Sensitivity
#define ACC_PA_ALL_SSEN_SEN$_MIN							0x01	//  0.1 V
#define ACC_PA_ALL_SSEN_SEN$_MAX							0xFA	// 25.0 V


/*************************************************************************/
/*	SRCTYP					:Setting Type of Input Line in (Source)		*/
/*************************************************************************/
//Common
//D1: Source_Line
//D2: SourceType

//---------------------------------------Specific-------------
//J2.1
//D1
#define ACC_PA_J21_SRCTYP_SL$_LINE1					0x00		//  (RCA1 /  OPT1)
#define ACC_PA_J21_SRCTYP_SL$_LINE2					0x01		//  (RCA2 /  OPT2)
#define ACC_PA_J21_SRCTYP_SL$_LINE3					0x02		//  (Dbay1)
//D2
#define ACC_PA_J21_SRCTYP_ST$_RCA					0x01		//  (RCA)
#define ACC_PA_J21_SRCTYP_ST$_COAX					0x02		//  (Coax.)
#define ACC_PA_J21_SRCTYP_ST$_OPT					0x04		//  (Optical)
#define ACC_PA_J21_SRCTYP_ST$_DBAY					0x08		//  (Dbay)
#define ACC_PA_J21_SRCTYP_ST$_DANTE					0x10		//  (Dante)

/*************************************************************************/
/*	SRCTOUT					:Setting Input Source to Output				*/
/*************************************************************************/
//Common
//D1: OPAGE
//D2: OPUT
//D3: SPAGE
//D4: SRC

/*************************************************************************/
/*	LSRCTOUT					:Setting Input Line in(Source) to Output */
/*************************************************************************/
//Common
//D1: OPAGE
//D2: OPUT
//D3: Source_Line

//---------------------------------------Specific-------------
//J2.1
//D3:Source_Line
#define ACC_PA_J21_LSRCTOUT_SL$_LINE1						0x00		//  (RCA1 /  OPT1)
#define ACC_PA_J21_LSRCTOUT_SL$_LINE2						0x01		//  (RCA2 / OPT2)
#define ACC_PA_J21_LSRCTOUT_SL$_LINE3						0x02		//  D-Bay1

/*************************************************************************/
/*	HLPF					:H/L Pass Filter							*/
/*************************************************************************/
//Common
//D1: OPAGE
//D2: OPUT
//D3: CHL / CHR /  CH L/R
//D4: 
#define ACC_PA_ALL_HLPF_TYPE$_HPF							0x01	//Left Channel
#define ACC_PA_ALL_HLPF_TYPE$_LPF							0x02	//Right Channel
//D5: Freq High Byte (Use Format: En & FreqH  Mske)
#define ACC_PA_ALL_HLPF_FREQ$_MIN							0x00	// x * 256  = 0 Hz
#define ACC_PA_ALL_HLPF_FREQ$_MAX							0x4E	 
//D6: Freq Low byte
//D7: Slope
#define ACC_PA_ALL_HLPF_SLOPE$_12							0x00	// -12 db/oct
#define ACC_PA_ALL_HLPF_SLOPE$_18							0x01	// -18 db/oct
#define ACC_PA_ALL_HLPF_SLOPE$_24							0x02	// -24 db/oct
#define ACC_PA_ALL_HLPF_SLOPE$_30							0x03	// -30 db/oct
#define ACC_PA_ALL_HLPF_SLOPE$_36							0x04	// -36 db/oct

/*************************************************************************/
/*	IRPKG					:IR Package							*/
/*************************************************************************/
//Common
//D1: Index
#define ACC_PA_ALL_IRPKG_IDX$_IR_STAT						0x01	//IR Status	
#define ACC_PA_ALL_IRPKG_IDX$_IR_POWER						0x02	//IR Power
#define ACC_PA_ALL_IRPKG_IDX$_IR_KEYCODE					0x03	//IR KeyCode
			
//D2: byIndex
//IR Status	
#define ACC_PA_ALL_IRPKG_BYIDX$_IR_ON						0x01	//Standy by
#define ACC_PA_ALL_IRPKG_BYIDX$_IR_OFF						0x00	//Function off
//IR Power
#define ACC_PA_ALL_IRPKG_BYIDX$_IR_POWER_ON					0x01	//Power On
#define ACC_PA_ALL_IRPKG_BYIDX$_IR_POWER_OFF				0x00	//Power Off
//IR KeyCode
#define ACC_PA_ALL_IRPKG_BYIDX$_IR_KEYCODE_PLAY				0x01	//Key Play
#define ACC_PA_ALL_IRPKG_BYIDX$_IR_KEYCODE_NEXT				0x02	//Key Next
#define ACC_PA_ALL_IRPKG_BYIDX$_IR_KEYCODE_PREV				0x03	//Key Prev
#define ACC_PA_ALL_IRPKG_BYIDX$_IR_KEYCODE_FF				0x04	//Key Fast Forward
#define ACC_PA_ALL_IRPKG_BYIDX$_IR_KEYCODE_REW				0x05	//Key Rewind

/*************************************************************************/
/*	LBLCROL					:LCM Backlight Ctrl							*/
/*************************************************************************/


/*************************************************************************/
/*	PHSHF					:Phase Shift							*/
/*************************************************************************/
//Common
//D1: OPAGE
//D2: OPUT
//D3: CHL, CHR, CHL/R
//D4
#define ACC_PA_ALL_PHSHF_PH$_0							0x00		//0		degree 
#define ACC_PA_ALL_PHSHF_PH$_45							0x01		//45	degree 
#define ACC_PA_ALL_PHSHF_PH$_90							0x02		//90	degree 
#define ACC_PA_ALL_PHSHF_PH$_135						0x03		//135	degree 
#define ACC_PA_ALL_PHSHF_PH$_180						0x04		//180	degree 
#define ACC_PA_ALL_PHSHF_PH$_225						0x05		//225	degree 
#define ACC_PA_ALL_PHSHF_PH$_270						0x06		//270	degree 
#define ACC_PA_ALL_PHSHF_PH$_315						0x07		//315	degree 

/*************************************************************************/
/*	DTIME					:Delay Time									 */
/*************************************************************************/
//Common
//D1: OPAGE
//D2: OPUT
//D3: CHL, CHR, CHL/R
//D4
#define ACC_PA_ALL_DTIME_DELAY$_MIN							0x00
#define ACC_PA_ALL_DTIME_DELAY$_MAX							0xA5

/*************************************************************************/
/*	PLIM					:Power_Limiter								*/
/*************************************************************************/
//Common
//D1: OPAGE
//D2: OPUT
//D3: CHL, CHR, CHL/R
//D4: Power Limit High byte
//D5: Power Limit Low byte

//---------------------------------------Specific-------------
//RM12
//D4: Power Limit High byte
#define ACC_PA_RM12_PLIM_LIM_HI$_MIN							0x00
#define ACC_PA_RM12_PLIM_LIM_HI$_MAX							0x00
//D5: Power Limit Low byte
#define ACC_PA_RM12_PLIM_LIM_LO$_MIN							0x14	//20W
#define ACC_PA_RM12_PLIM_LIM_LO$_MAX							0x50	//80W

//RH1004
//D4: Power Limit High byte
#define ACC_PA_RH1004_PLIM_LIM_HI$_MIN							0x00
#define ACC_PA_RH1004_PLIM_LIM_HI$_MAX							0x00
//D5: Power Limit Low byte
#define ACC_PA_RH1004_PLIM_LIM_LO$_MIN							0x14	//20W
#define ACC_PA_RH1004_PLIM_LIM_LO$_MAX							0xFA	//250W


/*************************************************************************/
/*	WUM						:Wake Up Mode								*/
/*************************************************************************/
//Common
//D1:
#define ACC_PA_ALL_WUM_DCTRIG$_EN							0x01	//External trigger
#define ACC_PA_ALL_WUM_DCTRIG$_DIS							0x00	//No Trigger
//D2: Auto Off  on <Byte Format>
//D3: Auto On Sensitivity
#define ACC_PA_ALL_WUM_AOS$_LOW								0x00	//12mv  Auto On Sensitivity
#define ACC_PA_ALL_WUM_AOS$_MID								0x01	// 9mv  Auto On Sensitivity
#define ACC_PA_ALL_WUM_AOS$_HI								0x02	// 6mv  Auto On Sensitivity

/*************************************************************************/
/*	NITM					:Night Mode									 */
/*************************************************************************/
//Common
//D1
#define ACC_PA_ALL_NITM_MODE$_EN							0x01	//Enable Night mode
#define ACC_PA_ALL_NITM_MODE$_DIS							0x00	//Disable Night mode

/*************************************************************************/
/*	ATKTI					:Attack Time								*/
/*************************************************************************/
//Common
//D1: OPAGE
//D2: OPUT
//D3: CHL, CHR, CHL/R
//D4
#define ACC_PA_ALL_ATKTI_ATK$_MIN							0x01
#define ACC_PA_ALL_ATKTI_ATK$_MAX							0xFA


/*************************************************************************/
/*	RELTI					:Release Time								*/
/*************************************************************************/
//Common
//D1: OPAGE
//D2: OPUT
//D3: CHL, CHR, CHL/R
//D4
#define ACC_PA_ALL_RELTI_REL$_MIN							0x01
#define ACC_PA_ALL_RELTI_REL$_MAN							0xFA

/*************************************************************************/
/*	SFACDEF					:Set Factory Default						*/ 
/*************************************************************************/


/*************************************************************************/
/*	POWOC					:Power on/off Control						*/ 
/*************************************************************************/
//Common
//D1: 
#define ACC_PA_ALL_POWOC_SW$_ON							0x01		//Power On				
#define ACC_PA_ALL_POWOC_SW$_OFF						0x00		//Power Off

/*************************************************************************/
/*	SRCPRY					:Input Source Priority						*/ 
/*************************************************************************/
//Common
//D1: Priority MSB Byte
//D2: Priority LSB Byte

#define ACC_PA_ALL_SRCPRY_EN$_ON							0x01		//enable	
#define ACC_PA_ALL_SRCPRY_EN$_OFF							0x00		//Disable

#define ACC_PA_ALL_SRCPRY_LINE$_L1							0x00		//Line in 1
#define ACC_PA_ALL_SRCPRY_LINE$_L2							0x01		//Line in 2
#define ACC_PA_ALL_SRCPRY_LINE$_L3							0x02		//Line in 3
#define ACC_PA_ALL_SRCPRY_LINE$_L4							0x03		//Line in 4
#define ACC_PA_ALL_SRCPRY_LINE$_L5							0x04		//Line in 5
#define ACC_PA_ALL_SRCPRY_LINE$_L6							0x05		//Line in 6
#define ACC_PA_ALL_SRCPRY_LINE$_L7							0x06		//Line in 7
#define ACC_PA_ALL_SRCPRY_LINE$_Ignore						0x07		//Ignore

#define ACC_PA_ALL_SRCPRY_POS$_U16_EN						0x15		//Get En (15bit)
#define ACC_PA_ALL_SRCPRY_POS$_U8_EN						0x07		//Get En (7bit)

#define ACC_PA_ALL_SRCPRY_POS$_U16_P1						0x0C		//Get P1 (14~12 bits)
#define ACC_PA_ALL_SRCPRY_POS$_U16_P2						0x09		//Get P2 (11~9 bits)
#define ACC_PA_ALL_SRCPRY_POS$_U16_P3						0x06		//Get P3 (8~6 bits)
#define ACC_PA_ALL_SRCPRY_POS$_U16_P4						0x03		//Get P4 (5~3 bits)
#define ACC_PA_ALL_SRCPRY_POS$_U16_P5						0x00		//Get P5 (2~0 bits)

#define ACC_PA_ALL_SRCPRY_MASK$_BIT1						0x01			//Mask 1 bit
#define ACC_PA_ALL_SRCPRY_MASK$_U16_BIT3					0x07			//Mask 3 bit


/*************************************************************************/
/*	PEQ						:Parametric EQ Boost						*/
/*************************************************************************/
//Common
//D1: OPAGE
//D2: OPUT
//D3: CHL, CHR, CHL/R

//D4:
#define ACC_PA_ALL_PEQ_MODE$_AMP							0x00	//AMP itself
#define ACC_PA_ALL_PEQ_MODE$_001							0x01	//Mode 1
#define ACC_PA_ALL_PEQ_MODE$_N(x)							( (x) + (ACC_PA_ALL_PEQ_MODE$_001 -1 )) //Mode N 
#define ACC_PA_ALL_PEQ_MODE$_MAX							0x64	//Mode Max (Temporarily defined)

//D5:
#define ACC_PA_ALL_PEQ_BAND$_001							0x00	//PEQ 1
#define ACC_PA_ALL_PEQ_BAND$_N(x)							( (x) + (ACC_PA_ALL_PEQ_BAND$_001)-1)	//PEQ N
#define ACC_PA_ALL_PEQ_BAND$_MAX							0x09	//PEQ Max : 10

//D6:
#define ACC_PA_ALL_PEQ_TYPE$_PEAKING						0x00	//Peaking
#define ACC_PA_ALL_PEQ_TYPE$_SHELVING_LOW					0x01	//Shelving Low
#define ACC_PA_ALL_PEQ_TYPE$_SHELVING_HI					0x02	//Shelving High

//D7:
#define ACC_PA_ALL_PEQ_GAIN$_MIN							0x00	//-10 db
#define ACC_PA_ALL_PEQ_GAIN$_MAX							0x14	// 10 db

//D8: Freq High Byte (Use Format: En & FreqH  Mske)
#define ACC_PA_ALL_PEQ_FREQ$_MIN							0x00	// x * 256  = 0 Hz
#define ACC_PA_ALL_PEQ_FREQ$_MAX							0x4E	 
//D9: Freq Low byte
//D10: Q-Factor
#define ACC_PA_ALL_PEQ_Q$_MIX								0x01	// 0.1
#define ACC_PA_ALL_PEQ_Q$_MAX								0x96	// 15.0

//---------------------------------------Specific-------------

//RM12
//D4:
#define ACC_PA_RM12_PEQ_MODE$_AMP							0x00	//AMP itself
#define ACC_PA_RM12_PEQ_MODE$_001							0x01	//Mode 1
#define ACC_PA_RM12_PEQ_MODE$_N(x)							( (x) + (ACC_PA_ALL_PEQ_MODE$_001 -1 )) //Mode N 
#define ACC_PA_RM12_PEQ_MODE$_MAX							0x64	//Mode Max (Temporarily defined)
//D5:
#define ACC_PA_RM12_PEQ_BAND$_001							0x00	//PEQ 1
#define ACC_PA_RM12_PEQ_BAND$_N(x)							( (x) + (ACC_PA_RM12_PEQ_BAND$_001)-1)	//PEQ N
#define ACC_PA_RM12_PEQ_BAND$_MAX							0x09	//PEQ Max : 10

//M4
//D4:
#define ACC_PA_M4_PEQ_MODE$_IGNORE								ACC_PA_GLOBAL_IGNORE	//Ignore
//D5:
#define ACC_PA_M4_PEQ_BAND$_001								0x00	//PEQ 1
#define ACC_PA_M4_PEQ_BAND$_N(x)							( (x) + (ACC_PA_M4_PEQ_BAND$_001)-1)	//PEQ N
#define ACC_PA_M4_PEQ_BAND$_MAX								0x03	//PEQ Max : 4

//RH1004
//D4:
#define ACC_PA_RH1004_PEQ_MODE$_IGNORE							ACC_PA_GLOBAL_IGNORE	//Ignore
//D5:
#define ACC_PA_RH1004_PEQ_BAND$_001							0x00	//PEQ 1
#define ACC_PA_RH1004_PEQ_BAND$_N(x)						( (x) + (ACC_PA_RH1004_PEQ_BAND$_001)-1)	//PEQ N
#define ACC_PA_RH1004_PEQ_BAND$_MAX							0x03	//PEQ Max : 4

/*************************************************************************/
/*	MTE						:Monitor Temp/Energy						*/
/*************************************************************************/

/*************************************************************************/
/*	FWVER					:Firmware Version							*/
/*************************************************************************/
//Common
//D1:
#define ACC_PA_ALL_FWVER_IDX$_AMP							0x01	//AMP MCU FW	
#define ACC_PA_ALL_FWVER_IDX$_LCM							0x02	//LCM MCU FW	
#define ACC_PA_ALL_FWVER_IDX$_CB							0x03	//Carry Board MCU FW	

/*************************************************************************/
/*	GEQ						:Graphic EQ Booster							*/
/*  Specific : RM12, M4, RH1004											*/
/*************************************************************************/
//Common
//D1: OPAGE
//D2: OPUT
//D3: CHL / CHR /  CH L/R
//D4:
#define ACC_PA_ALL_GEQ_MODE$_AMP							0x00	//AMP itself
#define ACC_PA_ALL_GEQ_MODE$_001							0x01	//Mode 1
#define ACC_PA_ALL_GEQ_MODE$_N(x)							( (x) + (ACC_PA_ALL_GEQ_MODE$_001 -1 )) //Mode N 
#define ACC_PA_ALL_GEQ_MODE$_MAX							0x64	//Mode Max (Temporarily defined)

//D5:
#define ACC_PA_ALL_GEQ_BAND$_001							0x00	//GEQ 1
#define ACC_PA_ALL_GEQ_BAND$_N(x)							( (x) + (ACC_PA_ALL_GEQ_BAND$_001)-1)	//GEQ N
#define ACC_PA_ALL_GEQ_BAND$_MAX							0x09	//GEQ Max : 10

//D6:
#define ACC_PA_ALL_GEQ_GAIN$_MIN							0x00	//-10 db
#define ACC_PA_ALL_GEQ_GAIN$_MAX							0x14	// 10 db

//---------------------------------------Specific-------------
//RM12
//D4:
#define ACC_PA_RM12_GEQ_MODE$_AMP							0x00	//AMP itself
#define ACC_PA_RM12_GEQ_MODE$_001							0x01	//Mode 1
#define ACC_PA_RM12_GEQ_MODE$_N(x)							( (x) + (ACC_PA_RM12_GEQ_MODE$_001 -1 )) //Mode N 
#define ACC_PA_RM12_GEQ_MODE$_MAX							0x64	//Mode 100


//RH1004
//D4:
#define ACC_PA_RH1004_GEQ_MODE$_FLAT							0x00	//Flat
#define ACC_PA_RH1004_GEQ_MODE$_PRESET_1						0x01	//Preset 1
#define ACC_PA_RH1004_GEQ_MODE$_PRESET_2						0x02	//Preset 2
#define ACC_PA_RH1004_GEQ_MODE$_PRESET_3						0x03	//Preset 3
#define ACC_PA_RH1004_GEQ_MODE$_USER_1							0x04	//User 1
#define ACC_PA_RH1004_GEQ_MODE$_USER_2							0x05	//User 2
#define ACC_PA_RH1004_GEQ_MODE$_MAX								0x05	//MAX

//M4
//D4:
#define ACC_PA_M4_GEQ_MODE$_FLAT								0x04	//Flat
#define ACC_PA_M4_GEQ_MODE$_PRESET_1							0x00	//Preset 1
#define ACC_PA_M4_GEQ_MODE$_PRESET_2							0x01	//Preset 2
#define ACC_PA_M4_GEQ_MODE$_PRESET_3							0x02	//Preset 3
#define ACC_PA_M4_GEQ_MODE$_USER								0x03	//User
#define ACC_PA_M4_GEQ_MODE$_USER_FLAT							0x05	//User_Flat 
#define ACC_PA_M4_GEQ_MODE$_MAX									0x06	//MAX

//D2.1 //J2.1
//D4:
#define ACC_PA_D21_GEQ_MODE$_PRESET_1							0x00	//Preset 1
#define ACC_PA_D21_GEQ_MODE$_PRESET_2							0x01	//Preset 2
#define ACC_PA_D21_GEQ_MODE$_PRESET_3							0x02	//Preset 3
#define ACC_PA_D21_GEQ_MODE$_USER								0x03	//User
#define ACC_PA_D21_GEQ_MODE$_FLAT								0x04	//Flat
#define ACC_PA_D21_GEQ_MODE$_USER_FLAT							0x05	//User_Flat 
#define ACC_PA_D21_GEQ_MODE$_MAX								0x06	//MAX



/*************************************************************************/
/*	ACCT					:Account number								*/
/*************************************************************************/
//Common
//D1: User
#define ACC_PA_ALL_ACCT_USER$_ROOT								0x00	//Root for Keiga
#define ACC_PA_ALL_ACCT_USER$_ADMIN						0x01	//Admin for SI
#define ACC_PA_ALL_ACCT_USER$_USER								0x02	//User for customer
#define ACC_PA_ALL_ACCT_USER$_ANY_USER							0xFF	//Any User
//D2: Type
#define ACC_PA_ALL_ACCT_TYPE$_CHECK_PASSWORD					0x00	//Check password
#define ACC_PA_ALL_ACCT_TYPE$_MODIFY_PASSWORD					0x01	//Modify password
#define ACC_PA_ALL_ACCT_TYPE$_CHANGE_PASSWORD_REQUEST			0x02	//Change password Request

//D3: Result
#define ACC_PA_ALL_ACCT_RESULT$_WRONG							0x00	//result of compare password is wrong.
#define ACC_PA_ALL_ACCT_RESULT$_CORRECT							0x01	//result of compare password is correct.

#define ACC_PA_ALL_ACCT_RESULT$_FAILURE							0x00	//result of modify password is failure.
#define ACC_PA_ALL_ACCT_RESULT$_SUCCESS							0x01	//result of modify password is successful.
//D3~D18 : Password 


/*************************************************************************/
/*	CARGO					:Cargo data							*/
/*************************************************************************/
//Common
//D1: Trading identiy
#define ACC_PA_ALL_CARGO_TID$_MAX								0xFF	//max of id

//D2: Sign
#define ACC_PA_ALL_CARGO_SIGN$_NO_SIGN							0x00	//No request signing packing (like udp)
#define ACC_PA_ALL_CARGO_SIGN$_REQ_SIGN							0x01	//Request signing (like tcp)

//D3: Length of Total package
//D4: Length of Total package

//D5: Address of memory of cargo
//D6: Address of memory of cargo
	
//D7~D?? : Data of cargoo 


/*************************************************************************/
/*	LINKAMP					:Link Amp.									*/
/*************************************************************************/


/*************************************************************************/
/*	LINUGEQ					:Link User Graphic EQ						*/ 
/*************************************************************************/

//RM12
//D1
#define ACC_PA_RM12_LINUGEQ_MODE$_001							0x01	//Mode 1
#define ACC_PA_RM12_LINUGEQ_MODE$_N(x)							( (x) + (ACC_PA_RM12_LINUGEQ_MODE$_001 -1 )) //Mode N 
#define ACC_PA_RM12_LINUGEQ_MODE$_MAX							0x64	//
#define ACC_PA_RM12_LINUGEQ_MODE$_O1							0x81
#define ACC_PA_RM12_LINUGEQ_MODE$_O2							0x82
#define ACC_PA_RM12_LINUGEQ_MODE$_O3							0x83
#define ACC_PA_RM12_LINUGEQ_MODE$_O4							0x84
#define ACC_PA_RM12_LINUGEQ_MODE$_O5							0x85
#define ACC_PA_RM12_LINUGEQ_MODE$_O6							0x86

/*************************************************************************/
/*	LINUPEQ					:Link User Parametric EQ					*/
/*************************************************************************/

//RM12
//D1
#define ACC_PA_RM12_LINUPEQ_MODE$_001							0x01	//Mode 1
#define ACC_PA_RM12_LINUPEQ_MODE$_N(x)							( (x) + (ACC_PA_RM12_LINUPEQ_MODE$_001 -1 )) //Mode N 
#define ACC_PA_RM12_LINUPEQ_MODE$_MAX							0x64	//
#define ACC_PA_RM12_LINUPEQ_MODE$_O1							0x81
#define ACC_PA_RM12_LINUPEQ_MODE$_O2							0x82
#define ACC_PA_RM12_LINUPEQ_MODE$_O3							0x83
#define ACC_PA_RM12_LINUPEQ_MODE$_O4							0x84
#define ACC_PA_RM12_LINUPEQ_MODE$_O5							0x85
#define ACC_PA_RM12_LINUPEQ_MODE$_O6							0x86



/*************************************************************************/
/*	LINALD					:Link All Data								 */
/*************************************************************************/

/*************************************************************************/
/*	DDGCB					:Deleted the data of GEQ on carry board		 */
/*************************************************************************/
#define ACC_PA_ALL_DDGCB_GMODE$_1					0x01	// GEQ Mode 1
#define ACC_PA_ALL_DDGCB_GMODE$_N(x)				((x) + ACC_PA_ALL_DDGCB_GMODE$_1-1)	// GEQ Mode N


/*************************************************************************/
/*	DDPCB					:Deleted the data of PEQ on carry board		  */
/*************************************************************************/
#define ACC_PA_ALL_DDPCB_PMODE$_1					0x01	// GEQ Mode 1
#define ACC_PA_ALL_DDPCB_PMODE$_N(x)				((x) + ACC_PA_ALL_DDPCB_PMODE$_1-1)	// PEQ Mode N

/*************************************************************************/
/*	RENAME					:Re-name									*/
/*************************************************************************/
//Common
//D1:
#define ACC_PA_ALL_RENAME_TYPE$_SERNUM						0x00	// Serial number
#define ACC_PA_ALL_RENAME_TYPE$_GEQM						0x10	// GEQ Mode
#define ACC_PA_ALL_RENAME_TYPE$_PEQM						0x11	// PEQ Mode
#define ACC_PA_ALL_RENAME_TYPE$_AREA						0x40	// Device
#define ACC_PA_ALL_RENAME_TYPE$_SRC							0x41	// Source  Type
#define ACC_PA_ALL_RENAME_TYPE$_LOC							0x42	// Speaker 
#define ACC_PA_ALL_RENAME_TYPE$_GRP							0x50	// Group
//D2:
#define ACC_PA_ALL_RENAME_IDX$_GEQM_1						0x01	//GEQ Mode 1
#define ACC_PA_ALL_RENAME_IDX$_GEQM_N(x)					((x)+ACC_PA_ALL_RENAME_IDX$_GEQM_1-1) //GEQ Mode 1

#define ACC_PA_ALL_RENAME_IDX$_PEQM_1						0x01	//PEQ Mode 1
#define ACC_PA_ALL_RENAME_IDX$_PEQM_N(x)					((x)+ACC_PA_ALL_RENAME_IDX$_PEQM_1-1) //PEQ Mode 1

#define ACC_PA_ALL_RENAME_IDX$_SRC_CH1_2						0x00	//Source CH1  / CH2
#define ACC_PA_ALL_RENAME_IDX$_SRC_CH3_4						0x01	//Source CH3  / CH4
#define ACC_PA_ALL_RENAME_IDX$_SRC_CH5_6						0x02	//Source CH5  / CH6
#define ACC_PA_ALL_RENAME_IDX$_SRC_CH7_8						0x03	//Source CH7  / CH8
#define ACC_PA_ALL_RENAME_IDX$_SRC_CH9_10						0x04	//Source CH9  / CH10
#define ACC_PA_ALL_RENAME_IDX$_SRC_CH11_12						0x05	//Source CH11 / CH12
#define ACC_PA_ALL_RENAME_IDX$_SRC_GB							0x06	//Source Golbal

#define ACC_PA_ALL_RENAME_IDX$_SRC_DIG							0xA0	//Source Digital
#define ACC_PA_ALL_RENAME_IDX$_SRC_DIFF							0xC0	//Source Differential

#define ACC_PA_ALL_RENAME_IDX$_LOC_CH1_2						0x00	//Speaker CH1  / CH2
#define ACC_PA_ALL_RENAME_IDX$_LOC_CH3_4						0x01	//Speaker CH3  / CH4
#define ACC_PA_ALL_RENAME_IDX$_LOC_CH5_6						0x02	//Speaker CH5  / CH6
#define ACC_PA_ALL_RENAME_IDX$_LOC_CH7_8						0x03	//Speaker CH7  / CH8
#define ACC_PA_ALL_RENAME_IDX$_LOC_CH9_10						0x04	//Speaker CH9  / CH10
#define ACC_PA_ALL_RENAME_IDX$_LOC_CH11_12						0x05	//Speaker CH11 / CH12

#define ACC_PA_ALL_RENAME_IDX$_GRP_1							0x00	//Group 1
#define ACC_PA_ALL_RENAME_IDX$_GRP_2							0x01	//Group 2
#define ACC_PA_ALL_RENAME_IDX$_GRP_3							0x02	//Group 3

//D3: Name[16]

/*************************************************************************/
/*	LOG						:LOG										 */
/*************************************************************************/

/*************************************************************************/
/*	LEC						:LOG Event Conditions										 */
/*************************************************************************/
//Common
//D1:
#define ACC_PA_ALL_LEC_TYPE$_CURRENT					0x00	// circuit current
#define ACC_PA_ALL_LEC_TYPE$_TEMP_DSP					0x08	// Temperature of DSP
#define ACC_PA_ALL_LEC_TYPE$_TEMP_XFMR					0x18	// Temperature of XFMR
#define ACC_PA_ALL_LEC_TYPE$_FAN_SPEED					0x20	// Fan Speed 


/*************************************************************************/
/*	CFS						: Control Firmware Storage									 */
/*************************************************************************/
//Common
//D1:
#define ACC_PA_ALL_CFS_TYPE$_MCU1						0x00	// Amplifier Firmware
#define ACC_PA_ALL_CFS_TYPE$_MCU2						0x01	// Storage Manager Firmware
#define ACC_PA_ALL_CFS_TYPE$_MCU3						0x02	// Dante Firmware
//D2:
#define ACC_PA_ALL_CFS_CMD$_CKSUM						0x00	// Firmware checksum
#define ACC_PA_ALL_CFS_CMD$_FW_VER						0x01	// Firmware version
#define ACC_PA_ALL_CFS_CMD$_RESUME						0x02	// Firmware resume
//D3~D6:
//byCMD: Get_CKSUM 
#define ACC_PA_ALL_CFS_DATA$_POS_CKSUM_H				0x00	// High byte position of Checksum on array	
#define ACC_PA_ALL_CFS_DATA$_POS_CKSUM_L				0x01	// Low byte position of Checksum on array
//byCMD: Get_FW_VER 
#define ACC_PA_ALL_CFS_DATA$_POS_FW_VER_YEAR			0x00	// Year position of firmware version on array	
#define ACC_PA_ALL_CFS_DATA$_POS_FW_VER_MON				0x01	// Month position of firmware version on array
#define ACC_PA_ALL_CFS_DATA$_POS_FW_VER_DAY				0x02	// Day position of firmware version on array
#define ACC_PA_ALL_CFS_DATA$_POS_FW_VER_VER				0x03	// Version position of firmware version on array

/*************************************************************************/
/*	SGFS						: Setting/Getting Firmware Storage									 */
/*************************************************************************/
//Common
//D1:
#define ACC_PA_ALL_SGFS_TYPE$_MCU1						ACC_PA_ALL_CFS_TYPE$_MCU1	// Amplifier Firmware
#define ACC_PA_ALL_SGFS_TYPE$_MCU2						ACC_PA_ALL_CFS_TYPE$_MCU2	// Storage Manager Firmware
#define ACC_PA_ALL_SGFS_TYPE$_MCU3						ACC_PA_ALL_CFS_TYPE$_MCU3	// Dante Firmware

//D2: SeqNumber_H: Sequence number High byte
//D3: SeqNumber_L: Sequence number Low byte
//D4: Length	 : Each is 32 bytes and the range is 1 to 255
//D5~n-1: Data[ length*32]
//D6: cksum

//note*: 
// To complete the upload ,do the following:
// complete send: [cmdFormat], [mcuType], 0xFF, 0xFF, 00, [ckSum] 	





/*************************************************************************/
/*************************************************************************/
/*****					   Create Struct							*****/
/*************************************************************************/
/*************************************************************************/
//Common Format
typedef struct __Channel_Format
{
	union
	{
		unsigned char Byte;
		struct {
			unsigned char bL : 1;				//Bit[0]
			unsigned char bR : 1;				//Bit[1]
			unsigned char bReserve1 : 1;		//Bit[2]
			unsigned char bReserve2 : 1;		//Bit[3]
			unsigned char bReserve3 : 1;		//Bit[4]
			unsigned char bReserve4 : 1;		//Bit[5]
			unsigned char bReserve5 : 1;		//Bit[6]
			unsigned char bReserve6 : 1;		//Bit[7]
		};
	};
}_Channel_Format, *_pChannel_Format;

typedef struct __EN_FREQ_Format
{
	union
	{
		unsigned char Byte;
		struct {
			unsigned char bFreq : 7;		 //Bit [0~6]
			unsigned char bEnable : 1;		 //Bit [7]
		};
	};
}_EN_FREQ_Format, *_pEN_FREQ_Format;

typedef struct __EN_Volume_Format
{
	union
	{
		unsigned char Byte;
		struct {
			unsigned char bVolume : 7;	//Bit [0~6]
			unsigned char bMute : 1;	//Bit [7]
		};
	};
}_EN_Volume_Format, *_pEN_Volume_Format;

typedef struct __Auto_Off_Format
{
	union
	{
		unsigned char Byte;
		struct {
			unsigned char bTime : 6;		//Bit [0~5]
			unsigned char bOn : 1;			//Bit [6]
			unsigned char bOff : 1;			//Bit [7]
		};
	};
}_Auto_Off_Format, *_pAuto_Off_Format;


//Command Format

typedef struct __COMMAND_LEAD
{
	unsigned char Customer;
	unsigned char Head;
	unsigned char Source;
	unsigned char Target;
	unsigned char Command;
	unsigned char Len_H;
	unsigned char Len_L;
}_Command_Lead, *_pComand_Lead;

typedef struct __Source_Page
{
	union
	{
		unsigned char Byte;
		struct {
			unsigned char bRCA_Page0 : 1;
			unsigned char bRCA_Page1 : 1;
			unsigned char bReserve1 : 1;
			unsigned char bReserve2 : 1;
			unsigned char bReserve3 : 1;
			unsigned char bWireless : 1;
			unsigned char bDD_Page : 1;//DBay & Dante
			unsigned char bOptical_Page : 1;
		};
	};
}_Source_Page, *_pSource_Page;

typedef struct __Source
{
	union
	{
		unsigned char Byte;
		struct {
			unsigned char bRCA1 : 1;
			unsigned char bRCA2 : 1;
			unsigned char bRCA3 : 1;
			unsigned char bRCA4 : 1;
			unsigned char bRCA5 : 1;
			unsigned char bRCA6 : 1;
			unsigned char bRCA7 : 1;
			unsigned char bRCA8 : 1;
		};
		struct {
			unsigned char bRCA9  : 1;
			unsigned char bRCA10 : 1;
			unsigned char bRCA11 : 1;
			unsigned char bRCA12 : 1;
			unsigned char bRCA13 : 1;
			unsigned char bRCA14 : 1;
			unsigned char bRCA15 : 1;
			unsigned char bRCA16 : 1;
		};
		struct {
			unsigned char bWireless1 : 1;
			unsigned char bWireless2 : 1;
			unsigned char bWireless3 : 1;
			unsigned char bWireless4 : 1;
			unsigned char bWireless5 : 1;
			unsigned char bWireless6 : 1;
			unsigned char bWireless7 : 1;
			unsigned char bWireless8 : 1;
		};
		struct {
			unsigned char bDante1 : 1;
			unsigned char bDante2 : 1;
			unsigned char bDante3 : 1;
			unsigned char bDante4 : 1;
			unsigned char bDBay1 : 1;
			unsigned char bDBay2 : 1;
			unsigned char bDBay3 : 1;
			unsigned char bDBay4 : 1;
		};
		struct {
			unsigned char bCoax1 : 1;
			unsigned char bCoax2 : 1;
			unsigned char bCoax3 : 1;
			unsigned char bCoax4 : 1;
			unsigned char bOptical1 : 1;
			unsigned char bOptical2 : 1;
			unsigned char bOptical3 : 1;
			unsigned char bOptical4 : 1;
		};
	};
}_Source, *_pSource;

typedef struct __Output_Page
{
	union
	{
		unsigned char Byte;
		struct {
			unsigned char bOut_Page0 : 1;
			unsigned char bOut_Page1 : 1;
			unsigned char bOut_Page2 : 1;
			unsigned char bOut_Page3 : 1;
			unsigned char bOut_Page4 : 1;
			unsigned char bOut_Page5 : 1;
			unsigned char bOut_Page6 : 1;
			unsigned char bSubwoofer_Page : 1;
		};
	};
}_Output_Page, *_pOutput_Page;

typedef struct __Output
{
	union
	{
		unsigned char Byte;
		struct {
			unsigned char bOutput1 : 1;
			unsigned char bOutput2 : 1;
			unsigned char bOutput3 : 1;
			unsigned char bOutput4 : 1;
			unsigned char bOutput5 : 1;
			unsigned char bOutput6 : 1;
			unsigned char bOutput7 : 1;
			unsigned char bOutput8 : 1;
		};
		struct {
			unsigned char bOutput9 : 1;
			unsigned char bOutput10 : 1;
			unsigned char bOutput11 : 1;
			unsigned char bOutput12 : 1;
			unsigned char bOutput13 : 1;
			unsigned char bOutput14 : 1;
			unsigned char bOutput15 : 1;
			unsigned char bOutput16 : 1;
		};
		struct {
			unsigned char bOutput17 : 1;
			unsigned char bOutput18 : 1;
			unsigned char bOutput19 : 1;
			unsigned char bOutput20 : 1;
			unsigned char bOutput21 : 1;
			unsigned char bOutput22 : 1;
			unsigned char bOutput23 : 1;
			unsigned char bOutput24 : 1;
		};
		struct {
			unsigned char bOutput25 : 1;
			unsigned char bOutput26 : 1;
			unsigned char bOutput27 : 1;
			unsigned char bOutput28 : 1;
			unsigned char bOutput29 : 1;
			unsigned char bOutput30 : 1;
			unsigned char bOutput31 : 1;
			unsigned char bOutput32 : 1;
		};
		struct {
			unsigned char bOutput33 : 1;
			unsigned char bOutput34 : 1;
			unsigned char bOutput35 : 1;
			unsigned char bOutput36 : 1;
			unsigned char bOutput37 : 1;
			unsigned char bOutput38 : 1;
			unsigned char bOutput39 : 1;
			unsigned char bOutput40 : 1;
		};
		struct {
			unsigned char bOutput41 : 1;
			unsigned char bOutput42 : 1;
			unsigned char bOutput43 : 1;
			unsigned char bOutput44 : 1;
			unsigned char bOutput45 : 1;
			unsigned char bOutput46 : 1;
			unsigned char bOutput47 : 1;
			unsigned char bOutput48 : 1;
		};
		struct {
			unsigned char bOutput49 : 1;
			unsigned char bOutput50 : 1;
			unsigned char bOutput51 : 1;
			unsigned char bOutput52 : 1;
			unsigned char bOutput53 : 1;
			unsigned char bOutput54 : 1;
			unsigned char bOutput55 : 1;
			unsigned char bOutput56 : 1;
		};
		struct {
			unsigned char bSubOut1 : 1;
			unsigned char bSubOut2 : 1;
			unsigned char bSubOut3 : 1;
			unsigned char bSubOut4 : 1;
			unsigned char bSubOut5 : 1;
			unsigned char bSubOut6 : 1;
			unsigned char bSubOut7 : 1;
			unsigned char bSubOut8 : 1;
		};
	};
}_Output, *_pOutput;


typedef struct __Data_MVOL
{
	_EN_Volume_Format Master;

	unsigned char Limit;
}_Data_MVOL, *_pData_MVOL;

typedef struct __Data_SRCVOL
{
	_Source_Page SourcePage;
	_Source Source;
	unsigned char VolumeL;
	unsigned char VolumeR;
}_Data_SRCVOL, *_pData_SRCVOL;
/*
typedef struct __Data_OUTVOL
{
	_Output_Page OutputPage;
	_Output Output;
	unsigned char VolumeL;
	unsigned char VolumeR;
	unsigned char MuteL;
	unsigned char MuteR;
}_Data_OUTVOL;
*/

typedef struct __Data_OUTVOL
{
	_Output_Page OutputPage;
	_Output Output;
	unsigned char VolumeL;
	unsigned char VolumeR;
	union
	{
		unsigned char Byte;
		struct {
			unsigned char Volume : 7;
			unsigned char Mute : 1;
		};
	}LMuteVolH;
	union
	{
		unsigned char Byte;
		struct {
			unsigned char Volume : 7;
			unsigned char Mute : 1;
		};
	}RMuteVolH;
}_Data_OUTVOL;

typedef struct __Data_SRCTOUT
{
	_Output_Page OutputPage;
	_Output Output;
	_Source_Page SourcePage;
	_Source Source;
}_Data_SRCTOUT, *_pData_SRCTOUT;

typedef struct __Data_GEQ
{
	_Output_Page OutputPage;
	_Output Output;
	_Channel_Format Channel;
	unsigned char Mode;
	unsigned char Index;
	unsigned char Gain;
}_Data_GEQ, *_pData_GEQ;

typedef struct __Data_PEQ
{
	_Output_Page OutputPage;
	_Output Output;
	_Channel_Format Channel;
	unsigned char Mode;
	unsigned char Index;
	unsigned char Type;
	unsigned char Gain;
	_EN_FREQ_Format EnFreqH;
	unsigned char FreqL;
	unsigned char Q_Factor;
	unsigned char ckSum;
}_Data_PEQ,*_pData_PEQ;

typedef struct __Data_HLPF
{
	_Output_Page OutputPage;
	_Output Output;
	_Channel_Format Channel;
	unsigned char Type;
	_EN_FREQ_Format EnFreqH;
	unsigned char FreqL;
	unsigned char Slope;
}_Data_HLPF;

typedef struct __Data_DTIME
{
	_Output_Page OutputPage;
	_Output Output;
	_Channel_Format Channel;
	unsigned char Delay;
}_Data_DTIME;

typedef struct __Data_PHSHT
{
	_Output_Page OutputPage;
	_Output Output;
	_Channel_Format Channel;
	unsigned char Degree;
}_Data_PHSHT;

typedef struct __Data_WUM
{
	unsigned char DC_Trigger;
	_Auto_Off_Format Auto;
	unsigned char Sensitivity;
}_Data_WUM;

typedef struct __Data_OUTLV
{
	_Output_Page OutputPage;
	_Output Output;
	unsigned char LV;
}_Data_OUTLV, *_pData_OUTLV;

typedef struct __Data_OUTM
{
	_Output_Page OutputPage;
	_Output Output;
	unsigned char Mode;
}_Data_OUTM, *_pData_OUTM;


typedef struct __Data_SRCPRY {
	union
	{
		struct {
			unsigned int PRY_LSB_BYTE : 8;
			unsigned int PRY_MSB_BYTE : 8;
		};
		struct {
			unsigned int bPRY5 : 3;				//Bit[0~2]
			unsigned int bPRY4 : 3;				//Bit[3~5]
			unsigned int bPRY3 : 3;				//Bit[6~8]
			unsigned int bPRY2 : 3;				//Bit[9~11]
			unsigned int bPRY1 : 3;				//Bit[12~14]
			unsigned int bEn : 1;				//Bit[15]
		};
	};
}_Data_SRCPRY;


typedef struct tagCommandLead
{
	union
	{
		unsigned char Byte[7];
		struct {
			unsigned char Customer;
			unsigned char Head;
			unsigned char Source;
			unsigned char Target;
			unsigned char Command;
			unsigned short Length;
		};
	}_;
}COMMANDLEAD, *PCOMMANDLEAD;

typedef struct tagSource_Page
{
	union
	{
		unsigned char Byte;
		struct {
			unsigned char RCA_Page0 : 1;
			unsigned char RCA_Page1 : 1;
			unsigned char Reserve1 : 1;
			unsigned char Reserve2 : 1;
			unsigned char Reserve3 : 1;
			unsigned char Wireless : 1;
			unsigned char DD_Page : 1;//DBay & Dante
			unsigned char Optical_Page : 1;
		};
	}_;
}SOURCEPAGE, *PSOURCEPAGE;

typedef struct tagSource
{
	union
	{
		unsigned char Byte;
		struct {
			unsigned char RCA1 : 1;
			unsigned char RCA2 : 1;
			unsigned char RCA3 : 1;
			unsigned char RCA4 : 1;
			unsigned char RCA5 : 1;
			unsigned char RCA6 : 1;
			unsigned char RCA7 : 1;
			unsigned char RCA8 : 1;
		};
		struct {
			unsigned char RCA9 : 1;
			unsigned char RCA10 : 1;
			unsigned char RCA11 : 1;
			unsigned char RCA12 : 1;
			unsigned char RCA13 : 1;
			unsigned char RCA14 : 1;
			unsigned char RCA15 : 1;
			unsigned char RCA16 : 1;
		};
		struct {
			unsigned char Wireless1 : 1;
			unsigned char Wireless2 : 1;
			unsigned char Wireless3 : 1;
			unsigned char Wireless4 : 1;
			unsigned char Wireless5 : 1;
			unsigned char Wireless6 : 1;
			unsigned char Wireless7 : 1;
			unsigned char Wireless8 : 1;
		};
		struct {
			unsigned char Dante1 : 1;
			unsigned char Dante2 : 1;
			unsigned char Dante3 : 1;
			unsigned char Dante4 : 1;
			unsigned char DBay1 : 1;
			unsigned char DBay2 : 1;
			unsigned char DBay3 : 1;
			unsigned char DBay4 : 1;
		};
		struct {
			unsigned char Coax1 : 1;
			unsigned char Coax2 : 1;
			unsigned char Coax3 : 1;
			unsigned char Coax4 : 1;
			unsigned char Optical1 : 1;
			unsigned char Optical2 : 1;
			unsigned char Optical3 : 1;
			unsigned char Optical4 : 1;
		};
	}_;
}SOURCE, *PSOURCE;

typedef struct tagOutputPage
{
	union
	{
		unsigned char Byte;
		struct {
			unsigned char Out_Page0 : 1;
			unsigned char Out_Page1 : 1;
			unsigned char Out_Page2 : 1;
			unsigned char Out_Page3 : 1;
			unsigned char Out_Page4 : 1;
			unsigned char Out_Page5 : 1;
			unsigned char Out_Page6 : 1;
			unsigned char Subwoofer_Page : 1;
		};
	}_;
}OUTPUTPAGE, *POUTPUTPAGE;

typedef struct tagOutput
{
	union
	{
		unsigned char Byte;
		struct {
			unsigned char Output1 : 1;
			unsigned char Output2 : 1;
			unsigned char Output3 : 1;
			unsigned char Output4 : 1;
			unsigned char Output5 : 1;
			unsigned char Output6 : 1;
			unsigned char Output7 : 1;
			unsigned char Output8 : 1;
		};
		struct {
			unsigned char Output9 : 1;
			unsigned char Output10 : 1;
			unsigned char Output11 : 1;
			unsigned char Output12 : 1;
			unsigned char Output13 : 1;
			unsigned char Output14 : 1;
			unsigned char Output15 : 1;
			unsigned char Output16 : 1;
		};
		struct {
			unsigned char Output17 : 1;
			unsigned char Output18 : 1;
			unsigned char Output19 : 1;
			unsigned char Output20 : 1;
			unsigned char Output21 : 1;
			unsigned char Output22 : 1;
			unsigned char Output23 : 1;
			unsigned char Output24 : 1;
		};
		struct {
			unsigned char Output25 : 1;
			unsigned char Output26 : 1;
			unsigned char Output27 : 1;
			unsigned char Output28 : 1;
			unsigned char Output29 : 1;
			unsigned char Output30 : 1;
			unsigned char Output31 : 1;
			unsigned char Output32 : 1;
		};
		struct {
			unsigned char Output33 : 1;
			unsigned char Output34 : 1;
			unsigned char Output35 : 1;
			unsigned char Output36 : 1;
			unsigned char Output37 : 1;
			unsigned char Output38 : 1;
			unsigned char Output39 : 1;
			unsigned char Output40 : 1;
		};
		struct {
			unsigned char Output41 : 1;
			unsigned char Output42 : 1;
			unsigned char Output43 : 1;
			unsigned char Output44 : 1;
			unsigned char Output45 : 1;
			unsigned char Output46 : 1;
			unsigned char Output47 : 1;
			unsigned char Output48 : 1;
		};
		struct {
			unsigned char Output49 : 1;
			unsigned char Output50 : 1;
			unsigned char Output51 : 1;
			unsigned char Output52 : 1;
			unsigned char Output53 : 1;
			unsigned char Output54 : 1;
			unsigned char Output55 : 1;
			unsigned char Output56 : 1;
		};
		struct {
			unsigned char SubOut1 : 1;
			unsigned char SubOut2 : 1;
			unsigned char SubOut3 : 1;
			unsigned char SubOut4 : 1;
			unsigned char SubOut5 : 1;
			unsigned char SubOut6 : 1;
			unsigned char SubOut7 : 1;
			unsigned char SubOut8 : 1;
		};
	}_;
}OUTPUT, *POUTPUT;

typedef struct tagDataMVOL
{
	union
	{
		unsigned char Byte;
		struct {
			unsigned char Volume : 7;
			unsigned char Mute : 1;
		};
	}Master;

	unsigned char Limit;

}DATAMVOL, *PDATAMVOL;

typedef struct tagDataSRCVOL
{
	SOURCEPAGE SourcePage;
	SOURCE Source;
	unsigned char VolumeL;
	unsigned char VolumeR;
}DATASRCVOL, *PDATASRCVOL;

typedef struct tagDataOUTVOL
{
	OUTPUTPAGE OutputPage;
	OUTPUT Output;
	unsigned char VolumeL;
	unsigned char VolumeR;
	union
	{
		unsigned char Byte;
		struct {
			unsigned char Volume : 7;
			unsigned char Mute : 1;
		};
	}LMuteVolH;
	union
	{
		unsigned char Byte;
		struct {
			unsigned char Volume : 7;
			unsigned char Mute : 1;
		};
	}RMuteVolH;
}DATAOUTVOL, *PDATAOUTVOL;

typedef struct tagDataSRCTOUT
{
	OUTPUTPAGE OutputPage;
	OUTPUT Output;
	SOURCEPAGE SourcePage;
	SOURCE Source;
}DATASRCTOUT, *PDATASRCOUT;

typedef struct tagDataGEQ
{
	OUTPUTPAGE OutputPage;
	OUTPUT Output;
	union
	{
		unsigned char Byte;
		struct {
			unsigned char L : 1;
			unsigned char R : 1;
			unsigned char Reserve1 : 1;
			unsigned char Reserve2 : 1;
			unsigned char Reserve3 : 1;
			unsigned char Reserve4 : 1;
			unsigned char Reserve5 : 1;
			unsigned char Reserve6 : 1;
		};
	}Channel;
	unsigned char Mode;
	unsigned char Index;
	unsigned char Gain;
}DATAGEQ, *PDATAGEQ;

typedef struct tagDataPEQ
{
	OUTPUTPAGE OutputPage;
	OUTPUT Output;
	union
	{
		unsigned char Byte;
		struct {
			unsigned char L : 1;
			unsigned char R : 1;
			unsigned char Reserve1 : 1;
			unsigned char Reserve2 : 1;
			unsigned char Reserve3 : 1;
			unsigned char Reserve4 : 1;
			unsigned char Reserve5 : 1;
			unsigned char Reserve6 : 1;
		};
	}Channel;
	unsigned char Mode;
	unsigned char Index;
	unsigned char Type;
	unsigned char Gain;
	union
	{
		unsigned char Byte;
		struct {
			unsigned char Frequency : 7;
			unsigned char Enable : 1;
		};
	}EnFreqH;
	unsigned char FreqL;
	unsigned char qH;
	unsigned char qL;
}DATAPEQ, *PDATAPEQ;

typedef struct tagDataHLPF
{
	OUTPUTPAGE OutputPage;
	OUTPUT Output;
	union
	{
		unsigned char Byte;
		struct {
			unsigned char L : 1;
			unsigned char R : 1;
			unsigned char Reserve1 : 1;
			unsigned char Reserve2 : 1;
			unsigned char Reserve3 : 1;
			unsigned char Reserve4 : 1;
			unsigned char Reserve5 : 1;
			unsigned char Reserve6 : 1;
		};
	}Channel;
	unsigned char Type;
	union
	{
		unsigned char Byte;
		struct {
			unsigned char Frequency : 7;
			unsigned char Enable : 1;
		};
	}EnFreqH;
	unsigned char FreqL;
	unsigned char Slope;
}DATAHLPF, *PDATAHLPF;

/*************************************************************************/
/*************************************************************************/
/*****					   Function									*****/
/*************************************************************************/
/*************************************************************************/
//Send function Call Back
typedef void(*Send_FUNC)(char *data, int len);
extern Send_FUNC ACC_SendHandlerFn;


//*****************************************************
//Check Sum
//*****************************************************
//Check Sum		Char Format
unsigned char cCheckSum(unsigned char* data, int len);
//Check Sum    Struct format
unsigned char stCheckSum(_pComand_Lead cmd, char *data);
//*****************************************************
//Transfer
//*****************************************************
// Transfer Acc Data to Ouput 
int Trans_AccToOutput(unsigned char opage, unsigned char oput);
// Transfer Ouput to Acc Data 
int Trans_OutputToAcc(unsigned char output);
/*****************************************************/
/*		SRCPRY										*/
/*****************************************************/
unsigned char SRCPRY_GET_Enable_U16(unsigned char msb, unsigned char lsb);
unsigned char SRCPRY_GET_Enable_U8(unsigned char msb);

//Ex: Setting enable
//SRCPRY_SET_Enable_U8(&MSB_byte, ACC_PA_ALL_SRCPRY_EN$_ON);
//Ex: Setting disable
//SRCPRY_SET_Enable_U8(&MSB_byte, ACC_PA_ALL_SRCPRY_EN$_OFF);
void SRCPRY_SET_Enable_U8(unsigned char *msb, unsigned char en);

//ex: Get Line from P3
//SRCPRY_GET_LineFromPry_U16(Byte_H,Byte_L, ACC_PA_ALL_SRCPRY_POS$_U16_P3 )
unsigned char SRCPRY_GET_LineFromPry_U16(unsigned char msb, unsigned char lsb, unsigned char pos_pry);

//ex: Set L3 to P2  
//SRCPRY_SET_LineToPry_U16( Byte_H, Byte_L,   ACC_PA_ALL_SRCPRY_POS$_U16_P2,  PA_ALL_SRCPRY_LINE$_L3 )
void SRCPRY_SET_LineToPry_U16(unsigned char *msb, unsigned char *lsb, unsigned char pos_pry, unsigned char line);


#endif _ACC_COMMAND_H_