#pragma once
#include "stdafx.h"

#define YES		1
#define NO		0

typedef struct tagCargo
{
	uint8_t pos_id;
	uint8_t pre_id;
	uint8_t pos_sign;
	uint8_t pos_totalLen;
	uint8_t pos_addrData;
	uint8_t pos_Data;
	DWORD timeout;
	int pos_addrBuffer;
	int data_len;
	int data_totalLen;
	uint8_t buff[1024];
	int buff_len;
	uint8_t crago[1024];
	int crago_count;

} CARGO, *PCARGO;

void dt_ReceiveCB(char *name, char *msg, int len);
boolean dt_Write_data(char* manufacturer, char* name, unsigned char* outputData, const unsigned int sizeBuffer);
void dt_Write_Split_data(char* manufacturer, char* name, unsigned char* outputData, const uint16_t sizeBuffer, const uint16_t maxSize);