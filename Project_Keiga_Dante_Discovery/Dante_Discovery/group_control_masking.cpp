#include "stdafx.h"
#include "group_control.h"
#include "ACC_Command.h"
#include "device_state.h"
#include "utils.h"

HWND hdlgGroupMasking = NULL;
static HWND hParent;
static HWND hEditHPFMasking, hEditLPFMasking;
static WNDPROC oldEditProcMasking, oldEditProcHPFMasking, oldEditProcLPFMasking;
static DATAGEQ masking_geq = { 0 };
static DATAPEQ masking_peq_b1 = { 0 };
static DATAPEQ masking_peq_b2 = { 0 };
static DATAPEQ masking_peq_b3 = { 0 };
static DATAPEQ masking_peq_b4 = { 0 };
static DATAPEQ masking_peq_b5 = { 0 };
static DATAPEQ masking_peq_b6 = { 0 };
static DATAPEQ masking_peq_b7 = { 0 };
static DATAPEQ masking_peq_b8 = { 0 };
static DATAPEQ masking_peq_b9 = { 0 };
static DATAPEQ masking_peq_b10 = { 0 };
static DATAPEQ masking_peq_b11 = { 0 };
static DATAPEQ masking_peq_b12 = { 0 };
static DATAPEQ masking_peq_b13 = { 0 };
static DATAPEQ masking_peq_b14 = { 0 };
static DATAPEQ masking_peq_b15 = { 0 };
static DATAPEQ masking_peq_b16 = { 0 };
static DATAPEQ masking_peq_b17 = { 0 };
static DATAPEQ masking_peq_b18 = { 0 };
static DATAHLPF masking_hpf = { 0 };
static DATAHLPF masking_lpf = { 0 };
static int radio_select = 0;
static int group_query_count = 0;
static unsigned char group_data[512] = { 0 };
static int group_data_length = 0;
void Change_G_Masking_Edit_Data(HWND hwnd);

LRESULT CALLBACK MaskingGEditProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_CHAR:
	{
		// Make sure we only allow specific characters
		if (!((wParam >= '0' && wParam <= '9')
			|| wParam == '.'
			|| wParam == '-'
			|| wParam == VK_RETURN
			|| wParam == VK_DELETE
			|| wParam == VK_BACK))
		{
			return 0;
		}
	}
	break;
	case WM_LBUTTONUP:
		SendMessage(hwnd, EM_SETSEL, 0, -1);
		break;
	case WM_KILLFOCUS:
		Change_G_Masking_Edit_Data(hwnd);
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_RETURN:
		case VK_TAB:
			Change_G_Masking_Edit_Data(hwnd);

			if (wParam == VK_RETURN)
				return 0;
		}
		break;

	}
	if (hwnd == hEditHPFMasking)
		return CallWindowProc(oldEditProcHPFMasking, hwnd, uMsg, wParam, lParam);
	else if (hwnd == hEditLPFMasking)
		return CallWindowProc(oldEditProcLPFMasking, hwnd, uMsg, wParam, lParam);
	return 0;
}


void Change_G_Masking_Edit_Data(HWND hwnd)
{
	char number[6];

	if (hwnd == hEditHPFMasking) {
		GetDlgItemTextA(hParent, IDC_EDIT_HP_F_MASKING, number, 6);
		int i = atoi(number);
		if (i < HIPASS_FERQUENCY_MIN) {
			i = HIPASS_FERQUENCY_MIN;
		}
		else if (i > HIPASS_FERQUENCY_MAX) {
			i = (int)HIPASS_FERQUENCY_MAX;
		}
		masking_hpf.OutputPage._.Byte = 1;
		masking_hpf.Output._.Byte = 3;
		masking_hpf.Channel.Byte = 1;
		masking_hpf.Type = FILTER_TYPE_HIPASS;
		masking_hpf.EnFreqH.Frequency = (unsigned char)(i / 256);
		masking_hpf.FreqL = (unsigned char)(i % 256);
		//		Set_Send_Flag(g_pDevice, COMMAND_HLPF, ACC_HEADER_WRITE, 7, masking_hlpf.OutputPage._.Byte, masking_hlpf.Output._.Byte, masking_hlpf.Channel.Byte, masking_hlpf.Type, masking_hlpf.EnFreqH, masking_hlpf.FreqL, masking_hlpf.Slope);
	}
	else if (hwnd == hEditLPFMasking) {
		GetDlgItemTextA(hParent, IDC_EDIT_LP_F_MASKING, number, 6);
		int i = atoi(number);
		if (i < LOPASS_FERQUENCY_MIN) {
			i = LOPASS_FERQUENCY_MIN;
		}
		else if (i > LOPASS_FERQUENCY_MAX) {
			i = (int)LOPASS_FERQUENCY_MAX;
		}

		masking_lpf.OutputPage._.Byte = 1;
		masking_lpf.Output._.Byte = 3;
		masking_lpf.Channel.Byte = 1;
		masking_lpf.Type = FILTER_TYPE_LOWPASS;
		masking_lpf.EnFreqH.Frequency = (unsigned char)(i / 256);
		masking_lpf.FreqL = (unsigned char)(i % 256);
		//Set_Send_Flag(g_pDevice, COMMAND_HLPF, ACC_HEADER_WRITE, 7, masking_hlpf.OutputPage._.Byte, masking_hlpf.Output._.Byte, masking_hlpf.Channel.Byte, masking_hlpf.Type, masking_hlpf.EnFreqH, masking_hlpf.FreqL, masking_hlpf.Slope);
	}
}


void Init_Group_Masking_UI(HWND hDlg)
{
	SendMessage(GetDlgItem(hDlg, IDC_RADIO1), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_RADIO2), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_RADIO3), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_RADIO4), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_RADIO5), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_RADIO6), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_RADIO7), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_RADIO8), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_RADIO9), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_RADIO10), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_RADIO11), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_RADIO12), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_RADIO13), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_RADIO14), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_RADIO15), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_RADIO16), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_RADIO17), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_RADIO18), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_RADIO19), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_RADIO20), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_RADIO21), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);


	if (radio_select == 0) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO1), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_MASKING), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON19), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_HP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_HP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON20), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_LP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_LP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_LP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON21), FALSE);
	}
	else if (radio_select == 1) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO2), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND1), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON1), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_MASKING), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON19), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_HP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_HP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON20), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_LP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_LP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_LP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON21), FALSE);
	}
	else if (radio_select == 2) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO3), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND2), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON2), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_MASKING), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON19), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_HP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_HP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON20), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_LP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_LP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_LP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON21), FALSE);
	}
	else if (radio_select == 3) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO4), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND3), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON3), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_MASKING), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON19), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_HP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_HP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON20), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_LP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_LP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_LP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON21), FALSE);
	}
	else if (radio_select == 4) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO5), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND4), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON4), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_MASKING), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON19), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_HP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_HP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON20), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_LP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_LP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_LP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON21), FALSE);
	}
	else if (radio_select == 5) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO6), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND5), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON5), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_MASKING), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON19), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_HP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_HP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON20), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_LP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_LP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_LP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON21), FALSE);
	}
	else if (radio_select == 6) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO7), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND6), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON6), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_MASKING), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON19), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_HP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_HP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON20), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_LP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_LP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_LP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON21), FALSE);
	}
	else if (radio_select == 7) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO8), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND7), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON7), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_MASKING), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON19), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_HP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_HP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON20), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_LP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_LP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_LP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON21), FALSE);
	}
	else if (radio_select == 8) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO9), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND8), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON8), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_MASKING), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON19), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_HP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_HP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON20), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_LP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_LP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_LP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON21), FALSE);
	}
	else if (radio_select == 9) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO10), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND9), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON9), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_MASKING), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON19), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_HP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_HP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON20), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_LP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_LP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_LP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON21), FALSE);
	}
	else if (radio_select == 10) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO11), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND10), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON10), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_MASKING), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON19), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_HP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_HP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON20), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_LP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_LP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_LP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON21), FALSE);
	}
	else if (radio_select == 11) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO12), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND11), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON11), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_MASKING), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON19), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_HP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_HP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON20), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_LP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_LP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_LP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON21), FALSE);
	}
	else if (radio_select == 12) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO13), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND12), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON12), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_MASKING), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON19), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_HP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_HP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON20), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_LP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_LP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_LP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON21), FALSE);
	}
	else if (radio_select == 13) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO14), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND13), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON13), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_MASKING), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON19), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_HP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_HP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON20), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_LP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_LP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_LP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON21), FALSE);
	}
	else if (radio_select == 14) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO15), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND14), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON14), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_MASKING), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON19), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_HP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_HP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON20), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_LP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_LP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_LP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON21), FALSE);
	}
	else if (radio_select == 15) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO16), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND15), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON15), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_MASKING), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON19), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_HP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_HP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON20), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_LP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_LP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_LP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON21), FALSE);
	}
	else if (radio_select == 16) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO17), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND16), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON16), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_MASKING), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON19), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_HP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_HP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON20), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_LP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_LP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_LP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON21), FALSE);
	}
	else if (radio_select == 17) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO18), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND17), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON17), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_MASKING), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON19), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_HP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_HP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON20), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_LP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_LP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_LP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON21), FALSE);
	}
	else if (radio_select == 18) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO19), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND18), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON18), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_MASKING), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON19), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_HP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_HP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON20), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_LP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_LP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_LP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON21), FALSE);
	}
	else if (radio_select == 19) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO20), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_MASKING), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON19), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_HP_F_MASKING2), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_MASKING2), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_HP_FILTER_MASKING2), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON20), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_LP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_LP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_LP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON21), FALSE);
	}
	else if (radio_select == 20) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO21), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON3), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON4), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON5), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON6), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON7), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON8), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON9), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON10), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON11), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON12), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON13), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON14), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON15), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON16), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON17), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON18), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_MASKING), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON19), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_HP_F_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_HP_FILTER_MASKING2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON20), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_LP_F_MASKING2), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_COMBO_LP_SLOPE_MASKING2), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_LP_FILTER_MASKING2), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON21), TRUE);
	}

	char *text = (char*)malloc(10 * sizeof(char));
	memset(text, 0, 10 * sizeof(char));
	//Filter
	SendMessage(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_MASKING2), CB_RESETCONTENT, 0, 0);
	SendMessage(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_MASKING2), (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)_T("6 dB/oct"));
	SendMessage(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_MASKING2), (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)_T("12 dB/oct"));
	SendMessage(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_MASKING2), CB_SETCURSEL, masking_hpf.Slope, 0);
	Set_Freq_Text(hDlg, IDC_EDIT_HP_F_MASKING2, (unsigned int)masking_hpf.EnFreqH.Frequency * 256 + masking_hpf.FreqL);
	SendMessage(GetDlgItem(hDlg, IDC_CHECK_HP_FILTER_MASKING2), BM_SETCHECK, (WPARAM)!masking_hpf.EnFreqH.Enable, (LPARAM)1);

	SendMessage(GetDlgItem(hDlg, IDC_COMBO_LP_SLOPE_MASKING2), CB_RESETCONTENT, 0, 0);
	SendMessage(GetDlgItem(hDlg, IDC_COMBO_LP_SLOPE_MASKING2), (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)_T("6 dB/oct"));
	SendMessage(GetDlgItem(hDlg, IDC_COMBO_LP_SLOPE_MASKING2), (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)_T("12 dB/oct"));
	SendMessage(GetDlgItem(hDlg, IDC_COMBO_LP_SLOPE_MASKING2), CB_SETCURSEL, masking_lpf.Slope, 0);
	Set_Freq_Text(hDlg, IDC_EDIT_LP_F_MASKING2, (unsigned int)masking_lpf.EnFreqH.Frequency * 256 + masking_lpf.FreqL);
	SendMessage(GetDlgItem(hDlg, IDC_CHECK_LP_FILTER_MASKING2), BM_SETCHECK, (WPARAM)!masking_lpf.EnFreqH.Enable, (LPARAM)1);

	//EQ
	SendMessage(GetDlgItem(hDlg, IDC_CHECK_MASKING), BM_SETCHECK, (WPARAM)!masking_geq.Mode, (LPARAM)1);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND1), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND1), TBM_SETPOS, (WPARAM)1, abs(masking_peq_b1.Gain - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND1, ((double)masking_peq_b1.Gain - 40) / 2);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND2), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND2), TBM_SETPOS, (WPARAM)1, abs(masking_peq_b2.Gain - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND2, ((double)masking_peq_b2.Gain - 40) / 2);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND3), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND3), TBM_SETPOS, (WPARAM)1, abs(masking_peq_b3.Gain - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND3, ((double)masking_peq_b3.Gain - 40) / 2);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND4), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND4), TBM_SETPOS, (WPARAM)1, abs(masking_peq_b4.Gain - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND4, ((double)masking_peq_b4.Gain - 40) / 2);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND5), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND5), TBM_SETPOS, (WPARAM)1, abs(masking_peq_b5.Gain - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND5, ((double)masking_peq_b5.Gain - 40) / 2);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND6), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND6), TBM_SETPOS, (WPARAM)1, abs(masking_peq_b6.Gain - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND6, ((double)masking_peq_b6.Gain - 40) / 2);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND7), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND7), TBM_SETPOS, (WPARAM)1, abs(masking_peq_b7.Gain - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND7, ((double)masking_peq_b7.Gain - 40) / 2);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND8), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND8), TBM_SETPOS, (WPARAM)1, abs(masking_peq_b8.Gain - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND8, ((double)masking_peq_b8.Gain - 40) / 2);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND9), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND9), TBM_SETPOS, (WPARAM)1, abs(masking_peq_b9.Gain - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND9, ((double)masking_peq_b9.Gain - 40) / 2);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND10), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND10), TBM_SETPOS, (WPARAM)1, abs(masking_peq_b10.Gain - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND10, ((double)masking_peq_b10.Gain - 40) / 2);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND11), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND11), TBM_SETPOS, (WPARAM)1, abs(masking_peq_b11.Gain - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND11, ((double)masking_peq_b11.Gain - 40) / 2);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND12), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND12), TBM_SETPOS, (WPARAM)1, abs(masking_peq_b12.Gain - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND12, ((double)masking_peq_b12.Gain - 40) / 2);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND13), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND13), TBM_SETPOS, (WPARAM)1, abs(masking_peq_b13.Gain - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND13, ((double)masking_peq_b13.Gain - 40) / 2);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND14), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND14), TBM_SETPOS, (WPARAM)1, abs(masking_peq_b14.Gain - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND14, ((double)masking_peq_b14.Gain - 40) / 2);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND15), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND15), TBM_SETPOS, (WPARAM)1, abs(masking_peq_b15.Gain - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND15, ((double)masking_peq_b15.Gain - 40) / 2);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND16), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND16), TBM_SETPOS, (WPARAM)1, abs(masking_peq_b16.Gain - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND16, ((double)masking_peq_b16.Gain - 40) / 2);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND17), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND17), TBM_SETPOS, (WPARAM)1, abs(masking_peq_b17.Gain - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND17, ((double)masking_peq_b17.Gain - 40) / 2);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND18), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND18), TBM_SETPOS, (WPARAM)1, abs(masking_peq_b18.Gain - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND18, ((double)masking_peq_b18.Gain - 40) / 2);
	free(text);
}


INT_PTR CALLBACK GroupMasking(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		radio_select = 0;
		Init_Group_Masking_UI(hDlg);

		hParent = hDlg;

		hEditHPFMasking = GetDlgItem(hDlg, IDC_EDIT_HP_F_MASKING);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_HP_F_MASKING), (UINT)EM_SETLIMITTEXT, (WPARAM)5, (LPARAM)0);
		oldEditProcHPFMasking = (WNDPROC)SetWindowLongPtr(hEditHPFMasking, GWLP_WNDPROC, (LONG_PTR)MaskingGEditProc);

		hEditLPFMasking = GetDlgItem(hDlg, IDC_EDIT_LP_F_MASKING);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_LP_F_MASKING), (UINT)EM_SETLIMITTEXT, (WPARAM)5, (LPARAM)0);
		oldEditProcLPFMasking = (WNDPROC)SetWindowLongPtr(hEditLPFMasking, GWLP_WNDPROC, (LONG_PTR)MaskingGEditProc);

		masking_geq.OutputPage._.Byte = 1;
		masking_geq.Output._.Byte = 1;
		masking_geq.Index = 0xFF;
		masking_geq.Gain = 0xFF;

		masking_peq_b1.OutputPage._.Byte = 1;
		masking_peq_b1.Output._.Byte = 1;
		masking_peq_b1.Index = 10;
		masking_peq_b1.EnFreqH.Frequency = 0;
		masking_peq_b1.FreqL = 160;
		masking_peq_b1.qH = 0x01;
		masking_peq_b1.qL = 0xAF;
		masking_peq_b1.EnFreqH.Enable = 1;
		masking_peq_b1.Channel.Byte = 1;

		masking_peq_b2.OutputPage._.Byte = 1;
		masking_peq_b2.Output._.Byte = 1;
		masking_peq_b2.Index = 11;
		masking_peq_b2.EnFreqH.Frequency = 0;
		masking_peq_b2.FreqL = 200;
		masking_peq_b2.qH = 0x01;
		masking_peq_b2.qL = 0xAF;
		masking_peq_b2.EnFreqH.Enable = 1;
		masking_peq_b2.Channel.Byte = 1;

		masking_peq_b3.OutputPage._.Byte = 1;
		masking_peq_b3.Output._.Byte = 1;
		masking_peq_b3.Index = 12;
		masking_peq_b3.EnFreqH.Frequency = 0;
		masking_peq_b3.FreqL = 250;
		masking_peq_b3.qH = 0x01;
		masking_peq_b3.qL = 0xAF;
		masking_peq_b3.EnFreqH.Enable = 1;
		masking_peq_b3.Channel.Byte = 1;

		masking_peq_b4.OutputPage._.Byte = 1;
		masking_peq_b4.Output._.Byte = 1;
		masking_peq_b4.Index = 13;
		masking_peq_b4.EnFreqH.Frequency = 315 / 256;
		masking_peq_b4.FreqL = 315 % 256;
		masking_peq_b4.qH = 0x01;
		masking_peq_b4.qL = 0xAF;
		masking_peq_b4.EnFreqH.Enable = 1;
		masking_peq_b4.Channel.Byte = 1;

		masking_peq_b5.OutputPage._.Byte = 1;
		masking_peq_b5.Output._.Byte = 1;
		masking_peq_b5.Index = 14;
		masking_peq_b5.EnFreqH.Frequency = 400 / 256;
		masking_peq_b5.FreqL = 400 % 256;
		masking_peq_b5.qH = 0x01;
		masking_peq_b5.qL = 0xAF;
		masking_peq_b5.EnFreqH.Enable = 1;
		masking_peq_b5.Channel.Byte = 1;

		masking_peq_b6.OutputPage._.Byte = 1;
		masking_peq_b6.Output._.Byte = 1;
		masking_peq_b6.Index = 15;
		masking_peq_b6.EnFreqH.Frequency = 500 / 256;
		masking_peq_b6.FreqL = 500 % 256;
		masking_peq_b6.qH = 0x01;
		masking_peq_b6.qL = 0xAF;
		masking_peq_b6.EnFreqH.Enable = 1;
		masking_peq_b6.Channel.Byte = 1;

		masking_peq_b7.OutputPage._.Byte = 1;
		masking_peq_b7.Output._.Byte = 1;
		masking_peq_b7.Index = 16;
		masking_peq_b7.EnFreqH.Frequency = 630 / 256;
		masking_peq_b7.FreqL = 630 % 256;
		masking_peq_b7.qH = 0x01;
		masking_peq_b7.qL = 0xAF;
		masking_peq_b7.EnFreqH.Enable = 1;
		masking_peq_b7.Channel.Byte = 1;

		masking_peq_b8.OutputPage._.Byte = 1;
		masking_peq_b8.Output._.Byte = 1;
		masking_peq_b8.Index = 17;
		masking_peq_b8.EnFreqH.Frequency = 800 / 256;
		masking_peq_b8.FreqL = 800 % 256;
		masking_peq_b8.qH = 0x01;
		masking_peq_b8.qL = 0xAF;
		masking_peq_b8.EnFreqH.Enable = 1;
		masking_peq_b8.Channel.Byte = 1;

		masking_peq_b9.OutputPage._.Byte = 1;
		masking_peq_b9.Output._.Byte = 1;
		masking_peq_b9.Index = 18;
		masking_peq_b9.EnFreqH.Frequency = 1000 / 256;
		masking_peq_b9.FreqL = 1000 % 256;
		masking_peq_b9.qH = 0x01;
		masking_peq_b9.qL = 0xAF;
		masking_peq_b9.EnFreqH.Enable = 1;
		masking_peq_b9.Channel.Byte = 1;

		masking_peq_b10.OutputPage._.Byte = 1;
		masking_peq_b10.Output._.Byte = 1;
		masking_peq_b10.Index = 19;
		masking_peq_b10.EnFreqH.Frequency = 1250 / 256;
		masking_peq_b10.FreqL = 1250 % 256;
		masking_peq_b10.qH = 0x01;
		masking_peq_b10.qL = 0xAF;
		masking_peq_b10.EnFreqH.Enable = 1;
		masking_peq_b10.Channel.Byte = 1;

		masking_peq_b11.OutputPage._.Byte = 1;
		masking_peq_b11.Output._.Byte = 1;
		masking_peq_b11.Index = 20;
		masking_peq_b11.EnFreqH.Frequency = 1600 / 256;
		masking_peq_b11.FreqL = 1600 % 256;
		masking_peq_b11.qH = 0x01;
		masking_peq_b11.qL = 0xAF;
		masking_peq_b11.EnFreqH.Enable = 1;
		masking_peq_b11.Channel.Byte = 1;

		masking_peq_b12.OutputPage._.Byte = 1;
		masking_peq_b12.Output._.Byte = 1;
		masking_peq_b12.Index = 21;
		masking_peq_b12.EnFreqH.Frequency = 2000 / 256;
		masking_peq_b12.FreqL = 2000 % 256;
		masking_peq_b12.qH = 0x01;
		masking_peq_b12.qL = 0xAF;
		masking_peq_b12.EnFreqH.Enable = 1;
		masking_peq_b12.Channel.Byte = 1;

		masking_peq_b13.OutputPage._.Byte = 1;
		masking_peq_b13.Output._.Byte = 1;
		masking_peq_b13.Index = 22;
		masking_peq_b13.EnFreqH.Frequency = 2500 / 256;
		masking_peq_b13.FreqL = 2500 % 256;
		masking_peq_b13.qH = 0x01;
		masking_peq_b13.qL = 0xAF;
		masking_peq_b13.EnFreqH.Enable = 1;
		masking_peq_b13.Channel.Byte = 1;

		masking_peq_b14.OutputPage._.Byte = 1;
		masking_peq_b14.Output._.Byte = 1;
		masking_peq_b14.Index = 23;
		masking_peq_b14.EnFreqH.Frequency = 3150 / 256;
		masking_peq_b14.FreqL = 3150 % 256;
		masking_peq_b14.qH = 0x01;
		masking_peq_b14.qL = 0xAF;
		masking_peq_b14.EnFreqH.Enable = 1;
		masking_peq_b14.Channel.Byte = 1;

		masking_peq_b15.OutputPage._.Byte = 1;
		masking_peq_b15.Output._.Byte = 1;
		masking_peq_b15.Index = 24;
		masking_peq_b15.EnFreqH.Frequency = 4000 / 256;
		masking_peq_b15.FreqL = 4000 % 256;
		masking_peq_b15.qH = 0x01;
		masking_peq_b15.qL = 0xAF;
		masking_peq_b15.EnFreqH.Enable = 1;
		masking_peq_b15.Channel.Byte = 1;

		masking_peq_b16.OutputPage._.Byte = 1;
		masking_peq_b16.Output._.Byte = 1;
		masking_peq_b16.Index = 25;
		masking_peq_b16.EnFreqH.Frequency = 5000 / 256;
		masking_peq_b16.FreqL = 5000 % 256;
		masking_peq_b16.qH = 0x01;
		masking_peq_b16.qL = 0xAF;
		masking_peq_b16.EnFreqH.Enable = 1;
		masking_peq_b16.Channel.Byte = 1;

		masking_peq_b17.OutputPage._.Byte = 1;
		masking_peq_b17.Output._.Byte = 1;
		masking_peq_b17.Index = 26;
		masking_peq_b17.EnFreqH.Frequency = 6300 / 256;
		masking_peq_b17.FreqL = 6300 % 256;
		masking_peq_b17.qH = 0x01;
		masking_peq_b17.qL = 0xAF;
		masking_peq_b17.EnFreqH.Enable = 1;
		masking_peq_b17.Channel.Byte = 1;

		masking_peq_b18.OutputPage._.Byte = 1;
		masking_peq_b18.Output._.Byte = 1;
		masking_peq_b18.Index = 27;
		masking_peq_b18.EnFreqH.Frequency = 8000 / 256;
		masking_peq_b18.FreqL = 8000 % 256;
		masking_peq_b18.qH = 0x01;
		masking_peq_b18.qL = 0xAF;
		masking_peq_b18.EnFreqH.Enable = 1;
		masking_peq_b18.Channel.Byte = 1;


		break;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDC_RADIO1:
			radio_select = 0;
			Init_Group_Masking_UI(hDlg);
			break;
		case IDC_RADIO2:
			radio_select = 1;
			Init_Group_Masking_UI(hDlg);
			break;
		case IDC_RADIO3:
			radio_select = 2;
			Init_Group_Masking_UI(hDlg);
			break;
		case IDC_RADIO4:
			radio_select = 3;
			Init_Group_Masking_UI(hDlg);
			break;
		case IDC_RADIO5:
			radio_select = 4;
			Init_Group_Masking_UI(hDlg);
			break;
		case IDC_RADIO6:
			radio_select = 5;
			Init_Group_Masking_UI(hDlg);
			break;
		case IDC_RADIO7:
			radio_select = 6;
			Init_Group_Masking_UI(hDlg);
			break;
		case IDC_RADIO8:
			radio_select = 7;
			Init_Group_Masking_UI(hDlg);
			break;
		case IDC_RADIO9:
			radio_select = 8;
			Init_Group_Masking_UI(hDlg);
			break;
		case IDC_RADIO10:
			radio_select = 9;
			Init_Group_Masking_UI(hDlg);
			break;
		case IDC_RADIO11:
			radio_select = 10;
			Init_Group_Masking_UI(hDlg);
			break;
		case IDC_RADIO12:
			radio_select = 11;
			Init_Group_Masking_UI(hDlg);
			break;
		case IDC_RADIO13:
			radio_select = 12;
			Init_Group_Masking_UI(hDlg);
			break;
		case IDC_RADIO14:
			radio_select = 13;
			Init_Group_Masking_UI(hDlg);
			break;
		case IDC_RADIO15:
			radio_select = 14;
			Init_Group_Masking_UI(hDlg);
			break;
		case IDC_RADIO16:
			radio_select = 15;
			Init_Group_Masking_UI(hDlg);
			break;
		case IDC_RADIO17:
			radio_select = 16;
			Init_Group_Masking_UI(hDlg);
			break;
		case IDC_RADIO18:
			radio_select = 17;
			Init_Group_Masking_UI(hDlg);
			break;
		case IDC_RADIO19:
			radio_select = 18;
			Init_Group_Masking_UI(hDlg);
			break;
		case IDC_RADIO20:
			radio_select = 19;
			Init_Group_Masking_UI(hDlg);
			break;
		case IDC_RADIO21:
			radio_select = 20;
			Init_Group_Masking_UI(hDlg);
			break;
		case IDC_CHECK_HP_FILTER_MASKING2:
			masking_hpf.OutputPage._.Byte = 1;
			masking_hpf.Output._.Byte = 3;
			masking_hpf.Channel.Byte = 1;
			masking_hpf.Type = FILTER_TYPE_HIPASS;
			masking_hpf.EnFreqH.Enable = !(unsigned char)SendMessageW((HWND)lParam, BM_GETCHECK, 0, 0);
			//Set_Send_Flag(g_pDevice, COMMAND_HLPF, ACC_HEADER_WRITE, 7, masking_hlpf.OutputPage._.Byte, masking_hlpf.Output._.Byte, masking_hlpf.Channel.Byte, masking_hlpf.Type, masking_hlpf.EnFreqH, masking_hlpf.FreqL, masking_hlpf.Slope);
			break;
		case IDC_CHECK_LP_FILTER_MASKING2:
			masking_lpf.OutputPage._.Byte = 1;
			masking_lpf.Output._.Byte = 3;
			masking_lpf.Channel.Byte = 1;
			masking_lpf.Type = FILTER_TYPE_LOWPASS;
			masking_lpf.EnFreqH.Enable = !(unsigned char)SendMessageW((HWND)lParam, BM_GETCHECK, 0, 0);
			//Set_Send_Flag(g_pDevice, COMMAND_HLPF, ACC_HEADER_WRITE, 7, masking_hlpf.OutputPage._.Byte, masking_hlpf.Output._.Byte, masking_hlpf.Channel.Byte, masking_hlpf.Type, masking_hlpf.EnFreqH, masking_hlpf.FreqL, masking_hlpf.Slope);
			break;
		case IDC_CHECK_MASKING:
			masking_geq.OutputPage._.Byte = 1;
			masking_geq.Output._.Byte = 1;
			masking_geq.Channel.Byte = 1;
			masking_geq.Mode = !(unsigned char)SendMessageW((HWND)lParam, BM_GETCHECK, 0, 0);
			masking_geq.Index = 0xFF;
			masking_geq.Gain = 0xFF;
			//Set_Send_Flag(g_pDevice, COMMAND_GEQ, ACC_HEADER_WRITE, 6, masking_geq.OutputPage._.Byte, masking_geq.Output._.Byte, masking_geq.Channel.Byte, masking_geq.Mode, masking_geq.Index, masking_geq.Gain);
			break;
		case IDC_COMBO_HP_SLOPE_MASKING2:
			if (HIWORD(wParam) == CBN_SELENDOK) {
				masking_hpf.OutputPage._.Byte = 1;
				masking_hpf.Output._.Byte = 3;
				masking_hpf.Channel.Byte = 1;
				masking_hpf.Type = FILTER_TYPE_HIPASS;
				masking_hpf.Slope = (unsigned char)SendMessage((HWND)lParam, (UINT)CB_GETCURSEL, (WPARAM)0, (LPARAM)0);
				//Set_Send_Flag(g_pDevice, COMMAND_HLPF, ACC_HEADER_WRITE, 7, masking_hlpf.OutputPage._.Byte, masking_hlpf.Output._.Byte, masking_hlpf.Channel.Byte, masking_hlpf.Type, masking_hlpf.EnFreqH, masking_hlpf.FreqL, masking_hlpf.Slope);
			}
			break;
		case IDC_COMBO_LP_SLOPE_MASKING2:
			if (HIWORD(wParam) == CBN_SELENDOK) {
				masking_lpf.OutputPage._.Byte = 1;
				masking_lpf.Output._.Byte = 3;
				masking_lpf.Channel.Byte = 1;
				masking_lpf.Type = FILTER_TYPE_LOWPASS;
				masking_lpf.Slope = (unsigned char)SendMessage((HWND)lParam, (UINT)CB_GETCURSEL, (WPARAM)0, (LPARAM)0);
				//Set_Send_Flag(g_pDevice, COMMAND_HLPF, ACC_HEADER_WRITE, 7, masking_hlpf.OutputPage._.Byte, masking_hlpf.Output._.Byte, masking_hlpf.Channel.Byte, masking_hlpf.Type, masking_hlpf.EnFreqH, masking_hlpf.FreqL, masking_hlpf.Slope);
			}
			break;
		case IDC_BUTTON1: {
			unsigned char data[19] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_PEQ,0,12,masking_peq_b1.OutputPage._.Byte, masking_peq_b1.Output._.Byte, masking_peq_b1.Channel.Byte, masking_peq_b1.Mode, masking_peq_b1.Index, masking_peq_b1.Type, masking_peq_b1.Gain, masking_peq_b1.EnFreqH.Byte, masking_peq_b1.FreqL, masking_peq_b1.qH, masking_peq_b1.qL,0 };
			strncpy((char*)group_data, (char*)data, 19);
			group_data_length = 19;
			send_group_command(data, 19);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
						  break;
		case IDC_BUTTON2: {
			unsigned char data[19] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_PEQ,0,12,masking_peq_b2.OutputPage._.Byte, masking_peq_b2.Output._.Byte, masking_peq_b2.Channel.Byte, masking_peq_b2.Mode, masking_peq_b2.Index, masking_peq_b2.Type, masking_peq_b2.Gain, masking_peq_b2.EnFreqH.Byte, masking_peq_b2.FreqL, masking_peq_b2.qH, masking_peq_b2.qL,0 };
			strncpy((char*)group_data, (char*)data, 19);
			group_data_length = 19;
			send_group_command(data, 19);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
						  break;
		case IDC_BUTTON3: {
			unsigned char data[19] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_PEQ,0,12,masking_peq_b3.OutputPage._.Byte, masking_peq_b3.Output._.Byte, masking_peq_b3.Channel.Byte, masking_peq_b3.Mode, masking_peq_b3.Index, masking_peq_b3.Type, masking_peq_b3.Gain, masking_peq_b3.EnFreqH.Byte, masking_peq_b3.FreqL, masking_peq_b3.qH, masking_peq_b3.qL,0 };
			strncpy((char*)group_data, (char*)data, 19);
			group_data_length = 19;
			send_group_command(data, 19);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
						  break;
		case IDC_BUTTON4: {
			unsigned char data[19] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_PEQ,0,12,masking_peq_b4.OutputPage._.Byte, masking_peq_b4.Output._.Byte, masking_peq_b4.Channel.Byte, masking_peq_b4.Mode, masking_peq_b4.Index, masking_peq_b4.Type, masking_peq_b4.Gain, masking_peq_b4.EnFreqH.Byte, masking_peq_b4.FreqL, masking_peq_b4.qH, masking_peq_b4.qL,0 };
			strncpy((char*)group_data, (char*)data, 19);
			group_data_length = 19;
			send_group_command(data, 19);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
						  break;
		case IDC_BUTTON5: {
			unsigned char data[19] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_PEQ,0,12,masking_peq_b5.OutputPage._.Byte, masking_peq_b5.Output._.Byte, masking_peq_b5.Channel.Byte, masking_peq_b5.Mode, masking_peq_b5.Index, masking_peq_b5.Type, masking_peq_b5.Gain, masking_peq_b5.EnFreqH.Byte, masking_peq_b5.FreqL, masking_peq_b5.qH, masking_peq_b5.qL,0 };
			strncpy((char*)group_data, (char*)data, 19);
			group_data_length = 19;
			send_group_command(data, 19);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
						  break;
		case IDC_BUTTON6: {
			unsigned char data[19] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_PEQ,0,12,masking_peq_b6.OutputPage._.Byte, masking_peq_b6.Output._.Byte, masking_peq_b6.Channel.Byte, masking_peq_b6.Mode, masking_peq_b6.Index, masking_peq_b6.Type, masking_peq_b6.Gain, masking_peq_b6.EnFreqH.Byte, masking_peq_b6.FreqL, masking_peq_b6.qH, masking_peq_b6.qL,0 };
			strncpy((char*)group_data, (char*)data, 19);
			group_data_length = 19;
			send_group_command(data, 19);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
						  break;
		case IDC_BUTTON7: {
			unsigned char data[19] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_PEQ,0,12,masking_peq_b7.OutputPage._.Byte, masking_peq_b7.Output._.Byte, masking_peq_b7.Channel.Byte, masking_peq_b7.Mode, masking_peq_b7.Index, masking_peq_b7.Type, masking_peq_b7.Gain, masking_peq_b7.EnFreqH.Byte, masking_peq_b7.FreqL, masking_peq_b7.qH, masking_peq_b7.qL,0 };
			strncpy((char*)group_data, (char*)data, 19);
			group_data_length = 19;
			send_group_command(data, 19);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
						  break;
		case IDC_BUTTON8: {
			unsigned char data[19] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_PEQ,0,12,masking_peq_b8.OutputPage._.Byte, masking_peq_b8.Output._.Byte, masking_peq_b8.Channel.Byte, masking_peq_b8.Mode, masking_peq_b8.Index, masking_peq_b8.Type, masking_peq_b8.Gain, masking_peq_b8.EnFreqH.Byte, masking_peq_b8.FreqL, masking_peq_b8.qH, masking_peq_b8.qL,0 };
			strncpy((char*)group_data, (char*)data, 19);
			group_data_length = 19;
			send_group_command(data, 19);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
						  break;
		case IDC_BUTTON9: {
			unsigned char data[19] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_PEQ,0,12,masking_peq_b9.OutputPage._.Byte, masking_peq_b9.Output._.Byte, masking_peq_b9.Channel.Byte, masking_peq_b9.Mode, masking_peq_b9.Index, masking_peq_b9.Type, masking_peq_b9.Gain, masking_peq_b9.EnFreqH.Byte, masking_peq_b9.FreqL, masking_peq_b9.qH, masking_peq_b9.qL,0 };
			strncpy((char*)group_data, (char*)data, 19);
			group_data_length = 19;
			send_group_command(data, 19);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
						  break;
		case IDC_BUTTON10: {
			unsigned char data[19] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_PEQ,0,12,masking_peq_b10.OutputPage._.Byte, masking_peq_b10.Output._.Byte, masking_peq_b10.Channel.Byte, masking_peq_b10.Mode, masking_peq_b10.Index, masking_peq_b10.Type, masking_peq_b10.Gain, masking_peq_b10.EnFreqH.Byte, masking_peq_b10.FreqL, masking_peq_b10.qH, masking_peq_b10.qL,0 };
			strncpy((char*)group_data, (char*)data, 19);
			group_data_length = 19;
			send_group_command(data, 19);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
						   break;
		case IDC_BUTTON11: {
			unsigned char data[19] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_PEQ,0,12,masking_peq_b11.OutputPage._.Byte, masking_peq_b11.Output._.Byte, masking_peq_b11.Channel.Byte, masking_peq_b11.Mode, masking_peq_b11.Index, masking_peq_b11.Type, masking_peq_b11.Gain, masking_peq_b11.EnFreqH.Byte, masking_peq_b11.FreqL, masking_peq_b11.qH, masking_peq_b11.qL,0 };
			strncpy((char*)group_data, (char*)data, 19);
			group_data_length = 19;
			send_group_command(data, 19);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
						   break;
		case IDC_BUTTON12: {
			unsigned char data[19] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_PEQ,0,12,masking_peq_b12.OutputPage._.Byte, masking_peq_b12.Output._.Byte, masking_peq_b12.Channel.Byte, masking_peq_b12.Mode, masking_peq_b12.Index, masking_peq_b12.Type, masking_peq_b12.Gain, masking_peq_b12.EnFreqH.Byte, masking_peq_b12.FreqL, masking_peq_b12.qH, masking_peq_b12.qL,0 };
			strncpy((char*)group_data, (char*)data, 19);
			group_data_length = 19;
			send_group_command(data, 19);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
						   break;
		case IDC_BUTTON13: {
			unsigned char data[19] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_PEQ,0,12,masking_peq_b13.OutputPage._.Byte, masking_peq_b13.Output._.Byte, masking_peq_b13.Channel.Byte, masking_peq_b13.Mode, masking_peq_b13.Index, masking_peq_b13.Type, masking_peq_b13.Gain, masking_peq_b13.EnFreqH.Byte, masking_peq_b13.FreqL, masking_peq_b13.qH, masking_peq_b13.qL,0 };
			strncpy((char*)group_data, (char*)data, 19);
			group_data_length = 19;
			send_group_command(data, 19);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
						   break;
		case IDC_BUTTON14: {
			unsigned char data[19] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_PEQ,0,12,masking_peq_b14.OutputPage._.Byte, masking_peq_b14.Output._.Byte, masking_peq_b14.Channel.Byte, masking_peq_b14.Mode, masking_peq_b14.Index, masking_peq_b14.Type, masking_peq_b14.Gain, masking_peq_b14.EnFreqH.Byte, masking_peq_b14.FreqL, masking_peq_b14.qH, masking_peq_b14.qL,0 };
			strncpy((char*)group_data, (char*)data, 19);
			group_data_length = 19;
			send_group_command(data, 19);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
						   break;
		case IDC_BUTTON15: {
			unsigned char data[19] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_PEQ,0,12,masking_peq_b15.OutputPage._.Byte, masking_peq_b15.Output._.Byte, masking_peq_b15.Channel.Byte, masking_peq_b15.Mode, masking_peq_b15.Index, masking_peq_b15.Type, masking_peq_b15.Gain, masking_peq_b15.EnFreqH.Byte, masking_peq_b15.FreqL, masking_peq_b15.qH, masking_peq_b15.qL,0 };
			strncpy((char*)group_data, (char*)data, 19);
			group_data_length = 19;
			send_group_command(data, 19);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
						   break;
		case IDC_BUTTON16: {
			unsigned char data[19] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_PEQ,0,12,masking_peq_b16.OutputPage._.Byte, masking_peq_b16.Output._.Byte, masking_peq_b16.Channel.Byte, masking_peq_b16.Mode, masking_peq_b16.Index, masking_peq_b16.Type, masking_peq_b16.Gain, masking_peq_b16.EnFreqH.Byte, masking_peq_b16.FreqL, masking_peq_b16.qH, masking_peq_b16.qL,0 };
			strncpy((char*)group_data, (char*)data, 19);
			group_data_length = 19;
			send_group_command(data, 19);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
						   break;
		case IDC_BUTTON17: {
			unsigned char data[19] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_PEQ,0,12,masking_peq_b17.OutputPage._.Byte, masking_peq_b17.Output._.Byte, masking_peq_b17.Channel.Byte, masking_peq_b17.Mode, masking_peq_b17.Index, masking_peq_b17.Type, masking_peq_b17.Gain, masking_peq_b17.EnFreqH.Byte, masking_peq_b17.FreqL, masking_peq_b17.qH, masking_peq_b17.qL,0 };
			strncpy((char*)group_data, (char*)data, 19);
			group_data_length = 19;
			send_group_command(data, 19);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
						   break;
		case IDC_BUTTON18: {
			unsigned char data[19] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_PEQ,0,12,masking_peq_b18.OutputPage._.Byte, masking_peq_b18.Output._.Byte, masking_peq_b18.Channel.Byte, masking_peq_b18.Mode, masking_peq_b18.Index, masking_peq_b18.Type, masking_peq_b18.Gain, masking_peq_b18.EnFreqH.Byte, masking_peq_b18.FreqL, masking_peq_b18.qH, masking_peq_b18.qL,0 };
			strncpy((char*)group_data, (char*)data, 19);
			group_data_length = 19;
			send_group_command(data, 19);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
						   break;
		case IDC_BUTTON19: {
			unsigned char data[14] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_GEQ,0,7,masking_geq.OutputPage._.Byte, masking_geq.Output._.Byte, masking_geq.Channel.Byte, masking_geq.Mode, masking_geq.Index, masking_geq.Gain,0 };
			strncpy((char*)group_data, (char*)data, 14);
			group_data_length = 14;
			send_group_command(data, 14);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
						   break;
		case IDC_BUTTON20: {
			unsigned char data[15] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_HLPF,0,8,masking_hpf.OutputPage._.Byte, masking_hpf.Output._.Byte, masking_hpf.Channel.Byte, masking_hpf.Type, masking_hpf.EnFreqH.Byte, masking_hpf.FreqL, masking_hpf.Slope,0 };
			strncpy((char*)group_data, (char*)data, 15);
			group_data_length = 15;
			send_group_command(data, 15);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
						   break;
		case IDC_BUTTON21: {
			unsigned char data[15] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_HLPF,0,8,masking_lpf.OutputPage._.Byte, masking_lpf.Output._.Byte, masking_lpf.Channel.Byte, masking_lpf.Type, masking_lpf.EnFreqH.Byte, masking_lpf.FreqL, masking_lpf.Slope,0 };
			strncpy((char*)group_data, (char*)data, 15);
			group_data_length = 15;
			send_group_command(data, 15);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
						   break;
		}
		break;
	case WM_VSCROLL:
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND1) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			masking_peq_b1.Gain = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND1), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND1, ((double)masking_peq_b1.Gain - 40) / 2);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND2) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			masking_peq_b2.Gain = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND2), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND2, ((double)masking_peq_b2.Gain - 40) / 2);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND3) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			masking_peq_b3.Gain = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND3), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND3, ((double)masking_peq_b3.Gain - 40) / 2);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND4) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			masking_peq_b4.Gain = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND4), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND4, ((double)masking_peq_b4.Gain - 40) / 2);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND5) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			masking_peq_b5.Gain = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND5), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND5, ((double)masking_peq_b5.Gain - 40) / 2);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND6) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			masking_peq_b6.Gain = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND6), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND6, ((double)masking_peq_b6.Gain - 40) / 2);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND7) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			masking_peq_b7.Gain = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND7), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND7, ((double)masking_peq_b7.Gain - 40) / 2);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND8) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			masking_peq_b8.Gain = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND8), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND8, ((double)masking_peq_b8.Gain - 40) / 2);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND9) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			masking_peq_b9.Gain = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND9), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND9, ((double)masking_peq_b9.Gain - 40) / 2);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND10) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			masking_peq_b10.Gain = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND10), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND10, ((double)masking_peq_b10.Gain - 40) / 2);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND11) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			masking_peq_b11.Gain = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND11), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND11, ((double)masking_peq_b11.Gain - 40) / 2);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND12) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			masking_peq_b12.Gain = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND12), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND12, ((double)masking_peq_b12.Gain - 40) / 2);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND13) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			masking_peq_b13.Gain = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND13), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND13, ((double)masking_peq_b13.Gain - 40) / 2);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND14) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			masking_peq_b14.Gain = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND14), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND14, ((double)masking_peq_b14.Gain - 40) / 2);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND15) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			masking_peq_b15.Gain = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND15), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND15, ((double)masking_peq_b15.Gain - 40) / 2);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND16) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			masking_peq_b16.Gain = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND16), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND16, ((double)masking_peq_b16.Gain - 40) / 2);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND17) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			masking_peq_b17.Gain = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND17), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND17, ((double)masking_peq_b17.Gain - 40) / 2);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND18) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			masking_peq_b18.Gain = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND18), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND18, ((double)masking_peq_b18.Gain - 40) / 2);
		}
		break;
	case WM_TIMER:
		switch (wParam) {
		case TIMER_QUERY_ACK:
		{
			group_query_count++;
			int iAck = 0;
			int iGroupChecked = 0;
			PDEVICESTATE p;
			p = g_dsHead;
			while (p) {
				if (p->check) {
					iGroupChecked++;
					if (p->ack == group_data[4]) {
						iAck++;
					}
					else {
						send_group_command(group_data, group_data_length);
					}
				}
				p = p->next_node;
			}
			char *text = (char*)malloc(10 * sizeof(char));
			sprintf(text, "%d%s%d", iAck, "/", iGroupChecked);

			if (group_query_count == 10) {
				if (iAck == iGroupChecked)
					sprintf(text, "%s", "Finished");
				else
					sprintf(text, "%s", "Timeout");
				KillTimer(hDlg, TIMER_QUERY_ACK);
			}
			SendMessage(GetDlgItem(hDlg, IDC_STATIC_GROUP_CMD_PROGRESS), WM_SETTEXT, (WPARAM)0, (LPARAM)text);
			free(text);
			break;
		}
		default:
			break;
		}
		break;
	case WM_CLOSE:
		DestroyWindow(hdlgGroupMasking);
		hdlgGroupMasking = NULL;
		break;
	case WM_DESTROY:
		hdlgGroupMasking = NULL;
		return (INT_PTR)TRUE;

	}

	DefWindowProc(hDlg, message, wParam, lParam);
}