#include "stdafx.h"
#include "group_control.h"
#include "ACC_Command.h"
#include "device_state.h"

HWND hdlgGroupLED = NULL;
static unsigned char led1_enable = 0;
static unsigned char led2_enable = 0;
static int radio_select = 0;
static int group_query_count = 0;
static unsigned char group_data[512] = { 0 };
static int group_data_length = 0;


void Init_Group_LED_UI(HWND hDlg)
{
	SendMessage(GetDlgItem(hDlg, IDC_RADIO1), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_RADIO2), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);


	if (radio_select == 0) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO1), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK1), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON1), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON2), FALSE);
	}
	else if (radio_select == 1) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO2), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK2), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON2), TRUE);
	}

	if (led1_enable > 0)
		SendMessage(GetDlgItem(hDlg, IDC_CHECK1), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
	else
		SendMessage(GetDlgItem(hDlg, IDC_CHECK1), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);

	if (led2_enable > 0)
		SendMessage(GetDlgItem(hDlg, IDC_CHECK2), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
	else
		SendMessage(GetDlgItem(hDlg, IDC_CHECK2), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
}

INT_PTR CALLBACK GroupLED(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		radio_select = 0;
		Init_Group_LED_UI(hDlg);
		break;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDC_RADIO1:
			radio_select = 0;
			Init_Group_LED_UI(hDlg);
			break;
		case IDC_RADIO2:
			radio_select = 1;
			Init_Group_LED_UI(hDlg);
			break;
		case IDC_CHECK1:
			if (SendMessage((HWND)lParam, BM_GETCHECK, 0, 0) == BST_CHECKED)
				led1_enable = 64;
			else
				led1_enable = 0;// 251;//大於250為Disable LED(after FW V21.4.15.1)
			break;
		case IDC_CHECK2:
			if (SendMessage((HWND)lParam, BM_GETCHECK, 0, 0) == BST_CHECKED)
				led2_enable = 64;
			else
				led2_enable = 0;// 251;//大於250為Disable LED(after FW V21.4.15.1)
			break;
		case IDC_BUTTON1: {
			unsigned char data[13] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_CHKSS,0,6,0xD0, 0x01, led1_enable, 0, 0,0 };
			strncpy((char*)group_data, (char*)data, 13);
			group_data_length = 13;
			send_group_command(data, 13);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
						  break;
		case IDC_BUTTON2: {
			unsigned char data[13] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_CHKSS,0,6,0xD0, 0x02, led2_enable, 0, 0,0 };
			strncpy((char*)group_data, (char*)data, 13);
			group_data_length = 13;
			send_group_command(data, 13);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
						  break;
		}
		break;
	case WM_TIMER:
		switch (wParam) {
		case TIMER_QUERY_ACK:
		{
			group_query_count++;
			int iAck = 0;
			int iGroupChecked = 0;
			PDEVICESTATE p;
			p = g_dsHead;
			while (p) {
				if (p->check) {
					iGroupChecked++;
					if (p->ack == group_data[4]) {
						iAck++;
					}
					else {
						send_group_command(group_data, group_data_length);
					}
				}
				p = p->next_node;
			}
			char *text = (char*)malloc(10 * sizeof(char));
			sprintf(text, "%d%s%d", iAck, "/", iGroupChecked);

			if (group_query_count == 10) {
				if (iAck == iGroupChecked)
					sprintf(text, "%s", "Finished");
				else
					sprintf(text, "%s", "Timeout");
				KillTimer(hDlg, TIMER_QUERY_ACK);
			}
			SendMessage(GetDlgItem(hDlg, IDC_STATIC_GROUP_CMD_PROGRESS), WM_SETTEXT, (WPARAM)0, (LPARAM)text);
			free(text);
			break;
		}
		default:
			break;
		}
		break;
	case WM_CLOSE:
		DestroyWindow(hdlgGroupLED);
		hdlgGroupLED = NULL;
		break;
	case WM_DESTROY:
		hdlgGroupLED = NULL;
		return (INT_PTR)TRUE;

	}

	DefWindowProc(hDlg, message, wParam, lParam);
}