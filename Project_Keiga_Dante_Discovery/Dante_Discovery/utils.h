#pragma once
#include "stdafx.h"
#define PI 3.14159
#define X_SHIFT 400
#define Y_SHIFT 0
#define FRAME_LEFT 30
#define FRAME_TOP 10
#define FRAME_RIGHT 590
#define FRAME_BOTTOM 260
#define DB_GRAD_LEN 7
#define DB_GRAD_RANGE 1
#define HZ_GRAD 3.375
#define Q 0.707
#define MIN_DB -25
#define MAX_DB 25
#define FILTER_MIN_DB -40
#define FILTER_MAX_DB 10
#define FRAME_WIDTH (FRAME_RIGHT-FRAME_LEFT)
#define FRAME_HEIGHT (FRAME_BOTTOM-FRAME_TOP)
#define TOTAL_DB (MAX_DB-MIN_DB)
#define DB_GRAD ((MAX_DB-MIN_DB)/DB_GRAD_RANGE)
#define DB_GAP ((FRAME_BOTTOM-FRAME_TOP)/((MAX_DB-MIN_DB)/DB_GRAD_RANGE))
//(323 - 80 = 243; 243 / ((24 + 30) / 2) = 9) gap最好是整數 dB刻度才能畫得準

#define HZ_GAP (FRAME_WIDTH/HZ_GRAD)
#define FONT_SIZE 14
#define MIN_FREQ 10
#define MAX_FREQ 23000//(pow(10,(HZ_GRAD + 1)))

#define POINT_R	10	//拉動點的半徑

#define FILTER_SLPOE_12				0
#define FILTER_SLPOE_18				1
#define FILTER_SLPOE_24				2
#define FILTER_SLPOE_30				3
#define FILTER_SLPOE_36				4

static const double freq_line[28] = { 20,30,40,50,60,70,80,90,100,200,300,400,500,600,700,800,900,1000,2000,3000,4000,5000,6000,7000,8000,9000,10000,20000 };

boolean cmpArray(unsigned char* a1, unsigned char* a2, int len);
int ascii_to_hex(char c);
unsigned char str_to_byte(char* c);
unsigned char str_to_dec(char* c);
int dB_to_Y(double dB, double max_db, double min_db, RECT rectFrame);
double Y_to_dB(int y, double max_db, double min_db, RECT rectFrame);
int Hz_to_X(double Hz, double max_hz, double min_hz, RECT rectFrame);
double X_to_Hz(int x, double max_hz, double min_hz, RECT rectFrame);
double First_Order_LowPass(double f0, double f);
double First_Order_HighPass(double f0, double f);
double Second_Order_LowPass(double f0, double f, int slope);
double Second_Order_HighPass(double f0, double f, int slope);
double Equalizer(double f0, double f, double gain, double q);
double dB_add_dB(double dB1, double dB2);
double dB_sub_dB(double dB1, double dB2);

boolean util_isSameStr_noSen(char* str1, char *str2);
boolean util_isContainStr_noSen(char* strSrc, char *strSub);

void CenterWindow(HWND hwnd);
void CenterInParentWindow(HWND hwnd, HWND hParent);

void Set_Freq_Text(HWND hwnd, unsigned int id, unsigned int freq);
void Set_Gain_Text(HWND hwnd, unsigned int id, double gain);
void Set_Gain_Text_Double(HWND hwnd, unsigned int id, double gain);
void Set_Q_Text(HWND hwnd, unsigned int id, double q);
void Set_Double2f_Text(HWND hwnd, unsigned int id, double q);
void Set_Int_Text(HWND hwnd, unsigned int id, int i);

char* read_file(wchar_t* filepath, long* lSize);
