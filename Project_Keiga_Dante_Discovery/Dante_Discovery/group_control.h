#pragma once
#include "stdafx.h"

#define VOLUME_MASTER_MIN 0
#define VOLUME_MASTER_MAX 50

#define VOLUME_CHANNEL_MIN 0
#define VOLUME_CHANNEL_MAX 500

#define VOLUME_MASKING_MAX	30//  -20db
#define VOLUME_MASKING_MIN	0	//  -50db

#define TIMER_QUERY_ACK 500

extern HWND hGroupLogin;
extern HWND hdlgGroupLevel;
extern HWND hdlgGroupDanteEQ;
extern HWND hdlgGroupMasking;
extern HWND hdlgGroupFindMe;
extern HWND hdlgGroupLED;
extern HWND hdlgGroupMasterLevel;

extern unsigned char gc_is_ch;//for identify data is ch1�Bch2 or others

INT_PTR CALLBACK GroupLoginProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam);

INT_PTR CALLBACK GroupLevel(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK GroupDanteEQ(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK GroupMasking(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK GroupFindMe(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK GroupLED(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);
INT_PTR CALLBACK GroupMasterLevel(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam);

void Set_G_ChSlider_Bar(HWND hwnd, unsigned int id, int progress);
void Set_G_ChSlider_Text(HWND hwnd, unsigned int id, int vol);
void send_group_command(unsigned char *cmd, unsigned int length);