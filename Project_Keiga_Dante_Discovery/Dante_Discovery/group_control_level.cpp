#include "stdafx.h"
#include "group_control.h"
#include "ACC_Command.h"
#include "device_state.h"

HWND hdlgGroupLevel = NULL;
static HWND hParent;
static HWND hEditMaster, hEditCh1, hEditCh2, hEditDante, hEditMasking;
static WNDPROC oldEditMaster, oldEditCh1, oldEditCh2, oldEditDante, oldEditMasking;
static _Data_MVOL data_mvol = { 0 };
static _Data_OUTVOL ch_volume = { 0 };
static _Data_OUTVOL dante_volume = { 0 };
static _Data_OUTVOL masking_volume = { 0 };
static bool ch2_invert;
static int radio_select = 0;
static int group_query_count = 0;
static unsigned char group_data[512] = { 0 };
static int group_data_length = 0;
void Change_G_Level_Edit_Data(HWND hwnd);


LRESULT CALLBACK LevelGEditProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_CHAR:
	{
		// Make sure we only allow specific characters
		if (!((wParam >= '0' && wParam <= '9')
			|| wParam == '.'
			|| wParam == '-'
			|| wParam == VK_RETURN
			|| wParam == VK_DELETE
			|| wParam == VK_BACK))
		{
			return 0;
		}
	}
	break;
	case WM_LBUTTONUP:
		SendMessage(hwnd, EM_SETSEL, 0, -1);
		break;
#ifdef EDIT_COLOR
	case WM_SETFOCUS:
	{
		for (int i = 0; i < 3; i++) {
			if (editcolor[i].hwnd == hwnd) {
				editcolor[i].color = 0xFF00;
				break;
			}
		}
	}
	break;
#endif
	case WM_KILLFOCUS:
		Change_G_Level_Edit_Data(hwnd);
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_RETURN:
		case VK_TAB:
			Change_G_Level_Edit_Data(hwnd);

			if (wParam == VK_RETURN)
				return 0;
		}
		break;

	}
	if (hwnd == hEditMaster)
		return CallWindowProc(oldEditMaster, hwnd, uMsg, wParam, lParam);
	if (hwnd == hEditCh1)
		return CallWindowProc(oldEditCh1, hwnd, uMsg, wParam, lParam);
	if (hwnd == hEditCh2)
		return CallWindowProc(oldEditCh2, hwnd, uMsg, wParam, lParam);
	if (hwnd == hEditDante)
		return CallWindowProc(oldEditDante, hwnd, uMsg, wParam, lParam);
	if (hwnd == hEditMasking)
		return CallWindowProc(oldEditMasking, hwnd, uMsg, wParam, lParam);
	return 0;
}

void Change_G_Level_Edit_Data(HWND hwnd)
{
	char number[6];

	if (hwnd == hEditMaster) {
		GetDlgItemTextA(hParent, IDC_EDIT_G_MASTER, number, 4);
		int i = atoi(number);
		if (i < -50) {
			i = -50;
			data_mvol.Master.bVolume = 0;
		}
		else if (i > 0) {
			i = 0;
			data_mvol.Master.bVolume = 50;
		}
		else {
			data_mvol.Master.bVolume = i + VOLUME_MASTER_MAX;
		}
		Set_G_ChSlider_Text(hParent, IDC_EDIT_G_MASTER, data_mvol.Master.bVolume);
		Set_G_ChSlider_Bar(hParent, IDC_SLIDER_G_MASTER, data_mvol.Master.bVolume);
	}
	else if (hwnd == hEditCh1) {
		GetDlgItemTextA(hParent, IDC_EDIT_G_CH1, number, 6);
		double i = atof(number);
		if (i < -50) {
			i = -50;
			ch_volume.LMuteVolH.Volume = 0;
			ch_volume.VolumeL = 0;
		}
		else if (i > 0) {
			i = 0;
			ch_volume.LMuteVolH.Volume = VOLUME_CHANNEL_MAX / 256;
			ch_volume.VolumeL = VOLUME_CHANNEL_MAX % 256;
		}
		else {
			ch_volume.LMuteVolH.Volume = ((int)(i * 10) + VOLUME_CHANNEL_MAX) / 256;
			ch_volume.VolumeL = ((int)(i * 10) + VOLUME_CHANNEL_MAX) % 256;
		}
		Set_G_ChSlider_Text(hParent, IDC_EDIT_G_CH1, ch_volume.LMuteVolH.Volume * 256 + ch_volume.VolumeL);
		Set_G_ChSlider_Bar(hParent, IDC_SLIDER_G_CH1, ch_volume.LMuteVolH.Volume * 256 + ch_volume.VolumeL);
	}
	else if (hwnd == hEditCh2) {
		GetDlgItemTextA(hParent, IDC_EDIT_G_CH2, number, 6);
		double i = atof(number);
		if (i < -50) {
			i = -50;
			ch_volume.RMuteVolH.Volume = 0;
			ch_volume.VolumeR = 0;
		}
		else if (i > 0) {
			i = 0;
			ch_volume.RMuteVolH.Volume = VOLUME_CHANNEL_MAX / 256;
			ch_volume.VolumeR = VOLUME_CHANNEL_MAX % 256;
		}
		else {
			ch_volume.RMuteVolH.Volume = ((int)(i * 10) + VOLUME_CHANNEL_MAX) / 256;
			ch_volume.VolumeR = ((int)(i * 10) + VOLUME_CHANNEL_MAX) % 256;
		}
		Set_G_ChSlider_Text(hParent, IDC_EDIT_G_CH2, ch_volume.RMuteVolH.Volume * 256 + ch_volume.VolumeR);
		Set_G_ChSlider_Bar(hParent, IDC_SLIDER_G_CH2, ch_volume.RMuteVolH.Volume * 256 + ch_volume.VolumeR);
	}
	else if (hwnd == hEditDante) {
		GetDlgItemTextA(hParent, IDC_EDIT_G_DANTE, number, 6);
		double i = atof(number);
		if (i < -50) {
			i = -50;
			dante_volume.LMuteVolH.Volume = 0;
			dante_volume.VolumeL = 0;
		}
		else if (i > 0) {
			i = 0;
			dante_volume.LMuteVolH.Volume = VOLUME_CHANNEL_MAX / 256;
			dante_volume.VolumeL = VOLUME_CHANNEL_MAX % 256;
		}
		else {
			dante_volume.LMuteVolH.Volume = ((int)(i * 10) + VOLUME_CHANNEL_MAX) / 256;
			dante_volume.VolumeL = ((int)(i * 10) + VOLUME_CHANNEL_MAX) % 256;
		}
		Set_G_ChSlider_Text(hParent, IDC_EDIT_G_DANTE, dante_volume.LMuteVolH.Volume * 256 + dante_volume.VolumeL);
		Set_G_ChSlider_Bar(hParent, IDC_SLIDER_G_DANTE, dante_volume.LMuteVolH.Volume * 256 + dante_volume.VolumeL);
	}
	else if (hwnd == hEditMasking) {
		GetDlgItemTextA(hParent, IDC_EDIT_G_MASKING, number, 6);
		double i = atof(number);
		if (i < -50) {
			i = -50;
			masking_volume.LMuteVolH.Volume = 0;
			masking_volume.VolumeL = 0;
		}
		else if (i > -20) {
			i = -20;
			masking_volume.LMuteVolH.Volume = 0;
			masking_volume.VolumeL = 30;
		}
		else {
			masking_volume.LMuteVolH.Volume = 0;
			masking_volume.VolumeL = i + 50;
		}
		Set_G_ChSlider_Text(hParent, IDC_EDIT_G_MASKING, masking_volume.VolumeL);
		Set_G_ChSlider_Bar(hParent, IDC_SLIDER_G_MASKING, masking_volume.VolumeL);
	}
}

void Init_Group_Level_UI(HWND hDlg)
{
	SendMessage(GetDlgItem(hDlg, IDC_RADIO1), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_RADIO2), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_RADIO3), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_RADIO4), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);
	SendMessage(GetDlgItem(hDlg, IDC_RADIO5), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);

	if (radio_select == 0) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO1), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_G_MUTE), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_G_MASTER), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_G_MASTER), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_RESET_MASTER), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_G_MUTE_CH1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_G_CH1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_G_CH1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_RESET_CH1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_G_MUTE_CH2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_INVERT_CH2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_G_CH2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_G_CH2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_RESET_CH2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_G_MUTE_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_G_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_G_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_RESET_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_G_MUTE_MASKING), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_G_MASKING), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_G_MASKING), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_RESET_MASKING), FALSE);
	}
	else if (radio_select == 1) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO2), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_G_MUTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_G_MASTER), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_G_MASTER), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_RESET_MASTER), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_G_MUTE_CH1), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_G_CH1), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_G_CH1), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_RESET_CH1), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_G_MUTE_CH2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_INVERT_CH2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_G_CH2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_G_CH2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_RESET_CH2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_G_MUTE_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_G_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_G_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_RESET_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_G_MUTE_MASKING), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_G_MASKING), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_G_MASKING), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_RESET_MASKING), FALSE);
	}
	else if (radio_select == 2) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO3), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_G_MUTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_G_MASTER), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_G_MASTER), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_RESET_MASTER), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_G_MUTE_CH1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_G_CH1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_G_CH1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_RESET_CH1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_G_MUTE_CH2), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_INVERT_CH2), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_G_CH2), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_G_CH2), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_RESET_CH2), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_G_MUTE_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_G_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_G_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_RESET_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_G_MUTE_MASKING), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_G_MASKING), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_G_MASKING), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_RESET_MASKING), FALSE);
	}
	else if (radio_select == 3) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO4), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_G_MUTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_G_MASTER), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_G_MASTER), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_RESET_MASTER), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_G_MUTE_CH1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_G_CH1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_G_CH1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_RESET_CH1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_G_MUTE_CH2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_INVERT_CH2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_G_CH2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_G_CH2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_RESET_CH2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_G_MUTE_DANTE), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_G_DANTE), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_G_DANTE), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_RESET_DANTE), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_G_MUTE_MASKING), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_G_MASKING), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_G_MASKING), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_RESET_MASKING), FALSE);
	}
	else if (radio_select == 4) {
		SendMessage(GetDlgItem(hDlg, IDC_RADIO5), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_G_MUTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_G_MASTER), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_G_MASTER), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_RESET_MASTER), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_G_MUTE_CH1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_G_CH1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_G_CH1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_RESET_CH1), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_G_MUTE_CH2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_INVERT_CH2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_G_CH2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_G_CH2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_RESET_CH2), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_G_MUTE_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_G_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_G_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_RESET_DANTE), FALSE);
		EnableWindow(GetDlgItem(hDlg, IDC_CHECK_G_MUTE_MASKING), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_SLIDER_G_MASKING), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_EDIT_G_MASKING), TRUE);
		EnableWindow(GetDlgItem(hDlg, IDC_BUTTON_RESET_MASKING), TRUE);
	}

	if (data_mvol.Master.bMute == 1)
		SendMessage(GetDlgItem(hDlg, IDC_CHECK_G_MUTE), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
	else
		SendMessage(GetDlgItem(hDlg, IDC_CHECK_G_MUTE), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);

	if (ch_volume.LMuteVolH.Mute == 1)
		SendMessage(GetDlgItem(hDlg, IDC_CHECK_G_MUTE_CH1), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
	else
		SendMessage(GetDlgItem(hDlg, IDC_CHECK_G_MUTE_CH1), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);

	if (ch_volume.RMuteVolH.Mute == 1)
		SendMessage(GetDlgItem(hDlg, IDC_CHECK_G_MUTE_CH2), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
	else
		SendMessage(GetDlgItem(hDlg, IDC_CHECK_G_MUTE_CH2), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);

	if (dante_volume.LMuteVolH.Mute == 1)
		SendMessage(GetDlgItem(hDlg, IDC_CHECK_G_MUTE_DANTE), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
	else
		SendMessage(GetDlgItem(hDlg, IDC_CHECK_G_MUTE_DANTE), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);

	if (masking_volume.LMuteVolH.Mute == 1)
		SendMessage(GetDlgItem(hDlg, IDC_CHECK_G_MUTE_MASKING), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
	else
		SendMessage(GetDlgItem(hDlg, IDC_CHECK_G_MUTE_MASKING), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);

	SendMessage(GetDlgItem(hDlg, IDC_CHECK_INVERT_CH2), BM_SETCHECK, (WPARAM)ch2_invert, (LPARAM)1);


	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_G_MASTER), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(VOLUME_MASTER_MIN, VOLUME_MASTER_MAX));
	Set_G_ChSlider_Bar(hDlg, IDC_SLIDER_G_MASTER, data_mvol.Master.bVolume);
	Set_G_ChSlider_Text(hDlg, IDC_EDIT_G_MASTER, data_mvol.Master.bVolume);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_G_CH1), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(VOLUME_CHANNEL_MIN, VOLUME_CHANNEL_MAX));
	Set_G_ChSlider_Bar(hDlg, IDC_SLIDER_G_CH1, ch_volume.LMuteVolH.Volume * 256 + ch_volume.VolumeL);
	Set_G_ChSlider_Text(hDlg, IDC_EDIT_G_CH1, ch_volume.LMuteVolH.Volume * 256 + ch_volume.VolumeL);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_G_CH2), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(VOLUME_CHANNEL_MIN, VOLUME_CHANNEL_MAX));
	Set_G_ChSlider_Bar(hDlg, IDC_SLIDER_G_CH2, ch_volume.RMuteVolH.Volume * 256 + ch_volume.VolumeR);
	Set_G_ChSlider_Text(hDlg, IDC_EDIT_G_CH2, ch_volume.RMuteVolH.Volume * 256 + ch_volume.VolumeR);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_G_DANTE), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(VOLUME_CHANNEL_MIN, VOLUME_CHANNEL_MAX));
	Set_G_ChSlider_Bar(hDlg, IDC_SLIDER_G_DANTE, dante_volume.LMuteVolH.Volume * 256 + dante_volume.VolumeL);
	Set_G_ChSlider_Text(hDlg, IDC_EDIT_G_DANTE, dante_volume.LMuteVolH.Volume * 256 + dante_volume.VolumeL);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_G_MASKING), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(VOLUME_MASKING_MIN, VOLUME_MASKING_MAX));
	Set_G_ChSlider_Bar(hDlg, IDC_SLIDER_G_MASKING, masking_volume.VolumeL);
	Set_G_ChSlider_Text(hDlg, IDC_EDIT_G_MASKING, masking_volume.VolumeL);

}



INT_PTR CALLBACK GroupLevel(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		hParent = hDlg;
		radio_select = 0;
		Init_Group_Level_UI(hDlg);

		hEditMaster = GetDlgItem(hDlg, IDC_EDIT_G_MASTER);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_G_MASTER), (UINT)EM_SETLIMITTEXT, (WPARAM)3, (LPARAM)0);
		oldEditMaster = (WNDPROC)SetWindowLongPtr(hEditMaster, GWLP_WNDPROC, (LONG_PTR)LevelGEditProc);

		hEditCh1 = GetDlgItem(hDlg, IDC_EDIT_G_CH1);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_G_CH1), (UINT)EM_SETLIMITTEXT, (WPARAM)5, (LPARAM)0);
		oldEditCh1 = (WNDPROC)SetWindowLongPtr(hEditCh1, GWLP_WNDPROC, (LONG_PTR)LevelGEditProc);

		hEditCh2 = GetDlgItem(hDlg, IDC_EDIT_G_CH2);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_G_CH2), (UINT)EM_SETLIMITTEXT, (WPARAM)5, (LPARAM)0);
		oldEditCh2 = (WNDPROC)SetWindowLongPtr(hEditCh2, GWLP_WNDPROC, (LONG_PTR)LevelGEditProc);

		hEditDante = GetDlgItem(hDlg, IDC_EDIT_G_DANTE);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_G_DANTE), (UINT)EM_SETLIMITTEXT, (WPARAM)5, (LPARAM)0);
		oldEditDante = (WNDPROC)SetWindowLongPtr(hEditDante, GWLP_WNDPROC, (LONG_PTR)LevelGEditProc);

		hEditMasking = GetDlgItem(hDlg, IDC_EDIT_G_MASKING);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_G_MASKING), (UINT)EM_SETLIMITTEXT, (WPARAM)5, (LPARAM)0);
		oldEditMasking = (WNDPROC)SetWindowLongPtr(hEditMasking, GWLP_WNDPROC, (LONG_PTR)LevelGEditProc);

		data_mvol.Limit = 0xFF;

		ch_volume.OutputPage.Byte = 1;
		ch_volume.Output.Byte = 1;
		ch_volume.LMuteVolH.Byte = 0xFF;
		ch_volume.VolumeL = 0xFF;
		ch_volume.RMuteVolH.Byte = 0xFF;
		ch_volume.VolumeR = 0xFF;

		dante_volume.OutputPage.Byte = 1;
		dante_volume.Output.Byte = 2;
		dante_volume.RMuteVolH.Byte = 0xFF;
		dante_volume.VolumeR = 0xFF;

		masking_volume.OutputPage.Byte = 1;
		masking_volume.Output.Byte = 3;
		masking_volume.LMuteVolH.Byte = 0;
		masking_volume.RMuteVolH.Byte = 0xFF;
		masking_volume.VolumeR = 0xFF;

		ch2_invert = FALSE;
		break;

	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDC_RADIO1:
			radio_select = 0;
			Init_Group_Level_UI(hDlg);
			break;
		case IDC_RADIO2:
			radio_select = 1;
			Init_Group_Level_UI(hDlg);
			break;
		case IDC_RADIO3:
			radio_select = 2;
			Init_Group_Level_UI(hDlg);
			break;
		case IDC_RADIO4:
			radio_select = 3;
			Init_Group_Level_UI(hDlg);
			break;
		case IDC_RADIO5:
			radio_select = 4;
			Init_Group_Level_UI(hDlg);
			break;
		case IDC_CHECK_G_MUTE:
			if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_G_MUTE), BM_GETCHECK, 0, 0) == BST_CHECKED)
				data_mvol.Master.bMute = 1;
			else
				data_mvol.Master.bMute = 0;
			break;
		case IDC_CHECK_G_MUTE_CH1:
			if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_G_MUTE_CH1), BM_GETCHECK, 0, 0) == BST_CHECKED)
				ch_volume.LMuteVolH.Mute = 1;
			else
				ch_volume.LMuteVolH.Mute = 0;
			break;
		case IDC_CHECK_G_MUTE_CH2:
			if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_G_MUTE_CH2), BM_GETCHECK, 0, 0) == BST_CHECKED)
				ch_volume.RMuteVolH.Mute = 1;
			else
				ch_volume.RMuteVolH.Mute = 0;
			break;
		case IDC_CHECK_G_MUTE_DANTE:
			if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_G_MUTE_DANTE), BM_GETCHECK, 0, 0) == BST_CHECKED)
				dante_volume.LMuteVolH.Mute = 1;
			else
				dante_volume.LMuteVolH.Mute = 0;
			break;
		case IDC_CHECK_G_MUTE_MASKING:
			if (SendMessage(GetDlgItem(hDlg, IDC_CHECK_G_MUTE_MASKING), BM_GETCHECK, 0, 0) == BST_CHECKED)
				masking_volume.LMuteVolH.Mute = 1;
			else
				masking_volume.LMuteVolH.Mute = 0;
			break;
		case IDC_CHECK_INVERT_CH2:
			ch2_invert = SendMessage(GetDlgItem(hDlg, IDC_CHECK_INVERT_CH2), BM_GETCHECK, 0, 0);
			break;
		case IDC_BUTTON_RESET_MASTER: {
			unsigned char data[10] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_MVOL,0,3,data_mvol.Master.Byte,0xFF,0 };
			strncpy((char*)group_data, (char*)data, 10);
			group_data_length = 10;
			send_group_command(data, 10);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
									  break;
		case IDC_BUTTON_RESET_CH1: {
			ch_volume.VolumeR = ch_volume.RMuteVolH.Byte = 0xFF;
			unsigned char data[14] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_OUTVOL,0,7,ch_volume.OutputPage.Byte, ch_volume.Output.Byte, ch_volume.VolumeL, ch_volume.VolumeR, ch_volume.LMuteVolH.Byte, ch_volume.RMuteVolH.Byte, 0 };
			strncpy((char*)group_data, (char*)data, 14);
			group_data_length = 14;
			gc_is_ch = 1;
			send_group_command(data, 14);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
								   break;
		case IDC_BUTTON_RESET_CH2: {
			ch_volume.VolumeL = ch_volume.LMuteVolH.Byte = 0xFF;
			unsigned char data[14] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_OUTVOL,0,7,ch_volume.OutputPage.Byte, ch_volume.Output.Byte, ch_volume.VolumeL, ch_volume.VolumeR, ch_volume.LMuteVolH.Byte, ch_volume.RMuteVolH.Byte, 0 };
			strncpy((char*)group_data, (char*)data, 14);
			group_data_length = 14;
			gc_is_ch = 2;
			send_group_command(data, 14);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
			unsigned char data2[12] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_PHSHF,0,5, 1, 1, 2, ch2_invert, 0 };
			send_group_command(data2, 12);
		}
								   break;
		case IDC_BUTTON_RESET_DANTE: {
			unsigned char data[14] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_OUTVOL,0,7,dante_volume.OutputPage.Byte, dante_volume.Output.Byte, dante_volume.VolumeL, dante_volume.VolumeR, dante_volume.LMuteVolH.Byte, dante_volume.RMuteVolH.Byte, 0 };
			strncpy((char*)group_data, (char*)data, 14);
			group_data_length = 14;
			send_group_command(data, 14);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
									 break;
		case IDC_BUTTON_RESET_MASKING: {
			unsigned char data[14] = { ACC_CUST_KEIGA,ACC_HEADER_WRITE,ACC_CMDER_COMMON,0,ACC_CMD_OUTVOL,0,7,masking_volume.OutputPage.Byte, masking_volume.Output.Byte, masking_volume.VolumeL, masking_volume.VolumeR, masking_volume.LMuteVolH.Byte, masking_volume.RMuteVolH.Byte, 0 };
			strncpy((char*)group_data, (char*)data, 14);
			group_data_length = 14;
			send_group_command(data, 14);
			group_query_count = 0;
			SetTimer(hDlg, TIMER_QUERY_ACK, TIMER_QUERY_ACK, NULL);
		}
									   break;
		}
		break;
	case WM_VSCROLL:

		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_G_MASTER) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			int value = SendMessage(GetDlgItem(hDlg, IDC_SLIDER_G_MASTER), TBM_GETPOS, 0, 0);

			data_mvol.Master.bVolume = abs(value - VOLUME_MASTER_MAX) + VOLUME_MASTER_MIN;

			Set_G_ChSlider_Bar(hDlg, IDC_SLIDER_G_MASTER, data_mvol.Master.bVolume);
			Set_G_ChSlider_Text(hDlg, IDC_EDIT_G_MASTER, data_mvol.Master.bVolume);
		}
		else if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_G_CH1) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			int value = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_G_CH1), TBM_GETPOS, 0, 0) - VOLUME_CHANNEL_MAX) + VOLUME_CHANNEL_MIN;

			ch_volume.LMuteVolH.Volume = value / 256;
			ch_volume.VolumeL = value % 256;

			char *text = (char*)malloc(10 * sizeof(char));
			memset(text, 0, 10 * sizeof(char));

			value = VOLUME_CHANNEL_MAX - value;

			if (value > 0) {
				sprintf(text, "%s%.1f", "-", (double)value / 10);
			}
			else {
				sprintf(text, "%.1f", (double)value / 10);
			}
			SendMessage(GetDlgItem(hDlg, IDC_EDIT_G_CH1), WM_SETTEXT, (WPARAM)0, (LPARAM)text);
			free(text);
		}
		else if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_G_CH2) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			int value = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_G_CH2), TBM_GETPOS, 0, 0) - VOLUME_CHANNEL_MAX) + VOLUME_CHANNEL_MIN;

			ch_volume.RMuteVolH.Volume = value / 256;
			ch_volume.VolumeR = value % 256;

			char *text = (char*)malloc(10 * sizeof(char));
			memset(text, 0, 10 * sizeof(char));

			value = VOLUME_CHANNEL_MAX - value;

			if (value > 0) {
				sprintf(text, "%s%.1f", "-", (double)value / 10);
			}
			else {
				sprintf(text, "%.1f", (double)value / 10);
			}
			SendMessage(GetDlgItem(hDlg, IDC_EDIT_G_CH2), WM_SETTEXT, (WPARAM)0, (LPARAM)text);
			free(text);
		}
		else if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_G_DANTE) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			int value = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_G_DANTE), TBM_GETPOS, 0, 0) - VOLUME_CHANNEL_MAX) + VOLUME_CHANNEL_MIN;

			dante_volume.LMuteVolH.Volume = value / 256;
			dante_volume.VolumeL = value % 256;

			char *text = (char*)malloc(10 * sizeof(char));
			memset(text, 0, 10 * sizeof(char));

			value = VOLUME_CHANNEL_MAX - value;

			if (value > 0) {
				sprintf(text, "%s%.1f", "-", (double)value / 10);
			}
			else {
				sprintf(text, "%.1f", (double)value / 10);
			}
			SendMessage(GetDlgItem(hDlg, IDC_EDIT_G_DANTE), WM_SETTEXT, (WPARAM)0, (LPARAM)text);
			free(text);
		}
		else if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_G_MASKING) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			masking_volume.VolumeL = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_G_MASKING), TBM_GETPOS, 0, 0) - VOLUME_MASKING_MAX) + VOLUME_MASKING_MIN;

			int value;
			char *text = (char*)malloc(10 * sizeof(char));
			memset(text, 0, 10 * sizeof(char));

			value = 50 - masking_volume.VolumeL;
			if (value > 0) {
				sprintf(text, "%s%d", "-", value);
			}
			else {
				sprintf(text, "%d", value);
			}
			SendMessage(GetDlgItem(hDlg, IDC_EDIT_G_MASKING), WM_SETTEXT, (WPARAM)0, (LPARAM)text);
			free(text);
		}
		break;
	case WM_TIMER:
		switch (wParam) {
		case TIMER_QUERY_ACK:
		{
			group_query_count++;
			int iAck = 0;
			int iGroupChecked = 0;
			PDEVICESTATE p;
			p = g_dsHead;
			while (p) {
				if (p->check) {
					iGroupChecked++;
					if (p->ack == group_data[4]) {
						iAck++;
					}
					else {
						send_group_command(group_data, group_data_length);
					}
				}
				p = p->next_node;
			}
			char *text = (char*)malloc(10 * sizeof(char));
			sprintf(text, "%d%s%d", iAck, "/", iGroupChecked);

			if (group_query_count == 10) {
				if (iAck == iGroupChecked)
					sprintf(text, "%s", "Finished");
				else
					sprintf(text, "%s", "Timeout");
				KillTimer(hDlg, TIMER_QUERY_ACK);
				gc_is_ch = 0;
			}
			SendMessage(GetDlgItem(hDlg, IDC_STATIC_GROUP_CMD_PROGRESS), WM_SETTEXT, (WPARAM)0, (LPARAM)text);
			free(text);
			break;
		}
		default:
			break;
		}
		break;
	case WM_CLOSE:
		DestroyWindow(hdlgGroupLevel);
		hdlgGroupLevel = NULL;
		break;
	case WM_DESTROY:
		hdlgGroupLevel = NULL;
		return (INT_PTR)TRUE;
	}

	DefWindowProc(hDlg, message, wParam, lParam);
}