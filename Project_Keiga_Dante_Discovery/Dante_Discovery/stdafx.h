// stdafx.h : 可在此標頭檔中包含標準的系統 Include 檔，
// 或是經常使用卻很少變更的
// 專案專用 Include 檔案
//

#pragma once

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // 從 Windows 標頭排除不常使用的成員
// Windows 標頭檔: 
#include <windows.h>

// C RunTime 標頭檔
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <stdint.h>

// TODO:  在此參考您的程式所需要的其他標頭
#include <comdef.h>
#include "gdiplus.h" 
using namespace Gdiplus;
#pragma comment(lib, "gdiplus.lib")
#include <CommCtrl.h>

#pragma comment(linker, \
  "\"/manifestdependency:type='Win32' "\
  "name='Microsoft.Windows.Common-Controls' "\
  "version='6.0.0.0' "\
  "processorArchitecture='*' "\
  "publicKeyToken='6595b64144ccf1df' "\
  "language='*'\"")

#pragma comment(lib, "ComCtl32.lib")
#include <direct.h>
#include <shellapi.h>
#pragma comment(lib, "Shell32.lib")

#include <commdlg.h>
#include <math.h>

#include "resource.h"

#define MAX_FILE_PATH 32767

extern boolean g_update;
extern boolean g_tx_done;
extern char g_currentPath[MAX_FILE_PATH];
extern char g_iniPath[MAX_FILE_PATH];