#include "stdafx.h"
#include "device_control.h"
#include "AL003M.h"
#include "ACC_Command.h"
#include "utils.h"

#define TIMER_SEND_CMD			300

#define DELAY_CMD_TYPE_VOL_MASKING	0
#define DELAY_CMD_TYPE_EQ_MASKING11	1
#define DELAY_CMD_TYPE_EQ_MASKING12	2
#define DELAY_CMD_TYPE_EQ_MASKING13	3
#define DELAY_CMD_TYPE_EQ_MASKING14	4
#define DELAY_CMD_TYPE_EQ_MASKING15	5
#define DELAY_CMD_TYPE_EQ_MASKING16	6
#define DELAY_CMD_TYPE_EQ_MASKING17	7
#define DELAY_CMD_TYPE_EQ_MASKING18	8
#define DELAY_CMD_TYPE_EQ_MASKING19	9
#define DELAY_CMD_TYPE_EQ_MASKING20	10
#define DELAY_CMD_TYPE_EQ_MASKING21	11
#define DELAY_CMD_TYPE_EQ_MASKING22	12
#define DELAY_CMD_TYPE_EQ_MASKING23	13
#define DELAY_CMD_TYPE_EQ_MASKING24	14
#define DELAY_CMD_TYPE_EQ_MASKING25	15
#define DELAY_CMD_TYPE_EQ_MASKING26	16
#define DELAY_CMD_TYPE_EQ_MASKING27	17
#define DELAY_CMD_TYPE_EQ_MASKING28	18

static struct tagDelaySendCmd {
	uint16_t type;
	int argc;
	int argv[20];
}mDelaySend;

static unsigned char gain, q;
static unsigned int freq;
static DATAPEQ data_peq;
static DATAHLPF data_hlpf;
static DATAOUTVOL data_vol;
static DATAGEQ data_geq;

#define PEQ_INDEX_START 10
#define PEQ_INDEX_STOP	27

void Init_Masking_UI(HWND hwnd);
void Change_Masking_Edit_Data(HWND hwnd);

static HWND hParent;
static HWND hEditMasking, hEditHPF, hEditLPF;
static WNDPROC oldEditProcMasking, oldEditProcHPF, oldEditProcLPF;


LRESULT CALLBACK MaskingEditProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	switch (uMsg)
	{
	case WM_CHAR:
	{
		// Make sure we only allow specific characters
		if (!((wParam >= '0' && wParam <= '9')
			|| wParam == '.'
			|| wParam == '-'
			|| wParam == VK_RETURN
			|| wParam == VK_DELETE
			|| wParam == VK_BACK))
		{
			return 0;
		}
	}
	break;
	case WM_LBUTTONUP:
		SendMessage(hwnd, EM_SETSEL, 0, -1);
		break;
	case WM_KILLFOCUS:
		if (TabCtrl_GetCurSel(g_hTabControl) == PAGE_MASKING)
			Change_Masking_Edit_Data(hwnd);
		break;
	case WM_KEYDOWN:
		switch (wParam)
		{
		case VK_RETURN:
		case VK_TAB:
			Change_Masking_Edit_Data(hwnd);

			if (wParam == VK_RETURN)
				return 0;
		}
		break;

	}
	if (hwnd == hEditMasking)
		return CallWindowProc(oldEditProcMasking, hwnd, uMsg, wParam, lParam);
	else if (hwnd == hEditHPF)
		return CallWindowProc(oldEditProcHPF, hwnd, uMsg, wParam, lParam);
	else if (hwnd == hEditLPF)
		return CallWindowProc(oldEditProcLPF, hwnd, uMsg, wParam, lParam);
	return 0;
}

INT_PTR CALLBACK MaskingProc(HWND hDlg, UINT uMsg, WPARAM wParam, LPARAM lParam)
{

	switch (uMsg)
	{
	case WM_INITDIALOG:
		Init_Masking_UI(hDlg);

		hParent = hDlg;

		hEditMasking = GetDlgItem(hDlg, IDC_EDIT_MASKING);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_MASKING), (UINT)EM_SETLIMITTEXT, (WPARAM)5, (LPARAM)0);
		oldEditProcMasking = (WNDPROC)SetWindowLongPtr(hEditMasking, GWLP_WNDPROC, (LONG_PTR)MaskingEditProc);

		hEditHPF = GetDlgItem(hDlg, IDC_EDIT_HP_F_MASKING);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_HP_F_MASKING), (UINT)EM_SETLIMITTEXT, (WPARAM)5, (LPARAM)0);
		oldEditProcHPF = (WNDPROC)SetWindowLongPtr(hEditHPF, GWLP_WNDPROC, (LONG_PTR)MaskingEditProc);

		hEditLPF = GetDlgItem(hDlg, IDC_EDIT_LP_F_MASKING);
		SendMessage(GetDlgItem(hDlg, IDC_EDIT_LP_F_MASKING), (UINT)EM_SETLIMITTEXT, (WPARAM)5, (LPARAM)0);
		oldEditProcLPF = (WNDPROC)SetWindowLongPtr(hEditLPF, GWLP_WNDPROC, (LONG_PTR)MaskingEditProc);

		break;
	case WM_COMMAND:
		switch (LOWORD(wParam))
		{
		case IDC_CHECK_MUTE_MASKING:
			g_pDevice->pa[AL003M_MUTE_MASKING] = (unsigned char)SendMessageW((HWND)lParam, BM_GETCHECK, 0, 0);
			data_vol.OutputPage._.Byte = 1;
			data_vol.Output._.Byte = 3;
			data_vol.VolumeR = 0xFF;
			data_vol.RMuteVolH.Byte = 0xFF;
			data_vol.VolumeL = 0xFF;
			data_vol.LMuteVolH.Volume = 0x7F;
			data_vol.LMuteVolH.Mute = g_pDevice->pa[AL003M_MUTE_MASKING];
			Set_Send_Flag(g_pDevice, ACC_CMD_OUTVOL, ACC_HEADER_WRITE, 6, data_vol.OutputPage._.Byte, data_vol.Output._.Byte, data_vol.VolumeL, data_vol.VolumeR, data_vol.LMuteVolH.Byte, data_vol.RMuteVolH.Byte);
			break;
		case IDC_CHECK_HP_FILTER_MASKING:
			g_pDevice->pa[AL003M_HIPASS_SWITCH_MASKING] = !(unsigned char)SendMessageW((HWND)lParam, BM_GETCHECK, 0, 0);
			data_hlpf.OutputPage._.Byte = 1;
			data_hlpf.Output._.Byte = 3;
			data_hlpf.Channel.Byte = 1;
			data_hlpf.Type = FILTER_TYPE_HIPASS;
			data_hlpf.EnFreqH.Frequency = 0x7F;
			data_hlpf.FreqL = 0xFF;
			data_hlpf.EnFreqH.Enable = g_pDevice->pa[AL003M_HIPASS_SWITCH_MASKING];
			data_hlpf.Slope = 0xFF;
			Set_Send_Flag(g_pDevice, ACC_CMD_HLPF, ACC_HEADER_WRITE, 7, data_hlpf.OutputPage._.Byte, data_hlpf.Output._.Byte, data_hlpf.Channel.Byte, data_hlpf.Type, data_hlpf.EnFreqH, data_hlpf.FreqL, data_hlpf.Slope);
			break;
		case IDC_CHECK_LP_FILTER_MASKING:
			g_pDevice->pa[AL003M_LOPASS_SWITCH_MASKING] = !(unsigned char)SendMessageW((HWND)lParam, BM_GETCHECK, 0, 0);
			data_hlpf.OutputPage._.Byte = 1;
			data_hlpf.Output._.Byte = 3;
			data_hlpf.Channel.Byte = 1;
			data_hlpf.Type = FILTER_TYPE_LOWPASS;
			data_hlpf.EnFreqH.Frequency = 0x7F;
			data_hlpf.FreqL = 0xFF;
			data_hlpf.EnFreqH.Enable = g_pDevice->pa[AL003M_LOPASS_SWITCH_MASKING];
			data_hlpf.Slope = 0xFF;
			Set_Send_Flag(g_pDevice, ACC_CMD_HLPF, ACC_HEADER_WRITE, 7, data_hlpf.OutputPage._.Byte, data_hlpf.Output._.Byte, data_hlpf.Channel.Byte, data_hlpf.Type, data_hlpf.EnFreqH, data_hlpf.FreqL, data_hlpf.Slope);
			break;
		case IDC_CHECK_MASKING:
			g_pDevice->pa[AL003M_SWITCH_MASKING_EQ] = !(unsigned char)SendMessageW((HWND)lParam, BM_GETCHECK, 0, 0);
			data_geq.OutputPage._.Byte = 1;
			data_geq.Output._.Byte = 1;
			data_geq.Channel.Byte = 1;
			data_geq.Mode = g_pDevice->pa[AL003M_SWITCH_MASKING_EQ];
			data_geq.Index = 0xFF;
			data_geq.Gain = 0xFF;
			Set_Send_Flag(g_pDevice, ACC_CMD_GEQ, ACC_HEADER_WRITE, 6, data_geq.OutputPage._.Byte, data_geq.Output._.Byte, data_geq.Channel.Byte, data_geq.Mode, data_geq.Index, data_geq.Gain);
			break;
		case IDC_COMBO_HP_SLOPE_MASKING:
			if (HIWORD(wParam) == CBN_SELENDOK) {
				g_pDevice->pa[AL003M_HIPASS_SLOPE_MASKING] = (unsigned char)SendMessage((HWND)lParam, (UINT)CB_GETCURSEL, (WPARAM)0, (LPARAM)0);
				data_vol.OutputPage._.Byte = 1;
				data_hlpf.Output._.Byte = 3;
				data_hlpf.Channel.Byte = 1;
				data_hlpf.Type = FILTER_TYPE_HIPASS;
				data_hlpf.EnFreqH.Frequency = 0x7F;
				data_hlpf.FreqL = 0xFF;
				data_hlpf.EnFreqH.Enable = g_pDevice->pa[AL003M_HIPASS_SWITCH_MASKING];
				data_hlpf.Slope = g_pDevice->pa[AL003M_HIPASS_SLOPE_MASKING];
				Set_Send_Flag(g_pDevice, ACC_CMD_HLPF, ACC_HEADER_WRITE, 7, data_hlpf.OutputPage._.Byte, data_hlpf.Output._.Byte, data_hlpf.Channel.Byte, data_hlpf.Type, data_hlpf.EnFreqH, data_hlpf.FreqL, data_hlpf.Slope);
			}
			break;
		case IDC_COMBO_LP_SLOPE_MASKING:
			if (HIWORD(wParam) == CBN_SELENDOK) {
				g_pDevice->pa[AL003M_LOPASS_SLOPE_MASKING] = (unsigned char)SendMessage((HWND)lParam, (UINT)CB_GETCURSEL, (WPARAM)0, (LPARAM)0);
				data_vol.OutputPage._.Byte = 1;
				data_hlpf.Output._.Byte = 3;
				data_hlpf.Channel.Byte = 1;
				data_hlpf.Type = FILTER_TYPE_LOWPASS;
				data_hlpf.EnFreqH.Frequency = 0x7F;
				data_hlpf.FreqL = 0xFF;
				data_hlpf.EnFreqH.Enable = g_pDevice->pa[AL003M_LOPASS_SWITCH_MASKING];
				data_hlpf.Slope = g_pDevice->pa[AL003M_LOPASS_SLOPE_MASKING];
				Set_Send_Flag(g_pDevice, ACC_CMD_HLPF, ACC_HEADER_WRITE, 7, data_hlpf.OutputPage._.Byte, data_hlpf.Output._.Byte, data_hlpf.Channel.Byte, data_hlpf.Type, data_hlpf.EnFreqH, data_hlpf.FreqL, data_hlpf.Slope);
			}
			break;
		}
		break;
	case WM_VSCROLL:
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			g_pDevice->pa[AL003M_VOL_MASKING_IN] = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING), TBM_GETPOS, 0, 0) - VOLUME_MASKING_MAX) + VOLUME_MASKING_MIN;

			int value;
			char *text = (char*)malloc(10 * sizeof(char));
			memset(text, 0, 10 * sizeof(char));

			sprintf(text, "%d", g_pDevice->pa[AL003M_VOL_MASKING_IN] - 50);
			/*
			value = 50 - g_pDevice->pa[AL003M_VOL_MASKING_IN];
			if (value > 0) {
				strcpy(text, _T("-"));
				sprintf(text + 1, _T("%d"), value);
			}
			else {
				sprintf(text, _T("%d"), value);
			}
			*/
			SendMessage(GetDlgItem(hDlg, IDC_EDIT_MASKING), WM_SETTEXT, (WPARAM)0, (LPARAM)text);
			free(text);

			KillTimer(g_hDeviceControl, TIME_LINK_AMP);
			mDelaySend.type = DELAY_CMD_TYPE_VOL_MASKING;
			KillTimer(hDlg, TIMER_SEND_CMD);
			SetTimer(hDlg, TIMER_SEND_CMD, TIMER_SEND_CMD, NULL);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND1) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			g_pDevice->pa[AL003M_C11_GAIN_MASKING] = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND1), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND1, ((double)g_pDevice->pa[AL003M_C11_GAIN_MASKING] - 40) / 2);
			KillTimer(g_hDeviceControl, TIME_LINK_AMP);
			mDelaySend.type = DELAY_CMD_TYPE_EQ_MASKING11;
			KillTimer(hDlg, TIMER_SEND_CMD);
			SetTimer(hDlg, TIMER_SEND_CMD, TIMER_SEND_CMD, NULL);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND2) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			g_pDevice->pa[AL003M_C12_GAIN_MASKING] = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND2), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND2, ((double)g_pDevice->pa[AL003M_C12_GAIN_MASKING] - 40) / 2);
			KillTimer(g_hDeviceControl, TIME_LINK_AMP);
			mDelaySend.type = DELAY_CMD_TYPE_EQ_MASKING12;
			KillTimer(hDlg, TIMER_SEND_CMD);
			SetTimer(hDlg, TIMER_SEND_CMD, TIMER_SEND_CMD, NULL);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND3) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			g_pDevice->pa[AL003M_C13_GAIN_MASKING] = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND3), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND3, ((double)g_pDevice->pa[AL003M_C13_GAIN_MASKING] - 40) / 2);
			KillTimer(g_hDeviceControl, TIME_LINK_AMP);
			mDelaySend.type = DELAY_CMD_TYPE_EQ_MASKING13;
			KillTimer(hDlg, TIMER_SEND_CMD);
			SetTimer(hDlg, TIMER_SEND_CMD, TIMER_SEND_CMD, NULL);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND4) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			g_pDevice->pa[AL003M_C14_GAIN_MASKING] = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND4), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND4, ((double)g_pDevice->pa[AL003M_C14_GAIN_MASKING] - 40) / 2);
			KillTimer(g_hDeviceControl, TIME_LINK_AMP);
			mDelaySend.type = DELAY_CMD_TYPE_EQ_MASKING14;
			KillTimer(hDlg, TIMER_SEND_CMD);
			SetTimer(hDlg, TIMER_SEND_CMD, TIMER_SEND_CMD, NULL);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND5) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			g_pDevice->pa[AL003M_C15_GAIN_MASKING] = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND5), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND5, ((double)g_pDevice->pa[AL003M_C15_GAIN_MASKING] - 40) / 2);
			KillTimer(g_hDeviceControl, TIME_LINK_AMP);
			mDelaySend.type = DELAY_CMD_TYPE_EQ_MASKING15;
			KillTimer(hDlg, TIMER_SEND_CMD);
			SetTimer(hDlg, TIMER_SEND_CMD, TIMER_SEND_CMD, NULL);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND6) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			g_pDevice->pa[AL003M_C16_GAIN_MASKING] = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND6), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND6, ((double)g_pDevice->pa[AL003M_C16_GAIN_MASKING] - 40) / 2);
			KillTimer(g_hDeviceControl, TIME_LINK_AMP);
			mDelaySend.type = DELAY_CMD_TYPE_EQ_MASKING16;
			KillTimer(hDlg, TIMER_SEND_CMD);
			SetTimer(hDlg, TIMER_SEND_CMD, TIMER_SEND_CMD, NULL);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND7) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			g_pDevice->pa[AL003M_C17_GAIN_MASKING] = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND7), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND7, ((double)g_pDevice->pa[AL003M_C17_GAIN_MASKING] - 40) / 2);
			KillTimer(g_hDeviceControl, TIME_LINK_AMP);
			mDelaySend.type = DELAY_CMD_TYPE_EQ_MASKING17;
			KillTimer(hDlg, TIMER_SEND_CMD);
			SetTimer(hDlg, TIMER_SEND_CMD, TIMER_SEND_CMD, NULL);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND8) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			g_pDevice->pa[AL003M_C18_GAIN_MASKING] = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND8), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND8, ((double)g_pDevice->pa[AL003M_C18_GAIN_MASKING] - 40) / 2);
			KillTimer(g_hDeviceControl, TIME_LINK_AMP);
			mDelaySend.type = DELAY_CMD_TYPE_EQ_MASKING18;
			KillTimer(hDlg, TIMER_SEND_CMD);
			SetTimer(hDlg, TIMER_SEND_CMD, TIMER_SEND_CMD, NULL);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND9) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			g_pDevice->pa[AL003M_C19_GAIN_MASKING] = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND9), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND9, ((double)g_pDevice->pa[AL003M_C19_GAIN_MASKING] - 40) / 2);
			KillTimer(g_hDeviceControl, TIME_LINK_AMP);
			mDelaySend.type = DELAY_CMD_TYPE_EQ_MASKING19;
			KillTimer(hDlg, TIMER_SEND_CMD);
			SetTimer(hDlg, TIMER_SEND_CMD, TIMER_SEND_CMD, NULL);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND10) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			g_pDevice->pa[AL003M_C20_GAIN_MASKING] = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND10), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND10, ((double)g_pDevice->pa[AL003M_C20_GAIN_MASKING] - 40) / 2);
			KillTimer(g_hDeviceControl, TIME_LINK_AMP);
			mDelaySend.type = DELAY_CMD_TYPE_EQ_MASKING20;
			KillTimer(hDlg, TIMER_SEND_CMD);
			SetTimer(hDlg, TIMER_SEND_CMD, TIMER_SEND_CMD, NULL);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND11) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			g_pDevice->pa[AL003M_C21_GAIN_MASKING] = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND11), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND11, ((double)g_pDevice->pa[AL003M_C21_GAIN_MASKING] - 40) / 2);
			KillTimer(g_hDeviceControl, TIME_LINK_AMP);
			mDelaySend.type = DELAY_CMD_TYPE_EQ_MASKING21;
			KillTimer(hDlg, TIMER_SEND_CMD);
			SetTimer(hDlg, TIMER_SEND_CMD, TIMER_SEND_CMD, NULL);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND12) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			g_pDevice->pa[AL003M_C22_GAIN_MASKING] = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND12), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND12, ((double)g_pDevice->pa[AL003M_C22_GAIN_MASKING] - 40) / 2);
			KillTimer(g_hDeviceControl, TIME_LINK_AMP);
			mDelaySend.type = DELAY_CMD_TYPE_EQ_MASKING22;
			KillTimer(hDlg, TIMER_SEND_CMD);
			SetTimer(hDlg, TIMER_SEND_CMD, TIMER_SEND_CMD, NULL);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND13) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			g_pDevice->pa[AL003M_C23_GAIN_MASKING] = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND13), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND13, ((double)g_pDevice->pa[AL003M_C23_GAIN_MASKING] - 40) / 2);
			KillTimer(g_hDeviceControl, TIME_LINK_AMP);
			mDelaySend.type = DELAY_CMD_TYPE_EQ_MASKING23;
			KillTimer(hDlg, TIMER_SEND_CMD);
			SetTimer(hDlg, TIMER_SEND_CMD, TIMER_SEND_CMD, NULL);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND14) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			g_pDevice->pa[AL003M_C24_GAIN_MASKING] = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND14), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND14, ((double)g_pDevice->pa[AL003M_C24_GAIN_MASKING] - 40) / 2);
			KillTimer(g_hDeviceControl, TIME_LINK_AMP);
			mDelaySend.type = DELAY_CMD_TYPE_EQ_MASKING24;
			KillTimer(hDlg, TIMER_SEND_CMD);
			SetTimer(hDlg, TIMER_SEND_CMD, TIMER_SEND_CMD, NULL);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND15) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			g_pDevice->pa[AL003M_C25_GAIN_MASKING] = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND15), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND15, ((double)g_pDevice->pa[AL003M_C25_GAIN_MASKING] - 40) / 2);
			KillTimer(g_hDeviceControl, TIME_LINK_AMP);
			mDelaySend.type = DELAY_CMD_TYPE_EQ_MASKING25;
			KillTimer(hDlg, TIMER_SEND_CMD);
			SetTimer(hDlg, TIMER_SEND_CMD, TIMER_SEND_CMD, NULL);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND16) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			g_pDevice->pa[AL003M_C26_GAIN_MASKING] = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND16), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND16, ((double)g_pDevice->pa[AL003M_C26_GAIN_MASKING] - 40) / 2);
			KillTimer(g_hDeviceControl, TIME_LINK_AMP);
			mDelaySend.type = DELAY_CMD_TYPE_EQ_MASKING26;
			KillTimer(hDlg, TIMER_SEND_CMD);
			SetTimer(hDlg, TIMER_SEND_CMD, TIMER_SEND_CMD, NULL);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND17) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			g_pDevice->pa[AL003M_C27_GAIN_MASKING] = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND17), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND17, ((double)g_pDevice->pa[AL003M_C27_GAIN_MASKING] - 40) / 2);
			KillTimer(g_hDeviceControl, TIME_LINK_AMP);
			mDelaySend.type = DELAY_CMD_TYPE_EQ_MASKING27;
			KillTimer(hDlg, TIMER_SEND_CMD);
			SetTimer(hDlg, TIMER_SEND_CMD, TIMER_SEND_CMD, NULL);
		}
		if ((HWND)lParam == GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND18) && (LOWORD(wParam) >= SB_LINEUP && LOWORD(wParam) <= SB_THUMBTRACK)) {
			g_pDevice->pa[AL003M_C28_GAIN_MASKING] = abs(SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND18), TBM_GETPOS, 0, 0) - DB_MASKING_MAX) + DB_MASKING_MIN;
			Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND18, ((double)g_pDevice->pa[AL003M_C28_GAIN_MASKING] - 40) / 2);
			KillTimer(g_hDeviceControl, TIME_LINK_AMP);
			mDelaySend.type = DELAY_CMD_TYPE_EQ_MASKING28;
			KillTimer(hDlg, TIMER_SEND_CMD);
			SetTimer(hDlg, TIMER_SEND_CMD, TIMER_SEND_CMD, NULL);
		}
		break;
	case WM_TIMER: {
		switch (wParam) {
		case TIMER_SEND_CMD: {
			switch (mDelaySend.type) {
			case DELAY_CMD_TYPE_VOL_MASKING: {
				data_vol.OutputPage._.Byte = 1;
				data_vol.Output._.Byte = 3;
				data_vol.VolumeR = 0xFF;
				data_vol.RMuteVolH.Byte = 0xFF;
				data_vol.VolumeL = g_pDevice->pa[AL003M_VOL_MASKING_IN];
				data_vol.LMuteVolH.Mute = g_pDevice->pa[AL003M_MUTE_MASKING];
				Set_Send_Flag(g_pDevice, ACC_CMD_OUTVOL, ACC_HEADER_WRITE, 6, data_vol.OutputPage._.Byte, data_vol.Output._.Byte, data_vol.VolumeL, data_vol.VolumeR, data_vol.LMuteVolH.Byte, data_vol.RMuteVolH.Byte);
				break; }
			case DELAY_CMD_TYPE_EQ_MASKING11: {
				data_peq.OutputPage._.Byte = 1;
				data_peq.Output._.Byte = 1;
				data_peq.Index = 10;
				data_peq.EnFreqH.Frequency = 0;
				data_peq.FreqL = 160;
				data_peq.Gain = g_pDevice->pa[AL003M_C11_GAIN_MASKING];
				data_peq.qH = 0x01;
				data_peq.qL = 0xAF;
				data_peq.EnFreqH.Enable = 1;
				data_peq.Channel.Byte = 1;
				Set_Send_Flag(g_pDevice, ACC_CMD_PEQ, ACC_HEADER_WRITE, 11, data_peq.OutputPage._.Byte, data_peq.Output._.Byte, data_peq.Channel.Byte, data_peq.Mode, data_peq.Index, data_peq.Type, data_peq.Gain, data_peq.EnFreqH, data_peq.FreqL, data_peq.qH, data_peq.qL);
				break; }
			case DELAY_CMD_TYPE_EQ_MASKING12: {
				data_peq.OutputPage._.Byte = 1;
				data_peq.Output._.Byte = 1;
				data_peq.Index = 11;
				data_peq.EnFreqH.Frequency = 0;
				data_peq.FreqL = 200;
				data_peq.Gain = g_pDevice->pa[AL003M_C12_GAIN_MASKING];
				data_peq.qH = 0x01;
				data_peq.qL = 0xAF;
				data_peq.EnFreqH.Enable = 1;
				data_peq.Channel.Byte = 1;
				Set_Send_Flag(g_pDevice, ACC_CMD_PEQ, ACC_HEADER_WRITE, 11, data_peq.OutputPage._.Byte, data_peq.Output._.Byte, data_peq.Channel.Byte, data_peq.Mode, data_peq.Index, data_peq.Type, data_peq.Gain, data_peq.EnFreqH, data_peq.FreqL, data_peq.qH, data_peq.qL);
				break; }
			case DELAY_CMD_TYPE_EQ_MASKING13: {
				data_peq.OutputPage._.Byte = 1;
				data_peq.Output._.Byte = 1;
				data_peq.Index = 12;
				data_peq.EnFreqH.Frequency = 0;
				data_peq.FreqL = 250;
				data_peq.Gain = g_pDevice->pa[AL003M_C13_GAIN_MASKING];
				data_peq.qH = 0x01;
				data_peq.qL = 0xAF;
				data_peq.EnFreqH.Enable = 1;
				data_peq.Channel.Byte = 1;
				Set_Send_Flag(g_pDevice, ACC_CMD_PEQ, ACC_HEADER_WRITE, 11, data_peq.OutputPage._.Byte, data_peq.Output._.Byte, data_peq.Channel.Byte, data_peq.Mode, data_peq.Index, data_peq.Type, data_peq.Gain, data_peq.EnFreqH, data_peq.FreqL, data_peq.qH, data_peq.qL);
				break; }
			case DELAY_CMD_TYPE_EQ_MASKING14: {
				data_peq.OutputPage._.Byte = 1;
				data_peq.Output._.Byte = 1;
				data_peq.Index = 13;
				data_peq.EnFreqH.Frequency = 315 / 256;
				data_peq.FreqL = 315 % 256;
				data_peq.Gain = g_pDevice->pa[AL003M_C14_GAIN_MASKING];
				data_peq.qH = 0x01;
				data_peq.qL = 0xAF;
				data_peq.EnFreqH.Enable = 1;
				data_peq.Channel.Byte = 1;
				Set_Send_Flag(g_pDevice, ACC_CMD_PEQ, ACC_HEADER_WRITE, 11, data_peq.OutputPage._.Byte, data_peq.Output._.Byte, data_peq.Channel.Byte, data_peq.Mode, data_peq.Index, data_peq.Type, data_peq.Gain, data_peq.EnFreqH, data_peq.FreqL, data_peq.qH, data_peq.qL);
				break; }
			case DELAY_CMD_TYPE_EQ_MASKING15: {
				data_peq.OutputPage._.Byte = 1;
				data_peq.Output._.Byte = 1;
				data_peq.Index = 14;
				data_peq.EnFreqH.Frequency = 400 / 256;
				data_peq.FreqL = 400 % 256;
				data_peq.Gain = g_pDevice->pa[AL003M_C15_GAIN_MASKING];
				data_peq.qH = 0x01;
				data_peq.qL = 0xAF;
				data_peq.EnFreqH.Enable = 1;
				data_peq.Channel.Byte = 1;
				Set_Send_Flag(g_pDevice, ACC_CMD_PEQ, ACC_HEADER_WRITE, 11, data_peq.OutputPage._.Byte, data_peq.Output._.Byte, data_peq.Channel.Byte, data_peq.Mode, data_peq.Index, data_peq.Type, data_peq.Gain, data_peq.EnFreqH, data_peq.FreqL, data_peq.qH, data_peq.qL);
				break; }
			case DELAY_CMD_TYPE_EQ_MASKING16: {
				data_peq.OutputPage._.Byte = 1;
				data_peq.Output._.Byte = 1;
				data_peq.Index = 15;
				data_peq.EnFreqH.Frequency = 500 / 256;
				data_peq.FreqL = 500 % 256;
				data_peq.Gain = g_pDevice->pa[AL003M_C16_GAIN_MASKING];
				data_peq.qH = 0x01;
				data_peq.qL = 0xAF;
				data_peq.EnFreqH.Enable = 1;
				data_peq.Channel.Byte = 1;
				Set_Send_Flag(g_pDevice, ACC_CMD_PEQ, ACC_HEADER_WRITE, 11, data_peq.OutputPage._.Byte, data_peq.Output._.Byte, data_peq.Channel.Byte, data_peq.Mode, data_peq.Index, data_peq.Type, data_peq.Gain, data_peq.EnFreqH, data_peq.FreqL, data_peq.qH, data_peq.qL);
				break; }
			case DELAY_CMD_TYPE_EQ_MASKING17: {
				data_peq.OutputPage._.Byte = 1;
				data_peq.Output._.Byte = 1;
				data_peq.Index = 16;
				data_peq.EnFreqH.Frequency = 630 / 256;
				data_peq.FreqL = 630 % 256;
				data_peq.Gain = g_pDevice->pa[AL003M_C17_GAIN_MASKING];
				data_peq.qH = 0x01;
				data_peq.qL = 0xAF;
				data_peq.EnFreqH.Enable = 1;
				data_peq.Channel.Byte = 1;
				Set_Send_Flag(g_pDevice, ACC_CMD_PEQ, ACC_HEADER_WRITE, 11, data_peq.OutputPage._.Byte, data_peq.Output._.Byte, data_peq.Channel.Byte, data_peq.Mode, data_peq.Index, data_peq.Type, data_peq.Gain, data_peq.EnFreqH, data_peq.FreqL, data_peq.qH, data_peq.qL);
				break; }
			case DELAY_CMD_TYPE_EQ_MASKING18: {
				data_peq.OutputPage._.Byte = 1;
				data_peq.Output._.Byte = 1;
				data_peq.Index = 17;
				data_peq.EnFreqH.Frequency = 800 / 256;
				data_peq.FreqL = 800 % 256;
				data_peq.Gain = g_pDevice->pa[AL003M_C18_GAIN_MASKING];
				data_peq.qH = 0x01;
				data_peq.qL = 0xAF;
				data_peq.EnFreqH.Enable = 1;
				data_peq.Channel.Byte = 1;
				Set_Send_Flag(g_pDevice, ACC_CMD_PEQ, ACC_HEADER_WRITE, 11, data_peq.OutputPage._.Byte, data_peq.Output._.Byte, data_peq.Channel.Byte, data_peq.Mode, data_peq.Index, data_peq.Type, data_peq.Gain, data_peq.EnFreqH, data_peq.FreqL, data_peq.qH, data_peq.qL);
				break; }
			case DELAY_CMD_TYPE_EQ_MASKING19: {
				data_peq.OutputPage._.Byte = 1;
				data_peq.Output._.Byte = 1;
				data_peq.Index = 18;
				data_peq.EnFreqH.Frequency = 1000 / 256;
				data_peq.FreqL = 1000 % 256;
				data_peq.Gain = g_pDevice->pa[AL003M_C19_GAIN_MASKING];
				data_peq.qH = 0x01;
				data_peq.qL = 0xAF;
				data_peq.EnFreqH.Enable = 1;
				data_peq.Channel.Byte = 1;
				Set_Send_Flag(g_pDevice, ACC_CMD_PEQ, ACC_HEADER_WRITE, 11, data_peq.OutputPage._.Byte, data_peq.Output._.Byte, data_peq.Channel.Byte, data_peq.Mode, data_peq.Index, data_peq.Type, data_peq.Gain, data_peq.EnFreqH, data_peq.FreqL, data_peq.qH, data_peq.qL);
				break; }
			case DELAY_CMD_TYPE_EQ_MASKING20: {
				data_peq.OutputPage._.Byte = 1;
				data_peq.Output._.Byte = 1;
				data_peq.Index = 19;
				data_peq.EnFreqH.Frequency = 1250 / 256;
				data_peq.FreqL = 1250 % 256;
				data_peq.Gain = g_pDevice->pa[AL003M_C20_GAIN_MASKING];
				data_peq.qH = 0x01;
				data_peq.qL = 0xAF;
				data_peq.EnFreqH.Enable = 1;
				data_peq.Channel.Byte = 1;
				Set_Send_Flag(g_pDevice, ACC_CMD_PEQ, ACC_HEADER_WRITE, 11, data_peq.OutputPage._.Byte, data_peq.Output._.Byte, data_peq.Channel.Byte, data_peq.Mode, data_peq.Index, data_peq.Type, data_peq.Gain, data_peq.EnFreqH, data_peq.FreqL, data_peq.qH, data_peq.qL);
				break; }
			case DELAY_CMD_TYPE_EQ_MASKING21: {
				data_peq.OutputPage._.Byte = 1;
				data_peq.Output._.Byte = 1;
				data_peq.Index = 20;
				data_peq.EnFreqH.Frequency = 1600 / 256;
				data_peq.FreqL = 1600 % 256;
				data_peq.Gain = g_pDevice->pa[AL003M_C21_GAIN_MASKING];
				data_peq.qH = 0x01;
				data_peq.qL = 0xAF;
				data_peq.EnFreqH.Enable = 1;
				data_peq.Channel.Byte = 1;
				Set_Send_Flag(g_pDevice, ACC_CMD_PEQ, ACC_HEADER_WRITE, 11, data_peq.OutputPage._.Byte, data_peq.Output._.Byte, data_peq.Channel.Byte, data_peq.Mode, data_peq.Index, data_peq.Type, data_peq.Gain, data_peq.EnFreqH, data_peq.FreqL, data_peq.qH, data_peq.qL);
				break; }
			case DELAY_CMD_TYPE_EQ_MASKING22: {
				data_peq.OutputPage._.Byte = 1;
				data_peq.Output._.Byte = 1;
				data_peq.Index = 21;
				data_peq.EnFreqH.Frequency = 2000 / 256;
				data_peq.FreqL = 2000 % 256;
				data_peq.Gain = g_pDevice->pa[AL003M_C22_GAIN_MASKING];
				data_peq.qH = 0x01;
				data_peq.qL = 0xAF;
				data_peq.EnFreqH.Enable = 1;
				data_peq.Channel.Byte = 1;
				Set_Send_Flag(g_pDevice, ACC_CMD_PEQ, ACC_HEADER_WRITE, 11, data_peq.OutputPage._.Byte, data_peq.Output._.Byte, data_peq.Channel.Byte, data_peq.Mode, data_peq.Index, data_peq.Type, data_peq.Gain, data_peq.EnFreqH, data_peq.FreqL, data_peq.qH, data_peq.qL);
				break; }
			case DELAY_CMD_TYPE_EQ_MASKING23: {
				data_peq.OutputPage._.Byte = 1;
				data_peq.Output._.Byte = 1;
				data_peq.Index = 22;
				data_peq.EnFreqH.Frequency = 2500 / 256;
				data_peq.FreqL = 2500 % 256;
				data_peq.Gain = g_pDevice->pa[AL003M_C23_GAIN_MASKING];
				data_peq.qH = 0x01;
				data_peq.qL = 0xAF;
				data_peq.EnFreqH.Enable = 1;
				data_peq.Channel.Byte = 1;
				Set_Send_Flag(g_pDevice, ACC_CMD_PEQ, ACC_HEADER_WRITE, 11, data_peq.OutputPage._.Byte, data_peq.Output._.Byte, data_peq.Channel.Byte, data_peq.Mode, data_peq.Index, data_peq.Type, data_peq.Gain, data_peq.EnFreqH, data_peq.FreqL, data_peq.qH, data_peq.qL);
				break; }
			case DELAY_CMD_TYPE_EQ_MASKING24: {
				data_peq.OutputPage._.Byte = 1;
				data_peq.Output._.Byte = 1;
				data_peq.Index = 23;
				data_peq.EnFreqH.Frequency = 3150 / 256;
					data_peq.FreqL = 3150 % 256;
				data_peq.Gain = g_pDevice->pa[AL003M_C24_GAIN_MASKING];
				data_peq.qH = 0x01;
				data_peq.qL = 0xAF;
				data_peq.EnFreqH.Enable = 1;
				data_peq.Channel.Byte = 1;
				Set_Send_Flag(g_pDevice, ACC_CMD_PEQ, ACC_HEADER_WRITE, 11, data_peq.OutputPage._.Byte, data_peq.Output._.Byte, data_peq.Channel.Byte, data_peq.Mode, data_peq.Index, data_peq.Type, data_peq.Gain, data_peq.EnFreqH, data_peq.FreqL, data_peq.qH, data_peq.qL);
				break; }
			case DELAY_CMD_TYPE_EQ_MASKING25: {
				data_peq.OutputPage._.Byte = 1;
				data_peq.Output._.Byte = 1;
				data_peq.Index = 24;
				data_peq.EnFreqH.Frequency = 4000 / 256;
				data_peq.FreqL = 4000 % 256;
				data_peq.Gain = g_pDevice->pa[AL003M_C25_GAIN_MASKING];
				data_peq.qH = 0x01;
				data_peq.qL = 0xAF;
				data_peq.EnFreqH.Enable = 1;
				data_peq.Channel.Byte = 1;
				Set_Send_Flag(g_pDevice, ACC_CMD_PEQ, ACC_HEADER_WRITE, 11, data_peq.OutputPage._.Byte, data_peq.Output._.Byte, data_peq.Channel.Byte, data_peq.Mode, data_peq.Index, data_peq.Type, data_peq.Gain, data_peq.EnFreqH, data_peq.FreqL, data_peq.qH, data_peq.qL);
				break; }
			case DELAY_CMD_TYPE_EQ_MASKING26: {
				data_peq.OutputPage._.Byte = 1;
				data_peq.Output._.Byte = 1;
				data_peq.Index = 25;
				data_peq.EnFreqH.Frequency = 5000 / 256;
				data_peq.FreqL = 5000 % 256;
				data_peq.Gain = g_pDevice->pa[AL003M_C26_GAIN_MASKING];
				data_peq.qH = 0x01;
				data_peq.qL = 0xAF;
				data_peq.EnFreqH.Enable = 1;
				data_peq.Channel.Byte = 1;
				Set_Send_Flag(g_pDevice, ACC_CMD_PEQ, ACC_HEADER_WRITE, 11, data_peq.OutputPage._.Byte, data_peq.Output._.Byte, data_peq.Channel.Byte, data_peq.Mode, data_peq.Index, data_peq.Type, data_peq.Gain, data_peq.EnFreqH, data_peq.FreqL, data_peq.qH, data_peq.qL);
				break; }
			case DELAY_CMD_TYPE_EQ_MASKING27: {
				data_peq.OutputPage._.Byte = 1;
				data_peq.Output._.Byte = 1;
				data_peq.Index = 26;
				data_peq.EnFreqH.Frequency = 6300 / 256;
				data_peq.FreqL = 6300 % 256;
				data_peq.Gain = g_pDevice->pa[AL003M_C27_GAIN_MASKING];
				data_peq.qH = 0x01;
				data_peq.qL = 0xAF;
				data_peq.EnFreqH.Enable = 1;
				data_peq.Channel.Byte = 1;
				Set_Send_Flag(g_pDevice, ACC_CMD_PEQ, ACC_HEADER_WRITE, 11, data_peq.OutputPage._.Byte, data_peq.Output._.Byte, data_peq.Channel.Byte, data_peq.Mode, data_peq.Index, data_peq.Type, data_peq.Gain, data_peq.EnFreqH, data_peq.FreqL, data_peq.qH, data_peq.qL);
				break; }
			case DELAY_CMD_TYPE_EQ_MASKING28: {
				data_peq.OutputPage._.Byte = 1;
				data_peq.Output._.Byte = 1;
				data_peq.Index = 27;
				data_peq.EnFreqH.Frequency = 8000 / 256;
				data_peq.FreqL = 8000 % 256;
				data_peq.Gain = g_pDevice->pa[AL003M_C28_GAIN_MASKING];
				data_peq.qH = 0x01;
				data_peq.qL = 0xAF;
				data_peq.EnFreqH.Enable = 1;
				data_peq.Channel.Byte = 1;
				Set_Send_Flag(g_pDevice, ACC_CMD_PEQ, ACC_HEADER_WRITE, 11, data_peq.OutputPage._.Byte, data_peq.Output._.Byte, data_peq.Channel.Byte, data_peq.Mode, data_peq.Index, data_peq.Type, data_peq.Gain, data_peq.EnFreqH, data_peq.FreqL, data_peq.qH, data_peq.qL);
				break; }
			}
			KillTimer(hDlg, TIMER_SEND_CMD);
			SetTimer(g_hDeviceControl, TIME_LINK_AMP, TIME_LINK_AMP, NULL);
			break;
		}
		}
		break;
	}
	case WM_NOTIFY:
		break;
	case WM_PAINT:
		if (g_reflash_ui) {
			Init_Masking_UI(hDlg);
			g_reflash_ui = FALSE;
		}
		break;
	case WM_CLOSE:
		return TRUE;

	case WM_DESTROY:
		return TRUE;
	}
	return FALSE;
}


void Init_Masking_UI(HWND hDlg)
{
	if (g_pDevice->pa[AL003M_MUTE_MASKING] == 1)
		SendMessage(GetDlgItem(hDlg, IDC_CHECK_MUTE_MASKING), BM_SETCHECK, (WPARAM)BST_CHECKED, (LPARAM)1);
	else
		SendMessage(GetDlgItem(hDlg, IDC_CHECK_MUTE_MASKING), BM_SETCHECK, (WPARAM)BST_UNCHECKED, (LPARAM)1);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(VOLUME_MASKING_MIN, VOLUME_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING), TBM_SETPOS, (WPARAM)1, abs(g_pDevice->pa[AL003M_VOL_MASKING_IN] - VOLUME_MASKING_MAX));
	int value;
	char *text = (char*)malloc(10 * sizeof(char));
	memset(text, 0, 10 * sizeof(char));
	//value = 50 - g_pDevice->pa[AL003M_VOL_MASKING_IN];
	sprintf(text, "%d", g_pDevice->pa[AL003M_VOL_MASKING_IN] - 50);
	/*
	if (value > 0) {
		strcpy(text, _T("-"));
		sprintf(text + 1, _T("%d"), value);
	}
	else {
		sprintf(text, _T("%d"), value);
	}
	*/
	SendMessage(GetDlgItem(hDlg, IDC_EDIT_MASKING), WM_SETTEXT, (WPARAM)0, (LPARAM)text);

	//Filter
	SendMessage(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_MASKING), CB_RESETCONTENT, 0, 0);
	SendMessage(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_MASKING), (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)_T("6 dB/oct"));
	SendMessage(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_MASKING), (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)_T("12 dB/oct"));
	SendMessage(GetDlgItem(hDlg, IDC_COMBO_HP_SLOPE_MASKING), CB_SETCURSEL, g_pDevice->pa[AL003M_HIPASS_SLOPE_MASKING], 0);
	Set_Freq_Text(hDlg, IDC_EDIT_HP_F_MASKING, (unsigned int)g_pDevice->pa[AL003M_HIPASS_FREQ_H_MASKING] * 256 + g_pDevice->pa[AL003M_HIPASS_FREQ_L_MASKING]);
	SendMessage(GetDlgItem(hDlg, IDC_CHECK_HP_FILTER_MASKING), BM_SETCHECK, (WPARAM)!g_pDevice->pa[AL003M_HIPASS_SWITCH_MASKING], (LPARAM)1);

	SendMessage(GetDlgItem(hDlg, IDC_COMBO_LP_SLOPE_MASKING), CB_RESETCONTENT, 0, 0);
	SendMessage(GetDlgItem(hDlg, IDC_COMBO_LP_SLOPE_MASKING), (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)_T("6 dB/oct"));
	SendMessage(GetDlgItem(hDlg, IDC_COMBO_LP_SLOPE_MASKING), (UINT)CB_ADDSTRING, (WPARAM)0, (LPARAM)_T("12 dB/oct"));
	SendMessage(GetDlgItem(hDlg, IDC_COMBO_LP_SLOPE_MASKING), CB_SETCURSEL, g_pDevice->pa[AL003M_LOPASS_SLOPE_MASKING], 0);
	Set_Freq_Text(hDlg, IDC_EDIT_LP_F_MASKING, (unsigned int)g_pDevice->pa[AL003M_LOPASS_FREQ_H_MASKING] * 256 + g_pDevice->pa[AL003M_LOPASS_FREQ_L_MASKING]);
	SendMessage(GetDlgItem(hDlg, IDC_CHECK_LP_FILTER_MASKING), BM_SETCHECK, (WPARAM)!g_pDevice->pa[AL003M_LOPASS_SWITCH_MASKING], (LPARAM)1);

	//EQ
	SendMessage(GetDlgItem(hDlg, IDC_CHECK_MASKING), BM_SETCHECK, (WPARAM)!g_pDevice->pa[AL003M_SWITCH_MASKING_EQ], (LPARAM)1);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND1), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND1), TBM_SETPOS, (WPARAM)1, abs(g_pDevice->pa[AL003M_C11_GAIN_MASKING] - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND1, ((double)g_pDevice->pa[AL003M_C11_GAIN_MASKING] - 40) / 2);
	
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND2), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND2), TBM_SETPOS, (WPARAM)1, abs(g_pDevice->pa[AL003M_C12_GAIN_MASKING] - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND2, ((double)g_pDevice->pa[AL003M_C12_GAIN_MASKING] - 40) / 2);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND3), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND3), TBM_SETPOS, (WPARAM)1, abs(g_pDevice->pa[AL003M_C13_GAIN_MASKING] - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND3, ((double)g_pDevice->pa[AL003M_C13_GAIN_MASKING] - 40) / 2);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND4), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND4), TBM_SETPOS, (WPARAM)1, abs(g_pDevice->pa[AL003M_C14_GAIN_MASKING] - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND4, ((double)g_pDevice->pa[AL003M_C14_GAIN_MASKING] - 40) / 2);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND5), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND5), TBM_SETPOS, (WPARAM)1, abs(g_pDevice->pa[AL003M_C15_GAIN_MASKING] - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND5, ((double)g_pDevice->pa[AL003M_C15_GAIN_MASKING] - 40) / 2);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND6), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND6), TBM_SETPOS, (WPARAM)1, abs(g_pDevice->pa[AL003M_C16_GAIN_MASKING] - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND6, ((double)g_pDevice->pa[AL003M_C16_GAIN_MASKING] - 40) / 2);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND7), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND7), TBM_SETPOS, (WPARAM)1, abs(g_pDevice->pa[AL003M_C17_GAIN_MASKING] - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND7, ((double)g_pDevice->pa[AL003M_C17_GAIN_MASKING] - 40) / 2);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND8), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND8), TBM_SETPOS, (WPARAM)1, abs(g_pDevice->pa[AL003M_C18_GAIN_MASKING] - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND8, ((double)g_pDevice->pa[AL003M_C18_GAIN_MASKING] - 40) / 2);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND9), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND9), TBM_SETPOS, (WPARAM)1, abs(g_pDevice->pa[AL003M_C19_GAIN_MASKING] - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND9, ((double)g_pDevice->pa[AL003M_C19_GAIN_MASKING] - 40) / 2);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND10), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND10), TBM_SETPOS, (WPARAM)1, abs(g_pDevice->pa[AL003M_C20_GAIN_MASKING] - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND10, ((double)g_pDevice->pa[AL003M_C20_GAIN_MASKING] - 40) / 2);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND11), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND11), TBM_SETPOS, (WPARAM)1, abs(g_pDevice->pa[AL003M_C21_GAIN_MASKING] - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND11, ((double)g_pDevice->pa[AL003M_C21_GAIN_MASKING] - 40) / 2);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND12), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND12), TBM_SETPOS, (WPARAM)1, abs(g_pDevice->pa[AL003M_C22_GAIN_MASKING] - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND12, ((double)g_pDevice->pa[AL003M_C22_GAIN_MASKING] - 40) / 2);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND13), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND13), TBM_SETPOS, (WPARAM)1, abs(g_pDevice->pa[AL003M_C23_GAIN_MASKING] - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND13, ((double)g_pDevice->pa[AL003M_C23_GAIN_MASKING] - 40) / 2);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND14), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND14), TBM_SETPOS, (WPARAM)1, abs(g_pDevice->pa[AL003M_C24_GAIN_MASKING] - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND14, ((double)g_pDevice->pa[AL003M_C24_GAIN_MASKING] - 40) / 2);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND15), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND15), TBM_SETPOS, (WPARAM)1, abs(g_pDevice->pa[AL003M_C25_GAIN_MASKING] - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND15, ((double)g_pDevice->pa[AL003M_C25_GAIN_MASKING] - 40) / 2);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND16), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND16), TBM_SETPOS, (WPARAM)1, abs(g_pDevice->pa[AL003M_C26_GAIN_MASKING] - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND16, ((double)g_pDevice->pa[AL003M_C26_GAIN_MASKING] - 40) / 2);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND17), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND17), TBM_SETPOS, (WPARAM)1, abs(g_pDevice->pa[AL003M_C27_GAIN_MASKING] - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND17, ((double)g_pDevice->pa[AL003M_C27_GAIN_MASKING] - 40) / 2);

	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND18), TBM_SETRANGE, (WPARAM)1, (LPARAM)MAKELONG(DB_MASKING_MIN, DB_MASKING_MAX));
	SendMessage(GetDlgItem(hDlg, IDC_SLIDER_MASKING_BAND18), TBM_SETPOS, (WPARAM)1, abs(g_pDevice->pa[AL003M_C28_GAIN_MASKING] - DB_MASKING_MAX));
	Set_Gain_Text(hDlg, IDC_STATIC_MASKING_BAND18, ((double)g_pDevice->pa[AL003M_C28_GAIN_MASKING] - 40) / 2);
	free(text);
	InvalidateRect(hDlg, NULL, TRUE);
}

void Change_Masking_Edit_Data(HWND hwnd)
{
	char number[6];

	if (hwnd == hEditMasking) {
		GetDlgItemTextA(hParent, IDC_EDIT_MASKING, number, 6);
		double i = atof(number);
		if (i < -50) {
			i = -50;
			g_pDevice->pa[AL003M_VOL_MASKING_IN] = 0;
		}
		else if (i > -17) {
			i = -17;
			g_pDevice->pa[AL003M_VOL_MASKING_IN] = 33;
		}
		else {
			g_pDevice->pa[AL003M_VOL_MASKING_IN] = i + 50;
		}
		SendMessage(GetDlgItem(hParent, IDC_SLIDER_MASKING), TBM_SETPOS, (WPARAM)1, abs(g_pDevice->pa[AL003M_VOL_MASKING_IN] - VOLUME_MASKING_MAX));

		data_vol.OutputPage._.Byte = 1;
		data_vol.Output._.Byte = 3;
		data_vol.VolumeR = 0xFF;
		data_vol.RMuteVolH.Byte = 0xFF;
		data_vol.VolumeL = g_pDevice->pa[AL003M_VOL_MASKING_IN];
		data_vol.LMuteVolH.Mute = g_pDevice->pa[AL003M_MUTE_MASKING];
		Set_Send_Flag(g_pDevice, ACC_CMD_OUTVOL, ACC_HEADER_WRITE, 6, data_vol.OutputPage._.Byte, data_vol.Output._.Byte, data_vol.VolumeL, data_vol.VolumeR, data_vol.LMuteVolH.Byte, data_vol.RMuteVolH.Byte);
	}
	else if (hwnd == hEditHPF) {
		GetDlgItemTextA(hParent, IDC_EDIT_HP_F_MASKING, number, 6);
		int i = atoi(number);
		if (i < HIPASS_FERQUENCY_MIN) {
			i = HIPASS_FERQUENCY_MIN;
		}
		else if (i > HIPASS_FERQUENCY_MAX) {
			i = (int)HIPASS_FERQUENCY_MAX;
		}
		g_pDevice->pa[AL003M_HIPASS_FREQ_H_MASKING] = (unsigned char)(i / 256);
		g_pDevice->pa[AL003M_HIPASS_FREQ_L_MASKING] = (unsigned char)(i % 256);

		data_hlpf.OutputPage._.Byte = 1;
		data_hlpf.Output._.Byte = 3;
		data_hlpf.Channel.Byte = 1;
		data_hlpf.Type = FILTER_TYPE_HIPASS;
		data_hlpf.EnFreqH.Frequency = g_pDevice->pa[AL003M_HIPASS_FREQ_H_MASKING];
		data_hlpf.FreqL = g_pDevice->pa[AL003M_HIPASS_FREQ_L_MASKING];
		data_hlpf.EnFreqH.Enable = g_pDevice->pa[AL003M_HIPASS_SWITCH_MASKING];
		data_hlpf.Slope = 0xFF;
		Set_Send_Flag(g_pDevice, ACC_CMD_HLPF, ACC_HEADER_WRITE, 7, data_hlpf.OutputPage._.Byte, data_hlpf.Output._.Byte, data_hlpf.Channel.Byte, data_hlpf.Type, data_hlpf.EnFreqH, data_hlpf.FreqL, data_hlpf.Slope);
	}
	else if (hwnd == hEditLPF) {
		GetDlgItemTextA(hParent, IDC_EDIT_LP_F_MASKING, number, 6);
		int i = atoi(number);
		if (i < LOPASS_FERQUENCY_MIN) {
			i = LOPASS_FERQUENCY_MIN;
		}
		else if (i > LOPASS_FERQUENCY_MAX) {
			i = (int)LOPASS_FERQUENCY_MAX;
		}
		g_pDevice->pa[AL003M_LOPASS_FREQ_H_MASKING] = (unsigned char)(i / 256);
		g_pDevice->pa[AL003M_LOPASS_FREQ_L_MASKING] = (unsigned char)(i % 256);

		data_hlpf.OutputPage._.Byte = 1;
		data_hlpf.Output._.Byte = 3;
		data_hlpf.Channel.Byte = 1;
		data_hlpf.Type = FILTER_TYPE_LOWPASS;
		data_hlpf.EnFreqH.Frequency = g_pDevice->pa[AL003M_LOPASS_FREQ_H_MASKING];
		data_hlpf.FreqL = g_pDevice->pa[AL003M_LOPASS_FREQ_L_MASKING];
		data_hlpf.EnFreqH.Enable = g_pDevice->pa[AL003M_LOPASS_SWITCH_MASKING];
		data_hlpf.Slope = 0xFF;
		Set_Send_Flag(g_pDevice, ACC_CMD_HLPF, ACC_HEADER_WRITE, 7, data_hlpf.OutputPage._.Byte, data_hlpf.Output._.Byte, data_hlpf.Channel.Byte, data_hlpf.Type, data_hlpf.EnFreqH, data_hlpf.FreqL, data_hlpf.Slope);
	}
}
