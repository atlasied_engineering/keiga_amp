#include "stdafx.h"
#include "update.h"
#include "utils.h"
#include "Dante_Discovery.h"
#include "myListView.h"
#include "ACC_Command.h"
#include "device_state.h"
//=======================================================================================================
//=================================(start) private =======================================
//=======================================================================================================

boolean			g_update;
VERSIONFW		g_update_file_version;
unsigned long	g_update_file_size;
unsigned char*	g_update_file_data;
unsigned long	g_update_file_cksum;
unsigned char	g_out_queue[OUT_QUEUE_MAX][1024];
int				g_queue_index = 0;
int				g_queue_to_send = 0;
unsigned char	g_cmd_echo;
int				g_update_page;
unsigned int	g_send_hex_data_flag;
int				g_update_state_ap;
int				g_update_progress;
boolean 		g_update_file_result;

unsigned long	g_tick_wait_point;
unsigned long	g_tick_count;
unsigned long	g_tick_resend_wait_point;

unsigned char	g_customer;
unsigned char	g_target;
char			g_name[32] = {0};



PDEVICESTATE p_ds = NULL;
#define DEBUG_SHOW_LOG 0
//callbacks
static upload_Write_fn *upload_cbWrite = NULL;

//set Acc cmd function for aprom 

void mSet_Send_Update_AP(PUPDATEPARAM pUpdate, unsigned char typeMcu, int sequence, unsigned char* data);
void mSet_Send_UpdateLast_AP(PUPDATEPARAM pUpdate, unsigned char typeMcu);
void mSet_Send_Version_AP(PUPDATEPARAM pUpdate, unsigned char typeMcu, int yy, int mm, int dd, int vv);
void mSet_Send_GetCksum_AP(PUPDATEPARAM pUpdate, unsigned char typeMcu);
void mSet_Send_GetResume_AP(PUPDATEPARAM pUpdate, unsigned char typeMcu);
void mSet_Send_Restart_AP(PUPDATEPARAM pUpdate);

//=======================================================================================================
//=================================( end )  private =======================================
//=======================================================================================================


//=======================================================================================================
//=================================(start) Public Function ==============================================
//=======================================================================================================

void select_file(void)
{
	OPENFILENAMEW ofn;
	wchar_t szFile[2048];
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = hMainWnd;
	ofn.lpstrFile = szFile;
	ofn.lpstrFile[0] = '\0';
	ofn.nMaxFile = sizeof(szFile);
	ofn.lpstrFilter = L"All\0*.*\0Hex file(*.hex)\0*.hex\0";
	ofn.nFilterIndex = 2;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	if (GetOpenFileNameW(&ofn)) {
		if (load_hex(ofn.lpstrFile)) {
			if (MessageBox(hMainWnd, "All parameters will be reset to default value after updated.\n Update? ", "Warning", MB_ICONWARNING | MB_YESNO) == IDYES) {
				g_update = TRUE;
			}
			else {
				if (g_update_file_data != NULL) {
					free(g_update_file_data);
					g_update_file_data = NULL;
				}
			}
		}
		else {
			//MessageBox(hMainWnd, "Load hex file error", "Error", MB_OK);
		}
	}
	else {
		//MessageBox(hMainWnd, "Open file error", "Error", MB_OK);
	}
}

wchar_t* select_preset_file(int type)
{
	OPENFILENAMEW ofn;
	wchar_t szFile[2048];
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = hMainWnd;
	ofn.lpstrFile = szFile;
	ofn.lpstrFile[0] = '\0';
	ofn.nMaxFile = sizeof(szFile);
	switch (type) {
	case PRESET_TYPE_FACTORY:
		ofn.lpstrFilter = L"Factory Preset file(*.prs)\0*.prs\0";
		ofn.nFilterIndex = 1;
		break;
	case PRESET_TYPE_INSTALLER:
		ofn.lpstrFilter = L"Installer Preset file(*.iprs)\0*.iprs\0";
		ofn.nFilterIndex = 1;
		break;
	default:
		ofn.lpstrFilter = L"Preset file(*.prs)\0*.prs\0";
		ofn.nFilterIndex = 1;
		break;
	}

	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	if (GetOpenFileNameW(&ofn))
		return ofn.lpstrFile;

	return NULL;
}


boolean load_hex(wchar_t* file)
{
	char *delim = "\n";
	char *pch;
	char *buff;
	char *final_data;
	int len;
	long buff_size = 0;

	buff = read_file(file, &buff_size);
	if (buff_size == 0) {
		//MessageBox(hMainWnd, "Read file error.", "Error", MB_OK);
		return FALSE;
	}

	if (str_to_byte(buff) != 0x10) {
		MessageBox(hMainWnd, "Firmware format error.", "Error", MB_OK);
		free(buff);
		return FALSE;
	}

	g_customer = ACC_CUST_KEIGA;// str_to_byte(buff);
	g_target = str_to_byte(buff + 2);
	g_update_file_version.yy = str_to_byte(buff + 4);
	g_update_file_version.mm = str_to_byte(buff + 6);
	g_update_file_version.dd = str_to_byte(buff + 8);
	g_update_file_version.vv = str_to_byte(buff + 10);


	final_data = (char*)malloc(sizeof(char)*(buff_size-14));//-14 : customer id, device id, year, month, date, number, \n, \a
	if (final_data == NULL) {
		free(buff);
		//MessageBox(hMainWnd, "Final data error.", "Error", MB_OK);
		return FALSE;
	}

	final_data[0] = 0;
	pch = strtok(buff+14, delim);
	while (pch != NULL)
	{
		if (pch[7] == '0' && pch[8] == '0') {
			len = str_to_byte(pch + 1);
			strncat(final_data, pch + 9, len * 2);
		}
		pch = strtok(NULL, delim);
	}

	g_update_file_size = (long)(strlen(final_data) / 2); //UPDATE_MAX_FILE_SIZE;
	g_update_file_size = g_update_file_size + (UPDATE_PTN_FILE_SIZE_AP - (g_update_file_size % UPDATE_PTN_FILE_SIZE_AP));

	if (g_update_file_data != NULL)
		free(g_update_file_data);
	g_update_file_data = (unsigned char*)malloc(sizeof(unsigned char) * g_update_file_size);
	if (g_update_file_data == NULL) {
		free(buff);
		free(final_data);
		MessageBox(hMainWnd, "malloc file data memory error.", "Error", MB_OK);
		return FALSE;
	}
	memset(g_update_file_data, 0x00, sizeof(char) * g_update_file_size);



	UINT64 temp_sum = 0;
	
	for (int i = 0, j = 0; i < (int)strlen(final_data); i += 2, j++) {
		g_update_file_data[j] = str_to_byte(final_data + i);

	}

	for (int i = 0; i < g_update_file_size; i+=4) {
		temp_sum += g_update_file_data[i+1] * 0x100 + g_update_file_data[i];
		if ((i+4) % 4096 == 0) {
			temp_sum = temp_sum;
		}
	}

	g_update_file_cksum = temp_sum & 0xFFFF;


	free(buff);
	free(final_data);

	return TRUE;
}

void mUpdate_Process_Initial(PUPDATEPARAM pUpdate) {
	
	pUpdate->update = TRUE;

	for (int i = 0; i < UPDATE_OUT_QUEUE_MAX; i++)
		pUpdate->out_queue[i][0] = 0;
	pUpdate->queue_index = pUpdate->queue_to_send = 0;
	pUpdate->update_page = 0;
	pUpdate->send_hex_data_flag = 0;


	pUpdate->update_state = ESAU_GetResume;
	pUpdate->update_progress = g_update_file_size / UPDATE_PTN_FILE_SIZE_AP;
	pUpdate->update_file_result = FALSE;

	pUpdate->queue_index = 0;
	pUpdate->queue_to_send = 0;
	pUpdate->tick_wait_point = GetTickCount();
}

void mUpdate_Process_Receiver(void* mparam, void* lparam) {
	PUPDATEPARAM pUpdate = (PUPDATEPARAM)mparam;
	PUPDATERECV p = (PUPDATERECV)lparam;
	int lenMessage = p->lenMessage;
	unsigned char* buff = p->buffParam;
	int lenBuff = p->sizeParam;

	char msg[50];

	pUpdate->tick_resend_wait_point = GetTickCount();
	pUpdate->tick_wait_point = GetTickCount();

	switch (pUpdate->update_state) {
	case ESAU_SetVerToZero:
	{
		if (buff[ACC_CMD_HEAD_POS] == ACC_HEADER_ACK &&
			buff[ACC_CMD_CMD_POS] == ACC_CMD_CFS) {

			pUpdate->update_state = ESAU_PreAmpData;
			pUpdate->update_page = 0;
			pUpdate->send_hex_data_flag = 0;
			pUpdate->send_hex_data_sum = 0;
			for (int i = 0; i < UPDATE_PTN_FILE_SIZE_AP; i++)
				pUpdate->send_hex_data_sum += g_update_file_data[pUpdate->send_hex_data_flag + i];
			mSet_Send_Update_AP(pUpdate,ACC_PA_ALL_SGFS_TYPE$_MCU1, pUpdate->update_page, &g_update_file_data[pUpdate->send_hex_data_flag]);
		}
		break;
	}
	case ESAU_PreAmpData: {
		if (buff[ACC_CMD_HEAD_POS] == ACC_HEADER_ECHO &&
			buff[ACC_CMD_CMD_POS] == ACC_CMD_SGFS) {
			sprintf(msg, "Recv: preData %d", g_update_page);
			//do someing
			unsigned char cs[2];
			cs[0] = (pUpdate->send_hex_data_sum >> 8) & 0xFF;
			cs[1] = pUpdate->send_hex_data_sum & 0xFF;
			if (buff[ACC_CMD_DATA_FIRST_POS] == cs[0] && buff[ACC_CMD_DATA_FIRST_POS + 1] == cs[1]) {

				if (pUpdate->send_hex_data_flag < (g_update_file_size - UPDATE_PTN_FILE_SIZE_AP)) {
					pUpdate->update_page++;
					pUpdate->send_hex_data_flag += UPDATE_PTN_FILE_SIZE_AP;

					sprintf(msg, "%s%.1f%s","FW Update - In Process - ", (double)pUpdate->update_page / pUpdate->update_progress * 100, "%");
					sprintf(pUpdate->update_msg, "%s", msg);
					pUpdate->fn_msg(pUpdate->index_in_listview, msg);
					pUpdate->send_hex_data_sum = 0;
					for (int i = 0; i < UPDATE_PTN_FILE_SIZE_AP; i++)
						pUpdate->send_hex_data_sum += g_update_file_data[pUpdate->send_hex_data_flag + i];
					mSet_Send_Update_AP(pUpdate, ACC_PA_ALL_SGFS_TYPE$_MCU1, pUpdate->update_page, &g_update_file_data[pUpdate->send_hex_data_flag]);
				}
				else {
					pUpdate->update_state = ESAU_PreAmpDone;
					mSet_Send_UpdateLast_AP(pUpdate, ACC_PA_ALL_SGFS_TYPE$_MCU1);
				}
			}//else//checksum error, send again
			//mSet_Send_Update_AP(pUpdate, ACC_PA_ALL_SGFS_TYPE$_MCU1, pUpdate->update_page, &g_update_file_data[pUpdate->send_hex_data_flag]);
		}
		break;
	}
	case ESAU_PreAmpDone: {
		if (buff[ACC_CMD_HEAD_POS] == ACC_HEADER_ACK &&
			buff[ACC_CMD_CMD_POS] == ACC_CMD_SGFS) {
			pUpdate->update_state = ESAU_GetCksum_PreAmp;
			mSet_Send_GetCksum_AP(pUpdate,ACC_PA_ALL_CFS_TYPE$_MCU1);
		}
		break;
	}
	case ESAU_GetCksum_PreAmp: {
		if (buff[ACC_CMD_HEAD_POS] == ACC_HEADER_ECHO &&
			buff[ACC_CMD_CMD_POS] == ACC_CMD_CFS) {
			if (g_update_file_cksum == ((buff[ACC_CMD_DATA_FIRST_POS + 2] << 8) + buff[ACC_CMD_DATA_FIRST_POS + 3]))
				pUpdate->update_file_result = TRUE;
			else
				pUpdate->update_file_result = FALSE;
			if (pUpdate->update_file_result) {
				pUpdate->update_state = ESAU_SetVer;
				mSet_Send_Version_AP(pUpdate, ACC_PA_ALL_CFS_TYPE$_MCU1,
					g_update_file_version.yy,
					g_update_file_version.mm,
					g_update_file_version.dd,
					g_update_file_version.vv
					);
			}
			else {
				pUpdate->update_state = ESAU_Failure;
			}
		}
		break;
	}
	case ESAU_SetVer: {
		sprintf(pUpdate->update_msg, "%s", "FW Update - Restart");
		pUpdate->fn_msg(pUpdate->index_in_listview, "FW Update - Restart");
		pUpdate->update_state = ESAU_Restart;
		mSet_Send_Restart_AP(pUpdate);
		break;
	}
	case ESAU_GetResume: {
		if (buff[ACC_CMD_HEAD_POS] == ACC_HEADER_ECHO &&
			buff[ACC_CMD_CMD_POS] == ACC_CMD_CFS) {
			if (buff[ACC_CMD_DATA_FIRST_POS + 2] == 1) {
				UINT64 temp_sum = 0;
				UINT64 sum = 0;
				for (int i = 0; i < buff[ACC_CMD_DATA_FIRST_POS + 3] * 4096; i++) {
					temp_sum += (g_update_file_data[i] * (1 << ((i % 4) * 8)));
				}

				sum = (sum << 16) + (temp_sum & 0xFFFF);
				if (sum == (buff[ACC_CMD_DATA_FIRST_POS + 4] << 8) + buff[ACC_CMD_DATA_FIRST_POS + 5]) {
					pUpdate->update_page = buff[ACC_CMD_DATA_FIRST_POS + 3] * 16;
					pUpdate->send_hex_data_flag = pUpdate->update_page * UPDATE_PTN_FILE_SIZE_AP;
					pUpdate->update_state = ESAU_PreAmpData;
					pUpdate->send_hex_data_sum = 0;
					for (int i = 0; i < UPDATE_PTN_FILE_SIZE_AP; i++)
						pUpdate->send_hex_data_sum += g_update_file_data[pUpdate->send_hex_data_flag + i];
					mSet_Send_Update_AP(pUpdate,ACC_PA_ALL_SGFS_TYPE$_MCU1, pUpdate->update_page, &g_update_file_data[pUpdate->send_hex_data_flag]);
				}
				else {
					pUpdate->update_state = ESAU_Init;
					pUpdate->update_progress = g_update_file_size / UPDATE_PTN_FILE_SIZE_AP;
				}
			}
			else {
				pUpdate->update_state = ESAU_Init;
				pUpdate->update_progress = g_update_file_size / UPDATE_PTN_FILE_SIZE_AP;
			}
		}
		break;
	}
	}
}

void mUpdate_Process_Upload(PUPDATEPARAM pUpdate, void* lparam) {
	//Delay sometime for Dante
	DWORD waiting_time = GetTickCount();
	while ((GetTickCount() - waiting_time) <  20);

	if(upload_cbWrite(pUpdate->manufacturer, pUpdate->device_name,	&pUpdate->out_queue[pUpdate->queue_to_send][3],	pUpdate->out_queue[pUpdate->queue_to_send][1] * 256 + pUpdate->out_queue[pUpdate->queue_to_send][2]))//TODO: Send msg to deive
		pUpdate->out_queue[pUpdate->queue_to_send][0] = 0;
}

void mUpdate_Process_Timer(PUPDATEPARAM pUpdate) {
	pUpdate->tick_count = GetTickCount();

	if (pUpdate->update_state == ESAU_Init) {
		pUpdate->tick_wait_point = GetTickCount();
		pUpdate->update_state = ESAU_SetVerToZero;
		pUpdate->tick_resend_wait_point = GetTickCount();
		return;
	}
	if ((pUpdate->tick_count - pUpdate->tick_resend_wait_point) > 2000) {
		switch (pUpdate->update_state) {
		case ESAU_SetVerToZero: {
			mSet_Send_Version_AP(pUpdate,ACC_PA_ALL_CFS_TYPE$_MCU1, 0, 0, 0, 0);
			break;
		}
		case ESAU_PreAmpData: {
			mSet_Send_Update_AP(pUpdate,ACC_PA_ALL_SGFS_TYPE$_MCU1, pUpdate->update_page, &g_update_file_data[pUpdate->send_hex_data_flag]);
			break;
		}
		case ESAU_PreAmpDone: {
			mSet_Send_UpdateLast_AP(pUpdate,ACC_PA_ALL_SGFS_TYPE$_MCU1);
			break;
		}
		case ESAU_GetCksum_PreAmp: {
			mSet_Send_GetCksum_AP(pUpdate,ACC_PA_ALL_CFS_TYPE$_MCU1);
			break;
		}
		case ESAU_SetVer: {
			mSet_Send_Version_AP(pUpdate,ACC_PA_ALL_CFS_TYPE$_MCU1,
				g_update_file_version.yy,
				g_update_file_version.mm,
				g_update_file_version.dd,
				g_update_file_version.vv
				);
			break;
		}
		case ESAU_Restart: {
			mSet_Send_Restart_AP(pUpdate);
			pUpdate->update_state = ESAU_Finished;
			break;
		}
		case ESAU_Finished: {
			sprintf(pUpdate->update_msg, "%s%s%s", "FW Update - Finished (from V", pUpdate->fw,")");
			pUpdate->fn_msg(pUpdate->index_in_listview, pUpdate->update_msg);
			pUpdate->update = FALSE;
			break;
		}
		case ESAU_Failure: {
			mUpdate_Process_Initial(pUpdate);
			//sprintf(pUpdate->update_msg, "%s%.1f%s", "FW Update - Failed ", (double)pUpdate->update_page / pUpdate->update_progress * 100, "%");
			//pUpdate->fn_msg(pUpdate->index_in_listview, pUpdate->update_msg);
			//pUpdate->update = FALSE;
			break;
		}
		case ESAU_GetResume: {
			sprintf(pUpdate->update_msg, "%s", "FW Update - Check Resume");
			pUpdate->fn_msg(pUpdate->index_in_listview, "FW Update - Check Resume");
			mSet_Send_GetResume_AP(pUpdate, ACC_PA_ALL_CFS_TYPE$_MCU1);
			break;
		}
		}

		pUpdate->tick_resend_wait_point = GetTickCount();
	}
	
	if ((pUpdate->tick_count - pUpdate->tick_wait_point) > 10000 * 3) {
		mUpdate_Process_Initial(pUpdate);
		//pUpdate->update = FALSE;
		//sprintf(pUpdate->update_msg, "%s%.1f%s", "FW Update - Timeout ", (double)pUpdate->update_page / pUpdate->update_progress * 100, "% - retry");
		//pUpdate->fn_msg(pUpdate->index_in_listview, pUpdate->update_msg);
	}
}
//---( end ) main function  Boot/ App  -------------------------------------------------

void Update_SetWriteCB(upload_Write_fn *fn) {
	upload_cbWrite = fn;
}

//=======================================================================================================
//=================================( end ) Public Function ==============================================
//=======================================================================================================


//=======================================================================================================
//=================================(start) Private Function =============================================
//=======================================================================================================


//---(start) app function -------------------------------------------------


//---(end)   app function  -------------------------------------------------

//---(start) other function -------------------------------------------------

//set Acc cmd function for aprom 

void mSet_Send_Update_AP(PUPDATEPARAM pUpdate, unsigned char typeMcu, int sequence, unsigned char* data)
{
	int cksum = 0;

	pUpdate->out_queue[pUpdate->queue_index][3] = g_customer;
	pUpdate->out_queue[pUpdate->queue_index][4] = 0x10;// HEAD_CODE_10;
	pUpdate->out_queue[pUpdate->queue_index][5] = ACC_CMDER_COMMON;
	pUpdate->out_queue[pUpdate->queue_index][6] = g_target;
	pUpdate->out_queue[pUpdate->queue_index][7] = ACC_CMD_SGFS;
	pUpdate->out_queue[pUpdate->queue_index][8] = HIBYTE(UPDATE_PTN_FILE_SIZE_AP + ACC_CMD_WLEN_SGFS_BASE);
	pUpdate->out_queue[pUpdate->queue_index][9] = LOBYTE(UPDATE_PTN_FILE_SIZE_AP + ACC_CMD_WLEN_SGFS_BASE);

	pUpdate->out_queue[pUpdate->queue_index][10] = typeMcu;
	pUpdate->out_queue[pUpdate->queue_index][11] = HIBYTE(sequence);
	pUpdate->out_queue[pUpdate->queue_index][12] = LOBYTE(sequence);
	pUpdate->out_queue[pUpdate->queue_index][13] = UPDATE_PTN_FILE_SIZE_AP / 32;

	for (int i = 3; i <= 13; i++) {
		cksum += pUpdate->out_queue[pUpdate->queue_index][i];
	}

	for (int i = 0; i < UPDATE_PTN_FILE_SIZE_AP; i++) {
		pUpdate->out_queue[pUpdate->queue_index][i + 14] = data[i];
		cksum += data[i];
		cksum = cksum & 0xFF;
	}

	pUpdate->out_queue[pUpdate->queue_index][13 + UPDATE_PTN_FILE_SIZE_AP + 1] = cksum;

	int lenTotal = UPDATE_PTN_FILE_SIZE_AP + ACC_CMD_FOMT_LEN + ACC_CMD_WLEN_SGFS_BASE;
	pUpdate->out_queue[pUpdate->queue_index][1] = HIBYTE(lenTotal);
	pUpdate->out_queue[pUpdate->queue_index][2] = LOBYTE(lenTotal);
	pUpdate->out_queue[pUpdate->queue_index][0] = 1;
}

void mSet_Send_UpdateLast_AP(PUPDATEPARAM pUpdate, unsigned char typeMcu) {
	int cksum = 0;

	pUpdate->out_queue[pUpdate->queue_index][3] = g_customer;
	pUpdate->out_queue[pUpdate->queue_index][4] = 0x10;// HEAD_CODE_10;
	pUpdate->out_queue[pUpdate->queue_index][5] = ACC_CMDER_COMMON;
	pUpdate->out_queue[pUpdate->queue_index][6] = g_target;
	pUpdate->out_queue[pUpdate->queue_index][7] = ACC_CMD_SGFS;
	pUpdate->out_queue[pUpdate->queue_index][8] = HIBYTE(ACC_CMD_WLEN_SGFS_BASE);
	pUpdate->out_queue[pUpdate->queue_index][9] = LOBYTE(ACC_CMD_WLEN_SGFS_BASE);

	pUpdate->out_queue[pUpdate->queue_index][10] = typeMcu;
	pUpdate->out_queue[pUpdate->queue_index][11] = HIBYTE(0xFFFF);
	pUpdate->out_queue[pUpdate->queue_index][12] = LOBYTE(0xFFFF);
	pUpdate->out_queue[pUpdate->queue_index][13] = 0;

	for (int i = 3; i <= 13; i++) {
		cksum += pUpdate->out_queue[pUpdate->queue_index][i];
	}
	pUpdate->out_queue[pUpdate->queue_index][13 + 1] = cksum;

	int lenTotal = 12;
	pUpdate->out_queue[pUpdate->queue_index][1] = HIBYTE(lenTotal);
	pUpdate->out_queue[pUpdate->queue_index][2] = LOBYTE(lenTotal);
	pUpdate->out_queue[pUpdate->queue_index][0] = 1;
}

void mSet_Send_Version_AP(PUPDATEPARAM pUpdate, unsigned char typeMcu, int yy, int mm, int dd, int vv)
{
	pUpdate->out_queue[pUpdate->queue_index][3] = g_customer;
	pUpdate->out_queue[pUpdate->queue_index][4] = 0x10;// HEAD_CODE_10;
	pUpdate->out_queue[pUpdate->queue_index][5] = ACC_CMDER_COMMON;
	pUpdate->out_queue[pUpdate->queue_index][6] = g_target;
	pUpdate->out_queue[pUpdate->queue_index][7] = ACC_CMD_CFS;
	pUpdate->out_queue[pUpdate->queue_index][8] = HIBYTE(ACC_CMD_WLEN_CFS);
	pUpdate->out_queue[pUpdate->queue_index][9] = LOBYTE(ACC_CMD_WLEN_CFS);

	pUpdate->out_queue[pUpdate->queue_index][10] = ACC_PA_ALL_CFS_TYPE$_MCU1;
	pUpdate->out_queue[pUpdate->queue_index][11] = ACC_PA_ALL_CFS_CMD$_FW_VER;
	pUpdate->out_queue[pUpdate->queue_index][12] = yy;
	pUpdate->out_queue[pUpdate->queue_index][13] = mm;
	pUpdate->out_queue[pUpdate->queue_index][14] = dd;
	pUpdate->out_queue[pUpdate->queue_index][15] = vv;

	int cksum = 0;
	for (int i = 3; i <= 15; i++) {
		cksum = (cksum + pUpdate->out_queue[pUpdate->queue_index][i]) & 0xFF;
	}
	pUpdate->out_queue[pUpdate->queue_index][16] = cksum;

	pUpdate->out_queue[pUpdate->queue_index][1] = HIBYTE(14);
	pUpdate->out_queue[pUpdate->queue_index][2] = LOBYTE(14);
	pUpdate->out_queue[pUpdate->queue_index][0] = 1;
}

void mSet_Send_GetCksum_AP(PUPDATEPARAM pUpdate, unsigned char typeMcu) {
	pUpdate->out_queue[pUpdate->queue_index][3] = g_customer;
	pUpdate->out_queue[pUpdate->queue_index][4] = ACC_HEADER_READ;
	pUpdate->out_queue[pUpdate->queue_index][5] = ACC_CMDER_COMMON;
	pUpdate->out_queue[pUpdate->queue_index][6] = g_target;
	pUpdate->out_queue[pUpdate->queue_index][7] = ACC_CMD_CFS;
	pUpdate->out_queue[pUpdate->queue_index][8] = HIBYTE(ACC_CMD_RLEN_CFS);
	pUpdate->out_queue[pUpdate->queue_index][9] = LOBYTE(ACC_CMD_RLEN_CFS);

	pUpdate->out_queue[pUpdate->queue_index][10] = typeMcu;
	pUpdate->out_queue[pUpdate->queue_index][11] = ACC_PA_ALL_CFS_CMD$_CKSUM;

	int cksum = 0;
	for (int i = 3; i <= 11; i++) {
		cksum = (cksum + pUpdate->out_queue[pUpdate->queue_index][i]) & 0xFF;
	}
	pUpdate->out_queue[pUpdate->queue_index][12] = cksum;

	pUpdate->out_queue[pUpdate->queue_index][1] = HIBYTE(10);
	pUpdate->out_queue[pUpdate->queue_index][2] = LOBYTE(10);
	pUpdate->out_queue[pUpdate->queue_index][0] = 1;
}

void mSet_Send_GetResume_AP(PUPDATEPARAM pUpdate, unsigned char typeMcu) {
	pUpdate->out_queue[pUpdate->queue_index][3] = g_customer;
	pUpdate->out_queue[pUpdate->queue_index][4] = ACC_HEADER_READ;
	pUpdate->out_queue[pUpdate->queue_index][5] = ACC_CMDER_COMMON;
	pUpdate->out_queue[pUpdate->queue_index][6] = g_target;
	pUpdate->out_queue[pUpdate->queue_index][7] = ACC_CMD_CFS;
	pUpdate->out_queue[pUpdate->queue_index][8] = HIBYTE(ACC_CMD_RLEN_CFS);
	pUpdate->out_queue[pUpdate->queue_index][9] = LOBYTE(ACC_CMD_RLEN_CFS);

	pUpdate->out_queue[pUpdate->queue_index][10] = typeMcu;
	pUpdate->out_queue[pUpdate->queue_index][11] = ACC_PA_ALL_CFS_CMD$_RESUME;

	int cksum = 0;
	for (int i = 3; i <= 11; i++) {
		cksum = (cksum + pUpdate->out_queue[pUpdate->queue_index][i]) & 0xFF;
	}
	pUpdate->out_queue[pUpdate->queue_index][12] = cksum;

	pUpdate->out_queue[pUpdate->queue_index][1] = HIBYTE(10);
	pUpdate->out_queue[pUpdate->queue_index][2] = LOBYTE(10);
	pUpdate->out_queue[pUpdate->queue_index][0] = 1;
}

void mSet_Send_Restart_AP(PUPDATEPARAM pUpdate)
{
	pUpdate->out_queue[pUpdate->queue_index][3] = g_customer;
	pUpdate->out_queue[pUpdate->queue_index][4] = 0x10;// HEAD_CODE_10;
	pUpdate->out_queue[pUpdate->queue_index][5] = ACC_CMDER_COMMON;
	pUpdate->out_queue[pUpdate->queue_index][6] = g_target;
	pUpdate->out_queue[pUpdate->queue_index][7] = ACC_CMD_SWRESET;
	pUpdate->out_queue[pUpdate->queue_index][8] = HIBYTE(ACC_CMD_WLEN_SWRESET);
	pUpdate->out_queue[pUpdate->queue_index][9] = LOBYTE(ACC_CMD_WLEN_SWRESET);

	int cksum = 0;
	for (int i = 3; i <= 9; i++) {
		cksum = (cksum + pUpdate->out_queue[pUpdate->queue_index][i]) & 0xFF;
	}
	pUpdate->out_queue[pUpdate->queue_index][10] = cksum;

	pUpdate->out_queue[pUpdate->queue_index][1] = HIBYTE(8);
	pUpdate->out_queue[pUpdate->queue_index][2] = LOBYTE(8);
	pUpdate->out_queue[pUpdate->queue_index][0] = 1;
}

//---(end)  other function -------------------------------------------------
//=======================================================================================================
//=================================( end ) Private Function =============================================
//=======================================================================================================

boolean Update_Check_Modle(PDEVICESTATE pds)
{
	switch (g_target) {
	case ACC_AMP_ED003:
		if (strcmp(pds->Model, ED003_MODEL_NAME) == 0)
			return TRUE;
		return FALSE;
	case ACC_AMP_ED004:
		if (strcmp(pds->Model, ED004_MODEL_NAME) == 0)
			return TRUE;
		return FALSE;
	case ACC_AMP_ED005:
		if (strcmp(pds->Model, ED004_MODEL_NAME) == 0)
			return TRUE;
		return FALSE;
	case ACC_AMP_SD001:
		if (strcmp(pds->Model, SD001_MODEL_NAME) == 0)
			return TRUE;
		return FALSE;
	case ACC_AMP_AL003:
		if (strcmp(pds->Model, AL003_MODEL_NAME) == 0)
			return TRUE;
		return FALSE;
	case ACC_AMP_AL003M:
		if (strcmp(pds->Model, AL003M_MODEL_NAME) == 0 || strcmp(pds->Model, DA_APX40N_MODEL_NAME) == 0 || strcmp(pds->Model, DA_22SYS_MODEL_NAME) == 0
			|| strcmp(pds->Model, DA_62SYS_MODEL_NAME) == 0 || strcmp(pds->Model, DA_APX_MODEL_NAME) == 0 || strcmp(pds->Model, DA_PM8GD_MODEL_NAME) == 0
			|| strcmp(pds->Model, DA_PD8DG_MODEL_NAME) == 0)
			return TRUE;
		return FALSE;
	}
	return FALSE;
}

DWORD WINAPI Thread_Update(LPVOID lpParam) {

	PDEVICESTATE pds = (PDEVICESTATE)lpParam;

	unsigned long	process_wait_point = 0;
	pds->pUpdate = (PUPDATEPARAM)malloc(sizeof(UPDATEPARAM));
	if (pds->pUpdate == NULL)
		return 1;
	memset(pds->pUpdate, 0, sizeof(UPDATEPARAM));
	strcpy(pds->pUpdate->device_name, pds->Name);
	strcpy(pds->pUpdate->manufacturer, pds->Manufacturer);
	strcpy(pds->pUpdate->fw, pds->Fw);

	pds->pUpdate->fn_reciver = mUpdate_Process_Receiver;
	pds->pUpdate->fn_msg = CB_SetListViewItem;
	mUpdate_Process_Initial(pds->pUpdate);
	
	if (!Update_Check_Modle(pds)) {
		pds->pUpdate->index_in_listview = ds_GetIndexByMac(pds->Mac);
		sprintf(pds->pUpdate->update_msg, "%s", "Model error");
		pds->pUpdate->fn_msg(pds->pUpdate->index_in_listview, pds->pUpdate->update_msg);
		
		pds->pUpdate->update = FALSE;
		pds->check = FALSE;
		free(pds->pUpdate);
		pds->pUpdate = NULL;
		return 1;
	}
	while (1) {
		
		if (pds->pUpdate->out_queue[pds->pUpdate->queue_to_send][0] == 1) {
			mUpdate_Process_Upload(pds->pUpdate,NULL);
		}
	
		if ((GetTickCount() - process_wait_point) > 100){
			pds->pUpdate->index_in_listview = ds_GetIndexByMac(pds->Mac); 
			mUpdate_Process_Timer(pds->pUpdate);
			process_wait_point = GetTickCount();
		}
		if (!pds->pUpdate->update)
			break;
		Sleep(100);
	}

	if (pds->pUpdate) {
		sprintf(pds->Update_State, "%s", pds->pUpdate->update_msg);
		pds->check = FALSE;
		free(pds->pUpdate);
		pds->pUpdate = NULL;
	}
	return 1;
}