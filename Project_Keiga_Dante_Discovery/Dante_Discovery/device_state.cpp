
#include "stdafx.h"
#include "device_state.h"
#include "dt_extend_api.h"
#include "dante_conmon.h"
#include "Dante_Discovery.h"
#include "ACC_Command.h"
#include "utils.h"

PDEVICESTATE g_dsHead = NULL;
DEVICESTATE g_ds[MAX_DEVICE_STATE];
boolean g_check_ready = FALSE;
boolean g_listchanged = FALSE;

void Update_Device_State()
{
	g_check_ready = FALSE;
	char Name[33];
	char Mac[18];
	char Ip[18];
	char InstallerFileName[33];
	PDANTENODE pnode;
	PDEVICEINFO pdevinfo = NULL;

	kk_dtGetFirstDevice(Name);
	while (strlen(Name)) {
		dc_QueryManfVer(Name);
		dc_QueryInterface(Name);
		kk_dtGetNextDevice(Name);
	}
	
	
	kk_dtGetFirstDevice(Name);
	while (strlen(Name)) {
		//find device node
		pnode = dt_FindNode(Name);
		if (!pnode)
			break;

		//unsigned char cmd[8] = { ACC_CUST_KEIGA,ACC_HEADER_READ,ACC_CMDER_COMMON,ACC_AMP_ANY,ACC_CMD_FWVER,0x00,0x01,0xB1 };
		unsigned char cmd[8] = { ACC_CUST_KEIGA,ACC_HEADER_READ,ACC_CMDER_COMMON,ACC_AMP_ANY,ACC_CMD_LINALD,0x00,0x01,0xF3 };
		dt_Write_data(pnode->devInfo.mf, Name, cmd, 8);

		sprintf(Mac, "%02X%s%02X%s%02X%s%02X%s%02X%s%02X", pnode->devInfo.networks.networks->mac_address[0], ":", pnode->devInfo.networks.networks->mac_address[1], ":", pnode->devInfo.networks.networks->mac_address[2], ":", pnode->devInfo.networks.networks->mac_address[3], ":", pnode->devInfo.networks.networks->mac_address[4], ":", pnode->devInfo.networks.networks->mac_address[5]);
		if (strcmp(Mac, "00:00:00:00:00:00") != 0) {
			ds_NodeAdd(Mac);
			PDEVICESTATE p;
			p = ds_FindNode(Mac);
			if (p) {
				if (strcmp(p->Name, Name) != 0) {
					sprintf(p->Name, "%s", Name);
					WritePrivateProfileString(Mac, "Name", Name, g_iniPath);
					g_listchanged = TRUE;
				}

				if (strcmp(p->Model, pnode->devInfo.modelname) != 0) {
					sprintf(p->Model, "%s", pnode->devInfo.modelname);
					WritePrivateProfileString(Mac, "Model", p->Model, g_iniPath);
					g_listchanged = TRUE;
				}

				sprintf(Ip, "%d%s%d%s%d%s%d", (pnode->devInfo.networks.networks->ip_address) & 0xFF, ".", (pnode->devInfo.networks.networks->ip_address >> 8) & 0xFF, ".", (pnode->devInfo.networks.networks->ip_address >> 16) & 0xFF, ".", (pnode->devInfo.networks.networks->ip_address >> 24) & 0xFF);
				if (strcmp(p->Ip, Ip) != 0) {
					sprintf(p->Ip, "%s", Ip);
					WritePrivateProfileString(Mac, "Ip", p->Ip, g_iniPath);
					g_listchanged = TRUE;
				}

				WritePrivateProfileString(Mac, "InstallerFileName", p->InstallerFileName, g_iniPath);
				WritePrivateProfileString(Mac, "PoE", p->Poe, g_iniPath);
				WritePrivateProfileString(Mac, "FW Version", p->Fw, g_iniPath);

				sprintf(p->Manufacturer, "%s", pnode->devInfo.mf);
			}
		}
		kk_dtGetNextDevice(Name);
	}
	
	g_check_ready = TRUE;
}


void ds_GetProfileData(char* path)
{
	int index_dev = 0, index_mac = 0;

	char *sections = (char*)malloc(MAX_DEVICE_STATE * 18 * sizeof(char));
	memset(sections, 0, MAX_DEVICE_STATE * 18 * sizeof(char));

	DWORD length = GetPrivateProfileSectionNames(sections, MAX_DEVICE_STATE * 18, path);

	int len = 0;
	for (int i = 0; i < length; i++, len++) {
		if (sections[i] != 0) {
			continue;
		}
		else {
			ds_NodeAdd(&sections[i - len]);
			len = -1;
			index_dev++;
		}
	}

	char mac[18] = {0};

	ds_GetFirstDevice(mac);
	while (strlen(mac)) {
		PDEVICESTATE p;
		p = ds_FindNode(mac);
		if (p) {
			GetPrivateProfileString(mac, "Name", "", p->Name, 33, path);
			GetPrivateProfileString(mac, "Model", "", p->Model, 33, path);
			GetPrivateProfileString(mac, "Zone", "", p->Zone, 18, path);
			GetPrivateProfileString(mac, "Speaker", "", p->Speaker, 18, path);
			GetPrivateProfileString(mac, "InstallerFileName", "", p->InstallerFileName, 33, path);
			GetPrivateProfileString(mac, "PoE", "", p->Poe, 6, path);
			GetPrivateProfileString(mac, "Ip", "", p->Ip, 18, path);
			GetPrivateProfileString(mac, "FW Version", "", p->Fw, 18, path);
			p->show = TRUE;
		}
		ds_GetNextDevice(mac);
	}
	free(sections);
}

void ds_ImportProfileData(void)
{
	OPENFILENAME ofn;
	char szFile[2048];
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = hMainWnd;
	ofn.lpstrFile = szFile;
	ofn.lpstrFile[0] = '\0';
	ofn.nMaxFile = sizeof(szFile);
	ofn.lpstrFilter = "Devices Profile file(*.dpf)\0*.dpf\0";
	ofn.nFilterIndex = 1;


	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_PATHMUSTEXIST | OFN_FILEMUSTEXIST;
	if (!GetOpenFileName(&ofn))
		return;
	if (strlen(ofn.lpstrFile) == 0)
		return;
	ds_GetProfileData(ofn.lpstrFile);
}

void ds_ExportProfileData(void)
{
	OPENFILENAME ofn;
	char szFile[2048] = { 0 };
	ZeroMemory(&ofn, sizeof(ofn));
	ofn.lStructSize = sizeof(ofn);
	ofn.hwndOwner = g_hDeviceControl;
	ofn.lpstrFile = szFile;
	ofn.lpstrDefExt = "dpf";
	ofn.nMaxFile = sizeof(szFile);
	ofn.lpstrFilter = "Devices Profile file(*.dpf)\0*.dpf\0";
	ofn.nFilterIndex = 1;
	ofn.lpstrFileTitle = NULL;
	ofn.nMaxFileTitle = 0;
	ofn.lpstrInitialDir = NULL;
	ofn.Flags = OFN_EXPLORER | OFN_FILEMUSTEXIST | OFN_OVERWRITEPROMPT;
	if (!GetSaveFileName(&ofn))
		return;
	if (strlen(ofn.lpstrFile) == 0)
		return;


	char mac[18] = { 0 };

	ds_GetFirstDevice(mac);
	while (strlen(mac)) {
		PDEVICESTATE p;
		p = ds_FindNode(mac);
		if (p) {
			WritePrivateProfileString(mac, "Name", p->Name, ofn.lpstrFile);
			WritePrivateProfileString(mac, "Model", p->Model, ofn.lpstrFile);
			WritePrivateProfileString(mac, "Zone", p->Zone, ofn.lpstrFile);
			WritePrivateProfileString(mac, "Speaker", p->Speaker, ofn.lpstrFile);
			WritePrivateProfileString(mac, "InstallerFileName", p->InstallerFileName, ofn.lpstrFile);
			WritePrivateProfileString(mac, "PoE", p->Poe, ofn.lpstrFile);
			WritePrivateProfileString(mac, "Ip", p->Ip, ofn.lpstrFile);
			WritePrivateProfileString(mac, "FW Version", p->Fw, ofn.lpstrFile);
		}
		ds_GetNextDevice(mac);
	}
}

int ds_GetCount(void)
{
	int count = 0;
	PDEVICESTATE p;

	p = g_dsHead;
	while (p) {
		count++;
		p = p->next_node;
	}
	return count;
}
PDEVICESTATE ds_FindNode(char* mac)
{
	PDEVICESTATE p;

	p = g_dsHead;
	if (mac == NULL || strlen(mac) == 0)
		return NULL;

	while (p && strcmp(p->Mac, mac) != 0)
	{
		p = p->next_node;
	}

	if (!p)
	{
		return NULL;
	}
	return p;
}

PDEVICESTATE ds_FindNodeByIndex(int index)
{
	if (index > ds_GetCount() - 1)
		return NULL;

	PDEVICESTATE p;
	int i;
	p = g_dsHead;

	for (i = 0; i < index; i++) {
		p = p->next_node;
	}
	return p;
}

int ds_GetIndexByMac(char* mac)
{
	PDEVICESTATE p;
	int index = 0;

	p = g_dsHead;
	if (mac == NULL || strlen(mac) == 0)
		return -1;

	while (p && strcmp(p->Mac, mac) != 0)
	{
		p = p->next_node;
		index++;
	}

	if (!p)
	{
		return -1;
	}
	return index;
}

void ds_NodeAdd(char* mac)
{
	PDEVICESTATE p, ptail;

	//check device name
	p = ds_FindNode(mac);
	if (p)
	{
		return;
	}

	//get ptail in listing
	p = g_dsHead;
	ptail = g_dsHead;
	while (p)
	{
		ptail = p;
		p = ptail->next_node;
	}

	//create new node
	p = (PDEVICESTATE)malloc(sizeof(DEVICESTATE));
	if (p == NULL)
	{
		return;
	}

	//set node's data
	memset(p, 0, sizeof(DEVICESTATE));
	sprintf(p->Mac, "%s", mac);


	//add node in the listing
	if (g_dsHead == NULL)
	{
		g_dsHead = p;
		g_dsHead->next_node = NULL;
		g_dsHead->prev_node = NULL;
	}
	else
	{
		ptail->next_node = p;
		p->next_node = NULL;
		p->prev_node = ptail;
	}
	g_listchanged = TRUE;
}

void ds_NodeRemove(char* mac)
{
	PDEVICESTATE p, prev;

	prev = NULL;
	p = g_dsHead;
	while (p && strcmp(p->Mac, mac) != 0)
	{
		prev = p;
		p = p->next_node;
	}

	if (!p)
	{
		return;
	}

	//delete node
	if (p == g_dsHead)
	{
		//first node
		g_dsHead = p->next_node;
		free(p);
		return;
	}
	else
	{
		if (!p->next_node)
			//tail node
			prev->next_node = NULL;
		else
			//middle node
			prev->next_node = p->next_node;
		free(p);
	}
}

void ds_GetFirstDevice(char *mac)
{
	PDEVICESTATE p;

	p = g_dsHead;
	while (p)
	{
		strcpy(mac, p->Mac);
		return;
	}
	strcpy(mac, "");
}

void ds_GetNextDevice(char *mac)
{
	PDEVICESTATE p;

	p = ds_FindNode(mac);
	if (!p)
	{
		return;
	}

	p = p->next_node;
	while (p)
	{
		strcpy(mac, p->Mac);
		return;
	}
	strcpy(mac, "");
}

void  ds_FilterDevice(uint32_t filteType, PFILTERDATA pFilterData) {
	char mac[19];
	ds_GetFirstDevice(mac);
	switch(filteType){
	case FILTER_DEVICE_ALL: {
		while (strlen(mac)) {
			PDEVICESTATE p;
			p = ds_FindNode(mac);
			if (p) {
				p->show = TRUE;
			}
			ds_GetNextDevice(mac);
		}
		break; 
	}
	case FILTER_DEVICE_MODEL: {
		while (strlen(mac)) {
			PDEVICESTATE p;
			p = ds_FindNode(mac);
			if (p) {
				if (pFilterData->model &&  util_isContainStr_noSen(p->Model, pFilterData->model))
					p->show = TRUE;
				else
					p->show = FALSE;
			}
			ds_GetNextDevice(mac);
		}
		break; 
	}
	case FILTER_DEVICE_COMPANY: {break;}
	case FILTER_DEVICE_NAMECONTAIN: {
		while (strlen(mac)) {
			PDEVICESTATE p;
			p = ds_FindNode(mac);
			if (p) {
				if (pFilterData->nameContain &&  util_isContainStr_noSen(p->Name, pFilterData->nameContain))
					p->show = TRUE;
				else
					p->show = FALSE;
			}
			ds_GetNextDevice(mac);
		}
		break; 
	}
	case FILTER_DEVICE_ZONE: {
		while (strlen(mac)) {
			PDEVICESTATE p;
			p = ds_FindNode(mac);
			if (p) {
				if (pFilterData->zone &&  util_isContainStr_noSen(p->Zone, pFilterData->zone))
					p->show = TRUE;
				else
					p->show = FALSE;
			}
			ds_GetNextDevice(mac);
		}
		break;
	}
							 /*
	case FILTER_DEVICE_SPEAKER: {
		while (strlen(mac)) {
			PDEVICESTATE p;
			p = ds_FindNode(mac);
			if (p) {
				if (pFilterData->speaker &&  util_isContainStr_noSen(p->Speaker, pFilterData->speaker))
					p->show = TRUE;
				else
					p->show = FALSE;
			}
			ds_GetNextDevice(mac);
		}
		break;
	}
	*/
	case FILTER_DEVICE_MAC: {
		while (strlen(mac)) {
			PDEVICESTATE p;
			p = ds_FindNode(mac);
			if (p) {
				if (pFilterData->mac &&  util_isContainStr_noSen(p->Mac, pFilterData->mac))
					p->show = TRUE;
				else
					p->show = FALSE;
			}
			ds_GetNextDevice(mac);
		}
		break; 
	}
	case FILTER_DEVICE_IP: {
		while (strlen(mac)) {
			PDEVICESTATE p;
			p = ds_FindNode(mac);
			if (p) {
				if (pFilterData->mac &&  util_isContainStr_noSen(p->Ip, pFilterData->ip))
					p->show = TRUE;
				else
					p->show = FALSE;
			}
			ds_GetNextDevice(mac);
		}
		break;
	}
	
	}
	return;
}
